package recomender.analysis;

import java.awt.image.BufferedImage;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.imageio.ImageIO;

import recomender.tesseract.TesseractAnalisis;
import recomender.validations.ValidaConexion;

public class AnalizaImagen {

	/**
	 * Metodo que analiza si esque existe texto en la imagen
	 * @param src Es el oath o link de la imagen
	 * @param archivo Es el archivo html que contiene la imagen
	 * @return Retorna el resultado./
	 */
	public String[] textoImagen(String src, java.io.File archivo) {
		String text="";
		String []retorna=new String[3];
		//si esque es web la ruta
		if(src.contains("//")|| src.contains("www.")) {
			if(!src.contains("http"))//en caso de que no tenga el http
				src="http:"+src;
			//aki llamo al tesseract para analizar la img
			ValidaConexion vc=new ValidaConexion();
			if(vc.hayConexion(src)) {//en caso de que si haya conexion
				try {
					System.out.println("Imageeeeeeeeeeeeeeeeeeen online: "+src);
					URL url=new URL(src);
					//BufferedImage image=ImageIO.read(url);
					HttpURLConnection.setFollowRedirects(false);//para que no se redireccione
					HttpURLConnection con=(HttpURLConnection)url.openConnection();//abro la conexion
					//System.out.println("Codigo de conexion: "+System.getProperty("http.agent"));
					//seteo el user-agent (sirve para usar certificados ttl y ssh cuando son paginas muy seguras)
					con.setRequestProperty("User-Agent", "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.167 Safari/537.36");
					BufferedImage image=ImageIO.read(con.getInputStream());//tomo el imputstream de la conexion la cual es la imagen
					TesseractAnalisis tess=new TesseractAnalisis();
					text=tess.retornaTextoEnImagen(image);
					System.out.println("Imprime el tamanio: height="+image.getHeight()+" width="+image.getWidth());
					retorna[0]=text;
					retorna[1]=image.getHeight()+"";
					retorna[2]=image.getWidth()+"";
				}catch(Exception e) {
					e.printStackTrace();
				}
			}else{
				System.out.println("Imagen caida!!");
				return null;
			}
		}else{//en caso de que no sea web
			if(!src.trim().equals("")) {
				recomender.validations.ValidaHTML vh= new recomender.validations.ValidaHTML();
				if(new java.io.File(src).exists()) {
					System.out.println("SRC: "+src);
					try {
						BufferedImage image=ImageIO.read(new java.io.File(src));
						TesseractAnalisis tess=new TesseractAnalisis();
						text=tess.retornaTextoEnImagen(image);
						System.out.println("SRC: "+text);
						retorna[0]=text;
						retorna[1]=image.getHeight()+"";
						retorna[2]=image.getWidth()+"";
					}catch(Exception ex) {
						ex.printStackTrace();
					}
				}else if(new java.io.File(vh.retornaAbsolutPath(archivo)+""+src).exists()) {//si existe el archivo con la ruta
					try {
						BufferedImage image=ImageIO.read(new java.io.File(vh.retornaAbsolutPath(archivo)+""+src));
						TesseractAnalisis tess=new TesseractAnalisis();
						text=tess.retornaTextoEnImagen(image);
						retorna[0]=text;
						retorna[1]=image.getHeight()+"";
						retorna[2]=image.getWidth()+"";
					}catch(Exception ex) {
						ex.printStackTrace();
					}
				}
			}
		}
		if(src.trim().equals(""))
			return null;
		return retorna;
	}
}
