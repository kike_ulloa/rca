package recomender.analysis;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.Properties;

import recomender.data.UploadDeletePDF;
import recomender.validations.ValidaConexion;

public class AnalizaPDFs {
//	String rutaPhantomjs="/home/kike/Documents/TESIS/phantomjs-2.1.1-linux-x86_64/examples/phantomj s";
//	String rutaArchivoValidacion="/home/kike/Documents/TESIS/phantomjs-2.1.1-linux-x86_64/examples/submit.j s";
//	String rutaArchivoValidacionOffline="/home/kike/Documents/TESIS/phantomjs-2.1.1-linux-x86_64/examples/submit_upload.j s";
//	String rutaPhantomjs="/home/kike/Documents/phantomJS/examples/phantomjs";
//	String rutaArchivoValidacion="/home/kike/Documents/phantomJS/examples/submit.js";
//	String rutaArchivoValidacionOffline="/home/kike/Documents/phantomJS/examples/submit_upload.js";
	
	String rutaPhantomjs="";
	String rutaArchivoValidacion="";
	String rutaArchivoValidacionOffline="";
	/**
	 * Metodo que busca los errores en los archivos pdf
	 * @param link Es el link del archivo pdf
	 * @param rutaArchivo Es la ruta del archivo en el que s eencuentra el link del pdf
	 * @param nombreOA Es el nombre del objeto de aprendizaje.
	 * @return Retorna los resuktados 
	 */
	public String retornaErrores(String link, String rutaArchivo,String nombreOA) {
		try {
			ClassLoader cl = Thread.currentThread().getContextClassLoader();
			Properties p = new Properties();
			p.load(cl.getResourceAsStream("paths.properties"));
			rutaPhantomjs = p.getProperty("phantomjs");
			rutaArchivoValidacion = p.getProperty("validaPDFonline");
			rutaArchivoValidacionOffline = p.getProperty("validaPDFoffline");
			//System.out.println("Este es el path de phantom: "+p.getProperty("phantomjs"));
		}catch(Exception e) {
				e.printStackTrace();
		}
		String resultado="";
		if(link.contains("//")||link.contains("www.")) {
			if(!link.contains("http"))
				link="http:"+link;
			if(!link.endsWith(".pdf")) {
				link=link.substring(0,link.indexOf(".pdf")+5);
				System.out.println("este es el link: "+link);
			}
			ValidaConexion vc=new ValidaConexion();
			if(vc.hayConexion(link)) {
				//String comando[]=new String[] {rutaPhantomjs, rutaArchivoValidacion, link};
				String comando[]=new String[3];
				comando[0]=rutaPhantomjs;
				comando[1]=rutaArchivoValidacion;
				comando[2]=link;
				resultado=ejecutaComando(comando);
			}
		}else {//si es offline
			//aki se necesita extraer la ruta de la app web, tambien al ruta donde se encuentra almacenado el oa pero dentro de la appweb
			//esta ruta seria algo asi: http://localhost:8080/TesseractExample/CarpetaOA/, y adicionar a ea ruta al ruta del archivo desde 
			//este punto, asi deberia quedar la ruta del archvio pdf de forma online:
			//---> http://localhost:8080/TesseractExample/CarpetaOA/archivo.pdf
			//--->http://localhost:8080/TesseractExample/CarpetaOA/carpeta1/archivo.pdf
			//--->http://localhost:8080/TesseractExample/CarpetaOA/carpeta1/carpeta2/carpeta3/etc/archivo.pdf
			//UploadDeletePDF updp=new UploadDeletePDF();
			java.io.File pdf=new java.io.File(link);
			if(pdf.exists()) {
				//String []res=updp.sube(pdf, nombreOA);
				//if(res!=null) {
					String comando[]=new String[3];
					comando[0]=rutaPhantomjs;
					comando[1]=rutaArchivoValidacionOffline;
					comando[2]=pdf.getAbsolutePath();
					resultado=ejecutaComando(comando);
					System.out.println(resultado);
					//updp.elimina(res[0], res[1]);
				//}
			}else {
				pdf=new java.io.File(rutaArchivo+"/"+link);
				if(pdf.exists()) {
					//String []res=updp.sube(pdf, nombreOA);
					//if(res!=null) {
						String comando[]=new String[3];
						comando[0]=rutaPhantomjs;
						comando[1]=rutaArchivoValidacionOffline;
						//System.out.println("RES 1111111----->>> "+res[1]);
						comando[2]=pdf.getAbsolutePath();
						resultado=ejecutaComando(comando);
						System.out.println(resultado);
						//updp.elimina(res[0], res[1]);
					//}
				}
			}
			
		}
		return resultado;
	}
	/**
	 * Metodo que ecuta un comando para llamar a phanton que interactue con una pagina web para osubir y obtener los erres de los pdf.
	 * @param comando Es el comando a ejecutarse.
	 * @return Retorna los resultados.
	 */
	public String ejecutaComando(String comando[]) {
		try {
			Process process= Runtime.getRuntime().exec(comando);
			BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
			String salida="";
			String linea ="";
			while((linea=reader.readLine())!=null) {
				salida+=linea+"\n";
			}
			process.waitFor();
			process.destroy();
			return salida;
		}catch(Exception e) {
			e.printStackTrace();
			return "Error Ejecutando";
		}
	}
	
	
}
