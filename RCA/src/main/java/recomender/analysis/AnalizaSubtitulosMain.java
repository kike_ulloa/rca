package recomender.analysis;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import recomender.analysis.javascript.AnalizaJavascriptJQuery;
import recomender.analysis.javascript.AnalizaSoloScript;
import recomender.staticcontent.VariablesEstaticas;

public class AnalizaSubtitulosMain {
	int []total=new int [3];
	int []total2=new int [3];
	/**
	 * Metodo que encuentra posibles enlacxes de videos e invoca a los metdos para analizar si esxisten o no subtitulos en lso videso.
	 * @param jsHTML Ruta del html o js a analizar
	 * @param archivo Es la ruta del archivo
	 * @return retorna los resultados
	 */
	public int [] analizaSubtitulos(String jsHTML, java.io.File archivo) {
		Document doc;
		if(jsHTML!=null) {//si es diferente de nulo significa que hay q analizar solo js
			doc=Jsoup.parse(jsHTML);
		}else {//si js es nulo entonces hay q analizar el archivo
			try {
				doc=Jsoup.parse(archivo, "UTF-8");
			}catch(Exception e) {
				e.printStackTrace();
				doc=null;
			}
		}
		if(doc!=null) {//si no ha habido ningun error al hacer el parse
			//html5
			Elements linksVideo=doc.select("video");
			Elements linksVideoSource=doc.select("source");//dentro de la etiqueta viTotaldeo
			Elements linksVideoTrack=doc.select("track");//dentro de la etiqueta video
			//video embebido
			Elements linksiFrames=doc.select("iframe");
			Elements linksEmbed=doc.select("embed");
			//object
			Elements linksObject=doc.select("object");
			
			SubtitlesProcess sp=new SubtitlesProcess();
			sp.recorre(linksVideo, "src", archivo.getAbsolutePath());
			sp.recorre(linksVideoSource, "src", archivo.getAbsolutePath());
			sp.recorre(linksVideoTrack, "src", archivo.getAbsolutePath());
			sp.recorre(linksiFrames, "src", archivo.getAbsolutePath());
			sp.recorre(linksEmbed, "src", archivo.getAbsolutePath());
			sp.recorre(linksObject, "data", archivo.getAbsolutePath());
			
			if(jsHTML==null) {
				total=sp.retornaVideos();
			}else
				total2=sp.retornaVideos();
			
			return total;
		}
		return null;
	}
	/**
	 * metodo que calcula un procentaje de video con subs
	 * @param numVideos Es el numero de videos
	 * @param numsubscap Es el numero de videos con subtitulos.
	 * @return ZRetorna el porcentaje total
	 */
	public double reglaTres(int numVideos, int numsubscap) {
		if(numVideos>0)
			return (numsubscap*100)/numVideos;
		else
			return 0;
	}
	/**
	 * Retorna los resultados generales.
	 * @return Retorna el resultado
	 */
	public Object[] retornaVideosSubs() {
		//en la otra clase llamo asi:
//		analizaSubtitulos(null, archivo);//envio con archivo html
//		analizaSubtitulos(jsHTML, archivo);//envio con archivo js
		//recorro el listado de videos y voy sumando
		for(int []videos:totalVideos) {
			total[0]+=videos[0];
			total[1]+=videos[1];
			total[2]+=videos[2];
		}
		VariablesEstaticas.numVideos=VariablesEstaticas.numVideos+total[0];
		double porcentaje=reglaTres(total[0], total[1]+total[2]);
		totalVideos=new java.util.ArrayList<int[]>();
		if(total[0]==0) {
			return new Object[] {"No se han encontrado videos.", false};
		}else
		if(porcentaje>=75) {//si el numero de videos con subtitulos es mayor al 75%
			return new Object[] {"Se ha encontrado que el "+porcentaje+"% de videos contiene subtitulos, al ser mayo al 75% se considera que el contenido si posee videos accesibles.", true};
		}else {
			return new Object[] {"Se ha encontrado que el "+porcentaje+"% de videos contiene subtitulos, por lo cual se recomienda que al menos mas del 75% posea videos con subtitulos.", false};
		}
	}
	private java.util.ArrayList<int[]> totalVideos=new java.util.ArrayList<int[]>();
	/**
	 * Metodo que analiza sibtitulos en archvos jquery
	 * @param js El texto jquery
	 * @param archivoJS La ruta del archivo
	 */
	public void analizaJQuery(String js, String archivoJS) {
		AnalizaJavascriptJQuery ajq=new AnalizaJavascriptJQuery();
		ajq.extraeDeJQuery(js, new java.io.File(archivoJS));
		totalVideos.add(ajq.retornaVideos());
	}
	/**
	 * Metodo que analiza sibtitulos en archvos javascript puros
	 * @param js El texto javascript
	 * @param archivoJS La ruta del archivo
	 */
	public void analizaJavascript(String js, String archivoJS) {
		AnalizaSoloScript as=new AnalizaSoloScript();
		as.setSrcJavascript(js, new java.io.File(archivoJS));
		totalVideos.add(as.retornaVideos());
	}
}
