package recomender.analysis;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import recomender.formats.VideoFormats;
import recomender.validations.ValidaConexion;
import recomender.validations.ValidaHTML;

public class SubtitlesProcess {
	private int numVideos=0;
	private int numVideosConSubtitulos=0;
	private int numVideosConCaptions=0;
	
	/**
	 * Metodo que mediante una linea de comando verifica si el video tiene o no tiene subtitulos
	 * Si es video offline se usa ffmpeg, si es online, se usa youtube-dl
	 * @param ruta Es la ruta del video a analizar
	 * @param web Indica si es una ruta de tipo web o de tipo local
	 * @param Indica la ruta completa del archivo en el que se esta llamando al video.. puede ser js o html
	 * @return retorna true o false, si tiene o no tiene subtitulos
	 */
	public boolean[] contieneSubtitulos(String ruta, boolean web, String rutaArchivoVideo) {
		//numVideos++;
		try {
			boolean []subscap=new boolean[2];
			String []command=new String [3];
			if(web) {
				command[0]="youtube-dl";
				command[1]="--list-subs";
				//command[2]="/run/media/kike/New Volume/MOVIES/DBZ- La Batalla de los Dioses 1080p Latino/DBZ1BD1D-www.DescargateloCorp.com.mkv";
				//command[2]="https://www.youtube.com/embed/m9TQboqmTdY";
				if(!ruta.toLowerCase().contains("http"))
					ruta="http:"+ruta;
				ValidaConexion vc=new ValidaConexion();
				if(vc.hayConexion(ruta)) {
					System.out.println("Entra al if porque si hay conexion");
					command[2]=ruta;
					Process process= Runtime.getRuntime().exec(command);
					
					BufferedReader error= new BufferedReader(new InputStreamReader(process.getErrorStream()));
					String salidaError= "";
					String line="";
					while((line=error.readLine())!=null) {
						salidaError+=line;
					}
					if(!salidaError.toLowerCase().contains("unsupported url")) {
						numVideos++;
					BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
					String salida="Esta es la salida: ";
					String linea ="";
					while((linea=reader.readLine())!=null) {
						salida+=linea+"\n";
						
					}
					process.waitFor();
					process.destroy();
					if(salida.toLowerCase().contains("has no subtitles")&& !salida.toLowerCase().contains("has no automatic captions")) {
						//numVideosConCaptions++;
						subscap[1]=true;
					}else
					if(salida.toLowerCase().contains("has no subtitles")&&salida.toLowerCase().contains("has no automatic captions")){
						System.out.println("No hay subtitulos--------------------");
						subscap[0]=false;
					}
					else {
						System.out.println(salida);
						subscap[0]=true;
					}
					}else {
						//System.out.println("No es video");
					}
				}
			}else {//si no es web
				if(!ruta.equals("")) {
					command[0]="ffmpeg";
					command[1]="-i";
					ValidaHTML vh=new ValidaHTML();
					if(new java.io.File(ruta).exists()) {//si existe el archivo directamente
						ruta=ruta;
					}else if(new java.io.File(vh.retornaAbsolutPath(new java.io.File(rutaArchivoVideo))+""+ruta).exists()) {
						ruta=vh.retornaAbsolutPath(new java.io.File(rutaArchivoVideo))+""+ruta;
					}else {
						ruta="empty";
					}
				
					if(!ruta.equals("empty")) {
						//recorro los formatos de video para saber si contiene algun formato
						boolean esVideo=false;
						VideoFormats vf=new VideoFormats();
						java.util.ArrayList<String> formats=vf.returnVideoFormats();
						for(String formato: formats) {
							if(ruta.toUpperCase().contains(formato))
								esVideo=true;
						}
						if(esVideo) {
							numVideos++;
						command[2]=ruta;
						Process process= Runtime.getRuntime().exec(command);
						BufferedReader reader = new BufferedReader(new InputStreamReader(process.getErrorStream()));
						String salida="Esta es la salida: ";
						String linea ="";
						while((linea=reader.readLine())!=null) {
							salida+=linea+"\n";
							
						}
						process.waitFor();
						process.destroy();
						if(salida.toLowerCase().contains("subtitle")){
							System.out.println(salida);
							subscap[0]=true;
						}else {
							System.out.println("No hay subtitulos--------------------");
							subscap[0]=false;
						}
						}
					}
				}
				
				//System.out.println(salida);
			}
			return subscap;
		}catch(Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	/**
	 * Valida si es o no web la ruta indicada
	 * @param ruta Ruta a validarse
	 * @return Retrna true o false
	 */
	public boolean esWeb(String ruta) {
		if(ruta.contains("//")||ruta.contains("www."))
			return true;
		return false;
	}
	/**
	 * Incrmenta el num de videos y subs encontrados
	 * @param ruta Es la ruta del archovo
	 * @param rutaArchivoVideo Es la ruta del archivo q contiene el video
	 */
	public void videosSubtitulos(String ruta, String rutaArchivoVideo) {
		boolean []res;
		if(esWeb(ruta)) {
			res=contieneSubtitulos(ruta, true, rutaArchivoVideo);
			if(res[0]) {
				numVideosConSubtitulos++;
			}else//para que si un video tiene subtitulos y captions no se sume dos
			if(res[1]) {
				numVideosConCaptions++;
			}
		}else {
			res=contieneSubtitulos(ruta, false, rutaArchivoVideo);
			if(res[0]) {
				numVideosConSubtitulos++;
			}
		}
	}
	/**
	 * Recorre cada uno e los elemmentos de los archovs en bisca de videos
	 * @param links Son los links encontrados
	 * @param llave Es la llave de video
	 * @param rutaArchivoVideo Es el archivo q contiene a los videos.
	 */
	public void recorre(Elements links, String llave, String rutaArchivoVideo) {
		for(Element e: links) {
			String ruta=e.attr(llave);/**
			 * Metodo que analiza sibtitulos en archvos jquery
			 * @param js El texto jquery
			 * @param archivoJS La ruta del archivo
			 */
			if(ruta!=null&&ruta!="") {
				System.out.println("video url video tag: "+ruta);
				videosSubtitulos(ruta, rutaArchivoVideo);	
			}
		}
		
	}
	public boolean imprime() {
		System.out.println("Videos totales: "+numVideos);
		System.out.println("Videos con Subtitulos: "+numVideosConSubtitulos);
		System.out.println("Videos con Captions: "+numVideosConCaptions);
		if(reglaTres(numVideos, numVideosConSubtitulos+numVideosConCaptions)>=75)
			return true;
		return false;
	}
	public int getnumVideo() {
		return this.numVideos;
	}
	public int reglaTres(int numVideos, int numsubscap) {
		return (numsubscap*100)/numVideos;
	}
	/**
	 * Retorna los videos encontrados con los subs
	 * @return Retorna un vector de num de videos, numVideos con subtitulos y numero de videos con captions
	 */
	public int[] retornaVideos() {
		int []retorna=new int[] {numVideos,numVideosConSubtitulos, numVideosConCaptions};
		numVideos=0;
		numVideosConSubtitulos=0;
		numVideosConCaptions=0;
		return retorna;
	}
}
