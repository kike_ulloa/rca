package recomender.analysis.css;

import recomender.calculations.UnidadesTamanoFuente;

public class FontSize {
	int numeroSicumple=0;
	int numeroTotal=0; 
	/**
	 * Anaiza el tamano fuente y retorna
	 * @param css Es el css a analizar
	 * @return Retrna los resultados
	 */
	public int [] fontSize(String css) {
		numeroSicumple=0;
		css.replaceAll("\n", "");
		css.replaceAll(" ", "");
		tamanoFuente(css);
		int res[]=new int[] {numeroSicumple,numeroTotal};
		numeroSicumple=0;
		numeroTotal=0;
		return res;
	}
	/**
	 * Aaliza cad auno de lso tamanos fuentes que existan
	 * @param css Es el css a analizar.
	 */
	public void tamanoFuente(String css) {
		if(css.contains("font-size")) {
			numeroTotal++;
			String size=css.substring(css.indexOf("font-size")+3);
			String aux=size;
			size=size.substring(0,size.indexOf(";"));
			UnidadesTamanoFuente utf=new UnidadesTamanoFuente();
			if(utf.analiza(size, "no"))
				numeroSicumple++;
			if(aux.contains("font-size"))
				tamanoFuente(aux);
		}
	}
}
