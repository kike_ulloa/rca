package recomender.analysis.css;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;

import recomender.analysis.html.PrincipalHTML;

public class PrincipalCSS {
	/**
	 * Este metodo nos permite obtener el codigo de CSS que se encuentre en las etiquetas style dentro de las paginas html.
	 * @param archivo-El archivo html que se esta analizando
	 * @return
	 */
	public String obtenerDeStyle(String archivo) {
		PrincipalHTML pHtml=new PrincipalHTML();
		ArrayList<String> etiquetas=pHtml.buscar(new ArrayList<String>(),true,archivo, null, "style",null,null,null,false);
		String contenidoStyle="";
		for (int i = 0; i < etiquetas.size(); i++) {
			for (int j = 0; j < etiquetas.size(); j++) {
				if(i!=j) {
					if(etiquetas.get(i).contains(etiquetas.get(j))) {
						etiquetas.remove(i);
						i=0;
						j=0;
					}
				}
			}	
		}
		for (String etiqueta : etiquetas) {
			while(etiqueta.length()>1) {
			int posicionInicial=etiqueta.toLowerCase().indexOf("style");
			if(posicionInicial>-1) {
				etiqueta=etiqueta.substring(posicionInicial+7);
				int posicionFinal=etiqueta.indexOf("\"");
				if(posicionFinal<0) {
					posicionFinal=etiqueta.indexOf("'");
				}
				contenidoStyle=contenidoStyle+"\n"+etiqueta.substring(0,posicionFinal);
				etiqueta=etiqueta.substring(posicionFinal+2);
			}
			else
				etiqueta="";
			}
		}
	ArrayList<String> resultadoEtiquetaStyle=pHtml.buscar(new ArrayList<String>(),true,archivo,"style",null, null, null, null, false);
		for (String etiquetaStyle:resultadoEtiquetaStyle) {
			contenidoStyle=contenidoStyle+etiquetaStyle;
		}
		return contenidoStyle;			
	}
	/**
	 * Este metodo permite obtener el codigo CSS de los archivos con esa extension.
	 * @param archivo.- archivo Html
	 * @param encontrados.- el arreglo en el que se agrega el contenido buscado en este metodo de busqueda.
	 * @return
	 */
	public ArrayList<String> obtenerArchivosCSS(String archivo,ArrayList<String> encontrados) {
		PrincipalHTML pHtml=new PrincipalHTML();
		 return pHtml.buscar(encontrados,true,archivo, "link", "href", null, "desconocido", null, false);	
	}
	public String obtenerDeCSS(String archivoCSS){
		BufferedReader br;
		String contenidoCSS="";
		try {
			File f =new File(archivoCSS);
			if(f.length()<8192) {
			br = new BufferedReader(new FileReader(archivoCSS));
			String linea=br.readLine();//Leemos la primera linea del texto.
			while(linea!=null) {//Recorreremos todas las lineas obtenidas.
				contenidoCSS=contenidoCSS+"\n"+linea;
				linea=br.readLine();
			}
			}
		} catch(Exception e){
            e.printStackTrace();
		}
		return contenidoCSS;
		
	}
	/**
	 * Busca el contenido que se desea encontrar dentro del codigo CSS obtenido.
	 * @param archivo.- Archivo html
	 * @param atributo.-parametro de busqueda
	 * @param atributo2.- segundo parametro de busqueda.
	 * @return
	 */
	public ArrayList<String> buscarEnCSS(String archivo,String atributo,String atributo2) {
		String css="";
		ArrayList<String> resultadoArchivoCSS=obtenerArchivosCSS(archivo,new ArrayList<String>());
		for (String archivoCSS :resultadoArchivoCSS ) {
			if(archivoCSS.contains(".css"))
			css=css+"\n"+obtenerDeCSS(archivoCSS);
		}
		css=css+"\n"+obtenerDeStyle(archivo);
		ArrayList<String>valores=new ArrayList<String>();
		String lineasCSS[]=css.split("\n");
		for (int i = 0; i < lineasCSS.length; i++) {
			while(lineasCSS[i].length()>0) {
				int posicionInicial=lineasCSS[i].toLowerCase().indexOf(atributo);
				if(posicionInicial>-1) {

					lineasCSS[i]=lineasCSS[i].substring(posicionInicial);	
					if(atributo2!=null)
						lineasCSS[i]=lineasCSS[i].substring(lineasCSS[i].toLowerCase().indexOf(atributo2)+atributo2.length());
					else
						lineasCSS[i]=lineasCSS[i].substring(lineasCSS[i].indexOf(":")+1);
		
				int posicionFinal=lineasCSS[i].indexOf(";");
				if(posicionFinal<0)
					posicionFinal=lineasCSS[i].indexOf(" ");
				if(posicionFinal<0)
					posicionFinal=lineasCSS[i].length()-1;				
				valores.add(lineasCSS[i].substring(0,posicionFinal));
				lineasCSS[i]=lineasCSS[i].substring(posicionFinal);
				}
				else
					lineasCSS[i]="";
			}
		}
		return valores;
	}
}
