package recomender.analysis.html;

import java.io.IOException;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

public class AnalizaCanvas {

	/**
	 * Analiza si contiene o no camnvas el archivo html
	 * @param archivo Es el archivo html a analizar
	 * @return Retorna true o false.
	 */
	public boolean contineCanvas(java.io.File archivo) {
		try {
			Document doc=Jsoup.parse(archivo, "UTF-8");
			Elements canvas=doc.select("canvas");
			if(canvas.size()>0)
				return true;
			return false;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
	}
}
