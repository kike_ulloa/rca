package recomender.analysis.html;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.imageio.ImageIO;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import recomender.analysis.AnalizaImagen;
import recomender.formats.ImageFormats;
import recomender.tesseract.TesseractAnalisis;
import recomender.validations.ValidaConexion;
import recomender.validations.ValidaHTML;
//clase que analizara los tags de html
import recomender.validations.ValidaTextoImagenTextoAlternativo;
public class AnalizaHtml {
	int numImagenes=0;//numeor de imagenes analizadas
	int numImagenesTextoAlternativo=0; //numero de imagenes uqe tienen texto alternativo de las analizadas
	int numImagenesConTextoBajaResolucion=0; //numero de imagenes con baja resolucion
	int pixelesPorCaracter=30;//numero de pixeles que ocupa un caracter en lo alto y ancho
	int numeroImagenesPocoTextoAlternativo=0;//numero de imagenes con texto alternativo menor al texto de la imagen.
	TesseractAnalisis tess;
	
	public AnalizaHtml() {
		tess=new TesseractAnalisis();
	}
	/**
	 * Metodo que recorre todo el archivo en busca de enlaces de video o imagenes
	 * @param htmlFile Es el archov a analizarse
	 * @param llave Es al llave a anbalizar
	 */
	public void analiza(java.io.File htmlFile, String llave) {
		try {
			Document doc=Jsoup.parse(htmlFile, "UTF-8");
			Elements elements=doc.select(llave);
			
			ValidaHTML valida=new ValidaHTML();
			//si esque es imagen
			if(llave.equals("img")||llave.equals("iframe")||llave.contains("input")) {
				recorreHTML(elements, valida.retornaAbsolutPath(htmlFile), "src", llave, htmlFile);
				
			}
			
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
	/**
	 * Recorre cada uno de los enlaces encontrados y analiza imagenes y videos.
	 * @param elements Son los elementos encotnrados
	 * @param absolutePath Es la ruta del archovo
	 * @param llave Es la llave a analizar
	 * @param tag Es el tag a analizar
	 * @param archivo Es el archov a analizar.
	 */
	public void recorreHTML(Elements elements, String absolutePath, String llave,String tag, java.io.File archivo) {
		for(Element e: elements) {
			String textoAlternativo="";
			String textoImagen="";
			int alto=0;
			int ancho=0;
			boolean tienePadreImagen=false;
			boolean enIframe=false;//si la imagen se encuentra dentro de un iframe
			boolean enInput=false;//si la imagen se encuentra dentro de un input
			if(e.attr(llave)!=null&&!e.attr(llave).trim().equals("")) {//compruebo que no sea ni nulo ni vacio el valor del src
				
				String rutaLlave=e.attr(llave);
				//cuando es solo un icono de un href o un boton (input type button
				if(e.parent().nodeName().equals("a")||e.parent().nodeName().equals("input")) {
					String padre=e.parent().attr("href");
					if(padre.contains("//")||padre.contains("www.")) {
						if(!padre.contains("http"))
							padre="http://"+padre;
						ValidaConexion vc=new ValidaConexion();
						if(vc.hayConexion(padre)) {//en caso de que si haya conexion
							try {
								URL url=new URL(padre);
								//BufferedImage image=ImageIO.read(url);
								HttpURLConnection.setFollowRedirects(false);//para que no se redireccione
								HttpURLConnection con=(HttpURLConnection)url.openConnection();//abro la conexion
								//System.out.println("Codigo de conexion: "+System.getProperty("http.agent"));
								//seteo el user-agent (sirve para usar certificados ttl y ssh cuando son paginas muy seguras)
								con.setRequestProperty("User-Agent", "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.167 Safari/537.36");
								BufferedImage image=ImageIO.read(con.getInputStream());//tomo el imputstream de la conexion la cual es la imagen
								if(image!=null) {
									rutaLlave=e.parent().attr("href");//indio que la ruta de la imagen no va a ser el thumball(imagen pequena) sino la imagen a la que se hace referencia, por eso tomo el padre de la img
									tienePadreImagen=true;//para indicar que no revise width y height de html
								}
									
							}catch(Exception ex) {
								ex.printStackTrace();
							}
						}
					}else {
						if(!padre.trim().equals("")) {
							ImageFormats imf=new ImageFormats();
							java.util.ArrayList<String> formats= imf.retornaFormatos();
							String extension=padre.substring(padre.lastIndexOf("."));
							if(formats.contains(extension.toUpperCase())) {
								rutaLlave=e.parent().attr("href");//indio que la ruta de la imagen no va a ser el thumball(imagen pequena) sino la imagen a la que se hace referencia, por eso tomo el padre de la img
								tienePadreImagen=true;//para indicar que no revise width y height de html
							}
						}
					}
					
					//rutaLlave=e.parent().attr("href");//indio que la ruta de la imagen no va a ser el thumball(imagen pequena) sino la imagen a la que se hace referencia, por eso tomo el padre de la img
					//tienePadreImagen=true;//para indicar que no revise width y height de html
					//System.out.println("Esta es la ruta cambiada: "+rutaLlave);
				}
				if(tag.equals("img")) {
					//llamo al metodo que analiza la imagen y la devuelve:
					AnalizaImagen ai=new AnalizaImagen();
					String data[]=ai.textoImagen(rutaLlave, archivo);
					if(data!=null) {
						numImagenes++;
						if(e.hasAttr("width")) 
							ancho=Integer.parseInt(e.attr("width"));
						else
							ancho=Integer.parseInt(data[2]);//si no tiene el atributo width, tomo el width de la imagen
						if(e.hasAttr("height"))
							alto=Integer.parseInt(e.attr("height"));
						else
							alto=Integer.parseInt(data[1]);//si no tiene el atributo height tomo el height de la imagen
						//en caso d que contenga texto alternativo
						String alternativo="";
						if(e.hasAttr("alt")) {
							alternativo=e.attr("alt");
						}
						ValidaTextoImagenTextoAlternativo vtita=new ValidaTextoImagenTextoAlternativo();
						System.out.println("Entraaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa: "+data[0]+"    "+rutaLlave);
						boolean []res=vtita.compruebaTextoImagenTextoAlternativo(alto, ancho, data[0], alternativo);
						if(res[0])
							numImagenesConTextoBajaResolucion++;
						if(res[1])
							numImagenesTextoAlternativo++;
						if(res[2])
							numeroImagenesPocoTextoAlternativo++;
					}else {
						System.out.println("Imagen con atributo src vacio......");
					}
				}
				if(tag.equals("iframe")) {
					ImageFormats imageFormats=new ImageFormats();
					java.util.ArrayList<String>imgFormats=imageFormats.retornaFormatos();
					for(String format:imgFormats) {//recorro todos los formatos hasta encontrar el que concuerde con el src
						if(rutaLlave.contains(format)) {
							//llamo al metodo que analiza la imagen y la devuelve:
							AnalizaImagen ai=new AnalizaImagen();
							String data[]=ai.textoImagen(rutaLlave, archivo);
							if(data!=null) {
								numImagenes++;
								if(e.hasAttr("width")) 
									ancho=Integer.parseInt(e.attr("width"));
								else
									ancho=Integer.parseInt(data[2]);//si no tiene el atributo width, tomo el width de la imagen
								if(e.hasAttr("height"))
									alto=Integer.parseInt(e.attr("height"));
								else
									alto=Integer.parseInt(data[1]);//si no tiene el atributo height tomo el height de la imagen
								//en caso d que contenga texto alternativo
								String alternativo="";
								if(e.hasAttr("alt")) {
									alternativo=e.attr("alt");
								}
								ValidaTextoImagenTextoAlternativo vtita=new ValidaTextoImagenTextoAlternativo();
								boolean []res=vtita.compruebaTextoImagenTextoAlternativo(alto, ancho, data[0], alternativo);
								if(res[0])
									numImagenesConTextoBajaResolucion++;
								if(res[1])
									numImagenesTextoAlternativo++;
								if(res[2])
									numeroImagenesPocoTextoAlternativo++;
							}else {
								System.out.println("Imagen con atributo src vacio......");
							}
						}
					}
				}
				if (tag.equals("input")) {
					if(e.hasAttr("type")) {
						if(e.attr("type").equals("image")) {
							//llamo al metodo que analiza la imagen y la devuelve:
							AnalizaImagen ai=new AnalizaImagen();
							String data[]=ai.textoImagen(rutaLlave, archivo);
							if(data!=null) {
								numImagenes++;
								if(e.hasAttr("width")) 
									ancho=Integer.parseInt(e.attr("width"));
								else
									ancho=Integer.parseInt(data[2]);//si no tiene el atributo width, tomo el width de la imagen
								if(e.hasAttr("height"))
									alto=Integer.parseInt(e.attr("height"));
								else
									alto=Integer.parseInt(data[1]);//si no tiene el atributo height tomo el height de la imagen
								//en caso d que contenga texto alternativo
								String alternativo="";
								if(e.hasAttr("alt")) {
									alternativo=e.attr("alt");
								}
								ValidaTextoImagenTextoAlternativo vtita=new ValidaTextoImagenTextoAlternativo();
								boolean []res=vtita.compruebaTextoImagenTextoAlternativo(alto, ancho, data[0], alternativo);
								if(res[0])
									numImagenesConTextoBajaResolucion++;
								if(res[1])
									numImagenesTextoAlternativo++;
								if(res[2])
									numeroImagenesPocoTextoAlternativo++;
							}else {
								System.out.println("Imagen con atributo src vacio......");
							}
						}
					}
				}
			}
		}
	}
	/**
	 * Metodo que agrega en un array el numero total de imagenes, el numero de imagenes con baja resolucion,
	 * el numero de imagenes con texto alternativo y el numero de imagenes con texto alterntativo menor al de la imagen.
	 * @return Retorna el total de imagenes con el total de texto alternativo y de imagenes con texto.
	 */
	public java.util.ArrayList<Integer> totales(){
		java.util.ArrayList<Integer> listado=new java.util.ArrayList<Integer>();
		listado.add(numImagenes);//primero el numero de imagenes
		listado.add(numImagenesConTextoBajaResolucion);//numero de imagenes con baja resolucion
		listado.add(numImagenesTextoAlternativo);//numero de imagenes con texto alternativo
		listado.add(numeroImagenesPocoTextoAlternativo);//numero de imagenes con texto alternativo menor al texto de la imagen.
		numImagenes=0;
		numImagenesTextoAlternativo=0;
		numImagenesConTextoBajaResolucion=0;
		numeroImagenesPocoTextoAlternativo=0;
		return listado;
	}
}
