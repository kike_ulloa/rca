package recomender.analysis.html;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import recomender.analysis.AnalizaPDFs;
import recomender.staticcontent.VariablesEstaticas;
import recomender.translate.Translate;

import org.jsoup.nodes.Element;

public class AnalizaPDF {
	java.util.ArrayList<String[]> resultado=new java.util.ArrayList<String[]>();
	/**
	 * Metodo que extrae los enlaces de los pdf con JSoup y llama a otros metodos para analizar dichos pdf
	 * @param archivo Archivo a analizar
	 * @param tipo Es el tipo a analizar(html o script)
	 * @param ruta Es la ruta del archivo a analizar o script a analizar
	 * @param script Es el script a analizar en caso de que sea tipo script y no html
	 * @return Retorna un listado con todos los archivos pdf y sus respectivos errores de accesibilidad.
	 */
	public java.util.ArrayList<String[]> sonAccesibles(java.io.File archivo, String tipo, String ruta, String script, String nombreOA) {
		resultado=new java.util.ArrayList<String[]>();
		try {
			Document doc=new Document("UTF-8");
			if(tipo.equals("html"))
				doc=Jsoup.parse(archivo, "UTF-8");
			if(tipo.equals("script"))
				doc=Jsoup.parse(script);
			Elements hrefs=doc.select("a[href]");//selecciono todos los a que tengan atributo href
			Elements iframes=doc.select("iframe[src]");
			Elements embed=doc.select("embed[src]");
			Elements objects=doc.select("object[data]");
			//hrefs
			System.out.println(hrefs.size());
			for(Element e: hrefs) {
				if(e.attr("href").contains(".pdf")) {//en caso de que la ruta sea a pdf
					System.out.println("entra");
					
					AnalizaPDFs ap=new AnalizaPDFs();
					String errores=ap.retornaErrores(e.attr("href"), ruta, nombreOA);//Analiza y retorna los errores 
					String []res=new String[] {e.attr("href"),analizaErrores(errores)};//Retorna los errores en formato texto y agrega en un vector junto con el nombre del pdf
					resultado.add(res);//agrega el vector resultante en el array a retornar.
				}
			}
			//iframes
			for(Element e: iframes) {
				if(e.attr("src").contains(".pdf")) {//en caso de que la ruta sea a pdf
					AnalizaPDFs ap=new AnalizaPDFs();
					String errores=ap.retornaErrores(e.attr("src"), ruta, nombreOA);
					String []res=new String[] {e.attr("src"),analizaErrores(errores)};
					resultado.add(res);
				}
			}
			//embed
			for(Element e: embed) {
				if(e.attr("src").contains(".pdf")) {//en caso de que la ruta sea a pdf
					AnalizaPDFs ap=new AnalizaPDFs();
					String errores=ap.retornaErrores(e.attr("src"), ruta, nombreOA);
					String []res=new String[] {e.attr("src"),analizaErrores(errores)};
					resultado.add(res);
				}
			}
			//object
			for(Element e: objects) {
				if(e.attr("data").contains(".pdf")) {//en caso de que la ruta sea a pdf
					AnalizaPDFs ap=new AnalizaPDFs();
					String errores=ap.retornaErrores(e.attr("data"), ruta, nombreOA);
					String []res=new String[] {e.attr("data"),analizaErrores(errores)};
					resultado.add(res);
				}
			}
			return resultado;
		}catch(Exception e) {
			e.printStackTrace();
			return null; 
		}
	}
	/**
	 * Metodo que extrae solo los errores en texto usando JSoup de el texto devuelto en html
	 * @param codigo Es el codigo que esta en formato html y se le va a hace run parse para devolver solo el texto.
	 * @return Retorna en texto plano los errores.
	 */
	public String analizaErrores(String codigo) {
		Document doc=Jsoup.parse(codigo);
		Elements el=doc.getElementsByClass("fail");//Selecciona solo los elementos que contengan la clase fail(Alli estan los errores)
		String errores="";
		//System.out.println("tamanio:::::: "+el.size());
		if(el.size()>0) {//si es mayor a cero significa que si hay errores.
			for(Element e: el) {
				if(!e.text().toLowerCase().contains("barriers found:"))//cuando hay errores, va a retornar "Barreries found: #", por lo cual solo se quiere los errores
					errores+=e.text()+"\n";
			}
		}
		if(!errores.equals("")) {
			errores=errores.substring(0, errores.lastIndexOf("\n"));//quito el salto de linea del final del string
			Translate t=new Translate();
			errores=t.taducir(errores);
		}
		return errores;
	}
	/**
	 * Retorna los fresultados totales
	 * @param re Array list con los datos de pdfs con errorees.
	 * @return retirna los resultados
	 */
	public Object[] retornaResultados(java.util.ArrayList<String[]> re){
		String completo="Se ha encontrado errores de accesibilidad en los siguientes archivos pdf:\n";
		int contador=0;
		boolean encontrado=false;
		System.out.println("TAMANOOOOOOOOOOOOOOOOOOOOOOOOO: "+re.size());
		if(re.size()>0) {
			VariablesEstaticas.numPDFs=VariablesEstaticas.numPDFs+re.size();
			encontrado =true;
			for (String[] strings : re) {
				if(!strings[1].equals("")&&!completo.contains(strings[0])) {
					//completo+="->"+strings[0]+"\n"+strings[1];
					completo+="<li><b>"+strings[0]+":</b>\n";
					String errores="<ul>";
					String []err=strings[1].split("\n");
					for(String e:err) {
						if(e!=null&&!e.isEmpty()) {
							
							errores+="<li><b>"+e.substring(0,e.indexOf(":"))+"</b>"+e.substring(e.indexOf(":"))+"</li>";
						}
					}
					errores+="</ul></li>";
					completo+=errores;
					
					contador++;
				}
			}
			
			System.out.println("DEL PDFFFFFFFFFFFFFFFFF "+completo);
		}
		if(contador>0) {
			return new Object[] {contador, completo, false};
		}else if(encontrado){
			return new Object[] {"No se han encontrado errores de accesibilidad en los pdfs encontrados",true};
		}else {
			return new Object[] {"No se han encontrado archivos PDFs",false};
		}
	}
}
