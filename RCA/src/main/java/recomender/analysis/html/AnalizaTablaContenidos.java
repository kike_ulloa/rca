package recomender.analysis.html;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import recomender.staticcontent.VariablesEstaticas;

public class AnalizaTablaContenidos {

	/**
	 * Metodo que analiza si el archivo posee o no posee tabla de contenidos
	 * @param htmlFile Es el archivo a nalizarse
	 * @return Retorna true o false 
	 */
	public Object[] tieneTablaContenido(java.io.File htmlFile) {
		try {
			Document doc=Jsoup.parse(htmlFile, "UTF-8");
			Elements li=doc.select("li");//selecciono todos los li
			int numeroLi=0;//numero de li que existen con hijo <a
			for(Element element: li) {
				if(element.getElementsByTag("a").size()>0) {//si contiene hijos con tag <a
					numeroLi++;
				}
			}
			if(numeroLi>2) {//si tiene mas de dos <li con tag <a si tiene tabla de contenidos
				VariablesEstaticas.numTablaContenidos=VariablesEstaticas.numTablaContenidos+numeroLi;
				return new Object[] {"Se a determinado que el contenido si posee tabla de contenidos",true};
			}
			else
				return new Object[] {"Se a determinado que el contenido no posee tabla de contenidos, por lo cual se recomienda que se implemente una tabla de contenidos.",false};
		}catch(Exception e) {
			e.printStackTrace();
			return new Object[] {"Se ha producido un error al analizar si el contenido posee o no posee tablas de contenidos.",false};
		}
	}
}
