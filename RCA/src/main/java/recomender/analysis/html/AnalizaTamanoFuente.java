package recomender.analysis.html;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import recomender.validations.RetornaSoloNumeroDeString;
import recomender.calculations.UnidadesTamanoFuente;

public class AnalizaTamanoFuente {
	double pixelesDefaultNavegador=20;//numero de pixeles minimos de la letra (default del navegador es 16px)
	double tamanioDefaultNavegadorTagFont=3.75;//tamanio de letra que equivale a 20px (3=16px 20px=? [20*3]/16=3.75)
	
	UnidadesTamanoFuente utf=new UnidadesTamanoFuente();
	/**
	 * Meotod que buca las etiquetas font-size
	 * @param htmlFile Archivo a analizarse
	 * @param llave Es al allave a encontrar
	 * @return Retorna si el tamano fuenter es adecado o no, un true o un false.
	 */
	public boolean tagFont(java.io.File htmlFile, String llave) {
		double pixeles=0;
		if(llave.equals("font")) {
			try {
			Document doc=Jsoup.parse(htmlFile, "UTF-8");
			Elements fonts=doc.select(llave);
			for(Element font: fonts) {
				if(font.hasAttr("size")) {
					double tamanio=Integer.parseInt(font.attr("size"));
					pixeles=(tamanio*pixelesDefaultNavegador)/tamanioDefaultNavegadorTagFont;//valor del size * 20 px del navegador / 3.75 
				}
			}
			if(pixeles>=20) {//si el tamanio es mayor a 20px
				return true;
			}
			}catch(Exception e) {
				e.printStackTrace();
				return false;
			}
		}
		return false;
	}
	/**
	 * Analiza el tamano fuente de los tags encontrados
	 * @param htmlFile Es el archivo a anlizarse
	 * @return Retorna el resuktado.
	 */
	public int[] analizaTodoHTML(java.io.File htmlFile) {
		try {
			int numeroSicumple=0;
			int numeroTotalAnalizado=0;
			Document doc=Jsoup.parse(htmlFile, "UTF-8");
			Elements elementos=doc.getElementsByAttribute("style");//obtengo todos los elementos que contienen el atributo style
			for(Element element:elementos) {
				String style=element.attr("style");
				style.replaceAll("/n", "");//quito todos los saltos de linea
				style=style.replaceAll(" ", "");//quito todos los espacios
				if(style.contains("font-size")) {//si el style contiene el font-size
					style=style.substring(style.indexOf("font-size"));//corto la cadena desde font style
					style=style.substring(0, style.indexOf(";"));//corto la cadena hasta el ;
					numeroTotalAnalizado++;//aumento el numero de analizados
					if(utf.analiza(style,"html")) {
						numeroSicumple++;
					}
//					if(style.contains("pt")) {//si es igual a puntos 
//						if(utf.pt(style))//si es mayor a 20px
//							numeroSicumple++;//aumento el numero de los que si son mayor a 20px
//					}else if(style.contains("pc")) {//si son igual a picos
//						if(utf.pc(style))//si es mayor a 20px
//							numeroSicumple++;//aumento el numero de los que son mayor a 20px
//					}else if(style.contains("mm")) {//si es igual a milimetros
//						if(utf.mm(style))////si es mayor a 20px
//							numeroSicumple++;//aumento el numero de los que son mayor a 20px
//					}else if(style.contains("cm")) {//si es igual a centimetros
//						if(utf.cm(style))//si es mayor a 20px
//							numeroSicumple++;//aumento el numero de los que si son mayor a 20px
//					}else if(style.contains("in")) {//si es igual a pulgadas
//						if(utf.in(style))//si es mayor a 20px
//							numeroSicumple++;//aumento el numero de los que si son mayor a 20px
//					}else if(style.contains("em")) {//si es igual a em
//						if(utf.em(style))//si es mayor a 20px
//							numeroSicumple++;//aumento el numero de los que si son mayor a 20px
//					}else if(style.contains("px")) {//si es igual a pixels
//						if(utf.px(style))//si es mayor a 20px
//							numeroSicumple++;//aumento el numero de los que si son mayor a 20px
//					}
				}
			}
			//if(numeroSicumple>0)//si existe mas de uno que si cumpla retorna verdadero.
				return new int[] {numeroSicumple,numeroTotalAnalizado};
		}catch(Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
}
