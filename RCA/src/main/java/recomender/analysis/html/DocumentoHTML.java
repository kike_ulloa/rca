package recomender.analysis.html;

import java.io.File;
import java.io.IOException;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import recomender.validations.Offline;
import recomender.validations.Online;

public class DocumentoHTML {
	/**
	 * Este metodo nos permitira obtener el documento html ya sea online u offline
	 * @param archivo
	 * @return
	 */
	public Document obtenerDocumento(String archivo) {
		Offline of=new Offline();
		Online on=new Online();
		Document doc=null;
		if(archivo.indexOf("//")>-1||archivo.indexOf("www.")>-1) { //Analizamos si es un archivo online u offline.
			if(on.existeOnline(on.conexionURL(archivo))) {//Analizamos que exista el link
			try {
				 doc = Jsoup.connect(archivo).get();//Obtenemos el archivo remoto.
			} catch (IOException e) {
				e.printStackTrace();
			}
			}
		}
		else {
			if(of.existeOffline(archivo)) {//Analizamos si existe el archivo local.
				File entrada = new File(archivo);//Recibimos el archivo a analizar.
				try {
					doc = Jsoup.parse(entrada, "UTF-8");//Mandamos a analizar el archivo
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return doc;
	}
}
