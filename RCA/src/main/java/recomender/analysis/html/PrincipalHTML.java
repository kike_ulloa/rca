package recomender.analysis.html;

import java.util.ArrayList;
import java.util.spi.TimeZoneNameProvider;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import recomender.validations.Offline;
import recomender.validations.Online;
import recomender.validations.ValidacionesMultimedia;
import recomender.validations.ValidacionesBasicas;
public class PrincipalHTML {
	DocumentoHTML dHtml=null;
	Online on=null;
	Offline of=null;
	ValidacionesMultimedia vm=null;
	ValidacionesBasicas vb=null;
	//ArrayList<String> encontrados=new ArrayList<String>();//Instanciamos la variable que nos ayudara a contar el numero de palabras dentro del archivo.
	/**
	 * En este metodo se buscara el diferente contenido deseado dentro de los archivos html y css.
	 * @param archivo.-El archivo html a analizar.
	 * @param etiqueta.-La etiqueta de la cual se quiere extraer informacion.
	 * @param atributo.-El atributo que nos devolvera el valor buscado.
	 * @param atributo2.-Este atributo se usara en caso de que deseemos confirmar la existencia del mismo o para obtener su valor.
	 * @param valorAtributo2.-A traves de este valor se comparara con un valor que nosotros consideremos para el atributo2.
	 * @param atributo3.-Es un atributo variante en caso de la ausencia del atributo2.
	 * @param esArchivo.-Indicamos si el valor que se desea encontrar es un archivo o no.
	 * @return
	 */
	public ArrayList<String> buscar(ArrayList<String> encontrados,boolean esArchivo,String archivo,String etiqueta,String atributosValores,String condicion,String tipoArchivo,String contenidoOtroArchivo,boolean buscaTexto) {
		dHtml=new DocumentoHTML();
		vb=new ValidacionesBasicas();
		Document doc=null;
		if(esArchivo) {
			if(archivo!=null&archivo.length()>2)  //Analizamos si el archivo es diferente de nulo y que no sea vacio o #
				doc=dHtml.obtenerDocumento(archivo);
		}else
			doc=Jsoup.parse(archivo);
		if(doc!=null) {
			Elements etiquetas;
				if(etiqueta!=null)
					etiquetas= doc.select(etiqueta);//Indicamos los parametros(etiquetas) que usaremos en la busqueda o analisis.
				else
					etiquetas=doc.getElementsByAttribute(atributosValores);
				for (Element resultadoEtiqueta : etiquetas) {//Recorremos el arreglo de los resultados obtenidos.
					if(buscaTexto)  //Analizamos si la busqueda que se va a realizar es de texto o de alguna etiqueta.
						encontrados=vb.analisisPalabras(encontrados, resultadoEtiqueta);// Buscamos el numero de palabras en el html.
					else {
						String parametroPrincipal=null;
						if(atributosValores!=null)
							parametroPrincipal=atributosValores.split(";")[0];
						if(tipoArchivo==null) {
							if(vb.tieneAtributo(resultadoEtiqueta, parametroPrincipal)||parametroPrincipal==null)
								encontrados.add(resultadoEtiqueta.toString());/////Esta seccion es para elementos que no son archivos ni texto.
							}else {
							String valor=resultadoEtiqueta.attr(parametroPrincipal);//Obtenemos el valor que contiene el atributo analizado.															
							if(valor.isEmpty()) {
								for (Element hijo : resultadoEtiqueta.children()) {
									valor=hijo.attr(parametroPrincipal);
									if(!valor.isEmpty())
										break;
								}
							}						
							if(valor.length()>2) {//Analizamos que el valor obtenido no se encuentre vacio
										if(vb.cumpleCondicionHTML(condicion,atributosValores,resultadoEtiqueta)) {//Con esta condicion podemos analizar si una etiqueta tiene cualquiera de dos atributos que queramos buscar.(Ej.video->controls,autoplay)
											if(tipoArchivo.equals("desconocido")&&valor.charAt(0)=='#')
												encontrados.add(valor);
											encontrados=vb.existeArchivo(archivo, valor, tipoArchivo, encontrados, contenidoOtroArchivo);
										}
									}
							}
					}
				}
		}
		return encontrados;
	}
/**
 * Este metodo permite encontrar los archivos html que se encuentren embebidas dentro de las html analizados.
 * @param archivo.- Archivo html analizado
 * @param htmlEncontrados.- arreglo donde se agrega los datos a analizar.
 * @return
 */

	public ArrayList<String>paginasHTMLIncrustadas(String archivo,ArrayList<String>htmlEncontrados){
		on=new Online();
		of=new Offline();
		vm=new ValidacionesMultimedia();
		dHtml=new DocumentoHTML();
		Document doc=null;
			if(archivo!=null&archivo.length()>2)  //Analizamos si el archivo es diferente de nulo y que no sea vacio o #
				doc=dHtml.obtenerDocumento(archivo);
		if(doc!=null) {
		String []etiquetas= {"iframe","embeded","object"};
		String []atributos= {"src","src","data"};
		for (int i = 0; i < atributos.length; i++) {
			Elements embebidas = doc.select(etiquetas[i]);//Obtendremos un arreglo de elementos de las etiquetas obtenidas en base a la etiqueta embebida que estamos buscando.
			for (Element resultadoEmbebidas : embebidas) {// Recorremos el arreglo obtenido.
				String nuevoArchivo=resultadoEmbebidas.attr(atributos[i]);//Guardamos el path que se encontro en el atributo indicado.
				if(nuevoArchivo!=null&nuevoArchivo.length()>2) { //Se analiza que el path no sea nulo y ni vacio o un #.
					if(nuevoArchivo.indexOf("//")>-1||nuevoArchivo.indexOf("www.")>-1) {//Se analiza si el path es online u offline.
						if(on.existeOnline(on.conexionURL(nuevoArchivo))) {//Se analiza que exista el link.
							if(!vm.tieneFormato("audio", nuevoArchivo)&&!vm.tieneFormato("imagen", nuevoArchivo)&&!vm.esVideo(nuevoArchivo)) { // Se comprueba que el link no es un video,imagen o audio.
								htmlEncontrados.add(nuevoArchivo);
								paginasHTMLIncrustadas(nuevoArchivo, htmlEncontrados);
							}
						}
					}else {
						nuevoArchivo=archivo.substring(0,archivo.lastIndexOf("/")+1)+nuevoArchivo;
						if(of.existeOffline(nuevoArchivo)) {//se va a aplicar los mismos criterios que arriba con diferencia, que aqui se analizara de manera local.
							if(!vm.tieneFormato("audio", nuevoArchivo)&&!vm.tieneFormato("imagen", nuevoArchivo)&&!vm.tieneFormato("video", nuevoArchivo)) {
								htmlEncontrados.add(nuevoArchivo);
							    paginasHTMLIncrustadas(nuevoArchivo, htmlEncontrados);
							}
						}
					}	
				}
			}
		}
		}
		return htmlEncontrados;
	}
}
