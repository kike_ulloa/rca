package recomender.analysis.javascript;

import recomender.calculations.UnidadesTamanoFuente;
public class AnalizaFontSize {
	int numeroSicumple=0;
	int numeroTotal=0;
	//1. Forma 1: Analizar como el inner html o como html puro
	//2. Forma 2: Analizar la funcion fontisize(7)
	//3. Forma 3: Analizar con style.fontsize="20px"
	/**
	 * Analiza tamano fuente con la funcion inner html
	 * @param javascript Es el codigo javascript a analizar
	 * @return Retorna si es de un tamano adecuado o no.
	 */
	//FORMA 1
	public boolean analizaHtml(String javascript) {
		//numeroSicumple=0;
		javascript.replaceAll("\n", "");
		javascript.replaceAll(" ", "");
		analiza(javascript);
		if(numeroSicumple>0) {
			return true;
		}else {
			return false;
		}
	}
	/**
	 * Analiza los tamanos fuentes para le metodo anterior, de forma recusriva 
	 * @param javascript Es el codigo javascript
	 */
	
	public void analiza(String javascript) {
		if(javascript.trim().contains("font-size:")) {
			numeroTotal++;
			String size=javascript.substring(javascript.indexOf("font-size:"));
			String aux=size.substring(size.indexOf(":"));
			size=size.substring(0, size.indexOf(";"));
			UnidadesTamanoFuente utf=new UnidadesTamanoFuente();
			//System.out.println(utf.analiza(size, "no")+"    "+size );
			if(utf.analiza(size, "no"))
				numeroSicumple++;
			if(aux.contains("font-size:"))//en caso de que aun exista mas font size entra de nuevo al mismo metodo
				analiza(aux);
		}
	}
	/**
	 * Analiza tamano fuente con la funcion fontSize() javascript
	 * @param javascript Es el codigo javascript a analizar
	 * @return Retorna si es de un tamano adecuado o no.
	 */
	//forma 2
	public boolean analizaFuncionFontSize(String javascript) {
		//numeroSicumple=0;
		javascript.replaceAll("\n", "");
		javascript.replaceAll(" ", "");
		analizaFuncion(javascript);
		if(numeroSicumple>0) {
			return true;
		}else {
			return false;
		}
	}
	/**
	 * Analiza los tamanos fuentes para le metodo anterior, de forma recusriva 
	 * @param javascript Es el codigo javascript
	 */
	public void analizaFuncion(String javascript) {
		if(javascript.trim().contains(".fontsize(")) {
			numeroTotal++;
			String size=javascript.substring(javascript.indexOf(".fontsize(")+2);//+2 porque el validador de numero toma en cuenta el punto
			String aux=size;
			size=size.substring(0,size.indexOf(")"));
			UnidadesTamanoFuente utf=new UnidadesTamanoFuente();
			//System.out.println(utf.analiza(size,"no"));
			if(utf.analiza(size,"no"))
				numeroSicumple++;
			if(aux.contains(".fontsize("))//en caso de que aun exista mas font size entra de nuevo al mismo metodo
				analizaFuncion(aux);
		}
	}
	/**
	 * Analiza tamano fuente con la funcion .fontSize= javascript
	 * @param javascript Es el codigo javascript a analizar
	 * @return Retorna si es de un tamano adecuado o no.
	 */
	//Forma 3
	public boolean analizaFuncionStyle(String javascript) {
		//numeroSicumple=0;
		javascript.replaceAll("\n", "");
		javascript.replaceAll(" ", "");
		analizaStyle(javascript);
		if(numeroSicumple>0) {
			return true;
		}else {
			return false;
		}
	}
	/**
	 * Analiza los tamanos fuentes para le metodo anterior, de forma recusriva 
	 * @param javascript Es el codigo javascript
	 */
	public void analizaStyle(String javascript) {
		if(javascript.trim().contains("style.fontsize")) {
			numeroTotal++;
			String size=javascript.substring(javascript.indexOf("style.fontsize")+10);//+10 para que no tome en cuenta el punto
			String aux=size;
			size=size.substring(0,size.indexOf(";"));
			UnidadesTamanoFuente utf=new UnidadesTamanoFuente();
			///System.out.println(utf.analiza(size, "no"));
			if(utf.analiza(size, "no"))
				numeroSicumple++;
			if(aux.contains("style.fontsize"))//en caso de que aun exista mas font size entra de nuevo al mismo metodo
				analizaStyle(aux);
		}
	}
	/**
	 * Analiza tamano fuente con la etiqueta <font incrustado en javascript
	 * @param javascript Es el codigo javascript a analizar
	 * @return Retorna si es de un tamano adecuado o no.
	 */
	//forma 4
	public boolean analizaTagFontSize(String javascript) {
		//numeroSicumple=0;
		javascript.replaceAll("\n", "");
		javascript.replaceAll(" ", "");
		analizaTagFont(javascript);
		if(numeroSicumple>0) {
			return true;
		}else {
			return false;
		}
	}
	/**
	 * Analiza los tamanos fuentes para le metodo anterior, de forma recusriva 
	 * @param javascript Es el codigo javascript
	 */
	public void analizaTagFont(String javascript) {
		if(javascript.trim().contains("<font")) {
			numeroTotal++;
			String size=javascript.substring(javascript.indexOf("<font")+3);//mas tres para que el auxiliar no lo tome en cuenta en el recursivo
			String aux=size;
			size=size.substring(0,size.indexOf(">"));
			if(size.contains("font-size:")) {
				size=size.substring(size.indexOf("font-size:")+3);
				UnidadesTamanoFuente utf=new UnidadesTamanoFuente();
				if(utf.analiza(size, "no"))
					numeroSicumple++;
				if(aux.contains("<font"))
					analizaTagFont(aux);
			}
		}
	}
	/**
	 * Metodo que llama a los metodos antes descritos para la revison del tamano fuente.
	 * @param script Es el codigo javascript a analizar
	 * @return Retorna El numero de tamanos fuents q cumple con un tamano fuente adecuado y el numero de tamanos fuente encontrados.
	 */
	public int [] retornaResultado(String script) {
		analizaHtml(script);
		analizaFuncionFontSize(script);
		analizaFuncionStyle(script);
		analizaTagFontSize(script);
		int[] res=new int[] {numeroSicumple,numeroTotal};
		numeroSicumple=0;
		numeroTotal=0;
		return res;
	}
}
