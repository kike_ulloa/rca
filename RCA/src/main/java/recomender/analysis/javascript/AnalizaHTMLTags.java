package recomender.analysis.javascript;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import recomender.analysis.AnalizaImagen;
import recomender.analysis.AnalizaPDFs;
import recomender.analysis.html.AnalizaPDF;
import recomender.formats.ImageFormats;
import recomender.validations.ValidaTextoImagenTextoAlternativo;

public class AnalizaHTMLTags {

	int numeroImagenes=0;
	int numeroImagenesTextoAlernativo=0;
	int numeroImagenesBajaResolucion=0;
	int numeroImagenesPocoTextoAlternativo=0;
	/**
	 * Analiza cad auna de las imagenes en busca de texto alternativo y texto incrustado en cada imagen.
	 * @param texto Es el texto a analizar (codigo javascript)
	 * @param tag Es la etoqueta a buscar.
	 * @param atributo Es el atributo de la etiqueta a analizar
	 * @param archivo Es el archivo que contiene el codigo.
	 */
	public void analia(String texto, String tag, String atributo, java.io.File archivo) {
		Document doc=Jsoup.parse(texto);
		Elements elements=doc.select(tag);
		for(Element e: elements) {
			if(tag.equals("img")) {
				int height=0;
				int width=0;
				//System.out.println(e.attr(atributo));
				String src=e.attr(atributo);
				if(src!=null&&!src.equals("")) {//en cao de que la imagen si tenga src y no sea vacio
					//llamo al metodo que analiza la imagen y la devuelve:
					AnalizaImagen ai=new AnalizaImagen();
					String data[]=ai.textoImagen(src, archivo);
					if(data!=null) {
						numeroImagenes++;
						if(e.hasAttr("width")) 
							width=Integer.parseInt(e.attr("width"));
						else
							width=Integer.parseInt(data[2]);//si no tiene el atributo width, tomo el width de la imagen
						if(e.hasAttr("height"))
							height=Integer.parseInt(e.attr("height"));
						else
							height=Integer.parseInt(data[1]);//si no tiene el atributo height tomo el height de la imagen
						//en caso d que contenga texto alternativo
						String alternativo="";
						if(e.hasAttr("alt")) {
							alternativo=e.attr("alt");
						}
						ValidaTextoImagenTextoAlternativo vtita=new ValidaTextoImagenTextoAlternativo();
						boolean []res=vtita.compruebaTextoImagenTextoAlternativo(height, width, data[0], alternativo);
						if(res[0])
							numeroImagenesBajaResolucion++;
						if(res[1])
							numeroImagenesTextoAlernativo++;
						if(res[2])
							numeroImagenesPocoTextoAlternativo++;
					}else {
						System.out.println("Imagen con atributo src vacio......");
					
					}
				}
			}
			if(tag.equals("iframe")) {
				String rutaLlave=e.attr(atributo);
				if(rutaLlave!=null&&!rutaLlave.equals("")) {//en caso de que si contenga src y este no sea vacio
					ImageFormats imageFormats=new ImageFormats();
					java.util.ArrayList<String>imgFormats=imageFormats.retornaFormatos();
					for(String format:imgFormats) {//recorro todos los formatos hasta encontrar el que concuerde con el src
						if(rutaLlave.contains(format)) {
							int ancho=0;
							int alto=0;
							//llamo al metodo que analiza la imagen y la devuelve:
							AnalizaImagen ai=new AnalizaImagen();
							String data[]=ai.textoImagen(rutaLlave, archivo);
							if(data!=null) {
								numeroImagenes++;
								if(e.hasAttr("width")) 
									ancho=Integer.parseInt(e.attr("width"));
								else
									ancho=Integer.parseInt(data[2]);//si no tiene el atributo width, tomo el width de la imagen
								if(e.hasAttr("height"))
									alto=Integer.parseInt(e.attr("height"));
								else
									alto=Integer.parseInt(data[1]);//si no tiene el atributo height tomo el height de la imagen
								//en caso d que contenga texto alternativo
								String alternativo="";
								if(e.hasAttr("alt")) {
									alternativo=e.attr("alt");
								}
								ValidaTextoImagenTextoAlternativo vtita=new ValidaTextoImagenTextoAlternativo();
								boolean []res=vtita.compruebaTextoImagenTextoAlternativo(alto, ancho, data[0], alternativo);
								if(res[0])
									numeroImagenesBajaResolucion++;
								if(res[1])
									numeroImagenesTextoAlernativo++;
								if(res[2])
									numeroImagenesPocoTextoAlternativo++;
							}else {
								System.out.println("Imagen con atributo src vacio......");
							}
						}
					}
				}
			}
			if (tag.equals("input")) {	
				String rutaLlave=e.attr(atributo);
				if(rutaLlave!=null&&!rutaLlave.equals("")) {
					if(e.hasAttr("type")) {
						if(e.attr("type").equals("image")) {
							int ancho, alto=0;
							
							//llamo al metodo que analiza la imagen y la devuelve:
							AnalizaImagen ai=new AnalizaImagen();
							String data[]=ai.textoImagen(rutaLlave, archivo);
							if(data!=null) {
								numeroImagenes++;
								if(e.hasAttr("width")) 
									ancho=Integer.parseInt(e.attr("width"));
								else
									ancho=Integer.parseInt(data[2]);//si no tiene el atributo width, tomo el width de la imagen
								if(e.hasAttr("height"))
									alto=Integer.parseInt(e.attr("height"));
								else
									alto=Integer.parseInt(data[1]);//si no tiene el atributo height tomo el height de la imagen
								//en caso d que contenga texto alternativo
								String alternativo="";
								if(e.hasAttr("alt")) {
									alternativo=e.attr("alt");
								}
								ValidaTextoImagenTextoAlternativo vtita=new ValidaTextoImagenTextoAlternativo();
								boolean []res=vtita.compruebaTextoImagenTextoAlternativo(alto, ancho, data[0], alternativo);
								if(res[0])
									numeroImagenesBajaResolucion++;
								if(res[1])
									numeroImagenesTextoAlernativo++;
								if(res[2])
									numeroImagenesPocoTextoAlternativo++;
							}else {
								System.out.println("Imagen con atributo src vacio......");
							}
						}
					}
				}
			}
		}
//		 if(tag.equals("pdf")) {
//			System.out.println("Entraaaaaaaaaaa");
//			AnalizaPDF ap=new AnalizaPDF();
////			String script="document.write(\"<a href=\"https://www.modelocurriculum.net/wp-content/formato_hoja_vida.pdf\">PDF1</a>"+
////			"<a href=\"https://www.modelocurriculum.net/wp-content/formato_hoja_vida.pdf\">PDF2</a>"+
////			"<a href=\"https://www.modelocurriculum.net/wp-content/formato_hoja_vida.pdf\">PDF3</a>"+
////			"<a href=\"https://www.modelocurriculum.net/wp-content/formato_hoja_vida.pdf\">PDF4</a>"+
////			"<a href=\"https://www.modelocurriculum.net/wp-content/formato_hoja_vida.pdf\">PDF5</a>\");";
//			java.util.ArrayList<String[]> resultado=ap.sonAccesibles(null, "script", null, texto);
//			for(String res[]:resultado) {
//				System.out.println("#######################################################");
//				for(String r: res) {
//					System.out.println(r);
//				}
//			}
//		}else if(tag.equals("video")) {
//			//AKI LLAMO A LA CLASE SIBTITLES MAIN SETEANDO  EL PATH EN DICHA CLASE
//		}
	}
	/**
	 * Metodo que Analiza los tag de pdf 
	 * @param script -> Es el script que se va a analizar
	 * @param ruta -> Es la ruta online a la que se le va a agregar el pdf
	 * @return retorna un array con todos los resultados de todos los pdf analizados de la pagina.
	 */
//	public java.util.ArrayList<String[]> sonAccesibles(String script, String ruta) {
//		java.util.ArrayList<String[]> resultado=new java.util.ArrayList<String[]>();
//		try {
//			Document doc=Jsoup.parse(script);
//			Elements hrefs=doc.select("a[href]");//selecciono todos los a que tengan atributo href
//			Elements iframes=doc.select("iframe[src]");
//			Elements embed=doc.select("embed[src]");
//			Elements objects=doc.select("object[data]");
//			//hrefs
//			for(Element e: hrefs) {
//				if(e.attr("href").contains(".pdf")) {//en caso de que la ruta sea a pdf
//					AnalizaPDFs ap=new AnalizaPDFs();
//					String []res=new String[] {e.attr("href"),ap.retornaErrores(e.attr("href"))};
//					resultado.add(res);
//				}
//			}
//			//iframes
//			for(Element e: iframes) {
//				if(e.attr("src").contains(".pdf")) {//en caso de que la ruta sea a pdf
//					AnalizaPDFs ap=new AnalizaPDFs();
//					String []res=new String[] {e.attr("src"),ap.retornaErrores(e.attr("src"))};
//					resultado.add(res);
//				}
//			}
//			//embed
//			for(Element e: embed) {
//				if(e.attr("src").contains(".pdf")) {//en caso de que la ruta sea a pdf
//					AnalizaPDFs ap=new AnalizaPDFs();
//					String []res=new String[] {e.attr("src"),ap.retornaErrores(e.attr("src"))};
//					resultado.add(res);
//				}
//			}
//			//object
//			for(Element e: objects) {
//				if(e.attr("data").contains(".pdf")) {//en caso de que la ruta sea a pdf
//					AnalizaPDFs ap=new AnalizaPDFs();
//					String []res=new String[] {e.attr("data"),ap.retornaErrores(e.attr("data"))};
//					resultado.add(res);
//				}
//			}
//			return resultado;
//		}catch(Exception e) {
//			e.printStackTrace();
//			return null; 
//		}
//	}
	/**
	 * Metodo que agrega en un array el numero total de imagenes, el numero de imagenes con baja resolucion,
	 * el numero de imagenes con texto alternativo y el numero de imagenes con texto alterntativo menor al de la imagen.
	 * @return Retorna el arraylist de todas las imagenes, imagenes con texto alternativo e imagenes de texto.
	 */
	public java.util.ArrayList<Integer> totales(){
		java.util.ArrayList<Integer> listado=new java.util.ArrayList<Integer>();
		listado.add(numeroImagenes);//primero el numero de imagenes
		listado.add(numeroImagenesBajaResolucion);//numero de imagenes con baja resolucion
		listado.add(numeroImagenesTextoAlernativo);//numero de imagenes con texto alternativo
		listado.add(numeroImagenesPocoTextoAlternativo);//numero de imagenes con texto alternativo menor al texto de la imagen.
		numeroImagenes=0;
		numeroImagenesTextoAlernativo=0;
		numeroImagenesBajaResolucion=0;
		numeroImagenesPocoTextoAlternativo=0;
		return listado;
	}
}
