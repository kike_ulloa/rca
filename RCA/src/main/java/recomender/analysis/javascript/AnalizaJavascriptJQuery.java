package recomender.analysis.javascript;

import java.awt.image.BufferedImage;
import java.net.URL;

import javax.imageio.ImageIO;

import recomender.analysis.AnalizaImagen;
import recomender.analysis.SubtitlesProcess;
import recomender.formats.ImageFormats;
import recomender.formats.VideoFormats;
import recomender.validations.ValidaConexion;
import recomender.validations.ValidaTextoImagenTextoAlternativo;
import recomender.validations.ValidaVideo;

public class AnalizaJavascriptJQuery {
	int numeroImagenes=0;
	int numeroImagenesTextoAlernativo=0;
	int numeroImagenesBajaResolucion=0;
	int numeroImagenesPocoTextoAlternativo=0;
	
	//videos
	int numeroVideos=0;
	int numeroVideosSubtitulos=0;
	int numeroVideosCaptions=0;
	
	/**
	 * Metodo que extrae los src de codigo javascript y jquery en el formato ('src','path')
	 * @param data Cadena a analizar
	 * @param archivo Es el archivo que contiene el codigo javascript
	 */
	public void extraeDeJQuery(String data, java.io.File archivo) {
		//data=trimCompleto(data);
		String src="";
		String recursivo="";
		String alt="";
		String texto="";
		int alto=0;
		int ancho=0;
		boolean esImagen=false;
		boolean esVideo=false;
		//System.out.println("Este es el dataaa:: "+data);
		data=data.replaceAll("\"", "'");
		data=data.replaceAll("‘", "'");
		data=data.replaceAll("’", "'");
		data=data.replaceAll("“", "'");
		data=data.replaceAll("”", "'");
		if(data.contains("'src',")) {
			src=data.substring(data.indexOf("'src',")+5).trim();
			//System.out.println("dataaaaa11111: "+src);
			src=src.substring(src.indexOf(",")+1).trim();
			src=src.substring(src.indexOf("'")+1).trim();
			//System.out.println("dataaaaa: "+src);
			recursivo=src;
			
			src=src.substring(0,src.indexOf(")")-1);
			
			//RECORRO FORMATOS 
			ImageFormats im=new ImageFormats();
			java.util.ArrayList<String> imF=im.retornaFormatos();
			for(int i=0;i<imF.size();i++) {
				if(src.toUpperCase().contains(imF.get(i))) {//Si es una imagen
					AnalizaImagen ai=new AnalizaImagen();
					String dat[]=ai.textoImagen(src, archivo);
					if(data!=null) {
						numeroImagenes++;
						ancho=Integer.parseInt(dat[2]);//si no tiene el atributo width, tomo el width de la imagen
						alto=Integer.parseInt(dat[1]);//si no tiene el atributo height tomo el height de la imagen
						//en caso d que contenga texto alternativo
						//para el texto alternativo enc aso de que siexista
						if(recursivo.contains("'alt',")) {
							alt=recursivo.substring(recursivo.indexOf("'alt',")+5).trim();
							alt=alt.substring(alt.indexOf(",")+1).trim();
							alt=alt.substring(alt.indexOf("'"),alt.indexOf(")")-1);
							
						}
						String alternativo=alt;
						ValidaTextoImagenTextoAlternativo vtita=new ValidaTextoImagenTextoAlternativo();
						boolean []res=vtita.compruebaTextoImagenTextoAlternativo(alto, ancho, dat[0], alternativo);
						if(res[0])
							numeroImagenesBajaResolucion++;
						if(res[1])
							numeroImagenesTextoAlernativo++;
						if(res[2])
							numeroImagenesPocoTextoAlternativo++;
						esImagen=true;
						i=imF.size();//si el enlace contiene una imagen hago que salga del for (vale tambien el comtinue)
					}else {
						System.out.println("Imagen con atributo src vacio......");
					}
				}
			}
			
			if(!esImagen) {//si o es imagen, compruebo que si es o no es video
				VideoFormats vid= new VideoFormats();
				imF.clear();//borro los formatos de imagenes que contiene le listado
				imF=vid.returnVideoFormats();//agrego el listado de formatos de video
				//valido si es online u offline
				SubtitlesProcess sp=new SubtitlesProcess();
				if(sp.esWeb(src)) {//en caso de que sea una ruta web, debo analizar si es o no es video
					//debo analizar si el url encontrado es de video o no
					ValidaVideo vv=new ValidaVideo();
					if(vv.esVideo(src)) {//en caso de que si sea video
						numeroVideos++;
						boolean []contiene=sp.contieneSubtitulos(src, true, null);
						if(contiene[0])
							numeroVideosSubtitulos++;
						else if(contiene[1])
							numeroVideosCaptions++;
					}
				}else {//si no es web
					if(!src.equals("")) {
						boolean []contiene=new boolean[2];
						for(int i=0;i<imF.size();i++) {
							if(src.toUpperCase().contains(imF.get(i))) {//en caso de que contenga el formato de video
								///AKI LLAMO A LA CALSE SIBTITLE MAIN
								numeroVideos++;
								contiene=sp.contieneSubtitulos(src, false, archivo.getAbsolutePath());
								if(contiene[0])
									numeroVideosSubtitulos++;
								else if(contiene[1])
									numeroVideosCaptions++;
								esVideo=true;
								i=imF.size();
							}
						}
					}
				}
			}
			if(!esImagen && !esVideo) {
				//AKI ANALIZO COMO SI FUESE UNA PAGINA, SEA WEB O SEA OFFLINE
			}
		}else {//aki debo poner un if para los otros tipos de video (data, etc
			
		}
		//System.out.println("Este es el recursivo: "+recursivo);
		if(recursivo.contains("\"src\",")||recursivo.contains("'src',")||recursivo.contains("‘src’,")||recursivo.contains("“src”,")) {
			extraeDeJQuery(recursivo, archivo);
		}
		//System.out.println("este es el src: "+src);
	}
	
	/**
	 * Metodo que analiza si el texto alternativo esta en blanco, si existe texto en la imagen y comprueba la resolucion
	 * de la imagen. Asi como tambien si el texto alternativo como minimo es igual en caracteres al texto de la imagen.
	 * @param alto Es la altura de la imagen.
	 * @param ancho Es el ancho o largo de la imagen.
	 * @param texto Es el texto encontrado en la imagen.
	 * @param textoAlternativo Es el texto alternativo que tenga la imagen.
	 */
	public void compruebaTextoImagenTextoAlternativo(int alto, int ancho, String texto, String textoAlternativo) {
		if(textoAlternativo.equals("")) {
			if(!texto.equals("")) {
				//llamo a la calse de las formulas
				recomender.calculations.Formulas form=new recomender.calculations.Formulas();
				//si es falso, no puede caber el texto en la imagen
				if(!form.formulaCalculoCaracteresPorPixeles(ancho, alto, texto)) {
					System.out.println("El texto de la imagen es dificil de leer a esalas mayores");
					numeroImagenesBajaResolucion++;
				}
			}
			System.out.println("La imagen no contiene texto alternativo");	
		}else {
			numeroImagenesTextoAlernativo++;
			if(!texto.equals("")) {//si el texto en la imagen es diferente de vacio
				//llamo a la calse de las formulas
				recomender.calculations.Formulas form=new recomender.calculations.Formulas();
				if(!form.formulaCalculoCaracteresPorPixeles(ancho, alto, texto)) {//si retorna falso la formula significa que los caracteres no seran visibles al hacer zoom
					//numImagenesConTextoBajaResolucion++;//aumento el numero de imagenes con demasiado texto
					System.out.println("El texto de la imagen es dificil de leer a esalas mayores");
					numeroImagenesBajaResolucion++;
				}
				if(texto.length()>textoAlternativo.length()) { //si el texto de la imagen es mayor al texto alternativo significa que el texto alternativo no esta explicando mucho lo que describe la imagen
					System.out.println("El texto en la imagen es mayor al texto alternativo, como minimo deberian concordar en caracteres");
					numeroImagenesPocoTextoAlternativo++;
				}
			}
		}
	}
	/**
	 * Metodo que agrega en un array el numero total de imagenes, el numero de imagenes con baja resolucion,
	 * el numero de imagenes con texto alternativo y el numero de imagenes con texto alterntativo menor al de la imagen.
	 * @return Retorna el listado con el num de imagenes, num de imagenes con texto alternativo y num de imagenes con texto.
	 */
	public java.util.ArrayList<Integer> totales(){
		java.util.ArrayList<Integer> listado=new java.util.ArrayList<Integer>();
		listado.add(numeroImagenes);//primero el numero de imagenes
		listado.add(numeroImagenesBajaResolucion);//numero de imagenes con baja resolucion
		listado.add(numeroImagenesTextoAlernativo);//numero de imagenes con texto alternativo
		listado.add(numeroImagenesPocoTextoAlternativo);//numero de imagenes con texto alternativo menor al texto de la imagen.
		return listado;
	}
	public int[] retornaVideos() {
		return new int[] {numeroVideos,numeroVideosSubtitulos,numeroVideosCaptions};
	}
}
