package recomender.analysis.javascript;

import java.awt.image.BufferedImage;
import java.net.URL;

import javax.imageio.ImageIO;

import recomender.analysis.AnalizaImagen;
import recomender.analysis.SubtitlesProcess;
import recomender.calculations.Formulas;
import recomender.formats.ImageFormats;
import recomender.formats.VideoFormats;
import recomender.validations.TrimAvanzado;
import recomender.validations.ValidaConexion;
import recomender.validations.ValidaTextoImagenTextoAlternativo;
import recomender.validations.ValidaVideo;

public class AnalizaSoloScript {
	int numeroImagenes=0;
	int numeroImagenesTextoAlernativo=0;
	int numeroImagenesBajaResolucion=0;
	int numeroImagenesPocoTextoAlternativo=0;
	
	//videos
	int numeroVideos=0;
	int numeroVideosSubtitulos=0;
	int numeroVideosCaptions=0;
	/**
	 * Metodo recursivo que analiza una cadena que esta en formato javascript.
	 * Este metodo analizara todos los .src= que existan y comprobara si son imagenes o  videos para su posterior analisis
	 * @param data Es la cadena en formato javascript a analizar.
	 */
	public void setSrcJavascript(String data, java.io.File archivo) {
		TrimAvanzado ta=new TrimAvanzado();
		data=data.replaceAll("\n", "");
		String dataAux=data.trim();
		String texto="";//texot para recuperar el texto que se encuentra en la imagen
		int ancho = 0;//ancho de la imagen
		int alto = 0;//alto de la imagen
		
		boolean esImagen=false;
		boolean esVideo=false;
		if(dataAux.replaceAll(" ", "").contains(".src=")) {
			System.out.println("source1: "+dataAux.trim());
			System.out.println("source2: "+ta.trimCompleto(dataAux));
			String aux=ta.trimCompleto(dataAux);
			aux=aux.substring(aux.indexOf(".src="));
			System.out.println("Este es el aux: "+aux);
			String src=aux.trim().substring(aux.trim().indexOf("=")+2);//+2 por el igual y la scomillas q se omitan
			String auxrecursivo=aux.trim().substring(aux.trim().indexOf("=")+1);//+1 por el igual pa q no se omitan las comillas y de error al momento de hacer el recursivo (si se omite esta comilla la comilla de cierre va a detectar como si fuese de apertura;
			src=src.trim().substring(0, src.trim().indexOf(";")-1);//cuando es hasta el punto y coma no va  aparecer el ; y si le resto uno no va a aparecer las comila ssimples odobles
			System.out.println("Src: "+src);
			
			//recorro los formatos d eimagen y compruebo que la ruta contenga un formato de imagen
			ImageFormats f=new ImageFormats();
			java.util.ArrayList<String> formatos=f.retornaFormatos();
			for(String format:formatos) {
				if(src.toUpperCase().contains(format)) {//en caso de q sea una imagen
					esImagen=true;
					AnalizaImagen ai=new AnalizaImagen();
					String dat[]=ai.textoImagen(src, archivo);
					if(dat!=null) {
						numeroImagenes++;
						//para el recursivo y texto alternativo de la img
						if(auxrecursivo.trim().contains(".src=")) {
							String srcAlt=auxrecursivo.trim().substring(0, auxrecursivo.trim().indexOf(".src="));
							if(srcAlt.trim().contains(".alt=")) {//si tiene texto alternativo esa imagen
								srcAlt=srcAlt.trim().substring(srcAlt.trim().indexOf(".alt="));
								srcAlt=srcAlt.trim().substring(srcAlt.trim().indexOf("=")+2);
								srcAlt=srcAlt.trim().substring(0, srcAlt.indexOf(";")-1);
								System.out.println("Texto Alternativo: "+srcAlt);
								//compruebo con la formula
								ValidaTextoImagenTextoAlternativo vtita=new ValidaTextoImagenTextoAlternativo();
								boolean []res=vtita.compruebaTextoImagenTextoAlternativo(Integer.parseInt(dat[1]), Integer.parseInt(dat[2]), dat[0], srcAlt);
								if(res[0])
									numeroImagenesBajaResolucion++;
								if(res[1])
									numeroImagenesTextoAlernativo++;
								if(res[2])
									numeroImagenesPocoTextoAlternativo++;
								//compruebaTextoImagenTextoAlternativo(alto, ancho, texto, srcAlt);
							}else {
								System.out.println("La imagen no tiene texto alternativo");
							}
							setSrcJavascript(auxrecursivo, archivo);
						}else if(auxrecursivo.trim().contains(".alt=")) {//texto alternativo ultimo o unico
							String srcAlt=auxrecursivo.trim().substring(auxrecursivo.trim().indexOf(".alt="));
							srcAlt=srcAlt.trim().substring(srcAlt.trim().indexOf("=")+2);
							srcAlt=srcAlt.trim().substring(0, srcAlt.indexOf(";")-1);
							System.out.println("Texto Alternativo ultimo: "+srcAlt);
							//compruebo con la formula
							//compruebo con la formula
							ValidaTextoImagenTextoAlternativo vtita=new ValidaTextoImagenTextoAlternativo();
							boolean []res=vtita.compruebaTextoImagenTextoAlternativo(Integer.parseInt(dat[1]), Integer.parseInt(dat[2]), dat[0], srcAlt);
							if(res[0])
								numeroImagenesBajaResolucion++;
							if(res[1])
								numeroImagenesTextoAlernativo++;
							if(res[2])
								numeroImagenesPocoTextoAlternativo++;
							//compruebaTextoImagenTextoAlternativo(alto, ancho, texto, srcAlt);
						}else {
							System.out.println("La imagen no tiene texto alternativo en 2");
						}
					}
					///////IMPORTANTEEEEEE
					//SI ESQUE ES ONLINE, PARA VIDEO LE MANDO A EJECUTAR EL COMANDO YOUTUBE-DL --LIST-SUBS URL
					//DEBO RECOGER TANTO EL ERROR STREAM COMO EL INPUTSTREAM, CUANDO ES ERROR DEBE SALIR UNSUPPORTED URL ASI QUE DEBERIA ANALIZAR COMO PAGINA NO COMO VIDEO
				}
			}
			//PARA EL VIDEO
			if(!esImagen) {//en caso de que el src no sea una imagen
				VideoFormats vid= new VideoFormats();
				formatos.clear();//borro los formatos de imagenes que contiene le listado
				formatos=vid.returnVideoFormats();//agrego el listado de formatos de video
				//valido si es online u offline
				SubtitlesProcess sp=new SubtitlesProcess();
				if(sp.esWeb(src)) {//en caso de que sea una ruta web, debo analizar si es o no es video
					//debo analizar si el url encontrado es de video o no
					ValidaVideo vv=new ValidaVideo();
					if(vv.esVideo(src)) {//en caso de que si sea video
						numeroVideos++;
						boolean []contiene=sp.contieneSubtitulos(src, true, null);
						if(contiene[0])
							numeroVideosSubtitulos++;
						else if(contiene[1])
							numeroVideosCaptions++;
					}
				}else {//si no es web
					if(!src.equals("")) {
						boolean []contiene=new boolean[2];
						for(int i=0;i<formatos.size();i++) {
							if(src.toUpperCase().contains(formatos.get(i))) {//en caso de que contenga el formato de video
								///AKI LLAMO A LA CALSE SIBTITLE MAIN
								numeroVideos++;
								contiene=sp.contieneSubtitulos(src, false, archivo.getAbsolutePath());
								if(contiene[0])
									numeroVideosSubtitulos++;
								else if(contiene[1])
									numeroVideosCaptions++;
								esVideo=true;
								i=formatos.size();
							}
						}
					}
				}
			}
			if(!esImagen && !esVideo) {//si no es ni imagen ni video
				
			}			
		}
	}
	/**
	 * Metodo que analiza si el texto alternativo esta en blanco, si existe texto en la imagen y comprueba la resolucion
	 * de la imagen. Asi como tambien si el texto alternativo como minimo es igual en caracteres al texto de la imagen.
	 * @param alto Es la altura de la imagen.
	 * @param ancho Es el ancho o largo de la imagen.
	 * @param texto Es el texto encontrado en la imagen.
	 * @param textoAlternativo Es el texto alternativo que tenga la imagen.
	 */
	public void compruebaTextoImagenTextoAlternativo(int alto, int ancho, String texto, String textoAlternativo) {
		if(textoAlternativo.equals("")) {
			if(!texto.equals("")) {
				//llamo a la calse de las formulas
				Formulas form=new Formulas();
				//si es falso, no puede caber el texto en la imagen
				if(!form.formulaCalculoCaracteresPorPixeles(ancho, alto, texto)) {
					System.out.println("El texto de la imagen es dificil de leer a esalas mayores");
					numeroImagenesBajaResolucion++;
				}
			}
			System.out.println("La imagen no contiene texto alternativo");	
		}else {
			numeroImagenesTextoAlernativo++;
			if(!texto.equals("")) {//si el texto en la imagen es diferente de vacio
				//llamo a la calse de las formulas
				Formulas form=new Formulas();
				if(!form.formulaCalculoCaracteresPorPixeles(ancho, alto, texto)) {//si retorna falso la formula significa que los caracteres no seran visibles al hacer zoom
					//numImagenesConTextoBajaResolucion++;//aumento el numero de imagenes con demasiado texto
					System.out.println("El texto de la imagen es dificil de leer a esalas mayores");
					numeroImagenesBajaResolucion++;
				}
				if(texto.length()>textoAlternativo.length()) { //si el texto de la imagen es mayor al texto alternativo significa que el texto alternativo no esta explicando mucho lo que describe la imagen
					System.out.println("El texto en la imagen es mayor al texto alternativo, como minimo deberian concordar en caracteres");
					numeroImagenesPocoTextoAlternativo++;
				}
			}
		}
	}
	/**
	 * Metodo que agrega en un array el numero total de imagenes, el numero de imagenes con baja resolucion,
	 * el numero de imagenes con texto alternativo y el numero de imagenes con texto alterntativo menor al de la imagen.
	 * @return Retorna el listado con el num de imagenes, num de imagenes con texto alternativo y num de imagenes con texto.
	 */
	public java.util.ArrayList<Integer> totales(){
		java.util.ArrayList<Integer> listado=new java.util.ArrayList<Integer>();
		listado.add(numeroImagenes);//primero el numero de imagenes
		listado.add(numeroImagenesBajaResolucion);//numero de imagenes con baja resolucion
		listado.add(numeroImagenesTextoAlernativo);//numero de imagenes con texto alternativo
		listado.add(numeroImagenesPocoTextoAlternativo);//numero de imagenes con texto alternativo menor al texto de la imagen.
		return listado;
	}
	
	public int[] retornaVideos() {
		return new int[] {numeroVideos,numeroVideosSubtitulos,numeroVideosCaptions};
	}
}
