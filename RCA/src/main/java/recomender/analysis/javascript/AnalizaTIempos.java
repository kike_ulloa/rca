package recomender.analysis.javascript;

import recomender.validations.TrimAvanzado;

public class AnalizaTIempos {
	/**
	 * Analiza si contiene o no referencias a tiempos el codigojavascript
	 * @param data Es el codigo javascript a analizar
	 * @return Retorna si contiene o no tiempos.
	 */
	public boolean contineTiempos(String data) {
		TrimAvanzado ta=new TrimAvanzado();
		data =  ta.trimCompleto(data);
		if(data.contains("setInterval(")||data.contains("setTimeout(")||data.contains("requestAnimationFrame("))
			return true;
		return false;
	}
	/**
	 * Analiza si contiene o no contiene canvas el en el codigo jabvascirpt
	 * @param data Es el codigo javascript a anlizar
	 * @return Retorna si contiene o no canvas.
	 */
	public boolean contieneCanvas(String data) {
		TrimAvanzado ta=new TrimAvanzado();
		data=ta.trimCompleto(data);
		if(data.contains("<canvas"))
			return true;
		return false;
	}
}
