package recomender.analysis.javascript;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;

import recomender.validations.Offline;
import recomender.analysis.html.PrincipalHTML;

public class CodigoJavaScript {

	/**
	 * permite obtener el codigo de archivos js
	 * @param archivoJS.- archivo js
	 * @return
	 */
	public ArrayList<String> obtenerDeJS(String archivoJS) {
		File f =new File(archivoJS);
		BufferedReader br;
		ArrayList<String> contenidoJS=new ArrayList<String>();
		try {
			if(f.length()<8192) {
			Offline of=new Offline();
			if(of.existeOffline(archivoJS)) {
			br = new BufferedReader(new FileReader(archivoJS));
			String linea=br.readLine();//Leemos la primera linea del texto.
			while(linea!=null) {//Recorreremos todas las lineas obtenidas.
				contenidoJS.add(linea);
				linea=br.readLine();
			}
			}
			}
		} catch(Exception e){
            e.printStackTrace();
		}
		return contenidoJS;
	}
	/**
	 * permite obtener las eqitueas script
	 * @param archivoHTML.- el archivo html del que se analiza.
	 * @return
	 */
	public ArrayList<String> obtenerDeEtiquetaScript(String archivoHTML) {	
		PrincipalHTML pHtml=new PrincipalHTML();
		ArrayList<String> codigoJs=new ArrayList<String>();
		ArrayList<String> resultadoBusqueda=pHtml.buscar(new ArrayList<String>(),true,archivoHTML, "script", null, null, null, null, false);
		for (String resultados :resultadoBusqueda ) {
			resultados=resultados.substring((resultados.indexOf(">")+1),resultados.lastIndexOf("</"));
			if(resultados.length()>1)
				codigoJs.add(resultados);
		}
		return codigoJs;
	}
	/**
	 * permite obtener el codigo js de las etiquetas scripts
	 * @param archivoHTML.-  archivo html
	 * @return
	 */
	public ArrayList<String> obtenerJSDeEtiquetaScript(String archivoHTML) {	
		PrincipalHTML pHtml=new PrincipalHTML();
		 return pHtml.buscar(new ArrayList<String>(),true,archivoHTML, "script", "src", null, "desconocido", null, false);	
	}
	/**
	 * une el codigo de las etiqutas script y arhcivos js
	 * @param archivoHTML.- archivo html en el que se analiza
	 * @return
	 */
	public String obtenerJSTotal(String archivoHTML){
		String codigoJSTotal="";
		ArrayList<String>resultadoDeEtiqueta=obtenerDeEtiquetaScript(archivoHTML);
		for (String codigoJSEtiqueta : resultadoDeEtiqueta) {
			codigoJSTotal=codigoJSTotal+"\n"+codigoJSEtiqueta;
		}
		ArrayList<String>resultadoJSDeEtiqueta=obtenerJSDeEtiquetaScript(archivoHTML);
		for (String js : resultadoJSDeEtiqueta) {
			ArrayList<String>resultadoDeJS=obtenerDeJS(js);
			for (String  codigoJSArchivo : resultadoDeJS) {
				codigoJSTotal=codigoJSTotal+"\n"+codigoJSArchivo;
			}
		}
		codigoJSTotal=eliminarComentariosLineas(codigoJSTotal);
		codigoJSTotal=codigoJSTotal.replaceAll("var ", "");
		codigoJSTotal=codigoJSTotal.replaceAll("\n", ";");
		codigoJSTotal=codigoJSTotal.replaceAll("\t","");
		codigoJSTotal=codigoJSTotal.replaceAll(" ","");
		codigoJSTotal=eliminarDuplicados(codigoJSTotal);
		codigoJSTotal=reemplazarComillas(codigoJSTotal);
		return codigoJSTotal;
	}
	/**
	 * permite elminiar posible codigo documentado de js
	 * @param codigoJS.- codigo js
	 * @return
	 */
	public String eliminarDocumentacion(String codigoJS){
			while(codigoJS.contains("/*")) {
				String aux1=codigoJS.substring(0, codigoJS.indexOf("/*"));
				String aux2="";
				if(codigoJS.contains("*/"))
					 aux2=codigoJS.substring(codigoJS.indexOf("*/")+2);
				codigoJS=aux1+aux2;	
			}
		return codigoJS;
	}
	/**
	 * elimina lineas comentadas
	 * @param codigoJS.- codigo js
	 * @return
	 */
	public String eliminarComentariosLineas(String codigoJS){
		codigoJS=eliminarDocumentacion(codigoJS);
		String lineas[]=codigoJS.split("\n");
		codigoJS="";
		for (String linea : lineas) {
			if(linea.contains("//")) {
				int posicionDobleSlash=linea.indexOf("//");
				int posicionComillaDobleInicio=linea.indexOf("\"");
				int posicionComillaDobleFinal=linea.lastIndexOf("\"");
				int posicionComillaSimpleInicio=linea.indexOf("\'");
				int posicionComillaSimpleFinal=linea.lastIndexOf("\'");
				if(!(posicionComillaDobleInicio<posicionDobleSlash&&posicionComillaDobleFinal>posicionDobleSlash)&&!(posicionComillaSimpleInicio<posicionDobleSlash&&posicionComillaSimpleFinal>posicionDobleSlash))
					linea=linea.substring(0, posicionDobleSlash);
			}
			codigoJS=codigoJS+"\n"+linea;
		}
		
		return codigoJS;
	}
	/**
	 * elimina duplicados
	 * @param codigoJS.- el codigo js
	 * @return
	 */
	public String eliminarDuplicados(String codigoJS)
	{
		while(codigoJS.indexOf(";;")>-1) {
			String aux1=codigoJS.substring(0,codigoJS.indexOf(";;"));
			String aux2=codigoJS.substring(codigoJS.indexOf(";;")+1);
			codigoJS=aux1+aux2;
		}
			return codigoJS;
	}
	/**
	 * reemplaza las posibles formas de comillas por la normla
	 * @param codigoJS.- codigo js
	 * @return
	 */
	public String reemplazarComillas(String codigoJS) {
		codigoJS=codigoJS.replaceAll("‘", "\"");
		codigoJS=codigoJS.replaceAll("“", "\"");
		codigoJS=codigoJS.replaceAll("'", "\"");
		return codigoJS;	
	}
}

