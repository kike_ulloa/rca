package recomender.analysis.javascript;
import java.util.ArrayList;
import recomender.validations.Offline;
import recomender.validations.Online;
import recomender.validations.ValidacionesBasicas;
import recomender.validations.ValidacionesMultimedia;

public class PrincipalJavaScript {
	ValidacionesBasicas vb=null;
	/**
	 * Este meotdo permite buscar lo analizado en codigo JS
	 * @param codigoJS.- el codigo JS al que se va a analizar.
	 * @param encontrados.- el arreglo en el que se va a agregar los datos encontrados en base a la busqueda
	 * @param archivo.- archivo html a anlizar.
	 * @param atributosValores.- los atributos que se pretende buscar
	 * @param condicion.- condicion para indicar si tiene mas de un atributo si debe cumplir con todos o con uno si y otro no.
	 * @param tipoArchivo.- indica el tipo de contenido
	 * @param contenidoOtroArchivo.- indica si se analiza un tipo de archivo que pertenece a otro tioo ejemplo audio en video.
	 * @param buscaTexto.- indica si se busca texto y no contenido multimedia.
	 * @return
	 */
	public ArrayList<String> buscar(String codigoJS,ArrayList<String> encontrados,String archivo,String atributosValores,String condicion,String tipoArchivo,String contenidoOtroArchivo,boolean buscaTexto) {
		vb=new ValidacionesBasicas();
		if(archivo!=null&archivo.length()>2) {
			String parametroPrincipal=obtenerParametroPrincipal(atributosValores);
			if(tipoArchivo!=null) { 
				int forma=1;
				int posInicialPrimerParametro=obtenerPosicionPrimerParametro(codigoJS, parametroPrincipal, forma);
				while(forma<=2) {
					while(posInicialPrimerParametro>-1) {
						String variableValor=extraerVariableValor(codigoJS, posInicialPrimerParametro,forma);
						String variable=variableValor.split("\n")[0];
						String valor=variableValor.split("\n")[1];
						int recorreCodigo=codigoJS.indexOf(variable+"=");
						if(recorreCodigo>-1) {
							String usoVariable=codigoJS.substring(recorreCodigo);
							usoVariable=usoVariable.substring(0,usoVariable.indexOf(";"));
							codigoJS=nuevoCodigoJS(codigoJS, recorreCodigo, (recorreCodigo+((usoVariable.length())+1)));
							String codigoYUsoVariable=extraerContenidoVariable(codigoJS, variable, usoVariable);
							codigoJS=codigoYUsoVariable.split("\n")[0];
							usoVariable=codigoYUsoVariable.split("\n")[1];
							if(valor.length()>2) {
								if(vb.cumpleCondicionJavaScript(usoVariable, condicion, atributosValores, variable)) {
									usoVariable="";
									if(tipoArchivo.equals("desconocido")&&valor.charAt(0)=='#')
										encontrados.add(valor);
									else {
										encontrados=vb.existeArchivo(archivo, valor, tipoArchivo, encontrados, contenidoOtroArchivo);
									}
								}
							}
							posInicialPrimerParametro=obtenerPosicionPrimerParametro(codigoJS, parametroPrincipal, forma);
						}
						else {
							posInicialPrimerParametro=-1;
						}
					}
					forma++;
					posInicialPrimerParametro=obtenerPosicionPrimerParametro(codigoJS, parametroPrincipal, forma);
				}
			}else {
				if(codigoJS.contains("."+parametroPrincipal.split("=")[0]+"="))
					encontrados.add(extraerVariableValor(codigoJS, codigoJS.indexOf("."+parametroPrincipal.split("=")[0]+"="),1).split("\n")[1]);/////Esta seccion es para elementos que no son archivos ni texto.
				else if(codigoJS.contains("'"+parametroPrincipal.split("=")[0]+"'"))
					encontrados.add(extraerVariableValor(codigoJS, codigoJS.indexOf("\""+parametroPrincipal.split("=")[0]+"\""),2).split("\n")[1]);/////Esta seccion es para elementos que no son archivos ni texto.		
			}
		}
		return encontrados;	
	}

/**
 * Permite encontrar paginas html embebidas en las paginas html analizadas
 * @param archivo.- el archivo a analizar
 * @param htmlEncontrados.-  el arreglo en el que se agrega lo encontrado
 * @param codigoJS.- codigoJS en el que se desea buscar.
 * @return
 */
	public ArrayList<String> paginasHTMLIncrustadas(String archivo,ArrayList<String>htmlEncontrados,String codigoJS) {
		Online on=new Online();
		Offline of=new Offline();
		ValidacionesMultimedia vm=new ValidacionesMultimedia();
		String []atributos= {"src","data"};
		for (int i = 0; i < atributos.length; i++) {
			String parametroPrincipal=obtenerParametroPrincipal(atributos[i]);
				int forma=1;
				int posInicialPrimerParametro=obtenerPosicionPrimerParametro(codigoJS, parametroPrincipal, forma);
				while(forma<=2) {
					while(posInicialPrimerParametro>-1) {	
						String variableValor=extraerVariableValor(codigoJS, posInicialPrimerParametro,forma);
						String variable=variableValor.split("\n")[0];
						String valor=variableValor.split("\n")[1];
						int recorreCodigo=codigoJS.indexOf(variable+"=");
						if(recorreCodigo>-1) {
							String usoVariable=codigoJS.substring(recorreCodigo);
							usoVariable=usoVariable.substring(0,usoVariable.indexOf(";"));
							codigoJS=nuevoCodigoJS(codigoJS, recorreCodigo, (recorreCodigo+((usoVariable.length())+1)));
							String codigoYUsoVariable=extraerContenidoVariable(codigoJS, variable, usoVariable);
							codigoJS=codigoYUsoVariable.split("\n")[0];
							if(valor!=null&valor.length()>2) { //Se analiza que el path no sea nulo y ni vacio o un #.
								if(valor.indexOf("//")>-1||valor.indexOf("www.")>-1) {//Se analiza si el path es online u offline.
									if(on.existeOnline(on.conexionURL(valor))) {//Se analiza que exista el link.
										if(!vm.tieneFormato("audio", valor)&&!vm.tieneFormato("imagen", valor)&&!vm.esVideo(valor)&&!valor.contains(".css")&&!valor.contains(".js")) { // Se comprueba que el link no es un video,imagen o audio.
													htmlEncontrados.add(valor);
												paginasHTMLIncrustadas(valor, htmlEncontrados,codigoJS);
										}
									}
								}else {
											valor=archivo.substring(0,archivo.lastIndexOf("/")+1)+valor;
											if(of.existeOffline(valor)) {//se va a aplicar los mismos criterios que arriba con diferencia, que aqui se analizara de manera local.
												if(!vm.tieneFormato("audio", valor)&&!vm.tieneFormato("imagen", valor)&&!vm.tieneFormato("video", valor)&&!valor.contains(".css")&&!valor.contains(".js")) {
													htmlEncontrados.add(valor);
												paginasHTMLIncrustadas(valor, htmlEncontrados,codigoJS);
												}
											}
										}	
									}
								posInicialPrimerParametro=obtenerPosicionPrimerParametro(codigoJS, parametroPrincipal, forma);
							}
					}
					forma++;
					posInicialPrimerParametro=obtenerPosicionPrimerParametro(codigoJS, parametroPrincipal, forma);
				}

		}
		return htmlEncontrados;// Retornaremos el numero de contenido encontrado acumulado en todas las paginas embebidas, en base al contenido que esta buscando.
	}
/**
 * Permite obtener el parametro principal de un conjunto de parametros
 * @param atributosValores.- conjunto de parametros
 * @return
 */
	public String  obtenerParametroPrincipal(String atributosValores) {
		String parametroPrincipal=null;
		if(atributosValores!=null)
			parametroPrincipal=atributosValores.split(";")[0];
		return parametroPrincipal;
		}
	/**
	 * obtiene la posicion del primer parametro
	 * @param codigoJS.- codigo js en el que se va a buscar.
	 * @param parametroPrincipal.- el paramtro inicial
	 * @param forma.- indica la forma de buscar en js siendo esta 2 formas.
	 * @return
	 */
	public int obtenerPosicionPrimerParametro(String codigoJS,String parametroPrincipal,int forma) {
		String primerParametro="";
		int posInicialPrimerParametro=-1; 
			if(forma==1) {
				primerParametro="."+parametroPrincipal.split("=")[0]+"=";
				posInicialPrimerParametro=codigoJS.indexOf(primerParametro);
			}
			else {
				primerParametro="\""+parametroPrincipal.split("=")[0]+"\"";
				posInicialPrimerParametro=codigoJS.indexOf(primerParametro);
			}
		return posInicialPrimerParametro;
	}
	/**
	 * obtiene el contenido de la variable que se busca
	 * @param codigoJS.- el codigo js del que se busca
	 * @param variable.- la variable que se indica
	 * @param usoVariable.- indica el uso de variable
	 * @return
	 */
	public String extraerContenidoVariable(String codigoJS,String variable,String usoVariable){
		usoVariable=usoVariable+";";
		while(codigoJS.indexOf(variable+".")>0) {
			int inicioVariable=codigoJS.indexOf(variable);
			String auxVariable=codigoJS.substring(inicioVariable);
			int puntoYComa=auxVariable.indexOf(";");
			if(auxVariable.substring(0,puntoYComa).contains(variable+"="))
				break;
			usoVariable=usoVariable+(auxVariable.substring(0,puntoYComa+1));
			codigoJS=nuevoCodigoJS(codigoJS, inicioVariable, inicioVariable+puntoYComa+1);
	}
		return codigoJS+"\n"+usoVariable;
	}
	/**
	 * permite extraer el valor presente en una variable
	 * @param codigoJS.- codigo js en el que se busca
	 * @param posInicialPrimerParametro.- posicion del parametro inicial
	 * @param forma.- la forma de busqueda.
	 * @return
	 */
	public String extraerVariableValor(String codigoJS,int posInicialPrimerParametro,int forma) {
		String variable="";
		String valor="";
		if(forma==1) {//la forma 1 es variable.atributo, la forma dos es variable.setAttribute o attr ("atributo", valor)
			variable=codigoJS.substring(0,posInicialPrimerParametro);
			variable=variable.substring(variable.lastIndexOf(";")+1);
			valor=codigoJS.substring(posInicialPrimerParametro);
			valor=valor.substring(valor.indexOf("=")+1,valor.indexOf(";"));
			valor=valor.replace("\"", "");
		}else {
			variable=codigoJS.substring(0,posInicialPrimerParametro);
			variable=variable.substring(variable.lastIndexOf(";")+1,variable.lastIndexOf("."));
			valor=codigoJS.substring(posInicialPrimerParametro);
			valor=valor.substring(valor.indexOf(",")+1);
			valor=valor.substring(0,valor.indexOf(")"));
			valor=valor.replace("\"", "");
		}
		return variable+"\n"+valor;
	}
	/**
	 * crea un nuevo codigo JS  en base a dos posiciones
	 * @param codigoJS.- codigo js anterior 
	 * @param pos1.- posicion 1
	 * @param pos2.- posicion 2
	 * @return
	 */
	public String nuevoCodigoJS(String codigoJS,int pos1,int pos2) {
		String aux1=codigoJS.substring(0,pos1);
		String aux2=codigoJS.substring(pos2);
		return aux1+aux2;
	}
	
	}

