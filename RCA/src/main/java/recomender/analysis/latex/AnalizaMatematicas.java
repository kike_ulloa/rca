package recomender.analysis.latex;

import recomender.staticcontent.VariablesEstaticas;

public class AnalizaMatematicas {

	/**
	 * Analiza contenido en busca de coincidencias con contenido matematico
	 * @param archivo Es el aerchivo a analizar
	 * @return Retorna un string con las librerias o codigos que usa.
	 */
	public Object [] analizaMathML(java.io.File archivo) {
		String usa="Para las formulas matematicas esta usando";
		LatexHTMLPage lhp=new LatexHTMLPage();
		int math=lhp.contieneLatex(archivo, "math");
		math+=lhp.contieneLatex(archivo, "x-text");
		int ascii=lhp.contieneLatex(archivo, "ascii");
		int latexmath=lhp.contieneLatex(archivo, "latexmathml");
		int jqmath=lhp.contieneLatex(archivo, "jqmath");
		int mathjax=lhp.contieneLatex(archivo, "mathjax");
		
		if(math>0) {
			usa+=" math o x-text";
		}
		if(ascii>-1)
			usa+=" ascii";
		if(latexmath>-1)
			usa+=" latexmathml";
		if(jqmath>-1)
			usa+=" jqmath";
		if(mathjax>-1)
			usa+=" mathjax";
		if(usa.equals("Para las formulas matematicas esta usando")) {
			usa="No esta usando ninguna libreria o eqtiqueta para implementar "
					+ "formulas matematicas, en caso de que existan estas formulas, WCAG recomienda usar MATHML.";
			return new Object[] {usa,false};
		}else {
			usa+=".";
			VariablesEstaticas.numFormulasMatematicas=VariablesEstaticas.numFormulasMatematicas+1;
			return new Object[] {usa,true};
		}
	}
	/**
	 * Analiza contenido en busca de coincidencias con formulas quimicas.
	 * @param archivo ES el archivo a analizarse
	 * @return Retorna si se usa o no chemml para las formulas.
	 */
	public Object[] analizaChemML(java.io.File archivo) {
		LatexHTMLPage lhp=new LatexHTMLPage();
		int chem=lhp.contieneLatex(archivo, "chem");
		if(chem>0) {
			VariablesEstaticas.numChem=VariablesEstaticas.numChem+chem;
			return new Object[] {"Para las formulas quimicas esta usando chemML.",true};
		}else {
			return new Object[]{"No esta usando ninguna libreria o eqtiqueta para implementar "
					+ "formulas quimicas, en caso de que existan estas formulas, WCAG recomienda usar MATHML.",false};
		}
	}
	/**
	 * Analiza el contenido en busca de coincidencia scon latex
	 * @param archivo Es el Archivo a analizarse
	 * @return Retorna si usa latex o no.
	 */
	public Object[] analizaLatex(java.io.File archivo) {
		Object [] math=analizaMathML(archivo);
		LatexHTMLPage lhp=new LatexHTMLPage();
		int l1=lhp.contieneLatex(archivo, "codecogs");
		int l2=lhp.contieneLatex(archivo, "codecogs.image");
		if((boolean)math[1]) {
			String resp=math[0].toString().replace("Para las formulas matematicas esta usando", "Para realizar contenido con latex se esta usando");
			if(l1>-1||l2>-1)
				resp+=" codecogs";
			VariablesEstaticas.numlatex=VariablesEstaticas.numlatex+1;
			return new Object[] {resp,true};
		}else if(l1>-1||l2>-1){
			return new Object[] {"Para realizar contenido con latex se esta usando codecogs",true};
		}else {
			return new Object[] {"No se ha detectado ninguna liberia o etiqueta para contenido en latex. Se reomienda implementar contenido en latex en caso de ser necesario",false};
		}
	}
}
