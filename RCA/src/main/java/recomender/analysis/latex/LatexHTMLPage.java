package recomender.analysis.latex;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class LatexHTMLPage {
	/**
	 * Metodo que analiza contenido matematico latex, etc 
	 * @param htmlFile Es el archivo a analizar
	 * @param llave Es la llave a buscar
	 * @return Retorna los resultados del analisis.
	 */
	public int contieneLatex(java.io.File htmlFile, String llave) {
		try {
			Document doc=Jsoup.parse(htmlFile, "UTF-8");
			
			if(llave.equals("math")||llave.equals("x-text")) {//MATHML O X-TEXT(PERTENECE A MATHML)
				Elements elements=doc.select(llave);
				if (elements!=null&&elements.size()>0) {
					return elements.size();
				}
			}else if(llave.equals("ascii")) {//ASCIIMATHML
				Elements scripts=doc.select("script");
				if(scripts!=null&&scripts.size()>0) {
					for(Element e:scripts) {
						if(e.attr("src").toLowerCase().contains("asciimathmL.js")) {
							return 1;
						}
					}
				}
			}else if(llave.equals("latexmathml")) {//LATEXMATHML
				boolean script=false;
				boolean css=false;
				Elements scripts=doc.select("script");
				if(scripts!=null&&scripts.size()>0) {
					for(Element e:scripts) {
						if(e.attr("src").toLowerCase().contains("latexmathmL.js")) {
							script=true;
						}
					}
				}
				Elements style=doc.select("link");
				if(style!=null && style.size()>0) {
					for(Element link:style) {
						if(link.attr("href").toLowerCase().contains("lateXmathml.standardarticle.css")) {
							css=true;
						}
					}
				}
				if(script&&css) {
					return 2;
				}
			}else if(llave.equals("jqmath")) {//JQMATH
				boolean script=false;
				boolean css=false;
				Elements scripts=doc.select("script");
				if(scripts!=null&&scripts.size()>0) {
					for(Element e:scripts) {
						if(e.attr("src").toLowerCase().contains("jqmath")) {
							script=true;
						}
					}
				}
				Elements style=doc.select("link");
				if(style!=null && style.size()>0) {
					for(Element link:style) {
						if(link.attr("href").toLowerCase().contains("jqmath")) {
							css=true;
						}
					}
				}
				if(script&&css) {
					return 3;
				}
			}else if(llave.equals("mathjax")) {//MATHJAX
				Elements scripts=doc.select("script");
				if(scripts!=null && scripts.size()>0) {
					for (Element e : scripts) {
						if(e.attr("src").toLowerCase().contains("mathjax")) {
							return 4;
						}
					}
				}
			}else if(llave.equals("codecogs")) {//laex.codecogs solo script
				Elements scripts=doc.select("script");
				if(scripts!=null && scripts.size()>0) {
					for (Element e : scripts) {
						if(e.attr("src").toLowerCase().contains("latex.codecogs")) {
							return 5;
						}
					}
				}
			}else if(llave.equals("codecogs.image")) {//latex.codecogs en una imagen directamente
				Elements images=doc.select("img");
				if(images!=null && images.size()>0) {
					for (Element img: images) {
						if(img.attr("src").toLowerCase().contains("latex.codecogs")) {
							return 6;
						}
					}
				}
			}
			else if(llave.equals("chem")) {//en caso de que sa con el ChemML
				Elements e=doc.select(llave);
				if(e!=null&& e.size()>0) {
					return e.size();
				}
			}
			
			return -1;
		}catch(Exception e) {
			e.printStackTrace();
			return -2;
		}
	}
}
