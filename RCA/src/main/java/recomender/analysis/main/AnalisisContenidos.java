package recomender.analysis.main;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

import javax.faces.bean.ManagedBean;

import recomender.analysis.html.PrincipalHTML;
import recomender.analysis.javascript.CodigoJavaScript;
import recomender.analysis.javascript.PrincipalJavaScript;
import recomender.staticcontent.VariablesEstaticas;
import recomender.validations.ValidacionesBasicas;
import recomender.analysis.css.PrincipalCSS;

public class AnalisisContenidos {	
	PrincipalHTML pHtml=null;
	PrincipalJavaScript pJavaScript=null;
	PrincipalCSS pCSS=null;
	/**
	 * este metodo manda a llamar a una parte de metadatos analizados
	 * @param archivo.- archivo html a analizar
	 * @param metas.- los metadatos a analizar
	 * @return
	 */
	public HashMap<String, Object[]> llamarPreguntas(String archivo, HashMap<String, String> metas) {
		CodigoJavaScript s=new CodigoJavaScript();
	
		String codigoJS=s.obtenerJSTotal(archivo);
		
		pHtml=new PrincipalHTML();
		pJavaScript=new PrincipalJavaScript();
		
		ArrayList<String>htmlEmbebidas=pHtml.paginasHTMLIncrustadas(archivo,new ArrayList<String>());
		
		htmlEmbebidas.add(archivo);
		
		///TRABAJO FUTURO
//		//ArrayList<String>paginasEnJavaScript=pJavaScript.paginasHTMLIncrustadas(archivo, new ArrayList<String>(),codigoJS);
//		System.out.println("htmincrustado1");
//		for (String paginaN :paginasEnJavaScript ) {
//			htmlEmbebidas.add(paginaN);
//		}
		  HashSet<String> hs = new HashSet<String>();
		  hs.addAll(htmlEmbebidas);
		  htmlEmbebidas.clear();
		  htmlEmbebidas.addAll(hs);

		int [] modosDeAcceso=new int [5];
		for (int i = 0; i < modosDeAcceso.length; i++) {
			modosDeAcceso[i]=0;
		}
		int resultadoDescripcionLarga=0;
		int resultadoSonidoFondo=0;
		int resultadoAnimaciones=0;
		int resultadoDependencia=0;
		for (String archivoN : htmlEmbebidas) {// me esta obteniendo x cada uno y me imprime el resultado por cada uno.
			int [] resultadoModoAcceso=modoAcceso(archivoN,codigoJS, metas);
			for (int i = 0; i < resultadoModoAcceso.length; i++) {
				modosDeAcceso[i]=modosDeAcceso[i]+resultadoModoAcceso[i];
			}
			if(metas.containsKey("http://www.accesiblemetada.com/DRDSchema#longDescription"))
				resultadoDescripcionLarga=resultadoDescripcionLarga+descripcionLarga(archivoN, codigoJS);
			if(metas.containsKey("http://www.accesiblemetada.com/DRDSchema#sound")||metas.containsKey("http://www.accesiblemetada.com/DRDSchema#noSoundHazard"))
				resultadoSonidoFondo=resultadoSonidoFondo+sonidoFondo(archivoN, codigoJS);
			if(metas.containsKey("http://www.accesiblemetada.com/DRDSchema#flashing")||metas.containsKey("http://www.accesiblemetada.com/DRDSchema#textual"))
				resultadoAnimaciones=resultadoAnimaciones+animaciones(archivoN,codigoJS);
			if(metas.containsKey("http://www.accesiblemetada.com/DRDSchema#colorDependent"))
				resultadoDependencia=dependenciaColor(archivoN);
	}
		ArrayList<Object[]> resultadosMetadatos=new ArrayList<Object[]>();
		if(modosDeAcceso[0]==0)
			 resultadosMetadatos.add(new Object[]{"No posee contenido textual. Tener solo contenido visual afecta de manera significativa a personas con discapacidad auditiva.",false});
		 else {
			 resultadosMetadatos.add(new Object[] {"Posee contenido textual con un numero de palabras de: "+modosDeAcceso[0],true});
			 VariablesEstaticas.numPalabras=VariablesEstaticas.numPalabras+modosDeAcceso[0];
		 }
		
		
		boolean tieneImagenes;
		
		if(modosDeAcceso[1]==0)
			tieneImagenes=false;	
		 else 
			 tieneImagenes=true;
		
		boolean tieneVideos;
		if(modosDeAcceso[2]==0)
			tieneVideos=false;
		else 
			tieneVideos=true;
		
		//if(!tieneVideos&&!tieneImagenes)
		if(modosDeAcceso[1]==0&&modosDeAcceso[2]==0)
			resultadosMetadatos.add(new Object[]{"No posee contenido visual(Ejem: imagenes o videos, esto podria afectar a personas con discapacidad auditiva.",false});
		else {    
			resultadosMetadatos.add(new Object[] {"Posee contenido visual con un numero de: "+modosDeAcceso[1]+" Imagenes y con un numero de: "+modosDeAcceso[2]+" Videos.",true});
			//VariablesEstaticas.numImagenes=VariablesEstaticas.numImagenes+modosDeAcceso[1];
			//VariablesEstaticas.numVideos=VariablesEstaticas.numVideos+modosDeAcceso[2];
		}
		
		//System.out.println("MNodos de acceso [] "+modosDeAcceso[3]);
		if(modosDeAcceso[3]==0)
			resultadosMetadatos.add(new Object[]{"No posee contenido Auditivo, esto podria afectar a personas con discapacidad visual.",false});
		 else {
			 resultadosMetadatos.add(new Object[] {"Posee contenido Auditivo con un numero de "+ modosDeAcceso[3]+" Audios.",true});
			 VariablesEstaticas.numContAuditivo=VariablesEstaticas.numContAuditivo+modosDeAcceso[3];
		 }
		if(modosDeAcceso[4]==0) {
			resultadosMetadatos.add(new Object[]{"No posee contenido Tactil, lo que dificultaria el uso del OA en dispositivos con esa caracteristica.",false});
			resultadosMetadatos.add(new Object[]{"No tiene un control total con el mouse, esto afecta a las personas con habilidad para controlar contenido con el mouse.",false});
			resultadosMetadatos.add(new Object[]{"No tiene un control total con el teclado, esto afecta a las personas con habilidad para controlar contenido con el teclado.",false});
		} else { 
			resultadosMetadatos.add(new Object[] {"Posee contenido Tactil con un numero de  "+ modosDeAcceso[4]+" elementos Tactiles.",true}); 
			resultadosMetadatos.add(new Object[]{"Tiene un control total con el mouse",true});
			resultadosMetadatos.add(new Object[]{"Tiene un control total con el teclado",true});
			VariablesEstaticas.numConTactil=VariablesEstaticas.numConTactil+modosDeAcceso[4];
		}
		
		if(tieneVideos) {
			resultadosMetadatos.add(new Object[]{"Dispone de videos que deberian tener audioDescripcion para facilitar su accesibilidad para usuarios que no pueden acceder a este contenido de forma visual.",false});
			resultadosMetadatos.add(new Object[]{"Dispone de videos que deberian tener adaptación para interprete en lengua de señas para facilitar su accesibilidad para usuarios que no pueden acceder a este contenido de forma auditiva.",false});
	
		}else {
			resultadosMetadatos.add(new Object[]{"No dispone de videos. Si no es asi, se recomienda anadir subtitulos, lenguaje de senas y, descripciones en texto.",false});
			resultadosMetadatos.add(new Object[]{"No dispone de videos. Si no es asi, se recomienda anadir subtitulos, lenguaje de senas y, descripciones en texto.",false});
		}
		if(resultadoDescripcionLarga>0) { 
			resultadosMetadatos.add(new Object[]{"Tiene "+resultadoDescripcionLarga+" imagenes con Descripcion Larga.",true});
			VariablesEstaticas.numImgLongDesc=VariablesEstaticas.numImgLongDesc+resultadoDescripcionLarga;
		}
		else
			resultadosMetadatos.add(new Object[]{"No tiene imagenes con Descripcion Larga. Se recomienda anadir descripciones en texto sobre la imagen.",false});
		if(resultadoSonidoFondo>0) {
			resultadosMetadatos.add(new Object[]{"Tiene sonido de fondo. No se recomienda usar sonido de fondo, ya que podria molestar a las personas con discapacidad cognitiva y psicosocial.",true});
			VariablesEstaticas.numSonidoFondo=VariablesEstaticas.numSonidoFondo+resultadoSonidoFondo;
		}
		else
			resultadosMetadatos.add(new Object[]{"No tiene sonido de fondo. Se recomienda que no use sonido de fondo, puesto que puede afectar a personas con discapacidad cognitiva y psicosocial.",false});
		if(resultadoAnimaciones>0) {
			resultadosMetadatos.add(new Object[]{"Tiene animaciones que estan en constante movimiento, se recomienda que no use animaciones pues podria afectar a las personas con discapacidad cognitiva y psicosocial.",true});
			VariablesEstaticas.numAnimaciones=VariablesEstaticas.numAnimaciones+resultadoAnimaciones;
		}
		else
			resultadosMetadatos.add(new Object[]{"No tiene animaciones que estan en constante movimiento",false});

		if(resultadoDependencia>0) {
			resultadosMetadatos.add(new Object[]{"Se encontro "+resultadoDependencia+" posibles actividades, las mismas pueden contener dependencia de colores. Se recomienda no usar colores como parte de actividades, pues, persona sco daltonismo no podrian resolverlas.",false});
			VariablesEstaticas.numDependenciaColor=VariablesEstaticas.numDependenciaColor+resultadoDependencia;
		}
		else
			resultadosMetadatos.add(new Object[]{"No se han encontrado posible actividades.",false});
		HashMap<String, Object[]> retorna=new HashMap<String, Object[]>();
		if(metas.containsKey("http://www.accesiblemetada.com/DRDSchema#textual"))
		retorna.put("textual", resultadosMetadatos.get(0));
		if(metas.containsKey("http://www.accesiblemetada.com/DRDSchema#visual"))
		retorna.put("visual", resultadosMetadatos.get(1));
		if(metas.containsKey("http://www.accesiblemetada.com/DRDSchema#auditory"))
		retorna.put("auditiva", resultadosMetadatos.get(2));
		if(metas.containsKey("http://www.accesiblemetada.com/DRDSchema#tactile"))
		retorna.put("tactil", resultadosMetadatos.get(3));
		if(metas.containsKey("http://www.accesiblemetada.com/DRDSchema#fullMouseControl"))
		retorna.put("mouse", resultadosMetadatos.get(4));
		if(metas.containsKey("http://www.accesiblemetada.com/DRDSchema#fullKeyboardControl"))
		retorna.put("teclado", resultadosMetadatos.get(5));
		if(metas.containsKey("http://www.accesiblemetada.com/DRDSchema#audioDescription"))
		retorna.put("audioDescripcion", resultadosMetadatos.get(6));
		if(metas.containsKey("http://www.accesiblemetada.com/DRDSchema#signLanguage"))
		retorna.put("senas", resultadosMetadatos.get(7));
		if(metas.containsKey("http://www.accesiblemetada.com/DRDSchema#longDescription"))
		retorna.put("longDesc", resultadosMetadatos.get(8));
		if(metas.containsKey("http://www.accesiblemetada.com/DRDSchema#sound")||metas.containsKey("http://www.accesiblemetada.com/DRDSchema#noSoundHazard"))
		retorna.put("sound", resultadosMetadatos.get(9));
		if(metas.containsKey("http://www.accesiblemetada.com/DRDSchema#flashing")||metas.containsKey("http://www.accesiblemetada.com/DRDSchema#noFlashingHazard"))
		retorna.put("movimiento", resultadosMetadatos.get(10));
		if(metas.containsKey("http://www.accesiblemetada.com/DRDSchema#colorDependent"))
		retorna.put("colorDependent", resultadosMetadatos.get(11));
//		for (Object[] objects : resultadosMetadatos) {
//			System.out.println(objects[0]);
//		}
		return retorna;
		}
	/**
	 * analiza el acceso textual
	 * @param archivo.- archivo html
	 * @return
	 */
	public int accesoTextual(String archivo) {
		int numeroPalabras=0;//Creamos un contador de palabras.
		//Para trabajo futuro mejorar que si se analizo una etiqueta y esta se encuentra como padre de otra etiqueta a analizar se omita este analisis dado que va a duplicar el numero de palabras.
		numeroPalabras=pHtml.buscar(new ArrayList<String>(),true,archivo,"p",null,null,null,null,true).size()+pHtml.buscar(new ArrayList<String>(),true,archivo,"h1",null,null,null,null,true).size();//Sumamos el numero de palabras encontradas en las busquedas.
		numeroPalabras=numeroPalabras+pHtml.buscar(new ArrayList<String>(),true,archivo,"h2",null,null,null,null,true).size()+pHtml.buscar(new ArrayList<String>(),true,archivo,"h3",null,null,null,null,true).size()+pHtml.buscar(new ArrayList<String>(),true,archivo,"h4",null,null,null,null,true).size();
		numeroPalabras=numeroPalabras+pHtml.buscar(new ArrayList<String>(),true,archivo,"h5",null,null,null,null,true).size()+pHtml.buscar(new ArrayList<String>(),true,archivo,"h6",null,null,null,null,true).size()+pHtml.buscar(new ArrayList<String>(),true,archivo,"h7",null,null,null,null,true).size();
		numeroPalabras=numeroPalabras+pHtml.buscar(new ArrayList<String>(),true,archivo,"b",null,null,null,null,true).size()+pHtml.buscar(new ArrayList<String>(),true,archivo,"strong",null,null,null,null,true).size()+pHtml.buscar(new ArrayList<String>(),true,archivo,"i",null,null,null,null,true).size();
		numeroPalabras=numeroPalabras+pHtml.buscar(new ArrayList<String>(),true,archivo,"em",null,null,null,null,true).size()+pHtml.buscar(new ArrayList<String>(),true,archivo,"u",null,null,null,null,true).size()+pHtml.buscar(new ArrayList<String>(),true,archivo,"small",null,null,null,null,true).size();
		numeroPalabras=numeroPalabras+pHtml.buscar(new ArrayList<String>(),true,archivo,"big",null,null,null,null,true).size()+pHtml.buscar(new ArrayList<String>(),true,archivo,"sub",null,null,null,null,true).size()+pHtml.buscar(new ArrayList<String>(),true,archivo,"font",null,null,null,null,true).size();
		numeroPalabras=numeroPalabras+pHtml.buscar(new ArrayList<String>(),true,archivo,"center",null,null,null,null,true).size();
		return numeroPalabras;
		}
	/**
	 * En este metodo analizaremos el modo de acceso visual a traves de las diferentes formas que se usan para representarlo.
	 * @param archivo.-El archivo html a analizar.
	 */
	public int accesoVisualImagenes(String archivo,String codigoJS) {
		//pCSS=new PrincipalCSS();
		ValidacionesBasicas vb=new ValidacionesBasicas();

//		ArrayList<String> imagenesCSS=new ArrayList<String>();
//		
//		ArrayList<String> resultadoCSS= pCSS.buscarEnCSS(archivo, "background-image","url");
//		
//				for (String imagenes:resultadoCSS) {
//			imagenes=imagenes.replace("(", "");
//			imagenes=imagenes.replace(")", "");
//			imagenes=imagenes.replace("\"", "");
//			imagenesCSS=vb.existeArchivo(archivo, imagenes, "imagen", imagenesCSS, null);
//		}
		int numeroImagenes=pHtml.buscar(new ArrayList<String>(),true,archivo, "img", "src",null, "imagen",null,false).size()+pHtml.buscar(new ArrayList<String>(),true,archivo, "input", "src;type=image",null, "imagen",null,false).size();//Creamos un contador de imagenes.
		numeroImagenes=numeroImagenes+pHtml.buscar(new ArrayList<String>(),true,archivo, "iframe", "src",null,"imagen",null,false).size()+pHtml.buscar(new ArrayList<String>(),true,archivo, "object", "data",null, "imagen",null,false).size()+pHtml.buscar(new ArrayList<String>(),true,archivo, "embeded", "src",null, "imagen",null,false).size();		
		numeroImagenes=numeroImagenes+pHtml.buscar(new ArrayList<String>(),false,codigoJS, "img", "src",null, "imagen",null,false).size()+pHtml.buscar(new ArrayList<String>(),false,codigoJS, "input", "src;type=image",null, "imagen",null,false).size();//Creamos un contador de imagenes.
		numeroImagenes=numeroImagenes+pHtml.buscar(new ArrayList<String>(),false,codigoJS, "iframe", "src",null,"imagen",null,false).size()+pHtml.buscar(new ArrayList<String>(),false,codigoJS, "object", "data",null, "imagen",null,false).size()+pHtml.buscar(new ArrayList<String>(),false,codigoJS, "embeded", "src",null, "imagen",null,false).size();		
		numeroImagenes=numeroImagenes+pJavaScript.buscar(codigoJS,new ArrayList<String>(),archivo, "src",null, "imagen",null,false).size()+pJavaScript.buscar(codigoJS,new ArrayList<String>(),archivo, "src;type=image",null, "imagen",null,false).size();//Creamos un contador de imagenes.
		numeroImagenes=numeroImagenes+pJavaScript.buscar(codigoJS,new ArrayList<String>(),archivo, "data",null, "imagen",null,false).size();		
		System.out.println("NUMEROOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO "+numeroImagenes);
		//numeroImagenes=numeroImagenes+imagenesCSS.size();
		
		return numeroImagenes;
	}
/**
 * analiza los videos en los html
 * @param archivo.- archivo html
 * @param codigoJS.- codigo JS
 * @return
 */
	public int accesoVisualVideos(String archivo,String codigoJS) {
		int numeroVideos=pHtml.buscar(new ArrayList<String>(),true,archivo, "video", "src;controls;autoplay","or","video",null,false).size();//Creamos un contador de videos.
		numeroVideos=numeroVideos+pHtml.buscar(new ArrayList<String>(),true,archivo, "iframe", "src",null,"video",null,false).size()+pHtml.buscar(new ArrayList<String>(),true,archivo, "object", "data",null,"video",null,false).size()+pHtml.buscar(new ArrayList<String>(),true,archivo, "embeded", "src",null, "video",null,false).size();
		numeroVideos=numeroVideos+pHtml.buscar(new ArrayList<String>(),false,codigoJS, "video", "src;controls;autoplay","or","video",null,false).size();
		numeroVideos=numeroVideos+pHtml.buscar(new ArrayList<String>(),false,codigoJS, "iframe", "src",null,"video",null,false).size()+pHtml.buscar(new ArrayList<String>(),false,codigoJS, "object", "data",null,"video",null,false).size()+pHtml.buscar(new ArrayList<String>(),false,codigoJS, "embeded", "src",null, "video",null,false).size();
		numeroVideos=numeroVideos+pJavaScript.buscar(codigoJS,new ArrayList<String>(),archivo, "src",null,"video",null,false).size()+pJavaScript.buscar(codigoJS,new ArrayList<String>(),archivo, "data",null,"video",null,false).size();
		return numeroVideos;
	}
	/**
	 * En este metodo analizaremos el modo de acceso auditivo a traves de las diferentes formas que se usan para representarlo.
	 * @param archivo.-El archivo html a analizar.
	 */
	public int accesoAuditivo(String archivo,String codigoJS) {
		int numeroAudios=pHtml.buscar(new ArrayList<String>(),true,archivo, "audio", "src;controls;autoplay","or","audio",null,false).size();//Creamos un contador de audio.
		numeroAudios=numeroAudios+pHtml.buscar(new ArrayList<String>(),true,archivo, "iframe", "src",null,"audio",null,false).size()+pHtml.buscar(new ArrayList<String>(),true,archivo, "object", "data",null, "audio",null,false).size()+pHtml.buscar(new ArrayList<String>(),true,archivo, "embeded", "src",null, "audio","audio",false).size();
		numeroAudios=numeroAudios+pHtml.buscar(new ArrayList<String>(),true,archivo, "video", "src;controls;autoplay","or","video","audio",false).size();//Creamos un contador de videos.
		numeroAudios=numeroAudios+pHtml.buscar(new ArrayList<String>(),true,archivo, "iframe", "src",null,"video","audio",false).size()+pHtml.buscar(new ArrayList<String>(),true,archivo, "object", "data",null,"video","audio",false).size()+pHtml.buscar(new ArrayList<String>(),true,archivo, "embeded", "src",null, "video","audio",false).size();
		System.out.println("ENTRO AUDIOS "+ numeroAudios);
		numeroAudios=numeroAudios+pHtml.buscar(new ArrayList<String>(),false,codigoJS, "audio", "src;controls;autoplay","or","audio",null,false).size();//Creamos un contador de audio.
		numeroAudios=numeroAudios+pHtml.buscar(new ArrayList<String>(),false,codigoJS, "iframe", "src",null,"audio",null,false).size()+pHtml.buscar(new ArrayList<String>(),false,codigoJS, "object", "data",null, "audio",null,false).size()+pHtml.buscar(new ArrayList<String>(),false,codigoJS, "embeded", "src",null, "audio","audio",false).size();
		numeroAudios=numeroAudios+pHtml.buscar(new ArrayList<String>(),false,codigoJS, "video", "src;controls;autoplay","or","video","audio",false).size();//Creamos un contador de videos.
		numeroAudios=numeroAudios+pHtml.buscar(new ArrayList<String>(),false,codigoJS, "iframe", "src",null,"video","audio",false).size()+pHtml.buscar(new ArrayList<String>(),false,codigoJS, "object", "data",null,"video","audio",false).size();
		numeroAudios=numeroAudios+pJavaScript.buscar(codigoJS,new ArrayList<String>(),archivo, "src",null,"audio",null,false).size()+pJavaScript.buscar(codigoJS,new ArrayList<String>(),archivo, "data",null, "audio",null,false).size();
		numeroAudios=numeroAudios+pJavaScript.buscar(codigoJS,new ArrayList<String>(),archivo, "src",null,"video","audio",false).size()+pJavaScript.buscar(codigoJS,new ArrayList<String>(),archivo, "data",null,"video","audio",false).size();
		return numeroAudios;
		}
	/**
	 * En este metodo analizaremos el modo de acceso auditivo a traves de las diferentes formas que se usan para representarlo.
	 * @param archivo.-El archivo html a analizar.
	 */
	public int accesoTactil(String archivo,String codigoJS) {
		int numeroElementosTactiles=pHtml.buscar(new ArrayList<String>(),true,archivo, "a", "href", null,"desconocido",null,false).size()+pHtml.buscar(new ArrayList<String>(),true,archivo, "area", "href", null,"desconocido",null,false).size()+pHtml.buscar(new ArrayList<String>(),true,archivo, "input", "type",null,null,null,false).size();//Creamos un contador de audio.
		numeroElementosTactiles=numeroElementosTactiles+pHtml.buscar(new ArrayList<String>(),true,archivo, "button", null,null,null,null,false).size()+pHtml.buscar(new ArrayList<String>(),true,archivo, "select",null,null, null,null,false).size()+pHtml.buscar(new ArrayList<String>(),true,archivo, "textarea",null,null,null,null,false).size();
		numeroElementosTactiles=numeroElementosTactiles+pHtml.buscar(new ArrayList<String>(),false,codigoJS, "a", "href", null,"desconocido",null,false).size()+pHtml.buscar(new ArrayList<String>(),false,codigoJS, "area", "href", null,"desconocido",null,false).size()+pHtml.buscar(new ArrayList<String>(),false,codigoJS, "input", "type",null,null,null,false).size();//Creamos un contador de audio.
		numeroElementosTactiles=numeroElementosTactiles+pHtml.buscar(new ArrayList<String>(),false,codigoJS, "button", null,null,null,null,false).size()+pHtml.buscar(new ArrayList<String>(),false,codigoJS, "select",null,null, null,null,false).size()+pHtml.buscar(new ArrayList<String>(),false,codigoJS, "textarea",null,null,null,null,false).size();
		numeroElementosTactiles=numeroElementosTactiles+pJavaScript.buscar(codigoJS,new ArrayList<String>(),archivo, "href", null,"desconocido",null,false).size()+pJavaScript.buscar(codigoJS,new ArrayList<String>(),archivo, "type",null,null,null,false).size();//Creamos un contador de audio.
		return numeroElementosTactiles;
	}
	public int[] modoAcceso(String archivo,String codigoJS, HashMap<String, String>metas) {
		int [] resultados=new int[5];
		if(metas.containsKey("http://www.accesiblemetada.com/DRDSchema#textual"))
			resultados[0]=accesoTextual(archivo);
		if(metas.containsKey("http://www.accesiblemetada.com/DRDSchema#visual")) {
			resultados[1]=accesoVisualImagenes(archivo,codigoJS);
			//VariablesEstaticas.numIm=VariablesEstaticas.numIm+resultados[1];
			VariablesEstaticas.numImagenes=VariablesEstaticas.numImagenes+resultados[1];
			resultados[2]=accesoVisualVideos(archivo,codigoJS);
		}
		if(metas.containsKey("http://www.accesiblemetada.com/DRDSchema#auditory"))
			resultados[3]=accesoAuditivo(archivo,codigoJS);
		if(metas.containsKey("http://www.accesiblemetada.com/DRDSchema#tactile"))
			resultados[4]=accesoTactil(archivo,codigoJS);	
		System.out.println("Resultadooooooo: "+resultados[3]);
		return resultados;		
		/*Para trabajo futuro.
		int valorRango=4;
		String textualTactil=vb.comparativaModosAcceso(numeroTextual, numeroTactil, numeroTactil*9, valorRango);//9 textual 1 tactil --> 9(numeroDePalabrasxCadaElementoTactil)*numeroTactil=numeroPalabrasMedio con relacion a accesoTactil.
		String textualVisual=vb.comparativaModosAcceso(numeroTextual, numeroVisual, numeroVisual*20, valorRango);//20 textual 1 visual
		String textualAuditivo=vb.comparativaModosAcceso(numeroTextual, numeroAuditivo, numeroAuditivo*15, valorRango);//15 textual 1 auditivo
		String tactilVisual=vb.comparativaModosAcceso(numeroTactil, numeroVisual, numeroVisual*3, valorRango);//3 tactil 1 visual
		String tactilAuditivo=vb.comparativaModosAcceso(numeroTactil, numeroAuditivo, numeroAuditivo*2, valorRango);//2 tactil 1 auditivo
		String auditivoVisual=vb.comparativaModosAcceso(numeroAuditivo, numeroVisual, numeroVisual*2, valorRango);//2 auditivo 1 visual
		System.out.println("El modo de acceso Textual tiene un porcentaje: "+vb.porcentajes(textualTactil,textualVisual,textualAuditivo));
		System.out.println("El modo de acceso Tactil tiene un porcentaje: "+vb.porcentajes(vb.relacionInversa(textualTactil),tactilVisual,tactilAuditivo));	
		System.out.println("El modo de acceso Visual tiene un porcentaje: "+vb.porcentajes(vb.relacionInversa(textualVisual),vb.relacionInversa(tactilVisual),vb.relacionInversa(auditivoVisual)));		
		System.out.println("El modo de acceso Auditivo tiene un porcentaje: "+vb.porcentajes(vb.relacionInversa(textualAuditivo),vb.relacionInversa(tactilAuditivo),auditivoVisual));
	*/
	}
	/**
	 * analiza descripcion larga en imagenes en los html
	 * @param archivo.- archivo html
	 * @param codigoJS.- codigo JS
	 * @return
	 */
	public int descripcionLarga(String archivo,String codigoJS) {
		int numeroDescripcionesLargas=pHtml.buscar(new ArrayList<String>(),true,archivo, "img", "longdesc", null,null,null,false).size()+pHtml.buscar(new ArrayList<String>(),true,archivo, "iframe", "longdesc", null,null,null,false).size()+pHtml.buscar(new ArrayList<String>(),true,archivo, "input", "longdesc;type=image",null,null,null,false).size();
		numeroDescripcionesLargas=numeroDescripcionesLargas+pHtml.buscar(new ArrayList<String>(),false,codigoJS, "img", "longdesc", null,null,null,false).size()+pHtml.buscar(new ArrayList<String>(),false,codigoJS, "iframe", "longdesc", null,null,null,false).size()+pHtml.buscar(new ArrayList<String>(),false,codigoJS, "input", "longdesc;type=image",null,null,null,false).size();
		numeroDescripcionesLargas=numeroDescripcionesLargas+pJavaScript.buscar(codigoJS,new ArrayList<String>(),archivo, "longdesc", null,null,null,false).size();
		return numeroDescripcionesLargas;
	}
	/**
	 * analiza sonidos de fondo en html
	 * @param archivo.- archivo html
	 * @param codigoJS.- codigo JS
	 * @return
	 */
	public int sonidoFondo(String archivo,String codigoJS) {
		int numeroAudiosFondo=pHtml.buscar(new ArrayList<String>(),true,archivo, "audio", "src;autoplay;loop","and","audio",null,false).size();//Creamos un contador de audio.
		numeroAudiosFondo=numeroAudiosFondo+pHtml.buscar(new ArrayList<String>(),false,codigoJS, "audio", "src;autoplay;loop","and","audio",null,false).size();//Creamos un contador de audio.
		numeroAudiosFondo=numeroAudiosFondo+pJavaScript.buscar(codigoJS,new ArrayList<String>(),archivo, "src;autoplay;loop","and","audio",null,false).size();//Creamos un contador de audio.
		return numeroAudiosFondo;
	}
	/**
	 * analiza animaciones de texto
	 * @param archivo.- archivo html
	 * @param codigoJS.- codigo JS
	 * @return
	 */
	public int animaciones(String archivo,String codigoJS) {
		int animaciones=0;
		animaciones=pHtml.buscar(new ArrayList<String>(),true,archivo,"marquee",null,null,null,null,true).size()+pHtml.buscar(new ArrayList<String>(),false,codigoJS,"marquee",null,null,null,null,true).size();		
		//pCSS=new PrincipalCSS();
		//ArrayList<String>resultadoCSS=pCSS.buscarEnCSS(archivo, "animation-iteration-count",null);
//		for (String iteraciones: resultadoCSS) {
//			if(iteraciones.toLowerCase().contains("infinite")) 
//				animaciones++;
//		}
		return animaciones;
		}
	/**
	 * analiza dependencia de colores
	 * @param archivo.- archivo html
	 * @return
	 */
	public int  dependenciaColor(String archivo) {	///TRABAJO FUTURO HACER CON ONTOLOGIAS O WEB SEMANTICA
		ValidacionesBasicas vb=new ValidacionesBasicas();
		ArrayList<ArrayList<String>> arregloTitulos=new ArrayList<ArrayList<String>>();//Creamos un contador de palabras.
		arregloTitulos.add(pHtml.buscar(new ArrayList<String>(),true,archivo,"h1",null,null,null,null,true));//Sumamos el numero de palabras encontradas en las busquedas.
		arregloTitulos.add(pHtml.buscar(new ArrayList<String>(),true,archivo,"h2",null,null,null,null,true));
		arregloTitulos.add(pHtml.buscar(new ArrayList<String>(),true,archivo,"h3",null,null,null,null,true));
		arregloTitulos.add(pHtml.buscar(new ArrayList<String>(),true,archivo,"h4",null,null,null,null,true));
		arregloTitulos.add(pHtml.buscar(new ArrayList<String>(),true,archivo,"h5",null,null,null,null,true));
		arregloTitulos.add(pHtml.buscar(new ArrayList<String>(),true,archivo,"h6",null,null,null,null,true));
		arregloTitulos.add(pHtml.buscar(new ArrayList<String>(),true,archivo,"b",null,null,null,null,true));
		arregloTitulos.add(pHtml.buscar(new ArrayList<String>(),true,archivo,"strong",null,null,null,null,true));
		
		ArrayList<ArrayList<String>> arregloPalabras=new ArrayList<ArrayList<String>>();
		arregloPalabras.add(pHtml.buscar(new ArrayList<String>(),true,archivo,"i",null,null,null,null,true));
		arregloPalabras.add(pHtml.buscar(new ArrayList<String>(),true,archivo,"em",null,null,null,null,true));
		arregloPalabras.add(pHtml.buscar(new ArrayList<String>(),true,archivo,"u",null,null,null,null,true));
		arregloPalabras.add(pHtml.buscar(new ArrayList<String>(),true,archivo,"small",null,null,null,null,true));
		arregloPalabras.add(pHtml.buscar(new ArrayList<String>(),true,archivo,"big",null,null,null,null,true));
		arregloPalabras.add(pHtml.buscar(new ArrayList<String>(),true,archivo,"sub",null,null,null,null,true));
		arregloPalabras.add(pHtml.buscar(new ArrayList<String>(),true,archivo,"font",null,null,null,null,true));
		arregloPalabras.add(pHtml.buscar(new ArrayList<String>(),true,archivo,"p",null,null,null,null,true));
			
		boolean dependencia=false;
		boolean tieneTituloActividad=false;
		for (ArrayList<String> titulos : arregloTitulos) {
			for (String titulo : titulos) {
				for (String posibleTitulo : vb.obtenerTitulos()) {
					if(titulo.toLowerCase().contains(posibleTitulo)) {
						tieneTituloActividad=true;
					}
				}
			}
		}
		if(tieneTituloActividad) {
		for (ArrayList<String> palabras : arregloPalabras) {
			for (String palabra : palabras) {
				ArrayList<String> resultadoColores=vb.obtenerColores();
				for (String colores : resultadoColores) {
					if(palabra.toLowerCase().contains(colores)) {
						dependencia=true;						
					}
			}
		}
	}
}
		if(dependencia)
			return 1;
		else
			return 0;
		
	}
		

}
