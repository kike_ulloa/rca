package recomender.analysis.main;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import recomender.ontology.inferirOntologia;
import recomender.calculations.Formulas;
import recomender.datamanipulation.MatrizDinamica;

public class AnalizaConOntologia {
	///ONTOLOGIA
	private double numMetadatos=28.0;
	@Inject
	private inferirOntologia inf;
	
	/**
	 * Metodo que invoca al metodo que se comunica con la ontologia para extraer los metadatos por discapaicdad.
	 * @param discapacidades Arreglo de las discapacidades seleccionadas.
	 * @return Retorna un HashMap con los metadatos a usarse.
	 */
	public HashMap<String, String> retornaMetasByDisability(String []discapacidades){
		String favoreceDesfavorece="sch:DRD000009R";//favorece
		String resultado[][]=inf.retornaMetasPorDiscapacidades(favoreceDesfavorece, discapacidades);
		favoreceDesfavorece="sch:DRD000007R";//Afecta
		//String resultado2[][]=inf.retornaMetasPorDiscapacidades(favoreceDesfavorece, discapacidades);
		//recorro cada resultado y agrego en un hashmap solo los metadatos
		HashMap<String, String> metas=new HashMap<String, String>();
		System.out.println("Favorece");
		for(String meta[]: resultado) {
			metas.put(meta[0], meta[0]);
			System.out.println(meta[0]);
		}
//		System.out.println("Afecta");
//		for(String meta[]: resultado2) {
//			metas.put(meta[0], meta[0]);
//			System.out.println(meta[0]);
//		}
		return metas;
	}
	/**
	 * Metodo que infiere los documentos.
	 * @param rutasHTML Arreglo de las rutas de los archivos html.
	 * @param obAprend Es la ruta del objeto de aprendizaje.
	 * @param resultadoTotal Es el resultado postanalisi de contenidos. Es un hashmap.
	 * @param analizarSoloExistentes Indica si se desea analizar solo los metas existentres en el archivo html o no (boolean).
	 * @param metas SOn los metadatos que se van a usar pro discapacidad.
	 * @return Retorna un arreglo con el iri y el label del objeto de aprendizaje inferenciado.
	 */
	public java.util.ArrayList inferirDocumentos(java.util.List<String> rutasHTML,String obAprend, HashMap<String,HashMap<String,Object[]>> resultadoTotal, boolean analizarSoloExistentes, HashMap<String, String>metas) {
		HashMap<String, java.util.ArrayList<String>> metasEnDocs=inf.queryDos(rutasHTML, obAprend, resultadoTotal, analizarSoloExistentes, metas);
		java.util.ArrayList retorna=new java.util.ArrayList();
		retorna.add(metasEnDocs);
		retorna.add(inf.returnIRIOA());
		retorna.add(inf.getLabel());
		return retorna;
	}
	/**
	 * Metodo que invoca a los querys necesarios para obtener los resultados de porcentajes de accesibilidad.
	 * @param discpacidades Es un arrgelo de las discapacidades seleccionadas.
	 * @param iriOA Es el iri del objeto de aprendizaje.
	 * @param label Es el label del Objet de Aprendizaje.
	 * @param noAplica ES un listado de los metadatos que no se van a aplicar.
	 * @param numeroHTML Es el numero de documentos html en el objeto de aprendizaje.
	 * @return Retorna los porcentajes de accesibilida dpara cada discapacidad en un hashmap.
	 */
	public java.util.ArrayList enviaArchivoInferir(String []discpacidades, String iriOA, String label, String []noAplica, String numeroHTML) {
		//inferirOntologia in=new inferirOntologia();
		//rutasHtml=new ArrayList<String>();
		//inf.htmls();
		//String rutaOa=pathNuevoOA.substring(pathNuevoOA.lastIndexOf('/'));
		//inf.queryDos(rutasHtml,rutaOa);
		Formulas f=new Formulas();
		HashMap<String, Double> porcentajes=new HashMap<>();
		java.util.ArrayList dataResp=inf.query(iriOA, label, noAplica, discpacidades);
		String [][] sumaFavorece=(String [][])dataResp.get(0);
		String [][] global=(String[][])dataResp.get(1);
		for(String [] favorece:sumaFavorece) {
			for(String [] g:global) {
				if(favorece[1].equals(g[1])&&favorece[2].equals(g[2])){
					if(porcentajes.containsKey(favorece[2])) {
						porcentajes.put(favorece[2], porcentajes.get(favorece[2])+f.calculaPorcentajeAccesibilidad(favorece[2], favorece[3], Double.parseDouble(g[3])));
					}else {
						porcentajes.put(favorece[2], f.calculaPorcentajeAccesibilidad(favorece[2], favorece[3], Double.parseDouble(g[3])));
					}
					
				}
			}
		}
		
//		ArrayList<String [][]> listadoPorDocsFavorece=(ArrayList<String [][]>)dataResp.get(0);
//		HashMap<String, HashMap<String, Integer>> contiene=(HashMap<String, HashMap<String, Integer>>)dataResp.get(1);
//		//ArrayList<String [][]> listadoPorDocs=inf.query(iriOA);
//		String [][] resultado=listadoPorDocsFavorece.get(0);
//		//for(String disc:discpacidades) {
//		HashMap<String, String> discapacidades=new HashMap<String, String>();
//		for(String discapacidad: discpacidades) {
//			discapacidades.put(discapacidad, discapacidad);
//		}
//		for(String [][] data:listadoPorDocsFavorece) {
//			//String [][]matrizMayores=df.ordenaRetorna(data, cols);
//			//System.out.println("=================================");
//			for(String []dat: data) {
//				if(discapacidades.containsKey(dat[2].substring(dat[2].indexOf("#")+1))) {
//					if(porcentajes.containsKey(dat[2])) {
//						if(contiene.get(dat[1]).containsKey(dat[2]))
//						porcentajes.put(dat[2], porcentajes.get(dat[2])+f.calculaPorcentajeAccesibilidad(dat[2],dat[3], contiene.get(dat[1]).get(dat[2])));
//					}else {
//						porcentajes.put(dat[2], f.calculaPorcentajeAccesibilidad(dat[2],dat[3], contiene.get(dat[1]).get(dat[2])));
//					}
//				}
//				//System.out.print(dat[3]+" "+dat[0]+" "+dat[1]+" "+dat[2]+" "+(Double.parseDouble(dat[3])*100)/contiene.get(dat[1]).get(dat[2]));//en vez del 28 poner el num de ocurrencias por discapacidad q saco al sumar los q restabas
//				
//			}
//			System.out.println();
//			System.out.println();
//			System.out.println();
//			//QUE PASA SI DOS DISCAPACIDADES TIENEN EL MISMO NUMERO de favorece al final
//		}
//		System.out.println();
//			//QUE PASA SI DOS DISCAPACIDADES TIENEN EL MISMO NUMERO de favorece al final
//		System.out.println("Porcentaaaajeeeeessssss: "+porcentajes.size());
		java.util.ArrayList resultadoOntologia=new java.util.ArrayList();
		//resultadoOntologia.add(listadoPorDocsFavorece);
		System.out.println("Este es el promedio global "+global.length);
		resultadoOntologia.add(returnPorcentajesTotales(porcentajes, Integer.parseInt(numeroHTML)));
		//resultadoOntologia.add(porcentajes);
		return resultadoOntologia;
	}
	/**
	 * Metodo que divide el porcentaje total de la discapacidad para el numero de documentos html.
	 * @param porcents Son los porcentaje spor discapaicidad en hashmap
	 * @param numDocs Es el numero de documentos html.
	 * @return Retorna el porcentaje final por discapacidad.
	 */
	public HashMap<String, Double> returnPorcentajesTotales(HashMap<String, Double> porcents, double numDocs){
		HashMap<String, Double> retorna= new HashMap<String, Double>();
		for(Map.Entry<String, Double> entry:porcents.entrySet()) {
			retorna.put(entry.getKey(), (entry.getValue()/numDocs));
		}
		return retorna;
	}
	/**
	 * Metodo que extrae los criterios de exito wcag.
	 * @param metasPorcentajes Hashmap con los porcentajes de cada discapacidad por metas
	 * @param discapacidades Listado de discapacidades seleccionadas.
	 * @param favoreceDesfavorece Es un string con la relacion de favorece o afecta para el query
	 * @param noAplica Son los metadatos que no se aplican
	 * @return Retorna un listado de los criteriso de exito con la discapaidad y metadato a la que pertenece.
	 */
	public java.util.ArrayList<String [][]> resultadoFinal(HashMap<String, Double> metasPorcentajes, String []discapacidades, String favoreceDesfavorece, String[] noAplica) {
		
		String lang="es";
		int cols=5;
		HashMap<String, String [][]>queryPorDiscapacidad=new HashMap<String, String[][]>();//hasmap donde se almacenara todos los resultados de los querys por discapacidad
		for(String discapacidad:discapacidades) {
			String [][]res=inf.returnResultadoFinal(discapacidad, favoreceDesfavorece, lang, noAplica);
//			System.out.println("RECIBE");
//			for(int i=0;i<res.length;i++) {
//				System.out.println(res[i][0]+" "+res[i][1]+" "+res[i][2]+" "+res[i][3]+" "+res[i][4]);
//			}
			queryPorDiscapacidad.put(discapacidad, res);
		}
		MatrizDinamica md=new MatrizDinamica();
		String [][] matrizResFinal=new String[0][cols];//para las 5 columnas del resultado
		String [][] matrizResFinalNoTiene=new String[0][cols];//para las 5 columnas del resultado que no tienen los emtadatos
		for(Map.Entry<String, String [][]> entry: queryPorDiscapacidad.entrySet()) {
			String [][]val=entry.getValue();
//			System.out.println("RECIBE");
//			for(int i=0;i<val.length;i++) {
//				System.out.println(val[i][0]+" "+val[i][1]+" "+val[i][2]+" "+val[i][3]+" "+val[i][4]);
//			}
			if(val!=null&&val.length>0) {
				for(int i=0;i<val.length;i++) {
					if(metasPorcentajes.containsKey(val[i][0])) {//en caso de que el hashmap de iris de metas q sic ontiene el doc si contenga el iri de metadatros de la mariz
					//	System.out.println("Entraaaaaaa: "+val[i][0]+" "+val[i][3]);
						String datos[]=new String[cols];
						datos[0]=val[i][0];
						datos[1]=val[i][1];
						datos[2]=val[i][2];
						datos[3]=val[i][3];
						datos[4]=val[i][4];
						matrizResFinal=md.matriz(matrizResFinal, cols, datos);
					}else {
						String datos[]=new String[cols];
						datos[0]=val[i][0];
						datos[1]=val[i][1];
						datos[2]=val[i][2];
						datos[3]=val[i][3];
						datos[4]=val[i][4];
						matrizResFinalNoTiene=md.matriz(matrizResFinalNoTiene, cols, datos);
					}
				}
			}
		}
		java.util.ArrayList<String [][]>tieneNoTiene=new java.util.ArrayList<String[][]>();
		tieneNoTiene.add(matrizResFinal);
		tieneNoTiene.add(matrizResFinalNoTiene);
		//return matrizResFinal;
		return tieneNoTiene;
	}
}
