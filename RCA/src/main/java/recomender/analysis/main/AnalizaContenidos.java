package recomender.analysis.main;

import java.util.ArrayList;
import java.util.HashMap;

import javax.inject.Inject;

import recomender.ontology.inferirOntologia;
import recomender.staticcontent.VariablesEstaticas;
import recomender.analysis.AnalizaSubtitulosMain;
import recomender.analysis.css.FontSize;
import recomender.analysis.css.PrincipalCSS;
import recomender.analysis.html.AnalizaCanvas;
import recomender.analysis.html.AnalizaHtml;
import recomender.analysis.html.AnalizaPDF;
import recomender.analysis.html.AnalizaTablaContenidos;
import recomender.analysis.html.AnalizaTamanoFuente;
import recomender.analysis.javascript.AnalizaFontSize;
import recomender.analysis.javascript.AnalizaHTMLTags;
import recomender.analysis.javascript.AnalizaJavascriptJQuery;
import recomender.analysis.javascript.AnalizaSoloScript;
import recomender.analysis.javascript.AnalizaTIempos;
import recomender.analysis.javascript.CodigoJavaScript;
import recomender.analysis.latex.AnalizaMatematicas;
import recomender.data.UploadDeletePDF;

public class AnalizaContenidos {
	/**
	 * Metodo que analiza el texto alternativo y el texto en imagenes.
	 * @param ruta Es la ruta del archivo html.
	 * @param jsTotal Es el total del js .
	 * @return Retyorna un objeto con los resultados.
	 */
	public Object[] textOnVisualAlternativeText(String ruta, String jsTotal) {
		//ANALIZA IMAGENES HTML
		AnalizaHtml ah=new AnalizaHtml();
		ah.analiza(new java.io.File(ruta), "img");
		ah.analiza(new java.io.File(ruta), "iframe");
		ah.analiza(new java.io.File(ruta), "input");
		java.util.ArrayList<Integer> totalImagenesHTML=ah.totales();
		//ANALIZA IMAGENES JSHTML
		AnalizaHTMLTags aht=new AnalizaHTMLTags();//TAMBIEN LA DEBO USAR PARA ANALIZAR PDFS
		aht.analia(jsTotal, "img", "src", new java.io.File(ruta));
		aht.analia(jsTotal, "iframe", "src", new java.io.File(ruta));
		aht.analia(jsTotal, "input", "src", new java.io.File(ruta));
		java.util.ArrayList<Integer> totalImagenesJSHTML=aht.totales();
		//ANALIZA IMAGENES JS
		AnalizaSoloScript as=new AnalizaSoloScript();
		as.setSrcJavascript(jsTotal, new java.io.File(ruta));
		java.util.ArrayList<Integer> totalImagenesJS=as.totales();
		//ANALIZA IMAGENES JQUERY
		AnalizaJavascriptJQuery ajj=new AnalizaJavascriptJQuery();
		ajj.extraeDeJQuery(jsTotal, new java.io.File(ruta));
		java.util.ArrayList<Integer> totalImagenesJQ=ajj.totales();
		///sumo todas la img de jquery, html y js
		int numImg=totalImagenesHTML.get(0)+totalImagenesJSHTML.get(0)+totalImagenesJS.get(0)+totalImagenesJQ.get(0);
		int numImgTxtAlt=totalImagenesHTML.get(2)+totalImagenesJSHTML.get(2)+totalImagenesJS.get(2)+totalImagenesJQ.get(2);
		int numImgBajRes=totalImagenesHTML.get(1)+totalImagenesJSHTML.get(1)+totalImagenesJS.get(1)+totalImagenesJQ.get(3);
		//VariablesEstaticas.numImagenes=VariablesEstaticas.numImagenes+numImg;
		//Imagenes con txtAlternativo
		boolean txtAlternativo=false;
		String textoAlternativo="";
		if(numImgTxtAlt>(numImg/2)) {//si el numero de imagenes cont exto alternativo es mayor a la mitad de num de img
			txtAlternativo=true;
			textoAlternativo="El contenido tiene "+numImg+" imagenes, de las cuales "+numImgTxtAlt+" tienen texto alternativo";
			VariablesEstaticas.numImagenesTxtAlt=VariablesEstaticas.numImagenesTxtAlt+numImgTxtAlt;
		}else {
			txtAlternativo=false;
			textoAlternativo="El contenido tiene "+numImg+", de las cuales "+numImgTxtAlt+" tienen texto alternativo, se recomiendo que mas de la mitad de imagenes tengan texto alternativo.";
			VariablesEstaticas.numImagenesTxtAlt=VariablesEstaticas.numImagenesTxtAlt+numImgTxtAlt;
		}
		//System.out.println("####### TEXTO ALTERNATIVO ##########");
		//System.out.println(txtAlternativo+" "+textoAlternativo);
		//Imagenes con texto y baja resolucion
		boolean txtOnVisual=false;
		String textOnVisual="";
		if(numImgBajRes>(numImg/2)) {//en caso de que el numero de imagenes con poca resolucion y texto sea mayor a la mitad de imagenes
			txtOnVisual=true;
			textOnVisual="Se encontraron "+numImg+" imagenes, de las cuales "+numImgBajRes+" imagenes con posible texto y baja resolucion. Se recomienda que se omita texto en las imagenes, pues es imposible que los lectores de pantalla los puedan leer.";
		}else {
			txtOnVisual=false;
			textOnVisual="Se encontraron "+numImg+" imagenes, de las cuales "+numImgBajRes+" imagenes con posible texto y baja resolucion. Se recomienda que se omita texto en las imagenes, pues es imposible que los lectores de pantalla los puedan leer.";
		}
		VariablesEstaticas.numtextOnVisual=VariablesEstaticas.numtextOnVisual+numImgBajRes;
		///seteo el numero de imagenes
		//VariablesEstaticas.numImagenes=VariablesEstaticas.numImagenes+numImg;
		//System.out.println("####### TEXT ON VISUAL ##########");
		//System.out.println(txtOnVisual+" "+textOnVisual);
		return new Object[] {textoAlternativo,txtAlternativo,textOnVisual,txtOnVisual};		
	}
	/**
	 * Metodo que analiza el tamano fuente de los textos.
	 * @param archivo Es el html 
	 * @param jsTotal es el total del js
	 * @param cssTotal es el total del css.
	 * @return Retorna un objeto con los resultados obtenids
	 */
	public Object [] tamanoFuente(java.io.File archivo, String jsTotal, String cssTotal) {
		AnalizaTamanoFuente atf=new AnalizaTamanoFuente();
		boolean tagHML=atf.tagFont(archivo, "font");//analiza solo la etiqueta font
		int[] cumpleTodoHTML=atf.analizaTodoHTML(archivo);//analiza todo el html
		AnalizaFontSize afs=new AnalizaFontSize();
		int []cumpleJS=afs.retornaResultado(jsTotal);//analizo todos los posibles font size de javascript
		FontSize fs=new FontSize();
		int []cumpleCSS=fs.fontSize(cssTotal);
		cumpleJS[0]+=cumpleCSS[0];
		cumpleJS[1]+=cumpleCSS[1];
		Object [] tamanoFuente=new Object[2];
		if(cumpleTodoHTML!=null) {
			cumpleTodoHTML[0]=cumpleTodoHTML[0]+cumpleJS[0];
			cumpleTodoHTML[1]=cumpleTodoHTML[1]+cumpleJS[1];
			if(tagHML) {
				cumpleTodoHTML[0]=cumpleTodoHTML[0]+1;
				cumpleTodoHTML[1]+=1;
			}
			if(cumpleTodoHTML[0]>(cumpleTodoHTML[1]/2)) {
				tamanoFuente[0]="Mas del 50% de los tamanos de fuente encontrados son de un tamano accesible.";
				tamanoFuente[1]=true;
			}else {
				tamanoFuente[0]="Menos del 50% de los tamanos de fuente encontrados tienen un tamano accesible, se recomienda que todas las fuentes de letras sean de un tamano accesible (18 a 20 px).";
				tamanoFuente[1]=false;
			}
			VariablesEstaticas.numCumpleTodoHTML=VariablesEstaticas.numCumpleTodoHTML+cumpleTodoHTML[0];
			VariablesEstaticas.numCumpleTodoHTMLTotal=VariablesEstaticas.numCumpleTodoHTMLTotal+cumpleTodoHTML[1];
		}else if(tagHML){
			cumpleJS[0]+=1;
			cumpleJS[1]+=1;
			if(cumpleJS[0]>(cumpleJS[1]/2)) {
				tamanoFuente[0]="Mas del 50% de los tamanos de fuente encontrados son de un tamano accesible.";
				tamanoFuente[1]=true;
			}else {
				tamanoFuente[0]="Menos del 50% de los tamanos de fuente encontrados tienen un tamano accesible, se recomienda que todas las fuentes de letras sean de un tamano accesible (18 a 20 px).";
				tamanoFuente[1]=false;
			}
			VariablesEstaticas.numCumpleTodoHTML=VariablesEstaticas.numCumpleTodoHTML+cumpleJS[0];
			VariablesEstaticas.numCumpleTodoHTMLTotal=VariablesEstaticas.numCumpleTodoHTMLTotal+cumpleJS[1];
		}else {
			if(cumpleJS[0]>(cumpleJS[1]/2)) {
				tamanoFuente[0]="Mas del 50% de los tamanos de fuente encontrados son de un tamano accesible.";
				tamanoFuente[1]=true;
			}else {
				tamanoFuente[0]="Menos del 50% de los tamanos de fuente encontrados tienen un tamano accesible, se recomienda que todas las fuentes de letras sean de un tamano accesible (18 a 20 px).";
				tamanoFuente[1]=false;
			}
			VariablesEstaticas.numCumpleTodoHTML=VariablesEstaticas.numCumpleTodoHTML+cumpleJS[0];
			VariablesEstaticas.numCumpleTodoHTMLTotal=VariablesEstaticas.numCumpleTodoHTMLTotal+cumpleJS[1];
		}
		return tamanoFuente;
	}
	/**
	 * Metodo princiapl que invoca a todos los emtdos necesarios para el analisis del contenido para los metadatos que se 
	 * definen dentro de este mismo metodo. Tambien usa la svariables estaticas para los resultados finales.
	 * @param ruta Es la ruta del htmlque se esta analizando.
	 * @param OA es la ruta del objeto de aprendizaje/
	 * @param metas Son los metadatos a usarse
	 * @return Retorna un arreglo de objetos con todos los resultados
	 */
	public HashMap<String, Object[]> analyzeContents(String ruta, String OA, HashMap<String, String>metas) {
		CodigoJavaScript c=new CodigoJavaScript();
		String jsTotal=c.obtenerJSTotal(ruta);//obtengo el jaascript de todos los archivos
		java.io.File archivo=new java.io.File(ruta);
		PrincipalCSS pcss=new PrincipalCSS();
		String cssTotal="";
		//ANALIZA IMGS
		Object []txtOnVisualAlternative=null;
		if(metas.containsKey("http://www.accesiblemetada.com/DRDSchema#textOnVisual")||metas.containsKey("http://www.accesiblemetada.com/DRDSchema#alternativeText"))
			txtOnVisualAlternative=textOnVisualAlternativeText(ruta, jsTotal);
		////ANALIZA TABLA DE CONTENIDOS
		Object [] tablaContenidos=null;
		if(metas.containsKey("http://www.accesiblemetada.com/DRDSchema#tableOfContents")) {
			AnalizaTablaContenidos atc=new AnalizaTablaContenidos();
		
			tablaContenidos=atc.tieneTablaContenido(new java.io.File(ruta));
		}
		//ANALIZA TAMANO FUENTE
		Object []tamanoFunte=null;
		if(metas.containsKey("http://www.accesiblemetada.com/DRDSchema#largePrint"))
		tamanoFunte=tamanoFuente(archivo, jsTotal, cssTotal);
		
		
		Object []mathML=null;
		Object []cheml=null;
		Object []latex=null;
		
		AnalizaMatematicas am=new AnalizaMatematicas();
		//ANALIZA MATHML
		if(metas.containsKey("http://www.accesiblemetada.com/DRDSchema#MathML"))
			mathML=am.analizaMathML(archivo);
		//ANALIZA CHEMML
		if(metas.containsKey("http://www.accesiblemetada.com/DRDSchema#ChemML"))
			cheml=am.analizaChemML(archivo);
		//ANALIZA LATEX
		if(metas.containsKey("http://www.accesiblemetada.com/DRDSchema#latex"))
			latex=am.analizaLatex(archivo);
		
//		System.out.println(txtOnVisualAlternative[0]+" "+txtOnVisualAlternative[1]);
//		System.out.println(txtOnVisualAlternative[2]+" "+txtOnVisualAlternative[3]);
//		System.out.println(tablaContenidos[0]+" "+tablaContenidos[1]);
//		System.out.println(tamanoFunte[0]+" "+tamanoFunte[1]);
//		System.out.println(mathML[0]+" "+mathML[1]);
//		System.out.println(cheml[0]+" "+cheml[1]);
//		System.out.println(latex[0]+" "+latex[1]);
		///////ANALIZA SIMULACIONES
		AnalizaCanvas ac=new AnalizaCanvas();
		AnalizaTIempos at=new AnalizaTIempos();
		//System.out.println(jsTotal);
		boolean contieneTiempos=at.contineTiempos(jsTotal);
		boolean contieneCanvasHTML=ac.contineCanvas(archivo);
		boolean contieneCanvasJS=at.contieneCanvas(jsTotal);
		Object [] simulations=new Object[2];
		simulations[0]="No contiene tiempos ni canvas por lo que se asimila que no contiene simulaciones.";
		simulations[1]=false;
		if(contieneCanvasHTML&& contieneTiempos) {
			System.out.println("Si contiene tiempos y canvas por lo cual se asemeja a una simulacion.");
			simulations[0]="Si contiene tiempos y canvas por lo cual se asemeja a una simulacion. Se recomienda, de ser posible, no tener simulaciones.";
			simulations[1]=true;
			VariablesEstaticas.numSimulaciones=VariablesEstaticas.numSimulaciones+1;
		}
		if(contieneCanvasJS&& contieneTiempos) {
			System.out.println("Si contiene tiempos y canvas por lo cual se asemeja a una simulacion.");
			simulations[0]="Si contiene tiempos y canvas por lo cual se asemeja a una simulacion. Se recomienda, de ser posible, no tener simulaciones.";
			simulations[1]=true;
			VariablesEstaticas.numSimulaciones=VariablesEstaticas.numSimulaciones+1;
		}
		
		
		Object [] ACTiempos=new Object[2];
		ACTiempos[0]="No contiene tiempos ni canvas, por lo cual no existe una actividad controlada por tiempos.";
		ACTiempos[1]=false;
		////PARA ACTIVIDAD CONTROLADA POR TIEMPOS 
		if(!contieneCanvasHTML&& contieneTiempos) {
			System.out.println("Si contiene tiempos y no canvas, por lo cual podria ser una actividad controlada por tiempos.");
			ACTiempos[0]="Si contiene tiempos y no canvas(para simulaciones), por lo cual podria ser una actividad controlada pro tiempos.";
			ACTiempos[1]=true;
			VariablesEstaticas.acTiempos=VariablesEstaticas.acTiempos+1;
		}
		if(!contieneCanvasJS&& contieneTiempos) {
			System.out.println("Si contiene tiempos y no canvas, por lo cual podria ser una actividad controlada por tiempos.");
			ACTiempos[0]="Si contiene tiempos y no canvas(para simulaciones), por lo cual podria ser una actividad controlada pro tiempos.";
			ACTiempos[1]=true;
			VariablesEstaticas.acTiempos=VariablesEstaticas.acTiempos+1;
		}
		
		//VIDEOS CON SUBS
		Object[] subs=null;
		//para extraer js de archivos js que esten dentro de el documento html (<script src='archivo.js'/>)
		ArrayList<String>resultadoJSDeEtiqueta=c.obtenerJSDeEtiquetaScript(ruta);//obtengo la ruta de todos los js
		if(metas.containsKey("http://www.accesiblemetada.com/DRDSchema#captions")||metas.containsKey("http://www.accesiblemetada.com/DRDSchema#audioDescription")||metas.containsKey("http://www.accesiblemetada.com/DRDSchema#signLanguague")) {
		java.util.ArrayList<String> jsEtiqueta=c.obtenerDeEtiquetaScript(ruta);
		String jsDeEtiqueta=java.util.Arrays.toString(jsEtiqueta.toArray());//convierto a string el contenido del js
		AnalizaSubtitulosMain asm=new AnalizaSubtitulosMain();
		asm.analizaSubtitulos(null, archivo);
		asm.analizaSubtitulos(jsDeEtiqueta,new java.io.File(ruta));//con etiqueta <script></script>
		
		for (String js : resultadoJSDeEtiqueta) {
			ArrayList<String>resultadoDeJS=c.obtenerDeJS(js);//obtengo el contenido de cada js
			String jsArchivo=java.util.Arrays.toString(resultadoDeJS.toArray());//convierto a string el contenido del js
			String rutaJS=new java.io.File(js).getAbsolutePath();//obtengo la ruta completa del archivo js
			asm.analizaJQuery(jsArchivo, rutaJS);//analizo videos en jquery
			asm.analizaJavascript(jsArchivo, rutaJS);//analizo videos en javascript
		}
		subs=asm.retornaVideosSubs();
		System.out.println(subs[0]+" "+subs[1]);
		if(subs[0].toString().contains("contiene subtitulos")) {
			double numVS=Double.parseDouble(subs[0].toString().substring(subs[0].toString().indexOf("Se ha")+24,subs[0].toString().indexOf("%")-1).trim());
			VariablesEstaticas.numVideosSubs=(VariablesEstaticas.numVideosSubs+numVS)/VariablesEstaticas.numVideos;
		}
		}
		////ACCESIBILIDAD EN PDF
		Object []pdfA=new Object[2];
		if(metas.containsKey("http://www.accesiblemetada.com/DRDSchema#taggedPDF")) {
		AnalizaPDF ap=new AnalizaPDF();
		java.util.ArrayList<String[]> re=ap.sonAccesibles(archivo, "html", archivo.getParent(), null, OA);//analiza archivo html
		java.util.ArrayList<String[]> repdf=new java.util.ArrayList<String[]>();
		for(String js: resultadoJSDeEtiqueta) {
			ArrayList<String>resultadoDeJS=c.obtenerDeJS(js);//obtengo el contenido de cada js
			String jsArchivo=java.util.Arrays.toString(resultadoDeJS.toArray());//convierto a string el contenido del js
			repdf=ap.sonAccesibles(null, "script", new java.io.File(js).getParent(), jsArchivo, OA);//analiza todo javascript
			re.addAll(repdf);
		}
		
		Object []res=ap.retornaResultados(re);
		pdfA=new Object[2];
		System.out.println(re.size()+" Tamanoooooooooooooo "+res.length);
		if(res.length==3) {
			pdfA[0]="El archivo posee errores de accesibilidad en pdf, siendo estos: "+res[1];
			pdfA[1]=res[2]; 
			VariablesEstaticas.pdfErrors=VariablesEstaticas.pdfErrors+"<li>El archivo "+ruta.substring(ruta.lastIndexOf("/")+1)+" posee errores de accesibilidad en pdf, siendo estos: <ul>"+res[1]+"</ul></li>";
		}else if(res.length==2) {
			pdfA=res;
		}
		}
		//System.out.println(pdfA[0]+" "+pdfA[1]);
		HashMap<String, Object[]> retorna=new HashMap<String, Object[]>();
		if(metas.containsKey("http://www.accesiblemetada.com/DRDSchema#textOnVisual")||metas.containsKey("http://www.accesiblemetada.com/DRDSchema#alternativeText"))
			retorna.put("imagenes", txtOnVisualAlternative);
		if(metas.containsKey("http://www.accesiblemetada.com/DRDSchema#tableOfContents"))
			retorna.put("tablaContenidos",tablaContenidos);
		if(metas.containsKey("http://www.accesiblemetada.com/DRDSchema#largePrint"))
			retorna.put("tamanoFuente",tamanoFunte);
		if(metas.containsKey("http://www.accesiblemetada.com/DRDSchema#MathML"))
			retorna.put("mathML",mathML);
		if(metas.containsKey("http://www.accesiblemetada.com/DRDSchema#ChemML"))
			retorna.put("chemML",cheml);
		if(metas.containsKey("http://www.accesiblemetada.com/DRDSchema#latex"))
			retorna.put("latex",latex);
		if(metas.containsKey("http://www.accesiblemetada.com/DRDSchema#motionSimulation")||metas.containsKey("http://www.accesiblemetada.com/DRDSchema#noMOtionSimulationHazard"))
			retorna.put("simulations", simulations);
		if(metas.containsKey("http://www.accesiblemetada.com/DRDSchema#timingControl"))
			retorna.put("tiempos", ACTiempos);
		if(metas.containsKey("http://www.accesiblemetada.com/DRDSchema#captions"))
			retorna.put("subs", subs);
		if(metas.containsKey("http://www.accesiblemetada.com/DRDSchema#taggedPDF"))
			retorna.put("pdf", pdfA);
		return retorna;
	}
	
	
}
