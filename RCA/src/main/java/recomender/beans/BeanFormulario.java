package recomender.beans;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.atomic.AtomicInteger;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;
import javax.inject.Inject;
import javax.servlet.http.Part;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.omnifaces.util.Faces;
import org.primefaces.PrimeFaces;
import org.primefaces.PrimeFaces.Dialog;
import org.primefaces.context.RequestContext;
import org.primefaces.event.FlowEvent;
import org.primefaces.model.menu.DefaultMenuItem;
import org.primefaces.model.menu.DefaultMenuModel;
import org.primefaces.model.menu.MenuModel;

import recomender.analysis.main.AnalisisContenidos;
import recomender.analysis.main.AnalizaConOntologia;
import recomender.analysis.main.AnalizaContenidos;
import recomender.data.Compress;
import recomender.data.EscribirMicrodatos;
import recomender.data.Upload;
import recomender.staticcontent.VariablesEstaticas;
import recomender.validations.RevisaTitlesHTML;

@ManagedBean
@SessionScoped
public class BeanFormulario {

	//Para el Directorio del OA que se selecciona
	private List<Part> files;//listado de todos los archivos y directorios que se van a subir (cuando selecciono del file manager)
	private String directorioSeleccionado="Seleccionar Directorio y Cargar";//es una cadena que contiene el nombre del dirctorio seleccionado y el numero de archivos del direcotrio
	
	//para el combo de los archivos html
	private List<SelectItem> archivosCombo;//los items que se vana  mostrar en el menuoption
	private String selectedMenu="Seleccione Archivo";//el archivo seleccionado en el selectmenu
	private String rutaArchivoEditando;//la ruta del archivo que se esta editando
	
	//para los checks de cada una de la spreguntas del formulario
	private List<Boolean> checks=new java.util.ArrayList<Boolean>();//listado booleano de los checks de cad apregunta del formulario
	private List<Boolean> checksNoAplica=new java.util.ArrayList<Boolean>();//listado booleano de los checks de cada pregunta de no aplica del formulario
	//auxiliar para saber si ya se cargo el oa
	private String ejecutado="no";
	
	private String seleccionado="No Aplica";
	//path del OA
	private String pathOA;
	
	private List<String>listadoAM=new ArrayList<String>();//listado que contiene las preguntas para AccessMode
	private List<String>listadoAF=new ArrayList<String>();//listado que contiene las preguntas para AccessibilityFeature
	private List<String>listadoMetasAF=new ArrayList<String>();//listado que contiene las los metadatos para AccessibilityFeature 
	
	private String mensaje="Subiendo.....";
	
	private String label;
	
	private String rutaImagenPhanthom;
	private String dirPhantom;
	private String urlArchivo;
	private HashMap<String, List<Boolean>> metasAEscribir;
	private HashMap<String, List<Boolean>> noAplica;
	private MenuModel model;
	private MenuModel modelCA;
	private MenuModel modelCOA;
	private MenuModel modelH;
	private int activeIndex=0;
	
	private HashMap<String, Boolean> analizados;
	/**
	 * Constructor de la clase
	 */
	public BeanFormulario() {
		archivosCombo=new ArrayList<SelectItem>();
		checks = new ArrayList<Boolean>();
		checksNoAplica = new ArrayList<Boolean>();
		listadoAF=new ArrayList<String>();
		listadoMetasAF=new ArrayList<String>();
		listadoAM=new ArrayList<String>();
		metasAEscribir=new HashMap<String,List<Boolean>>();
		noAplica=new HashMap<String,List<Boolean>>();
		analizados=new HashMap<String, Boolean>();
		//llena preguntas
		llenarPreguntas();
		//todas las posiciones del listado van a ser falsas
		resetearChecks(null);
		model = new DefaultMenuModel();
		modelCA = new DefaultMenuModel();
		modelCOA = new DefaultMenuModel();
		modelH = new DefaultMenuModel();
		activeIndex=0;
//		DefaultMenuItem mi=new DefaultMenuItem();
//		mi.setUrl("http://www.google.com");
//		mi.setHref("http://www.google.com");
//		mi.setDisabled(false);
//		
//		
//		model.addElement(mi);
//		//mi.setUrl("http://www.google.com");
//		mi=new DefaultMenuItem();
//		mi.setHref("#stepam-1");
//		mi.setDisabled(false);
//		mi.setOnclick("alert('holap'); document.getElementById('stepam-1').style.display='block';");
//		//mi.setCommand("javascript:alert('hla');");
//		model.addElement(mi);
		llenaSteps();
	}
	/**
	 * Llena el lustado de paso a paso
	 */
	public void llenaSteps() {
		DefaultMenuItem mi;
		for(int i=0;i<listadoAM.size();i++) {
			mi=new DefaultMenuItem();
			mi.setHref("#stepam-"+(i));
			mi.setDisabled(false);
			mi.setId("am-"+(i));
			
			mi.setOnclick("hide();  document.getElementById('stepam-"+(i)+"').style.display='block';");
			model.addElement(mi);
		}
		///caracteristicas de accesibilidad
		for(int i=0;i<listadoAF.size();i++) {
			mi = new DefaultMenuItem();
			mi.setHref("#stepca-"+(i));
			mi.setDisabled(false);
			
			mi.setOnclick("hide(); document.getElementById('stepca-"+(i)+"').style.display='block';");
			modelCA.addElement(mi);
		}
		//accessibility control
		mi = new DefaultMenuItem();
		mi.setHref("#stepcoa-"+(0));
		mi.setDisabled(false);
		
		mi.setOnclick("hide(); document.getElementById('stepcoa-"+(0)+"').style.display='block';");
		modelCOA.addElement(mi);
		
		mi = new DefaultMenuItem();
		mi.setHref("#stepcoa-"+(1));
		mi.setDisabled(false);
		
		mi.setOnclick("hide(); document.getElementById('stepcoa-"+(1)+"').style.display='block';");
		modelCOA.addElement(mi);
		
		//acessibility hazard
		for(int i=0;i<3;i++) {
			mi = new DefaultMenuItem();
			mi.setHref("#stepra-"+(i));
			mi.setDisabled(false);
			
			mi.setOnclick("hide(); document.getElementById('stepra-"+(i)+"').style.display='block';");
			modelH.addElement(mi);
			
		}
	}
	/**
	 * Setea el indice a cero para le sptep by step
	 */
	public void setActiveIndexZero() {
		activeIndex=0;
	}
	/**
	 * Metood que carga los estilos en la interfaz formulario cuando se saltan los pasos.
	 * @param id
	 */
	public void saltar(int id) {
		if(id==0) {
			if(activeIndex==5) {
				PrimeFaces.current().executeScript("hideComplement(2);");
				activeIndex=0;
			}
			else {
				
				activeIndex=activeIndex+1;
				PrimeFaces.current().executeScript("saltar(0,"+activeIndex+");");
			}
		}
		if(id==1) {
			if(activeIndex==13) {
				PrimeFaces.current().executeScript("hideComplement(3);");
				activeIndex=0;
			}
			else {
				activeIndex=activeIndex+1;
				PrimeFaces.current().executeScript("saltar(1,"+activeIndex+");");
			}
		}	
		if(id==2) {
			if(activeIndex==1) {
				PrimeFaces.current().executeScript("hideComplement(4);");
				activeIndex=0;
			}
			else {
				activeIndex=activeIndex+1;
				PrimeFaces.current().executeScript("saltar(2,"+activeIndex+");");
			}
		}	
		if(id==3) {
			//aki le hago q seleccione otro elemento del listado
			if(activeIndex==2)
				activeIndex=0;
			else {
				activeIndex=activeIndex+1;
				PrimeFaces.current().executeScript("saltar(3,"+activeIndex+");");
			}
		}
	}
	/**
	 * Metodo que cambia los estilos al regresar al paso anterior en la interfaz formualrio,
	 * @param id El id a cambiar el estilo.
	 */
	public void atras(int id) {
		if(id==0) {
			if(activeIndex==0)
				activeIndex=0;
			else {
				activeIndex=activeIndex-1;
				PrimeFaces.current().executeScript("anterior(0,"+activeIndex+")");
			}
		}
		if(id==1) {
			if(activeIndex==0) {
				PrimeFaces.current().executeScript("hideComplement(1);");
				activeIndex=0;
			}
			else {
				activeIndex=activeIndex-1;
				PrimeFaces.current().executeScript("anterior(1,"+activeIndex+")");
			}
		}	
		if(id==2) {
			if(activeIndex==0) {
				PrimeFaces.current().executeScript("hideComplement(2);");
				activeIndex=0;
			}
			else {
				activeIndex=activeIndex-1;
				PrimeFaces.current().executeScript("anterior(2,"+activeIndex+")");
			}
		}	
		if(id==3) {
			//aki le hago q seleccione otro elemento del listado
			if(activeIndex==0) {
				PrimeFaces.current().executeScript("hideComplement(3);");
				activeIndex=0;
			}
			else {
				activeIndex=activeIndex-1;
				PrimeFaces.current().executeScript("anterior(3,"+activeIndex+")");
			}
		}
	}
	public List<Part> getFiles() {
		return files;
	}
	public void setFiles(List<Part> files) {
		this.files = files;
	}
	public String getDirectorioSeleccionado() {
		return directorioSeleccionado;
	}
	public void setDirectorioSeleccionado(String directorioSeleccionado) {
		this.directorioSeleccionado = directorioSeleccionado;
	}
	public String getEjecutado() {
		return ejecutado;
	}
	public void setEjecutado(String ejecutado) {
		this.ejecutado = ejecutado;
	}
	public List<SelectItem> getArchivosCombo() {
		return archivosCombo;
	}
	public void setArchivosCombo(List<SelectItem> archivosCombo) {
		this.archivosCombo = archivosCombo;
	}
	public String getSelectedMenu() {
		return selectedMenu;
	}
	public void setSelectedMenu(String selectedMenu) {
		this.selectedMenu = selectedMenu;
	}
	public List<Boolean> getChecks() {
		return checks;
	}
	public void setChecks(List<Boolean> checks) {
		this.checks = checks;
	}
	
	public List<String> getListadoAF() {
		return listadoAF;
	}
	public void setListadoAF(List<String> listadoAF) {
		this.listadoAF = listadoAF;
	}
	
	public String getMensaje() {
		return mensaje;
	}
	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
	
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}
	
	
	public String getRutaImagenPhanthom() {
		return rutaImagenPhanthom;
	}
	public void setRutaImagenPhanthom(String rutaImagenPhanthom) {
		this.rutaImagenPhanthom = rutaImagenPhanthom;
	}
	
	public String getUrlArchivo() {
		return urlArchivo;
	}
	public void setUrlArchivo(String urlArchivo) {
		this.urlArchivo = urlArchivo;
	}
	
	public List<String> getListadoMetasAF() {
		return listadoMetasAF;
	}
	public void setListadoMetasAF(List<String> listadoMetasAF) {
		this.listadoMetasAF = listadoMetasAF;
	}
	
	public List<String> getListadoAM() {
		return listadoAM;
	}
	public void setListadoAM(List<String> listadoAM) {
		this.listadoAM = listadoAM;
	}
	
	
	public MenuModel getModel() {
		return model;
	}
	public void setModel(MenuModel model) {
		this.model = model;
	}
	
	public int getActiveIndex() {
		return activeIndex;
	}
	public void setActiveIndex(int activeIndex) {
		this.activeIndex = activeIndex;
	}
	
	public MenuModel getModelCA() {
		return modelCA;
	}
	public void setModelCA(MenuModel modelCA) {
		this.modelCA = modelCA;
	}
	
	public MenuModel getModelCOA() {
		return modelCOA;
	}
	public void setModelCOA(MenuModel modelCOA) {
		this.modelCOA = modelCOA;
	}
	public MenuModel getModelH() {
		return modelH;
	}
	public void setModelH(MenuModel modelH) {
		this.modelH = modelH;
	}
	
	public String getSeleccionado() {
		return seleccionado;
	}
	public void setSeleccionado(String seleccionado) {
		this.seleccionado = seleccionado;
	}
	
	public HashMap<String, List<Boolean>> getMetasAEscribir() {
		return metasAEscribir;
	}
	public void setMetasAEscribir(HashMap<String, List<Boolean>> metasAEscribir) {
		this.metasAEscribir = metasAEscribir;
	}
	
	public HashMap<String, List<Boolean>> getNoAplica() {
		return noAplica;
	}
	public void setNoAplica(HashMap<String, List<Boolean>> noAplica) {
		this.noAplica = noAplica;
	}
	/**
	 * Metodo que llena e la lista las preguntas para la categoria AccessibilityFeature del formulario.
	 */
	public void llenarPreguntas() {
		//AccessMode
		listadoAM=new ArrayList<String>();
		listadoAM.add("¿Dispone de contenido auditivo?");
		listadoAM.add("¿Dispone de contenido tactil?");
		listadoAM.add("¿Dispone de contenido textual?");
		listadoAM.add("¿Dispone de contenido visual?");
		listadoAM.add("¿Existen actividades que requieran la distinción de colores y contrastes?");
		listadoAM.add("¿Hay texto en la imagen, que puede no ser legible a escalas mayores debido a la pixelación, o por el software de texto a voz?");
		
		//Accessibility Feature
		listadoAF=new ArrayList<String>();
		listadoAF.add("¿Se brindan opciones de alto contraste?");
		listadoAF.add("¿Recurso está formateado para lectura de letra grande?");
		listadoAF.add("¿El contenido matemático está disponible en formato laTeX?");
		listadoAF.add("¿Dispone de una estructura básica o  tabla de contenidos?");
		listadoAF.add("¿Al incrustar imagenes no decorativas se ha descrito en texto su contenido?");
		listadoAF.add("¿Al incrustar imágenes o videos  se ha considerado  incorporar  una transcripción y descripción larga?");
		listadoAF.add("¿Los videos empleados tienen sutitulado?");
		listadoAF.add("¿Los videos empleados tienen audiodescripción?");
		listadoAF.add("¿Los videos empleados tienen adaptación para interprete en lengua de señas?");
		listadoAF.add("¿Las fórmulas químicas empleadas son texto y no imágenes?");
		listadoAF.add("¿Las fórmulas matemáticas empleadas son texto y no imágenes?");
		listadoAF.add("¿Si una actividad es controlada por tiempos, existe la posibilidad de modificación (habilitar o no)?");
		listadoAF.add("¿La navegación en el objeto de aprendizaje tiene una estructura  organizada e intuitiva?");
		listadoAF.add("¿Los archivos PDF empleados  tiene un orden de lectura y su accesibilidad fue comprobada?");
		
		///metas
		listadoMetasAF=new ArrayList<String>();
		listadoMetasAF.add("high Contrast Display");
		listadoMetasAF.add("large Print");
		listadoMetasAF.add("latex");
		listadoMetasAF.add("table Of Contents");
		listadoMetasAF.add("alternative Text");
		listadoMetasAF.add("long Description");
		listadoMetasAF.add("captions");
		listadoMetasAF.add("audio Description");
		listadoMetasAF.add("sign Language");
		listadoMetasAF.add("ChemML");
		listadoMetasAF.add("MathML");
		listadoMetasAF.add("timing Control");
		listadoMetasAF.add("structural Navigation");
		listadoMetasAF.add("tagged PDF");
		
	
	}
	@PostConstruct
	public void init() {
		resetearChecks(null);
	}
	/**
	 * Metodo que setea los valores del array de checks, todos a false excepto los del index
	 * @param index Es el listado de index de las posiciones en las que va a ser verdadero 
	 */
	public void resetearChecks(int []index) {
		checks=new java.util.ArrayList<Boolean>();
		checksNoAplica=new java.util.ArrayList<Boolean>();
		for(int i=0;i<25;i++) {
				checks.add(false);
				checksNoAplica.add(true);
		}
		if(index!=null) {
			for(int ind:index) {
				checks.set(ind, true);
			}
		}
	}
	/**
	 * Metodo que setea los valores de si, no o no aplica, de acuerdo a como seleccione el usuario
	 * @param ind Es el indice donde se va a setear el array
	 */
	public void seteaFalse(int ind) {
		System.out.println(seleccionado);
		if(seleccionado.toLowerCase().equals("si")) {
			checks.set(ind, true);
			checksNoAplica.set(ind, false);
		}
		if(seleccionado.toLowerCase().equals("no")) {
			checks.set(ind, false);
			checksNoAplica.set(ind, false);
		}
		if(seleccionado.toLowerCase().equals("no aplica")) {
			checksNoAplica.set(ind, true);
			checks.set(ind, false);
		}
		System.out.println("checcks "+checks.get(ind)+" "+checksNoAplica.get(ind));
		
		////en el hash map inserto los arraysy luego voy seteando en este metodo
	}
	private int incrementa=0;
	/**
	 * Metodo que carga al servidor el OA
	 */
	public void upload() {
		startTask();
		Upload upload=new Upload();
		FacesContext context=FacesContext.getCurrentInstance();//la instancia actual, es decir la ruta actualdonde se encuentra el proyecto en el servidor
		incrementa=20;
		List<Object> lista=upload.subirOA(context, files);//envio a cargar el oA en el servidor y guardo en la lista su retorno
		incrementa=80;
		if(lista!=null) {//si el listado es diferente de nulo
			File rutaOAs=(File)lista.get(0);//recupero el dorectorio de todos los OAs en formato File
			String rutaOA=(String)lista.get(1);//recupero la ruta del OA
			List<String> rutasHTML=(List<String>)lista.get(2);//recupero el listato de todas las rutas de los archivos html
			List<String> rutasJS=(List<String>)lista.get(3);
			List<String> rutasCSS=(List<String>)lista.get(4);
			System.out.println("Ruta Directorio: "+rutaOAs.getAbsolutePath());
			System.out.println("Ruta OA: "+rutaOA);
			System.out.println("Numero HTMLs: "+rutasHTML.size());
			System.out.println("Numero JS: "+rutasJS.size());
			System.out.println("Numero CSS: "+rutasCSS.size());	
			pathOA=rutaOA;
			//lleno los items con las rutas y nombres de archivos
			archivosCombo=new ArrayList<SelectItem>();
			
			for(String ruta: rutasHTML) {
				RevisaTitlesHTML rt=new RevisaTitlesHTML();
				String title=rt.retornaTitulo(ruta);
				if(title!=null&&!title.isEmpty())
					archivosCombo.add(new SelectItem(ruta, title));
				else
					archivosCombo.add(new SelectItem(ruta, FilenameUtils.getName(ruta)));
			}
			
		}
		incrementa=100;
		PrimeFaces.current().executeScript("cerrarCargando();");
		ejecutado="si";
		//return "formulario?faces-redirect=true";
		//return "#";
	}
	/**
	 * Metodo que agrega los datos ya analizados en el hashmap par ano volver a analizar.
	 */
	public void agregaEnHashMap() {
		boolean bandera=false;
		List<Boolean> aux=new java.util.ArrayList<Boolean>(checks);;
		if(checks!=null) {
			aux=new java.util.ArrayList<Boolean>(checks);
			int cont=0;
			for(boolean check:aux) {
				System.out.println("CHECKS DIFERENTE DE NULL "+check);
				if(check) {
					bandera=true;
					checksNoAplica.set(cont, false);
				}
				cont++;
			}
		}
		if(bandera) {
			System.out.println(metasAEscribir.size());
			metasAEscribir.put(rutaArchivoEditando, aux);
			noAplica.put(rutaArchivoEditando, checksNoAplica);
		}
	}
	/**
	 * Metodo que se ejecutara cuando selecciono un item del combo box, recorrera los itemas para saber 
	 * cual fue el seleccionado y seteara la ruta del archivo ediitando extrayendo del array rutasHTML
	 */
	public void archivoSeleccionado() {		
//		for(int i=0;i<archivosCombo.size();i++) {//recorro los items del combo
//			if(archivosCombo.get(i).getValue().equals(selectedMenu)) {//pregunto si el item que recorro es igual al item seleccionado
//				rutaArchivoEditando=rutasHtml.get(i);//digo que el archivo que estoy editando es el que esta en la posicion i de las rutasHTML (items y rutas mismo tamanio)
//			}
//		
	//	}
		PrimeFaces.current().executeScript("$.blockUI({ message: null });");
		for(boolean chec:checks) {
			System.out.print(" "+chec+" ");
		}
		agregaEnHashMap();
		resetearChecks(null);
//		System.out.println(//"Ruta editando "+);
		rutaArchivoEditando=selectedMenu;
		Upload up=new Upload();
		String directorioImagenes=pathOA+"/phantom_images";
		dirPhantom=directorioImagenes;
		String dir=up.escribe(directorioImagenes);
		for (SelectItem selectItem : archivosCombo) {
			if(selectItem.getValue().equals(selectedMenu)) {
				label=selectItem.getLabel();
			}
		}
		String rutaImagen=dir+"/"+label+".png";
		FacesContext context= FacesContext.getCurrentInstance();
		
		up.captura(rutaArchivoEditando, rutaImagen);
		//rutaImagen="uploads/"+pathOA.substring(pathOA.lastIndexOf("/"+1))+""+rutaImagen.lastIndexOf(ch)
		String rutaGeneral="";
		try {
			ClassLoader cl = Thread.currentThread().getContextClassLoader();
			Properties p = new Properties();
			p.load(cl.getResourceAsStream("paths.properties"));
			rutaGeneral = p.getProperty("uploads");
			//System.out.println("Este es el path de phantom: "+p.getProperty("phantomjs"));
		}catch(Exception e) {
				e.printStackTrace();
		}
		try {
			//leo la imagen:
			java.io.File file=new java.io.File(rutaImagen);
			FileInputStream f=new FileInputStream(file);
			//elimino el oa analizado
			byte [] img= new byte[(int)file.length()];
			f.read(img);
			String encode=new String(Base64.encodeBase64(img),"UTF-8");
			rutaImagenPhanthom="data:image/png+xml;utf8;base64,"+encode;
		}catch(Exception e) {
			e.printStackTrace();
		}
		//rutaImagen=rutaImagen.substring(rutaImagen.indexOf("RCA.war")+8);//corto la cadena para que me tome desde el proyect en adelante el path (ej: /folder1/forlder2/imagen.png) asi no me va a tomar dese la raiz(ej:/home/user/documents/....)
		//rutaImagenPhanthom=rutaImagen;
		//urlArchivo=rutaArchivoEditando.substring(rutaArchivoEditando.indexOf("RCA.war")+8);
//		urlArchivo="http://www.observatorioweb.ups.edu.ec/"+rutaArchivoEditando.substring(rutaArchivoEditando.indexOf("tesis"));
//		rutaImagenPhanthom = "http://www.observatorioweb.ups.edu.ec/"+rutaImagen.substring(rutaImagen.indexOf("tesis"));
		urlArchivo="http://www.observatorioweb.ups.edu.ec/"+rutaArchivoEditando.substring(rutaArchivoEditando.indexOf("tesis"));
		rutaImagenPhanthom = "http://www.observatorioweb.ups.edu.ec/"+rutaImagen.substring(rutaImagen.indexOf("tesis"));
		System.out.println("Ruta Archivo Editando: "+rutaArchivoEditando+" label: "+label+" dir: "+dir);
		if(!analizados.containsKey(rutaArchivoEditando)) {
			analizaYSeteaChecks();
			analizados.put(rutaArchivoEditando, true);
		}
		//setear en caso de que ya contenga
		if(metasAEscribir.size()>0) {
			if(metasAEscribir.containsKey(rutaArchivoEditando)) {
				checks=metasAEscribir.get(rutaArchivoEditando);
				checksNoAplica=noAplica.get(rutaArchivoEditando);
				
				String siNo="";
				String noA="";
				for(int i=0;i<checks.size();i++) {
					siNo+=checks.get(i)+";";
					noA+=checksNoAplica.get(i)+";";
				}
				siNo=siNo.substring(0,siNo.lastIndexOf(";"));
				noA=noA.substring(0,noA.lastIndexOf(";"));
				PrimeFaces.current().executeScript("setStyleRadioChecked('"+siNo+"', '"+noA+"')");
			}
			else {
				String siNo="";
				String noA="";
				for(int i=0;i<checks.size();i++) {
					siNo+=checks.get(i)+";";
					if(checks.get(i))
						noA+="false;";
					else
						noA+=checksNoAplica.get(i)+";";
				}
				siNo=siNo.substring(0,siNo.lastIndexOf(";"));
				noA=noA.substring(0,noA.lastIndexOf(";"));
				//PrimeFaces.current().executeScript("cerrarCargando2()");
				PrimeFaces.current().executeScript("setStyleRadioChecked('"+siNo+"', '"+noA+"')");
			}
		}
		///TRABAJO FUTURO===> MOSTRAR LOS CHECKS YA SELECICONADOS DE LSO QUE SI CONTIENEN AL ANALIZAR DE FORMA AUTOMATICA
		else {
			String siNo="";
			String noA="";
			for(int i=0;i<checks.size();i++) {
				siNo+=checks.get(i)+";";
				if(checks.get(i))
					noA+="false;";
				else
					noA+=checksNoAplica.get(i)+";";
			}
			siNo=siNo.substring(0,siNo.lastIndexOf(";"));
			noA=noA.substring(0,noA.lastIndexOf(";"));
			
			PrimeFaces.current().executeScript("setStyleRadioChecked('"+siNo+"', '"+noA+"')");
		}
	}
	/**
	 * Metodo de prueba para setear los checks y verificar su funcionamiento.. no se usa.
	 */
	public void setea() {
		System.out.println("entraaaaaaaaaaaaaaaaaaaaaaaaaaaaa "+checks.get(0));
		checks.set(0, true);
		System.out.println("entraaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa "+checks.get(0));
	}
	/**
	 * Metodo que imprime los datos e invoca al metod de setear los datos analizados en el hashmap.
	 */
	public void imprimir() {
		agregaEnHashMap();
		for(boolean b:checks) {
			System.out.println(b);
		}
		for(Map.Entry<String, List<Boolean>> entry:metasAEscribir.entrySet()) {
			System.out.print(entry.getKey()+"====>");
			for(boolean val: entry.getValue()) {
				System.out.print(val+" ");
			}
			System.out.println();
		}
	}
	/**
	 * Metodo que invoca al metodo que escribe los metdatos en los archivos html.
	 */
	public void escribirMetadatos() {
		
		String title="Error al Intentar Escribir Metadato!!";
		String body="Ya se han escrito los datos en este OA, debe presionar en <b>Seleccionar nuevo OA</b> y escoger un objeto de aprendizaje para volver a escibirlo!!";
		System.out.println("RITA ARCHIVO EDITANDOOOOOO: "+rutaArchivoEditando);
		if(rutaArchivoEditando==null||rutaArchivoEditando.isEmpty()|| rutaArchivoEditando.equals("Seleccione Archivo")) {//si no ha seleccionado ningun archivo
			PrimeFaces.current().executeScript("showModalErrors('"+title+"','"+body+"');");
			System.out.println("Entraaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");
		}else {
			archivoSeleccionado();
			for(Map.Entry<String, List<Boolean>> list:metasAEscribir.entrySet()) {
				
			
			EscribirMicrodatos em=new EscribirMicrodatos();
			java.util.ArrayList<String> microdatos=em.retornaListaMetas();
			List<Boolean> noA=noAplica.get(list.getKey());
			boolean [] noAp=new boolean[] {noA.get(noA.size()-3),noA.get(noA.size()-2),noA.get(noA.size()-1)};
			if(em.escribriMicrodatos(list.getKey(), microdatos, list.getValue(),true,noAp)) {
				System.out.println("Se escribio");
				
//				if(archivosCombo.remove(FilenameUtils.getName(rutaArchivoEditando))) {
//					System.out.println("se eliminoooooooooooooooooo");
//				}else {
//					System.out.println("no se elimino");
//				}
				
				}
			
			}
			
			download();
			//actualizaDespEscribir();
			resetearChecks(null);
//			try {
//				Faces.redirect("formulario_metas ? faces-redirect=true");
//			}catch(IOException e) {
//				e.printStackTrace();
//			}
//			title="Exito al escribir metadatos!";
//			body="Se han escrito los metadatos en el archivo seleccionado de forma exitosa.";
//			PrimeFaces.current().executeScript("showModalErrors('"+title+"','"+body+"');");
			
		}
		
	}
	/**
	 * Quita la ruta del archivo esditado y su correspondiente item del combo
	 */
	public void actualizaDespEscribir() {
//		for(int i=0;i<archivosCombo.size();i++) {
//			
//			if(rutaArchivoEditando.equals(archivosCombo.get(i).getValue())||archivosCombo.get(i).getValue().equals(FilenameUtils.getName(rutaArchivoEditando))){
//				//rutasHtml.remove(i);//elimina la ruta completa del archivo que ya se edito..
//				archivosCombo.remove(i);//elimina el nombre del archivo que se edito en el combo box
//				
//			}
//		}
		archivosCombo.clear();
		metasAEscribir.clear();
		analizados.clear();
		noAplica.clear();
		selectedMenu="Seleccione Archivo";
		rutaArchivoEditando="Seleccione Archivo";
		pathOA="";
		rutaImagenPhanthom="";
		urlArchivo="";
	}
	/**
	 * Trabajo Futuro: Metodo que selecciona el sguiente documento html
	 */
	public void siguienteArchivo() {
		for(int i=0;i<archivosCombo.size();i++) {
			
			if(rutaArchivoEditando.equals(archivosCombo.get(i).getValue())||archivosCombo.get(i).getValue().equals(FilenameUtils.getName(rutaArchivoEditando))){
				//rutasHtml.remove(i);//elimina la ruta completa del archivo que ya se edito..
				//archivosCombo.remove(i);//elimina el nombre del archivo que se edito en el combo box
				if((i+1)<archivosCombo.size()) {
				this.selectedMenu=archivosCombo.get(i+1).getLabel();
				System.out.println("selected: "+selectedMenu);
				PrimeFaces.current().executeScript("selecciona("+(i+1)+");");
				}
			}
		}
	}
	/**
	 * Metodo que invoca al metodo que comprime los archivos y descarga el objeto de aprendizaje analizdo.
	 */
	public void download() {
		try {
			FacesContext context=FacesContext.getCurrentInstance();
			Compress comp=new Compress();
			FileUtils.deleteDirectory(new java.io.File(dirPhantom));
			if(pathOA!=null) {
			String rutaComprimido=comp.comprimir(context, pathOA);//COmprime el archivo y obtiene la ruta del archivo comprimido
				if(rutaComprimido!=null) {
					File f=new File(rutaComprimido);
					Faces.sendFile(f, true);//descarga el archivo como attachment (attachment=true)
					//elimino la carpeta del oa
					
					FileUtils.deleteDirectory(new java.io.File(pathOA));
					FileUtils.deleteQuietly(new java.io.File(rutaComprimido));
					actualizaDespEscribir();
					
					
				}else {
					String title="Error Al Intentar Descargar!!";
					String body="No se ha encontrado ningun OA para descargar!!";
					PrimeFaces.current().executeScript("showModalErrors('"+title+"','"+body+"');");
				}
			}else {
				String title="Error Al Intentar Descargar!!";
				String body="No se ha encontrado ningun OA para descargar!!";
				PrimeFaces.current().executeScript("showModalErrors('"+title+"','"+body+"');");
			}
		}catch(Exception e) {
			e.printStackTrace();
			String title="Error Al Intentar Descargar!!";
			String body="Se ha producido un error durante la compresion del archivo o durante la descarga!!";
			PrimeFaces.current().executeScript("showModalErrors('"+title+"','"+body+"');");
		}
	}
	/**
	 * Metodo que s eejecuta cuando se selecciona un archivo html, este metodo realiza el analisis del archivo y 
	 * setea en la interfaz formulario los metadatos existentes.
	 */
	public void analizaYSeteaChecks() {
		AnalizaContenidos ac=new AnalizaContenidos();
		HashMap<String, String >metas=returnHashMapTodosMetas();
		HashMap<String, Object[]>resultados=ac.analyzeContents(rutaArchivoEditando,new java.io.File(pathOA).getName(), metas);
		java.util.ArrayList<Integer> indexTrue=new java.util.ArrayList<Integer>();
		if((boolean)resultados.get("imagenes")[1]) {
			indexTrue.add(10);//texto Alternativo
		}
		if((boolean)resultados.get("imagenes")[3]) {
			indexTrue.add(5);//textonVisual
		}
		if((boolean)resultados.get("tablaContenidos")[1]) {
			indexTrue.add(9);//tabla de COntenidos
		}
		if((boolean)resultados.get("tamanoFuente")[1]) {
			indexTrue.add(7);//tamanoFuente
		}
		if((boolean)resultados.get("mathML")[1]) {
			indexTrue.add(16);//mathML
		}
		if((boolean)resultados.get("chemML")[1]) {
			indexTrue.add(15);//ChemML
		}
		if((boolean)resultados.get("latex")[1]) {
			indexTrue.add(8);//Latex
		}
		if((boolean)resultados.get("simulations")[1]) {
			indexTrue.add(23);//simulaciones
		}
		if((boolean)resultados.get("tiempos")[1]) {
			indexTrue.add(17);//tiempos
		}
		if((boolean)resultados.get("subs")[1]) {
			indexTrue.add(12);//Captions
		}
		if((boolean)resultados.get("pdf")[1]){
			indexTrue.add(19);
		}
		//Otras preguntas
		AnalisisContenidos anc=new AnalisisContenidos();
		HashMap<String, Object[]>resultados2=anc.llamarPreguntas(rutaArchivoEditando, metas);
		if((boolean)resultados2.get("textual")[1]) {
			indexTrue.add(2);//Texual
		}
		if((boolean)resultados2.get("visual")[1]) {
			indexTrue.add(3);//Visual
		}
		if((boolean)resultados2.get("auditiva")[1]) {
			indexTrue.add(0);//Auditiva
		}
		if((boolean)resultados2.get("tactil")[1]) {
			indexTrue.add(1);//Tactil
		}
		if((boolean)resultados2.get("mouse")[1]) {
			indexTrue.add(21);//Mouse
		}
		if((boolean)resultados2.get("teclado")[1]) {
			indexTrue.add(20);//Teclado
		}
		if((boolean)resultados2.get("audioDescripcion")[1]) {
			indexTrue.add(13);//audioDescription
		}
		if((boolean)resultados2.get("senas")[1]) {
			indexTrue.add(14);//Lenguaje de Senas
		}
		if((boolean)resultados2.get("longDesc")[1]) {
			indexTrue.add(11);//LongDescription
		}
		if((boolean)resultados2.get("sound")[1]) {
			indexTrue.add(24);//Sound
		}
		if((boolean)resultados2.get("movimiento")[1]) {
			indexTrue.add(22);//Animaciones
		}
		if((boolean)resultados2.get("colorDependent")[1]) {
			indexTrue.add(4);//ColorDependent
		}
		int []indexes=new int[indexTrue.size()];
		int cont=0;
		for(int i:indexTrue) {
			indexes[cont]=i;
			cont++;
		}
		resetearChecks(indexes);//mando a resetear los checks poniendo true a los indices
		PrimeFaces.current().executeScript("cerrarCargando();");
	}
	/**
	 * NO SE USA: Metodo que carga el archivo pdf a un servidor externo para su posterior analisis
	 */
	public void sube() {
		String archivo="/home/kike/Documents/how to manage wifi capabilities android studio.pdf";
		
		FTPClient con=null;
		try {
			con= new FTPClient();
			//con.setConnectTimeout(5000);
			//con.connect("198.23.48.87");
			con.connect("accesibilidadecuador.com");
			System.out.println("sale");
			if (con.login("paoingavelez", "auquilla2018","paoingavelez.us.tempcloudsite.com")) {
				System.out.println("Logueado");
				con.enterLocalPassiveMode(); // important!//para atravesar el firewall
	            con.setFileType(FTP.BINARY_FILE_TYPE);
	            FileInputStream in = new FileInputStream(new File(archivo));
	           // System.out.println(con.dele("test/test1/test11"));
	            con.removeDirectory("test/test1/test11");
	            
	            con.makeDirectory("accesibilidadecuador.com/test");
	            boolean result = con.storeFile("accesibilidadecuador.com/test/how to manage wifi capabilities android studio.pdf", in);
	            in.close();
                if (result) System.out.println("upload result succeeded");
                con.logout();
                con.disconnect();
			}
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
	/**
	 * Metodo que torna a los valores por default.
	 */
	public void resetearTodo() {
		directorioSeleccionado="Seleccionar Directorio y Cargar";
		archivosCombo=new ArrayList<SelectItem>();
		llenarPreguntas();
		resetearChecks(null);
	}
	public String onFlowProcess(FlowEvent event) {
            return event.getNewStep();
    }
	 private AtomicInteger progressInteger = new AtomicInteger();
	  private ExecutorService executorService;
	  /**
	   * Metodo que corre un proceso para mostrar el progreso de la barra de progreso
	   */
	  public void startTask() {
	      executorService = Executors.newSingleThreadExecutor();
	      //executorService.execute(this.startLongTask);
	      executorService.execute(new Runnable() {
			
			@Override
			public void run() {
				// TODO Auto-generated method stub
				startLongTask();
			}
		});
//	      for(int i=0;i<10000000;i++) {
//	      System.out.println("Imprimeeeeeeee");
//	      }
	  }
	  /**
	   * Meotodo que actualiza el porcentaje en la barra de progreso.
	   */
	  private void startLongTask() {
	      progressInteger.set(0);
//	      for (int i = 0; i < 100; i++) {
//	          //progressInteger.getAndIncrement();
//	          progressInteger.getAndSet(incrementa);
//	          //simulating long running task
//	          try {
//	        	  System.out.println("Imprimeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee");
//	              Thread.sleep(ThreadLocalRandom.current().nextInt(1, 100));
//	              
//	          } catch (InterruptedException e) {
//	              e.printStackTrace();
//	          }
//	      }
	      boolean bandera=false;
	      while(!bandera) {
	    	  progressInteger.getAndSet(VariablesEstaticas.incrementaF);
	    	  //System.out.println(VariablesEstaticas.incrementaF);
//	    	  if(VariablesEstaticas.incrementaF>=30){
//	    		  System.out.println("impremeeeeeeeeeeeeeeee "+VariablesEstaticas.incrementaF);
//	    	  }
//	    	  if(VariablesEstaticas.incrementaF>=60){
//	    		  System.out.println("impremeeeeeeeeeeeeeeee "+VariablesEstaticas.incrementaF);
//	    	  }
//	    	  if(VariablesEstaticas.incrementaF>=80){
//	    		  System.out.println("impremeeeeeeeeeeeeeeee "+VariablesEstaticas.incrementaF);
//	    	  }
	    	  if(VariablesEstaticas.incrementaF>=100){
	    		  progressInteger.getAndSet(100);
	    		  bandera=true;
	    	  }
	      }
	      executorService.shutdownNow();
	      progressInteger.set(0);
	      VariablesEstaticas.incrementaF=0;
	      executorService = null;
	  }

	  public int getProgress() {
	      return progressInteger.get();
	  }

	  public String getResult() {
	      return progressInteger.get() == 100 ? "task done" : "";
	  }
	  /**
	   * Metodo que devuelve un hashmap con todos los iris de metadatos
	   * @return Retorna un HashMap con los iris d elos metas
	   */
	  public HashMap<String , String> returnHashMapTodosMetas(){
		  HashMap<String, String> metas=new HashMap<String, String>();
		  metas.put("http://www.accesiblemetada.com/DRDSchema#auditory","http://www.accesiblemetada.com/DRDSchema#auditory");
		  metas.put("http://www.accesiblemetada.com/DRDSchema#ChemML","http://www.accesiblemetada.com/DRDSchema#ChemML");
		  metas.put("http://www.accesiblemetada.com/DRDSchema#latex","http://www.accesiblemetada.com/DRDSchema#latex");
		  metas.put("http://www.accesiblemetada.com/DRDSchema#visual","http://www.accesiblemetada.com/DRDSchema#visual");
		  metas.put("http://www.accesiblemetada.com/DRDSchema#textOnVisual","http://www.accesiblemetada.com/DRDSchema#textOnVisual");
		  metas.put("http://www.accesiblemetada.com/DRDSchema#colorDependent","http://www.accesiblemetada.com/DRDSchema#colorDependent");
		  metas.put("http://www.accesiblemetada.com/DRDSchema#sound","http://www.accesiblemetada.com/DRDSchema#sound");
		  metas.put("http://www.accesiblemetada.com/DRDSchema#flashing","http://www.accesiblemetada.com/DRDSchema#flashing");
		  metas.put("http://www.accesiblemetada.com/DRDSchema#noFlashingHazard","http://www.accesiblemetada.com/DRDSchema#noFlashingHazard");
		  metas.put("http://www.accesiblemetada.com/DRDSchema#audioDescription","http://www.accesiblemetada.com/DRDSchema#audioDescription");
		  metas.put("http://www.accesiblemetada.com/DRDSchema#noSoundHazard","http://www.accesiblemetada.com/DRDSchema#noSoundHazard");
		  metas.put("http://www.accesiblemetada.com/DRDSchema#structuralNavigation","http://www.accesiblemetada.com/DRDSchema#structuralNavigation");
		  metas.put("http://www.accesiblemetada.com/DRDSchema#longDescription","http://www.accesiblemetada.com/DRDSchema#longDescription");
		  metas.put("http://www.accesiblemetada.com/DRDSchema#largePrint","http://www.accesiblemetada.com/DRDSchema#largePrint");
		  metas.put("http://www.accesiblemetada.com/DRDSchema#highContrastDisplay","http://www.accesiblemetada.com/DRDSchema#highContrastDisplay");
		  metas.put("http://www.accesiblemetada.com/DRDSchema#motionSimulation","http://www.accesiblemetada.com/DRDSchema#motionSimulation");
		  metas.put("http://www.accesiblemetada.com/DRDSchema#timingControl","http://www.accesiblemetada.com/DRDSchema#timingControl");
		  metas.put("http://www.accesiblemetada.com/DRDSchema#alternativeText","http://www.accesiblemetada.com/DRDSchema#alternativeText");
		  metas.put("http://www.accesiblemetada.com/DRDSchema#captions","http://www.accesiblemetada.com/DRDSchema#captions");
		  metas.put("http://www.accesiblemetada.com/DRDSchema#MathML","http://www.accesiblemetada.com/DRDSchema#MathML");
		  metas.put("http://www.accesiblemetada.com/DRDSchema#taggedPDF","http://www.accesiblemetada.com/DRDSchema#taggedPDF");
		  metas.put("http://www.accesiblemetada.com/DRDSchema#signLanguage","http://www.accesiblemetada.com/DRDSchema#signLanguage");
		  metas.put("http://www.accesiblemetada.com/DRDSchema#noMotionSimulationHazard","http://www.accesiblemetada.com/DRDSchema#noMotionSimulationHazard");
		  metas.put("http://www.accesiblemetada.com/DRDSchema#tableOfContents","http://www.accesiblemetada.com/DRDSchema#tableOfContents");
		  metas.put("http://www.accesiblemetada.com/DRDSchema#textual","http://www.accesiblemetada.com/DRDSchema#textual");
		  metas.put("http://www.accesiblemetada.com/DRDSchema#tactile","http://www.accesiblemetada.com/DRDSchema#tactile");
		  metas.put("http://www.accesiblemetada.com/DRDSchema#fullKeyboardControl","http://www.accesiblemetada.com/DRDSchema#fullKeyboardControl");
		  metas.put("http://www.accesiblemetada.com/DRDSchema#fullMouseControl","http://www.accesiblemetada.com/DRDSchema#fullMouseControl");
		  return metas;
	  }
}
