package recomender.beans;
import java.io.File;
import java.io.FileInputStream;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.print.attribute.standard.PrinterMoreInfoManufacturer;
import javax.servlet.http.Part;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.FileUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.omnifaces.util.Ajax;
import org.primefaces.PrimeFaces;

import recomender.analysis.main.AnalisisContenidos;
import recomender.analysis.main.AnalizaContenidos;
import recomender.data.Upload;
import recomender.datamanipulation.ComparaDatos;
import recomender.staticcontent.VariablesEstaticas;

@ManagedBean
@SessionScoped
public class BeanSeleccion {
	boolean visual;//check discapacidad visual
	boolean auditiva;//check discapacidad auditiva
	boolean cognitiva;//check discapacidad cognitiva
	boolean fisicaMotriz;//check discapacidad fisica/Motriz
	boolean psicosocial;//Check Psicosocial
	String todas;
	//Para el Directorio del OA que selecciona
	private List<Part> files;//listado de todos los archivos y directorios que se van a subir (cuando selecciono del file manager)
	private String directorioSeleccionado="Seleccionar Directorio y Analizar";//es una cadena que contiene el nombre del dirctorio seleccionado y el numero de archivos del direcotrio
	
	//PARA LA RECOMENDACION
	private String titulo;
	private String numeroHTML;
	private String numeroCSS;
	private String numeroJS;
	private String numeroImagenes;
	private String numeroVideos;
	private String fechaHora;
	
	private String descripcionGeneral;
	private String descripcionGeneralN;
	private String descripcionGeneralF;
	private String consideracionesGenerales;
	private HashMap<String, String> recomendacion=new HashMap<String, String>();
	
	private String imagenBase64;
	
	/**
	 * COnstructor de la clase
	 */
	public BeanSeleccion() {
		visual=true;
		auditiva=false;
		cognitiva=false;
		fisicaMotriz=false;
		psicosocial=false;
		todas="Todas";
		resetearTodo();
		System.out.println("Resetea");
		//todas=false;
		try {
		ClassLoader cl = Thread.currentThread().getContextClassLoader();
		Properties p = new Properties();
		p.load(cl.getResourceAsStream("paths.properties"));
		
		System.out.println("Este es el path de phantom: "+p.getProperty("phantomjs"));
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
	public boolean isVisual() {
		return visual;
	}
	public void setVisual(boolean visual) {
		this.visual = visual;
	}
	public boolean isAuditiva() {
		return auditiva;
	}
	public void setAuditiva(boolean auditiva) {
		this.auditiva = auditiva;
	}
	public boolean isCognitiva() {
		return cognitiva;
	}
	public void setCognitiva(boolean cognitiva) {
		this.cognitiva = cognitiva;
	}
	public boolean isFisicaMotriz() {
		return fisicaMotriz;
	}
	public void setFisicaMotriz(boolean fisicaMotriz) {
		this.fisicaMotriz = fisicaMotriz;
	}
	public boolean isPsicosocial() {
		return psicosocial;
	}
	public void setPsicosocial(boolean psicosocial) {
		this.psicosocial = psicosocial;
	}
	
	public String getTodas() {
		return todas;
	}
	public void setTodas(String todas) {
		this.todas = todas;
	}
	
	/*public void imprimeValores() {
	
		System.out.println("Esta es todas: "+todas);
		if(todas.equals("Todas")) {
			setVisual(true);
			setAuditiva(true);
			setCognitiva(true);
			setFisicaMotriz(true);
			setPsicosocial(true);
			setTodas("Ninguna");
		}else {
			setVisual(false);
			setAuditiva(false);
			setCognitiva(false);
			setFisicaMotriz(false);
			setPsicosocial(false);
			setTodas("Todas");
		}
		System.out.println("visual: "+isVisual()+" auditiva: "+isAuditiva()+" cognitiva: "+isCognitiva()+" fisica: "+isFisicaMotriz()+" psicosocial: "+isPsicosocial());
	}*/
	
	public HashMap<String, String> getRecomendacion() {
		return recomendacion;
	}
	public String getDescripcionGeneralN() {
		return descripcionGeneralN;
	}
	public void setDescripcionGeneralN(String descripcionGeneralN) {
		this.descripcionGeneralN = descripcionGeneralN;
	}
	public String getDescripcionGeneralF() {
		return descripcionGeneralF;
	}
	public void setDescripcionGeneralF(String descripcionGeneralF) {
		this.descripcionGeneralF = descripcionGeneralF;
	}
	public void setRecomendacion(HashMap<String, String> recomendacion) {
		this.recomendacion = recomendacion;
	}
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public String getNumeroHTML() {
		return numeroHTML;
	}
	public void setNumeroHTML(String numeroHTML) {
		this.numeroHTML = numeroHTML;
	}
	public String getNumeroCSS() {
		return numeroCSS;
	}
	public void setNumeroCSS(String numeroCSS) {
		this.numeroCSS = numeroCSS;
	}
	public String getNumeroJS() {
		return numeroJS;
	}
	public void setNumeroJS(String numeroJS) {
		this.numeroJS = numeroJS;
	}
	public String getNumeroImagenes() {
		return numeroImagenes;
	}
	public void setNumeroImagenes(String numeroImagenes) {
		this.numeroImagenes = numeroImagenes;
	}
	public String getNumeroVideos() {
		return numeroVideos;
	}
	public void setNumeroVideos(String numeroVideos) {
		this.numeroVideos = numeroVideos;
	}
	public String getFechaHora() {
		return fechaHora;
	}
	public void setFechaHora(String fechaHora) {
		this.fechaHora = fechaHora;
	}
	public String getDescripcionGeneral() {
		return descripcionGeneral;
	}
	public void setDescripcionGeneral(String descripcionGeneral) {
		this.descripcionGeneral = descripcionGeneral;
	}
	public List<Part> getFiles() {
		return files;
	}
	public void setFiles(List<Part> files) {
		this.files = files;
	}
	
	public String getDirectorioSeleccionado() {
		return directorioSeleccionado;
	}
	public void setDirectorioSeleccionado(String directorioSeleccionado) {
		this.directorioSeleccionado = directorioSeleccionado;
	}
	
	public String getConsideracionesGenerales() {
		return consideracionesGenerales;
	}
	public void setConsideracionesGenerales(String consideracionesGenerales) {
		this.consideracionesGenerales = consideracionesGenerales;
	}
	
	public String getImagenBase64() {
		return imagenBase64;
	}
	public void setImagenBase64(String imagenBase64) {
		this.imagenBase64 = imagenBase64;
	}
	/**
	 * Imprime el total de archivos seleccionados
	 */
	public void imprime() {
		System.out.println("visual: "+visual+" auditiva: "+auditiva+" cognitiva: "+cognitiva+" fisica: "+fisicaMotriz+" psicosocial: "+psicosocial);
		
		///IMPRIMO ARCHIVOS
		if(files!=null) {
			System.out.println(files.size());
		}
		
	}
	private HashMap<String, String> metadatos=new HashMap<String, String>();
	/**
	 * Metodo que llama al otros metodos para cargar el Objeto de Aprendizaje en el servidor y analizaerlo.
	 */
	public void upload() {
		//PrimeFaces.current().executeScript("mostrarCargando();");
		System.out.println("Incio");
		startTask();
		Upload upload=new Upload();
		FacesContext context=FacesContext.getCurrentInstance();//la instancia actual, es decir la ruta actualdonde se encuentra el proyecto en el servidor
		List<Object> lista=upload.subirOA(context, files);//envio a cargar el oA en el servidor y guardo en la lista su retorno
		if(lista!=null) {//si el listado es diferente de nulo
			File rutaOAs=(File)lista.get(0);//recupero el dorectorio de todos los OAs en formato File
			String rutaOA=(String)lista.get(1);//recupero la ruta del OA
			List<String> rutasHTML=(List<String>)lista.get(2);//recupero el listato de todas las rutas de los archivos html
			List<String> rutasJS=(List<String>)lista.get(3);
			List<String> rutasCSS=(List<String>)lista.get(4);
			System.out.println("Ruta Directorio: "+rutaOAs.getAbsolutePath());
			System.out.println("Ruta OA: "+rutaOA);
			System.out.println("Numero HTMLs: "+rutasHTML.size());
			System.out.println("Numero JS: "+rutasJS.size());
			System.out.println("Numero CSS: "+rutasCSS.size());
			int cont = 1;
			//NECESITO LAS DISCPACIDADES QUE SE HAN SELECCIONADO
			String []discapacidades=getSelectedDisabilities();
			//agrego los prefijos a las discapacidades
			for(int i=0; i<discapacidades.length;i++) {
				discapacidades[i]="sch:"+discapacidades[i];
			}
			HashMap<String, String> metas=aco.retornaMetasByDisability(discapacidades);
			metadatos=metas;
			for(Map.Entry<String, String> m:metas.entrySet()) {
				System.out.println(m.getKey()+"     "+m.getValue());
			}
			numeroHTML=rutasHTML.size()+"";
			for (String rutaHTML : rutasHTML) {
				System.out.println("Analizando "+rutaHTML);
				muestra(rutaHTML, rutaOA);
				VariablesEstaticas.incrementaFA=((cont*50)/rutasHTML.size())+30;//contador es el numero de archivo analizado, 70 es q va a ser el 70%maximo dividido pa el num de htm  mas el 30% faltant
				cont++;
			}
			//luego de analizar, necesito inferir
			ArrayList<String> listadoHTML=(ArrayList<String>) rutasHTML;
			if(listadoHTML!=null && !listadoHTML.isEmpty()) {
			inferirOntologia(listadoHTML, rutaOA);
			}
			titulo=rutaOA.substring(rutaOA.lastIndexOf("/")+1);
			numeroHTML=rutasHTML.size()+"";
			numeroCSS=rutasCSS.size()+"";
			numeroJS=rutasJS.size()+"";
			numeroImagenes=VariablesEstaticas.numImagenes+"";
			numeroVideos=VariablesEstaticas.numVideos+"";
			
			DateFormat df=new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
			fechaHora=df.format(new Date());
			///CREO EL DIRECTORIO DE IMAGEN DE PHANTOM, CREO AL IMAGEN Y LA GUARDO EN UN ARRAY DE BYTES, LUEGO CONVIERTO A BASE 64
			String directorioImagenes=rutaOA+"/phantom_images";
			String dirPhantom= upload.escribe(directorioImagenes);
			
			//busco el index, caso contrario pongo cualquier html
			String label="none";
			for(String ruta:rutasHTML) {
				if(ruta.contains("index.html")) {
					label=ruta;
				}
			}
			if(label.equals("none")&& rutasHTML!=null && !rutasHTML.isEmpty()) {//en caso de que no exista index, tomo a cualquier html para hacer la captura de la imagen
				label=rutasHTML.get(0);
			}
			if(!label.equals("none")) {
			String rutaImagen=dirPhantom+"/"+label+".png";//creo la ruta de la imagen
			upload.captura(label, rutaImagen);//envio a hacer la captura de la imagen
			System.out.println("Saleeeeeeeeeeeeeeeeee");
			
			try {
				//leo la imagen:
				java.io.File file=new java.io.File(rutaImagen);
				FileInputStream f=new FileInputStream(file);
				//elimino el oa analizado
				byte [] img= new byte[(int)file.length()];
				f.read(img);
				String encode=new String(Base64.encodeBase64(img),"UTF-8");
				imagenBase64=encode;
				FileUtils.deleteDirectory(new java.io.File(rutaOA));
				
			}catch(Exception e) {
				e.printStackTrace();
			}
			VariablesEstaticas.incrementaFA=VariablesEstaticas.incrementaFA+3;
			}
		}
		System.out.println("Fin");
		PrimeFaces.current().executeScript("window.location.replace('resultados.jsf');");
	}
	private HashMap<String, HashMap<String,Object[]>> resultadoTotal=new HashMap<String, HashMap<String,Object[]>>();
	/**
	 * Metodo que analiza cada el contenido de cada uno de los archivos necesarios del Objeto de Aprendizaje.
	 * @param rutaArchivo Es la ruta del archivo html.
	 * @param oa Es la ruta del objeto de Aprendizaje.
	 */
	public void muestra(String rutaArchivo, String oa) {
		
//		try {
//	          TimeUnit.SECONDS.sleep(5);//tiempo que va a estar el progress bar
//	          
//	      } catch (InterruptedException e) {
//	          e.printStackTrace();
	    
		//String rutaArchivo="/home/kike/Documents/TESIS/OASAnalizados/1-1 OA Sistemas de Numeracion profudizacion/sistema_numrico_decimal.html";
		AnalisisContenidos ac1=new AnalisisContenidos();
		HashMap<String, Object[]>analisis2=ac1.llamarPreguntas(rutaArchivo, metadatos);
		AnalizaContenidos ac=new AnalizaContenidos();
		HashMap<String, Object[]>analisis1=ac.analyzeContents(rutaArchivo, new java.io.File(oa).getName(), metadatos);
		//agrego en el hashmap todos los resultados de lso hashmap anteriores
		HashMap<String, Object[]>aux=new HashMap<String, Object[]>();
		for(Map.Entry<String, Object[]> entry: analisis2.entrySet()){
			aux.put(entry.getKey(), entry.getValue());
		}
		for(Map.Entry<String, Object[]> entry: analisis1.entrySet()){
			aux.put(entry.getKey(), entry.getValue());
		}
		resultadoTotal.put(rutaArchivo, aux);
		//comparaMetadatos(rutaArchivo, analisis1, analisis2);
		//PrimeFaces.current().executeScript("cerrarCargando();");
		//}
	}
	private String message="Subiendo y Analizando...";
	
	
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	/**
	 * Metodo que invoca al metodo de carga y analisis del Objeto de Aprendizaje.
	 * Tambien cierra la interfaz de cargando cuando haya finalizado la carga y analisis.
	 */
	public void analizaCOntenidos() {
		//PrimeFaces.current().executeScript("mostrarCargando();");
		//muestra();
		//message="subido";
		//Ajax.update("message");
		//PrimeFaces.current().ajax().update("message");
		upload();
		//message="Completado";
		//PrimeFaces.current().ajax().update("message");
		//Ajax.update("message");
		PrimeFaces.current().executeScript("cerrarCargando(); window.location.replace(resultados.jsf)");	
		//return "resultados";
	}
	/**
	 * Metodo que extrae los metadatos que no se van a aplicar en el analisis.
	 * @return Retorna un arreglo de los metadatos que no se van a aplicar en el analisis.
	 */
	public String[] retornaMetasNoAplica() {
		java.util.ArrayList<String> listadoNoAplica=new java.util.ArrayList<String>();
		//if(VariablesEstaticas.numConTactil>0 && (numeroCSS==null || numeroCSS.isEmpty())) {
			listadoNoAplica.add("sch:highContrastDisplay");
		//}
		if(VariablesEstaticas.numPalabras<=0) {
			listadoNoAplica.add("sch:largePrint");
			//listadoNoAplica.add("sch:latex");
			//listadoNoAplica.add("sch:MathML");
			//listadoNoAplica.add("sch:ChemML");
		}
		if(VariablesEstaticas.numlatex<=0) {
			listadoNoAplica.add("sch:latex");
		}
		if(VariablesEstaticas.numChem<=0) {
			listadoNoAplica.add("sch:ChemML");
		}
		if(VariablesEstaticas.numFormulasMatematicas<=0) {
			listadoNoAplica.add("sch:MathML");
		}
		if(VariablesEstaticas.numImagenes==0) {
			listadoNoAplica.add("sch:alternativeText");
			listadoNoAplica.add("sch:textOnVisual");
		}
		if(VariablesEstaticas.numVideos==0) {
			listadoNoAplica.add("sch:captions");
			listadoNoAplica.add("sch:audioDescription");
			listadoNoAplica.add("sch:signLanguage");
		}
		if(VariablesEstaticas.numImagenes==0 &&VariablesEstaticas.numVideos==0) {
			listadoNoAplica.add("sch:longDescription");
		}
		if(VariablesEstaticas.numVideos>0) {
			listadoNoAplica.add("sch:audioDescription");
			listadoNoAplica.add("sch:signLanguage");
		}
		if(VariablesEstaticas.numPDFs==0) {
			listadoNoAplica.add("sch:taggedPDF");
		}
//		if(VariablesEstaticas.numContAuditivo<=0) {
//			listadoNoAplica.add("sch:noSoundhazard");
//		}else {
//			listadoNoAplica.add("sch:sound");
//		}
//		if(VariablesEstaticas.numSimulaciones>0) {
//			listadoNoAplica.add("sch:noMotionSimulationHazard");
//		}else {
//			listadoNoAplica.add("sch:motionSimulation");
//		}
//		if(VariablesEstaticas.numAnimaciones>0) {
//			listadoNoAplica.add("sch:noFlashingHazard");
//		}else {
//			listadoNoAplica.add("sch:flashing");
//		}
		if(VariablesEstaticas.numPalabras<=0) {
			listadoNoAplica.add("sch:colorDependent");
		}
		listadoNoAplica.add("sch:audioDescription");
		listadoNoAplica.add("sch:signLanguage");
		listadoNoAplica.add("sch:timingControl");
		listadoNoAplica.add("sch:structuralNavigation");
		listadoNoAplica.add("sch:flashing");
		listadoNoAplica.add("sch:noFlashingHazard");
		listadoNoAplica.add("sch:motionSimulation");
		listadoNoAplica.add("sch:noMotionSimulationHazard");
		
		String [] data=new String[listadoNoAplica.size()];
		System.out.println("VECTOR STRING CON LOS METAS QUE NO SE VAN A CONSIDERAR:");
		for(int i=0;i<listadoNoAplica.size();i++) {
			data[i]=listadoNoAplica.get(i);
			System.out.println(data[i]);
		}
		return data;
	}
	private HashMap<String, String> resultadosGenerales=new HashMap<String, String>();
	private ArrayList<HashMap<String, String[]>> resultadosTotalesDetalles=new ArrayList<HashMap<String, String[]>>();
	
	public HashMap<String, String> ordena(HashMap<String, Double> d){
		HashMap<String, String> ordenado= new HashMap<String, String>();
		java.util.ArrayList<String> l=new java.util.ArrayList<String>();
		for(Map.Entry<String,Double> en:d.entrySet()) {
			l.add(en.getKey());
			l.add(en.getValue()+"");
		}
		for(int i=1;i<l.size();i+=2) {
			double val=Double.parseDouble(l.get(i));
			for(int j=i;j<l.size();j+=2) {
				double val2=Double.parseDouble(l.get(j));
				if(val2>val) {
					String disc=l.get(i-1);
					l.set(i-1, l.get(j-1));
					l.set(i, val2+"");
					l.set(j-1, disc);
					l.set(j, val+"");
				}
			}
		}
		for(int i=1;i<l.size();i+=2) {
			ordenado.put(l.get(i-1), l.get(i));
			System.out.println(l.get(i));
		}
		System.out.println("Este es el hash ordenado que debe servidor");
		for(Map.Entry<String,String> en:ordenado.entrySet()) {
			System.out.println("Key: "+en.getKey()+" Val: "+en.getValue());
		}
		return ordenado;
	}
	@Inject
	recomender.analysis.main.AnalizaConOntologia aco;
	/**
	 * Metodo que nvoca a cad auno de los metodos que interactuan con la ontologia
	 * Incluye: inferir documentos y los querys necesarios para el resultado final, asi como
	 * tambien formar las cadenas de los resultados finales que se van a mostrar en la interfaz resultados.
	 * @param rutasHTML Es el listado de todas las rutas de los archivos HTML
	 * @param rutaOA Es la ruta del Objeto de Aprendizaje.
	 */
	public void inferirOntologia(java.util.ArrayList<String> rutasHTML, String rutaOA) {
		java.util.ArrayList resp=aco.inferirDocumentos(rutasHTML, rutaOA, resultadoTotal,true, metadatos);
		
		///IMPRIME RESULTADOS DE METAS
		for(Map.Entry<String , HashMap<String, Object[]>> l:resultadoTotal.entrySet()) {
			for(Map.Entry<String , Object[]> ent: l.getValue().entrySet()) {
				//System.out.println(ent.getKey()+" ========  "+ent.getValue()[0]+"  ========  "+ent.getValue()[1]);
			}
		}
		HashMap<String, java.util.ArrayList<String>> metasEnDocs=(HashMap<String, java.util.ArrayList<String>>)resp.get(0);
		String iri=(String)resp.get(1);
		String label=(String)resp.get(2);
		if(iri!=null&&!iri.isEmpty()) {
			String data[]=retornaMetasNoAplica();
			java.util.ArrayList resultado=aco.enviaArchivoInferir(getSelectedDisabilities(), iri, label,data, numeroHTML);
			//ArrayList<String [][]> listadoPorDocs=(ArrayList<String [][]>)resultado.get(0);
			HashMap<String, Double> porcentajeTotal=(HashMap<String, Double>)resultado.get(0);
			//ordena(porcentajeTotal);
			//resultadosGenerales=new HashMap<String, Double>(porcentajeTotal);
			//rduzco el tamano:
			for(Map.Entry<String, Double> d:porcentajeTotal.entrySet()) {
				String disc="";
				if(d.getKey().contains("Auditory"))
					disc="Auditiva";
				if(d.getKey().contains("Cognitive"))
					disc="Cognitiva";
				if(d.getKey().contains("Physical"))
					disc="Física";
				if(d.getKey().contains("Visual"))
					disc="Visual";
				if(d.getKey().contains("Psychosocial"))
					disc="Psicosocial";
				resultadosGenerales.put(disc, new DecimalFormat("###.00").format(d.getValue())+"%");
				//resultadosGenerales.put(d.getKey().substring(d.getKey().lastIndexOf("#")+1, d.getKey().length()), new DecimalFormat("###,00%").format(d.getValue()));
				
			}
			//HashMap<String, Double> total=aco.returnPorcentajesTotales(porcentajeTotal, porcentajeTotal.size());
			//imprimo porcentajes totales
			System.out.println("porcentajes");
			imprimePorcentajesTotales(porcentajeTotal);
			setea(porcentajeTotal);
			//ordeno porcentajes para mostrar en la reocmendacion 
			String [][] paraOrdenar=new String [porcentajeTotal.size()][2];
			int row=0;
			for(Map.Entry<String, Double> entry: porcentajeTotal.entrySet()) {
				paraOrdenar[row][0]=entry.getKey();
				paraOrdenar[row][1]=entry.getValue()+"";
				row++;
			}
			for(int i=0;i<paraOrdenar.length;i++) {
				
				for(int j=i;j<paraOrdenar.length;j++) {
					
					if(Double.parseDouble(paraOrdenar[i][1])<Double.parseDouble(paraOrdenar[j][1])) {
						String aux0=paraOrdenar[i][0];
						String aux1=paraOrdenar[i][1];
						paraOrdenar[i][0]=paraOrdenar[j][0];
						paraOrdenar[i][1]=paraOrdenar[j][1];
						paraOrdenar[j][0]=aux0;
						paraOrdenar[j][1]=aux1;
					}
				}
			}
			///PARA LA DESCRIPCION GENERAL ADICIONO LO QUE NO ESTA CUMPLIENDO
			if(paraOrdenar[0][0].contains("Auditory"))
				paraOrdenar[0][0]="Auditiva";
			if(paraOrdenar[0][0].contains("Cognitive"))
				paraOrdenar[0][0]="Cognitiva";
			if(paraOrdenar[0][0].contains("Physical"))
				paraOrdenar[0][0]="Física";
			if(paraOrdenar[0][0].contains("Visual"))
				paraOrdenar[0][0]="Visual";
			if(paraOrdenar[0][0].contains("Psychosocial"))
				paraOrdenar[0][0]="Psicosocial";
			descripcionGeneral="<p class='infor'>Se ha encontrado que el Objeto de Aprendizaje presenta un <b>"+new DecimalFormat("###.00").format(Double.parseDouble(paraOrdenar[0][1]))+""
					+ "%</b> de accesibilidad para la discapacidad <b>"+paraOrdenar[0][0]+"</b>. "
							+ "Expanda los contenidos para ver mayores detalles y recomendaciones. En la parte derecha se encuentran los datos estadísticos "
					+ "del análisis asociado a las discapacidades seleccionadas, así como también una gráfica  comparativa de su accesibilidad.</p>"
							+ "";
			String []res=resultados(resultadoTotal);
			descripcionGeneralN+="<h3>Modos de Acceso</h3>";
			descripcionGeneralN+="<p><i>Describe la forma en la que una persona puede procesar o percibir la información.</i></p>";
			if(res[1]!=null && !res[1].isEmpty())
				descripcionGeneralN+="<ul>"+res[1]+"</ul>";
			else
				descripcionGeneralN+="<ul>No se han encontrado contenidos no accesibles en esta categoria.</ul>";
			descripcionGeneralN+="<h3>Caracteristicas de Accesibilidad</h3>";
			descripcionGeneralN+="<p><i>Describe las características del contenido del recurso, tales como medios accesibles, alternativas y mejoras admitidas para la accesibilidad.</i></p>";
			if(res[3]!=null && !res[3].isEmpty())
				descripcionGeneralN+="<ul>"+res[3]+"</ul>";
			else
				descripcionGeneralN+="<ul>No se han encontrado contenidos no accesibles en esta categoria.</ul>";
			descripcionGeneralN+="<h3>Control de Accesibilidad</h3>";
			descripcionGeneralN+="<p><i>Describe los métodos de entrada que son suficientes para controlar completamente el recurso descrito, tales como teclado y raton.</i></p>";
			if(res[5]!=null && !res[5].isEmpty())
				descripcionGeneralN+="<ul>"+res[5]+"</ul>";
			else
				descripcionGeneralN+="<ul>No se han encontrado contenidos no accesibles en esta categoria.</ul>";
			descripcionGeneralN+="<h3>Riesgo de Accesibilidad</h3>";
			descripcionGeneralN+="<p><i>Describe las características del recurso que pueden ser fisiológicamente peligrosas para algunos usuarios.</i></p>";
			if(res[7]!=null && !res[7].isEmpty())
				descripcionGeneralN+="<ul>"+res[7]+"</ul>";
			else
				descripcionGeneralN+="<ul>No se han encontrado contenidos no accesibles en esta categoria.</ul>";
			
			///sine rrores
			descripcionGeneralF+="<h3>Modos de Acceso</h3>";
			descripcionGeneralF+="<p><i>Describe la forma en la que una persona puede procesar o percibir la información.</i></p>";
			if(res[0]!=null && !res[0].isEmpty())
				descripcionGeneralF+="<ul>"+res[0]+"</ul>";
			else
				descripcionGeneralF+="<ul>No se han encontrado contenidos accesibles en esta categoria.</ul>";
			descripcionGeneralF+="<h3>Caracteristicas de Accesibilidad</h3>";
			descripcionGeneralF+="<p><i>Describe las características del contenido del recurso, tales como medios accesibles, alternativas y mejoras admitidas para la accesibilidad.</i></p>";
			if(res[2]!=null && !res[2].isEmpty())
				descripcionGeneralF+="<ul>"+res[2]+"</ul>";
			else
				descripcionGeneralF+="<ul>No se han encontrado contenidos accesibles en esta categoria.</ul>";
			descripcionGeneralF+="<h3>Control de Accesibilidad</h3>";
			descripcionGeneralF+="<p><i>Describe los métodos de entrada que son suficientes para controlar completamente el recurso descrito, tales como teclado y raton.</i></p>";
			if(res[4]!=null && !res[4].isEmpty())
				descripcionGeneralF+="<ul>"+res[4]+"</ul>";
			else
				descripcionGeneralF+="<ul>No se han encontrado contenidos accesibles en esta categoria.</ul>";
			descripcionGeneralF+="<h3>Riesgo de Accesibilidad</h3>";
			descripcionGeneralF+="<p><i>Describe las características del recurso que pueden ser fisiológicamente peligrosas para algunos usuarios.</i></p>";
			if(res[6]!=null && !res[6].isEmpty())
				descripcionGeneralF+="<ul>"+res[6]+"</ul>";
			else
				descripcionGeneralF+="<ul>No se han encontrado contenidos accesibles en esta categoria.</ul>";
			
			consideracionesGenerales+="<h3>Se recomienda tomar en cuenta las siguientes observaciones:</h3>";
			consideracionesGenerales+="<ul>"+res[8]+"</ul>";
			//descripcionGeneral.replaceAll("\n", "</p><p>");
			//System.out.println("########################################################################");
			//imprimePorcentajesTotales(total);
		//	FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("porcentajes", porcentajeTotal);
			//ELIMINO DUPLICADOS Y SACO PORCENTAJES DE LOS QUE YA ESTAN DUPLICADOS
			//DEBO RECORRER LOS METAS DE CADA DOC Y VER CUALES METAS ESTAN EN CADA DOC
			ComparaDatos cd =new ComparaDatos();
			HashMap<String, Double> metasPorcentajes=cd.eliminaDuplicados(metasEnDocs);
			//imprimePorcentajesTotales(metasPorcentajes);
			///AQUI MANDO A LLAMAR AL QUERY PARA QUE RETORNE LOS RESULTADOS 
			String favoreceDesfavorece="?Discapacidad_Favorecida";
			String []discapacidadesSeleccionadas=getSelectedDisabilities();
			java.util.ArrayList<String[][]> resTieneNoTiene=aco.resultadoFinal(metasPorcentajes, discapacidadesSeleccionadas, favoreceDesfavorece, data);//llamo al metodo que devuelve la matriz de datos con metas y wcag q favorecen a las discapacidades seleccionadas
			String [][] resFinalFavorece=resTieneNoTiene.get(0);
			//System.out.println("FINAL");
//			for(int i=0;i<resFinalFavorece.length;i++) {
//				System.out.println(resFinalFavorece[i][0]+" "+resFinalFavorece[i][1]+" "+resFinalFavorece[i][2]+" "+resFinalFavorece[i][3]+" "+resFinalFavorece[i][4]);
//			}
			HashMap<String, Object[]> recomendacionFavorece=new HashMap<String, Object[]>();//hash map donde se guardaran los criterios que cumpel acorde a la discapacidad.
			for(String discapacidad: discapacidadesSeleccionadas) {
				java.util.ArrayList<String> listado=new java.util.ArrayList<String>();
				for(int i=0;i<resFinalFavorece.length;i++) {
					if(resFinalFavorece[i][3].contains(discapacidad)) {
//						System.out.println("Entraaaaa: "+resFinalFavorece[i][3]+" "+discapacidad+" "+resFinalFavorece[i][2]);
//						if(!recomendacionFavorece.containsKey(discapacidad)) {//si esque no contiene la discapacidad la agrego
//							String criterioExito="Criterio de Exito"+resFinalFavorece[i][1].substring(resFinalFavorece[i][1].indexOf("_")+1);
//							recomendacionFavorece.put(discapacidad, new String[] {criterioExito+":\n"+resFinalFavorece[i][2]});
//						}else {//si esque ya contiene la discpacidad, recupero lo q ya esta agregado en el hashmap y adiciono el nuevo criterio de exito
//							String criterioExito="Criterio de Exito"+resFinalFavorece[i][1].substring(resFinalFavorece[i][1].indexOf("_")+1);
//							String []contiene=recomendacionFavorece.get(discapacidad);
//							String []aux=new String[contiene.length+1];
//							for(int j=0;i<contiene.length;j++) {
//								aux[j]=contiene[j];
//							}
//							aux[aux.length-1]=criterioExito+":\n"+resFinalFavorece[i][2];//agrego el criterio con su descripcion de wcag
//							recomendacionFavorece.put(discapacidad, aux);
//						}
						//String aux[] = new String[dat.length+1];
						String criterioExito="<div class=\"alert bg-light\"><li><h5><strong>Criterio de Éxito "+resFinalFavorece[i][1].substring(resFinalFavorece[i][1].indexOf("_")+1)+":</strong></h5>";
						listado.add(criterioExito+"<p>"+resFinalFavorece[i][2]+"</p></li></div>");
					}
				}
				if(discapacidad.contains("Auditory"))
					discapacidad="Auditiva";
				if(discapacidad.contains("Cognitive"))
					discapacidad="Cognitiva";
				if(discapacidad.contains("Physical"))
					discapacidad="Fisica";
				if(discapacidad.contains("Visual"))
					discapacidad="Visual";
				if(discapacidad.contains("Psychosocial"))
					discapacidad="Psicosocial";
				recomendacionFavorece.put(discapacidad, listado.toArray());
			}
			///IMPRIMO LOS FAVORECE A CADA DISCAPACIDAD
			for(Map.Entry<String, Object[]> entry:recomendacionFavorece.entrySet()) {
				System.out.println(entry.getKey());
				String recom="";
				
				Object []criterios=entry.getValue();
				if(criterios.length==0) {
					recom= "<div class=\"alert bg-light\"><p>No se han encontrado <b>Criterios de Éxito</b> que se cumplan para esta discapacidad.</p></div>";
					//recomendacion.put(entry.getKey(), recom);
				}else {
					recom= "<h4>Se estan cumpliendo los siguientes <b>Criterios de Extito</b> para esta discapacidad:</h4><ul>";
				}
				for(Object criterio:criterios) {
					System.out.println(criterio);
					recom+=criterio;
				}
				//recom.replaceAll("\n", "&lt;br/&gt;"); //reemplazo los saltos de linea por br
				recomendacion.put(entry.getKey(), recom+"</u>");
				System.out.println("===============================================");
			}
			//VariablesEstaticas.incrementaFA=VariablesEstaticas.incrementaFA+3;
//			///AFECTA
//			favoreceDesfavorece="?Discapacidad_Afectada";
//			java.util.ArrayList<String [][]>resAfecta=aco.resultadoFinal(metasPorcentajes, discapacidadesSeleccionadas,favoreceDesfavorece);//llamo al metodo que devuelve la matriz de datos con metas y wcag q favorecen a las discapacidades seleccionadas
//			String [][] resFinalAfecta=resAfecta.get(0);
//			HashMap<String, Object[]> recomendacionAfecta=new HashMap<String, Object[]>();//hash map donde se guardaran los criterios que cumpel acorde a la discapacidad.
//			for(String discapacidad: discapacidadesSeleccionadas) {
//				java.util.ArrayList<String> listado=new java.util.ArrayList<String>();
//				for(int i=0;i<resFinalAfecta.length;i++) {
//					if(resFinalAfecta[i][4].contains(discapacidad)) {
//						String criterioExito="Criterio de Exito"+resFinalAfecta[i][1].substring(resFinalAfecta[i][1].indexOf("_")+1);
//						listado.add(criterioExito+":\n"+resFinalAfecta[i][2]);
//					}
//				}
//				recomendacionAfecta.put(discapacidad, listado.toArray());
//			}
//			///IMPRIMO LOS AFECTA A CADA DISCAPACIDAD
//			//System.out.println("AFECTAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA");
//			for(Map.Entry<String, Object[]> entry:recomendacionAfecta.entrySet()) {
//				//System.out.println(entry.getKey());
//				Object []criterios=entry.getValue();
//				for(Object criterio:criterios) {
//					//System.out.println(criterio);
//				}
//				//System.out.println("===============================================");
//			}
//			
//			
//			
//			String [][]resNotieneAfecta=resAfecta.get(1);
//			
//			HashMap<String, Object[]> recomendacionAfectaNo=new HashMap<String, Object[]>();//hash map donde se guardaran los criterios que cumpel acorde a la discapacidad.
//			for(String discapacidad: discapacidadesSeleccionadas) {
//				java.util.ArrayList<String> listado=new java.util.ArrayList<String>();
//				for(int i=0;i<resNotieneAfecta.length;i++) {
//					if(resNotieneAfecta[i][4].contains(discapacidad)) {
//						String criterioExito="Criterio de Exito"+resNotieneAfecta[i][1].substring(resNotieneAfecta[i][1].indexOf("_")+1);
//						listado.add(criterioExito+":\n"+resNotieneAfecta[i][2]);
//					}
//				}
//				recomendacionAfectaNo.put(discapacidad, listado.toArray());
//			}
//			///IMPRIMO LOS AFECTA A CADA DISCAPACIDAD
//			//System.out.println("AFECTAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA NOOOOOOOOOOOOOOOOOOOOOOOO");
//			for(Map.Entry<String, Object[]> entry:recomendacionAfectaNo.entrySet()) {
//				//System.out.println(entry.getKey());
//				Object []criterios=entry.getValue();
//				for(Object criterio:criterios) {
//					//System.out.println(criterio);
//				}
//			//	System.out.println("===============================================");
//			}
//			
//			String [][]noTiene=resTieneNoTiene.get(1);
//			if(noTiene!=null) {
//				HashMap<String, Object[]> recomendacionNoTiene=new HashMap<String, Object[]>();//hash map donde se guardaran los criterios que cumpel acorde a la discapacidad.
//				for(String discapacidad: discapacidadesSeleccionadas) {
//					java.util.ArrayList<String> listado=new java.util.ArrayList<String>();
//					for(int i=0;i<noTiene.length;i++) {
//						if(noTiene[i][4]!=null) {
//						if(noTiene[i][4].contains(discapacidad)) {
//							String criterioExito="Criterio de Exito"+noTiene[i][1].substring(noTiene[i][1].indexOf("_")+1);
//							listado.add(criterioExito+":\n"+noTiene[i][2]);
//						}
//						}
//					}
//					recomendacionNoTiene.put(discapacidad, listado.toArray());
//				}
//				//IMPRIMO LOS FAVORECE QUE NO TIENE A CADA DISCAPACIDAD
//				//System.out.println("NOOOOOOOOOOOOOOOOOO TIENEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE");
//				for(Map.Entry<String, Object[]> entry:recomendacionNoTiene.entrySet()) {
//					//System.out.println(entry.getKey());
//					Object []criterios=entry.getValue();
//					for(Object criterio:criterios) {
//						//System.out.println(criterio);
//					}
//					//System.out.println("===============================================");
//				}
//			}
			System.out.println("Variable "+VariablesEstaticas.numIm);
			
		}
	}
	/**
	 * Metodo que revisa  retorna en ingles(idioma en la que esta en la ontologia) 
	 * todas las discapacidades seleccionadas. 
	 * @return Retorna un listado de las discapacidades seleccionadas traducidas al ingles.
	 */
	public String[] getSelectedDisabilities() {
		java.util.ArrayList<String> discapacidadesSeleccionadas=new java.util.ArrayList<String>();
		if(visual)
			discapacidadesSeleccionadas.add("Visual");
		if(auditiva)
			discapacidadesSeleccionadas.add("Auditory");
		if(cognitiva)
			discapacidadesSeleccionadas.add("Cognitive");
		if(fisicaMotriz)
			discapacidadesSeleccionadas.add("Physical");
		if(psicosocial)
			discapacidadesSeleccionadas.add("Psychosocial");
		String []dSelected=new String[discapacidadesSeleccionadas.size()];
		for(int i=0;i<dSelected.length;i++) {
			dSelected[i]=discapacidadesSeleccionadas.get(i);
		}
		return dSelected;
	}
	/**
	 * Metodo que imprime los porcentajes totales
	 * @param porcents HashMap de tipo String, Double de los porcentajes totales.
	 */
	public void imprimePorcentajesTotales(HashMap<String, Double> porcents){
		HashMap<String, Double> retorna= new HashMap<String, Double>();
		for(Map.Entry<String, Double> entry:porcents.entrySet()) {
			System.out.println(entry.getKey()+" "+entry.getValue());
		}
	}
	public HashMap<String, String> getResultadosGenerales() {
		return resultadosGenerales;
	}
	public void setResultadosGenerales(HashMap<String, String> resultadosGenerales) {
		this.resultadosGenerales = resultadosGenerales;
	}
	/**
	 * Metodo que redirecciona a la pagina resultados.
	 * @return Retorna el nombre de la pagina resultados.
	 */
	public String test() {
		System.out.println("Entraaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");
		PrimeFaces.current().executeScript("cerrarCargando();");
		return "resultados";
	}
	private String keys="";
	private String vals="";
	
	
	public String getKeys() {
		return keys;
	}
	public void setKeys(String keys) {
		this.keys = keys;
	}
	public String getVals() {
		return vals;
	}
	public void setVals(String vals) {
		this.vals = vals;
	}
	/**
	 * Metodo que crea dos String, una con todas las discapacidades separadas por comas y,
	 * otro con todos los valores correspondientes a esas discapacidades para graficar.
	 * @param porcentajes Es un hashmap con las discapacidades y sus respectivos porcentajes.
	 */
	public void setea(HashMap<String, Double> porcentajes) {
		keys="";
		vals="";
		DecimalFormat d=new DecimalFormat("#,###.00");//para dos decimales
		for(Map.Entry<String, Double> entry: porcentajes.entrySet()) {
			String disc="";
			if(entry.getKey().contains("Auditory"))
				disc="Auditiva";
			if(entry.getKey().contains("Cognitive"))
				disc="Cognitiva";
			if(entry.getKey().contains("Physical"))
				disc="Fisica";
			if(entry.getKey().contains("Visual"))
				disc="Visual";
			if(entry.getKey().contains("Psychosocial"))
				disc="Psicosocial";
			keys+=disc+";";
			vals+=d.format(entry.getValue())+";";
		}
		keys=keys.substring(0,keys.length()-1);
		vals=vals.substring(0,vals.length()-1);
	}
	/**
	 * Metodo que crea los textos que se van a recomendar al uduario en la interfaz resultados.
	 * @param resultadoAn Es un hashMap con un HashMap dentro donde se encuentranlos resultados del analisis.
	 * @return Retorna un listado con los diferentes String que conforman el texto de la reocmendacion.
	 */
	public String[] resultados(HashMap<String, HashMap<String,Object[]>> resultadoAn) {
		//HashMap<String, HashMap<String,Object[]>> clon=new HashMap<String, HashMap<String,Object[]>>(resultadoAn);
		HashMap<String,Object[]> clon=new HashMap<String,Object[]>();
		for(Map.Entry<String, HashMap<String, Object[]>>entry: resultadoAn.entrySet()) {
			for(Map.Entry<String, Object[]>ent:entry.getValue().entrySet()) {
				clon.put(ent.getKey(), ent.getValue());
			}
		}
		String [] noAplica=retornaMetasNoAplica();
		//java.util.ArrayList<String> noA=new java.util.ArrayList<String>();
		String noA="";
		for(String n:noAplica) {
			//noA.add(n);
			noA+=n+";";
		}
		
		String resultadoN="";
		String resultadoF="";
		String ModosAcceso="";
		String ModosAccesoN="";
		String caracteristicasAccesibilidad="";
		String caracteristicasAccesibilidadN="";
		String controlAccesibilidad="";
		String controlAccesibilidadN="";
		String riesgoAccesibilidad="";
		String riesgoAccesibilidadN="";
		
		String noAplicat="";
		for(Map.Entry<String , Object[]> entry: clon.entrySet()) {
			if(entry.getKey().equals("textual")) {//si esque es solo textual
			//	if(metadatos.containsKey("http://www.accesiblemetada.com/DRDSchema#textual")) {
				if(VariablesEstaticas.numPalabras>0) {
					ModosAcceso+="<li>Posee contenido textual con un numero de palabras de: "+VariablesEstaticas.numPalabras+"</li>";
				}else {
					ModosAccesoN+="<li>No posee contenido textual. Tener solo contenido textual afecta de manera significativa a personas con discapacidad auditiva y visual.</li>";
				}
				//}
			}else
			if(entry.getKey().equals("visual")) {
				//if(metadatos.containsKey("http://www.accesiblemetada.com/DRDSchema#visual")) {
				if(VariablesEstaticas.numImagenes>0||VariablesEstaticas.numVideos>0) {
					ModosAcceso+="<li>Posee contenido visual con un numero de: "+VariablesEstaticas.numImagenes+" imágenes y con un numero de: "+VariablesEstaticas.numVideos+" videos.</li>";
				}else {
					ModosAccesoN+="<li>No posee contenido visual(Ej: imágenes o videos, esto podría afectar a personas con discapacidad auditiva.</li>";
				}
				//}
			}else
			if(entry.getKey().equals("auditiva")) {
				//if(metadatos.containsKey("http://www.accesiblemetada.com/DRDSchema#auditory")) {
				if(VariablesEstaticas.numContAuditivo>0) {
					ModosAcceso+="<li>Posee contenido Auditivo con un numero de "+ VariablesEstaticas.numContAuditivo+" audios.</li>";
				}else {
					ModosAccesoN+="<li>No posee contenido Auditivo, a pesar de que favorece a personas con discapacidad auditiva, podría afectar a personas con discapacidad visual.</li>";
				}
			//}
			}else
			if(entry.getKey().equals("tactil")) {
				//if(metadatos.containsKey("http://www.accesiblemetada.com/DRDSchema#tactile")) {
				if(VariablesEstaticas.numConTactil>0) {
					ModosAcceso+="<li>Posee contenido Táctil con un numero de  "+ VariablesEstaticas.numConTactil+" elementos Táctiles.</li>";
					controlAccesibilidad+="<li>Posee control con el mouse.</li>";
					controlAccesibilidad+="<li>Posee control con el teclado.</li>";
				}else {
					ModosAccesoN+="<li>No posee contenido Táctil, lo que dificultaría el uso del OA en dispositivos touch o móviles.</li>";
					controlAccesibilidadN+="<li>No posee control muy limitado con el mouse.</li>";
					controlAccesibilidadN+="<li>No posee control muy limitado con el teclado.</li>";
				}
				//}
			}else
			if(entry.getKey().equals("audioDescripcion")||entry.getKey().equals("senas")) {
				//if(metadatos.containsKey("http://www.accesiblemetada.com/DRDSchema#audioDescription")||metadatos.containsKey("http://www.accesiblemetada.com/DRDSchema#signLanguague")) {
				if(VariablesEstaticas.numVideos>0) {
					noAplicat+="<li>Dispone de videos que al tener Audio Descripcion apoyaría significativamente a usuarios que no pueden acceder a este contenido de forma visual.</li>";
					noAplicat+="<li>Dispone de videos que al tener adaptación para interprete en lengua de señas apoyaria significativamente a usuarios que no pueden acceder a este contenido de forma auditiva y conocen lengua de señas .</li>";
				}else {
					noAplicat+="<li>No dispone de videos. Los vídeos constituyen un elemento multimedia que facilta la comprensión y el aprendizaje. Y al hacerlo, considerar añadir subtitulos, posible audio descripcion o lengua de señas en casado de requerirlo.</li>";
				}
				//}
			}else
			if(entry.getKey().equals("longDesc")) {
				//if(metadatos.containsKey("http://www.accesiblemetada.com/DRDSchema#longDescription")) {
				if(!noA.contains("longDescription")) {
				if(VariablesEstaticas.numImgLongDesc>0) {
					caracteristicasAccesibilidad+="<li>Tiene "+VariablesEstaticas.numImgLongDesc+" imágenes con Descripción Larga.</li>";
				}else {
					caracteristicasAccesibilidadN+="<li>No tiene imágenes ni videos con Descripcion Larga. Se recomienda, en caso de que la imagen aporte significativamente en el aprendizaje,  añadir descripciones textuales.</li>";
				}
				}else {
					noAplicat+="<li>En el caso de incrustar contenido visual como imagenes o videos, se recomienda describir en texto su contenido, favoreciendo de esta manera a personas con discapacidad visual. Se recomienda no presentar texto muy extenso puesto que podria afectar a personas con discapacidad cognitiva.</li>";
				}
				//}
			}else
			if(entry.getKey().equals("sound")) {
				//if(metadatos.containsKey("http://www.accesiblemetada.com/DRDSchema#sound")||metadatos.containsKey("http://www.accesiblemetada.com/DRDSchema#noSoundHazard")) {
				if(VariablesEstaticas.numSonidoFondo<=0) {
					riesgoAccesibilidad+="<li>No tiene sonido de fondo. Se considera una práctica satisfactoria en virtud de que podría afectar a personas con discapacidad cognitiva y psicosocial.</li>";
				}else {
					riesgoAccesibilidadN+="<li>Tiene sonido de fondo. No se recomienda usar sonido de fondo, ya que podría molestar a las personas con discapacidad cognitiva y psicosocial. En caso de ser necesaria, se sugiere que exista la alternativa para que el usuario pueda pausarla.</li>";
				}
				//}
			}else
			if(entry.getKey().equals("movimiento")) {
				//if(metadatos.containsKey("http://www.accesiblemetada.com/DRDSchema#flashing")||metadatos.containsKey("http://www.accesiblemetada.com/DRDSchema#noFlashingHazard")) {
				if(VariablesEstaticas.numAnimaciones<=0) {
					riesgoAccesibilidad+="<li>No tiene animaciones que están en constante movimiento (no flashing).</li>";
				}else {
					riesgoAccesibilidadN+="<li>Tiene animaciones que están en constante movimiento (conocidas como flashing), se recomienda que no use estas animaciones, pues podría afectar a las personas con discapacidad cognitiva y psicosocial (epilepsia fotosensitiva).</li>";
				}
				//}
			}else
			if(entry.getKey().equals("colorDependent")) {
				//if(metadatos.containsKey("http://www.accesiblemetada.com/DRDSchema#colorDependet")) {
				if(!noA.contains("colorDependent")) {
				if(VariablesEstaticas.numDependenciaColor<=0) {
					ModosAcceso+="<li>No se han encontrado posibles actividades que tengan dependencia de color.</li>";
				}else {
					ModosAccesoN+="<li>Se encontró "+VariablesEstaticas.numDependenciaColor+" posibles actividades con dependencia de colores. Se recomienda no usar colores como parte de actividades, pues, personas con daltonismo o baja visión o trastornos oculares no podrían resolverlas, a pesar de que personas con discapacidad cognitiva esten favorecidas por esto.</li>";
				}
				}else {
					noAplicat+="<li>Para implementar actividades dentro del objeto de aprendizaje, se recomienda que estas no tengan dependencia de color (Ej: Arrastre las opciones correctas al cuadro rojo), pues aunque favorezca a personas con discapacidad cognitiva, afectaría de manera significativa a las personas con discapacidad visual.</li>";
				}
				//}
			}
			else
			if(entry.getKey().equals("imagenes")) {
				//if(metadatos.containsKey("http://www.accesiblemetada.com/DRDSchema#alternativeText")||metadatos.containsKey("http://www.accesiblemetada.com/DRDSchema#textOnVisual")) {
				if(!noA.contains("alternativeText")) {
				if(VariablesEstaticas.numImagenesTxtAlt>(VariablesEstaticas.numImagenes/1.33)) {//1.33es el 75%
					caracteristicasAccesibilidad+="<li>El contenido tiene "+VariablesEstaticas.numImagenesTxtAlt+" Imágenes con texto alternativo, esto significa que más del 75% de las imágenes tienen texto alternativo.</li>";
				}else {
					caracteristicasAccesibilidadN+="<li>El contenido tiene "+VariablesEstaticas.numImagenes+" imágenes, de las cuales "+VariablesEstaticas.numImagenesTxtAlt+" tienen texto alternativo. Se recomienda considerar aquellas que aportan significativamente en el aprendizaje y describirlas.</li>";
				}
				}else {
					noAplicat+="<li>En caso de necesitar incrustar imagenes en el contenido del objeto de aprendizaje, se recomienda considerar aquellas que aportan significativamente en el aprendizaje y describirlas.</li>";
				}
				if(!noA.contains("textOnVisual")) {
				if(VariablesEstaticas.numtextOnVisual>(VariablesEstaticas.numImagenes/1.33)) {
					ModosAccesoN+="<li>El contenido tiene "+VariablesEstaticas.numtextOnVisual+" Imágenes con texto incrustado o baja resolución, esto significa que no se podrá leer el texto o no estará habilitado para un lector de pantalla. Se recomienda no tener texto incrustado en imágenes pues afecta a las personas con discapacidad visual y cognitiva.</li>";
				}else {
					ModosAcceso+="<li>No se han encontrado imágenes con  texto o han sido muy pocas las imágenes  con texto  encontradas, lo cual es considerada una buena práctica para facilitar el acceso a información para personas con discapacidad visual que usan lectores de pantalla.</li>";
				}
				}else {
					noAplicat+="<li>En caso de necesitar incrustar imagenes en el contenido del objeto de aprendizaje, se recomienda no tener texto incrustado en imágenes pues afecta a las personas con discapacidad visual y cognitiva.</li>";
				}
				//}
			}
			else
			if(entry.getKey().equals("tablaContenidos")) {
				//if(metadatos.containsKey("http://www.accesiblemetada.com/DRDSchema#tableOfContents")) {
				if(VariablesEstaticas.numTablaContenidos>0) {
					caracteristicasAccesibilidad+="<li>Se ha determinado que si posee tabla de contenidos.</li>";
				}else {
					caracteristicasAccesibilidadN+="<li>Se ha determinado que el contenido no posee tabla de contenidos, por lo cual se recomienda que se implemente una tabla de contenidos para facilitar la ubicación y navegación.</li>";
				}
				//}
			}
			else
			if(entry.getKey().equals("tamanoFuente")) {
				//if(metadatos.containsKey("http://www.accesiblemetada.com/DRDSchema#largePrint")) {
				if(!noA.contains("largePrint")) {//para el no aplica
				if(VariablesEstaticas.numCumpleTodoHTML>(VariablesEstaticas.numCumpleTodoHTMLTotal/1.33)) {
					caracteristicasAccesibilidad+="<li>Mas del 75% de los tamaños de fuente encontrados son de un tamaño adecuado. Se recomienda considerar un tamaño  estándar  (18 a 20px aprox) que facilite la interacción de  personas baja visión, de igual manera el empleo de un tipo de letra sin serigrafía.</li>";
				}else {
					caracteristicasAccesibilidadN+="<li>Menos del 75% de los tamaños de fuente encontrados tienen un tamañono adecuado. Se recomienda considerar un tamaño  estándar  (18 a 20px aprox) que facilite la interacción de  personas baja visión, de igual manera el empleo de un tipo de letra sin serigrafía.</li>";
				}
				}else {
					noAplicat+="<li>Para el contenido textual, se recomienda considerar un tamaño  estándar  (18 a 20px aprox) que facilite la interacción de  personas baja visión, de igual manera el empleo de un tipo de letra sin serigrafía.</li>";
				}
				//}
			}
			else
			if(entry.getKey().equals("mathML")) {
				//if(metadatos.containsKey("http://www.accesiblemetada.com/DRDSchema#MathML")) {
				if(!noA.contains("MathML")) {//para el no aplica
				if(VariablesEstaticas.numFormulasMatematicas>0) {
					caracteristicasAccesibilidad+="<li>Se detecta la implementación de librerías estándar para formulas matemáticas, como MathML.</li>";
				}else {
					caracteristicasAccesibilidadN+="<li>No está usando librería o etiqueta para implementar formulas matemáticas, en caso de necesitar, WCAG recomienda usar MATHML.</li>";
				}
				}else {
					noAplicat+="<li>En caso de que necesite incluir fórmulas matemáticas en su objeto de aprendizajes, se recomienda ponerlas en texto y no imagenes. WCAG recomienda usar MATHML.</li>";
				}
				//}
			}
			else
			if(entry.getKey().equals("chemML")) {
				//if(metadatos.containsKey("http://www.accesiblemetada.com/DRDSchema#ChemML")) {
				if(!noA.contains("ChemML")) {//para el no aplica
				if(VariablesEstaticas.numChem>0) {
					caracteristicasAccesibilidad+="<li>Para las formulas químicas está usando chemML.</li>";
				}else {
					caracteristicasAccesibilidadN+="<li>No está usando librería o etiqueta para implementar formulas químicas, en caso de que existan estas fórmulas, WCAG recomienda usar CHEMML.</li>";
				}
				}else {
					noAplicat+="<li>En caso de que necesite incluir fórmulas químicas en su objeto de aprendizajes, se recomienda ponerlas en texto y no imagenes. WCAG recomienda usar CHEMML.</li>";
				}
				//}
			}
			else
			if(entry.getKey().equals("latex")) {
				//if(metadatos.containsKey("http://www.accesiblemetada.com/DRDSchema#latex")) {
				if(!noA.contains("latex")) {//para el no aplica
				if(VariablesEstaticas.numlatex>0) {
					caracteristicasAccesibilidad+="<li>Se  detecta el empleo de  libreria o un estándar para implementar contenido en formato latex, como codecogs.</li>";
				}else {
					caracteristicasAccesibilidadN+="<li>No está usando libreria o etiqueta para implementar contenido en formato latex, en caso de que existan estos contenidos, se recomienda usar alguna librería desarrollada para este propósito, como codecogs.</li>";
				}
				}else {
					noAplicat+="<li>En caso de que necesite incluir contenido en formato latex en su objeto de aprendizajes, se recomienda usar alguna librería desarrollada para este propósito, como codecogs.</li>";
				}
				//}
			}
			else
			if(entry.getKey().equals("simulations")) {
				//if(metadatos.containsKey("http://www.accesiblemetada.com/DRDSchema#motionSimulation")||metadatos.containsKey("http://www.accesiblemetada.com/DRDSchema#noMotionSimulation")) {
				if(VariablesEstaticas.numSimulaciones>0) {
					riesgoAccesibilidadN+="<li>Se han detectado posibles simulaciones. Se recomienda, considerar que las simulaciones puedan ser controladas por el usuario.</li>";
				}else {
					riesgoAccesibilidad+="<li>No se han detectado posibles Simulaciones.</li>";
				}
				//}
			}else
			if(entry.getKey().equals("tiempos")) {
				//if(metadatos.containsKey("http://www.accesiblemetada.com/DRDSchema#timingControl")) {
				if(VariablesEstaticas.acTiempos>0) {
					caracteristicasAccesibilidad+="<li>Se han detectado posibles actividades controladas por tiempos. Se recomienda considerar la posibilidad de habilitarlo o no.</li>";
				}else {
					caracteristicasAccesibilidadN+="<li>No se han detectado posibles actividades controladas por tiempos.</li>";
				}
				//}
			}
			else
			if(entry.getKey().equals("subs")) {
				//if(metadatos.containsKey("http://www.accesiblemetada.com/DRDSchema#captions")) {
				if(!noA.contains("captions")) {
				if(VariablesEstaticas.numVideosSubs>75) {
					caracteristicasAccesibilidad+="<li>Se ha encontrado que el "+new DecimalFormat("###.00").format(VariablesEstaticas.numVideosSubs)+"% de los videos contiene subtítulos, es considerada  una buena práctica que facilita la comprensión.</li>";
				}else {
					caracteristicasAccesibilidadN+="<li>Se a encontrado que el "+new DecimalFormat("###.00").format(VariablesEstaticas.numVideosSubs)+"% de los videos contiene subtítulos, se recomienda que todos los videos tengan subtítulos para facilitar la comprensión</li>";
				}
				}
				//}
			}
			else
			if(entry.getKey().equals("pdf")) {
				//if(metadatos.containsKey("http://www.accesiblemetada.com/DRDSchema#taggedPDF")) {
				if(!noA.contains("taggedPDF")) {
				if(VariablesEstaticas.pdfErrors.toLowerCase().contains("error")) {
					String []errorespPDF=VariablesEstaticas.pdfErrors.split("\n");
//					String muestra="";
//					int cont=0;
//					for(String e: errorespPDF) {
//						if(!e.isEmpty()) {
//							///if(cont==0) {
////								muestra+=e;
////								muestra+="<ul>";
//							//}else 
//							if(e.toLowerCase().contains("el archivo")) {
//								//muestra+="</ul>";
//								muestra+=e;
//								muestra+="<ul>";
//							}
//							else {
//								if(e.contains("->")) {
//									muestra+="<li>"+e;
//								}else {
//									muestra+=e+"</li></ul>";
//									//muestra+="</ul>";
//								}
//								//muestra+="<li>"+e+"</li>";
//							}
//							cont++;
//						}
//					}
//					muestra+="</ul>";
//					System.out.println("ESTE ES EL MUESTRAAAAA\n"+muestra);
					caracteristicasAccesibilidadN+=""+VariablesEstaticas.pdfErrors+" <li>Puede revisar los errores de accesibilidad de los archivos pdf desde el siguiente enlace: <a href='http://checkers.eiii.eu/en/pdfcheck/' style='color: #337ab7; text-decoration: underline;' target='_blank'>European Internet Inclusion Initiative</a>.</li>";
					//caracteristicasAccesibilidadN+=""+muestra+" <li>Puede revisar los errores de accesibilidad de los archivos pdf desde el siguiente enlace: <a href='http://checkers.eiii.eu/en/pdfcheck/' style='color: #337ab7; text-decoration: underline;' target='_blank'>European Internet Inclusion Initiative</a>.</li>";
				}else {
					caracteristicasAccesibilidad+="<li>No se han encontrado errores de accesibilidad en los pdfs o no existen pdfs.</li>";
				}
				}else {
					noAplicat+="<li>En caso de que necesite añadir archivos PDF, es importante que compruebe la accesibilidad del mismo, esto lo puede hacer desde el siguiente enlace: <a href='http://checkers.eiii.eu/en/pdfcheck/' style='color: #337ab7; text-decoration: underline;' target='_blank' >European Internet Inclusion Initiative</a>.</li>";
				}
				//}
			}
//			else
//			if(!Boolean.parseBoolean(entry.getValue()[1].toString())) {
//				if(entry.getValue()[0].toString()!=null)
//					caracteristicasAccesibilidadN+="<li>"+entry.getValue()[0].toString()+"</li>";
//				
//			}else {
//				if(entry.getValue()[0].toString()!=null)
//					caracteristicasAccesibilidad+="<li>"+entry.getValue()[0].toString()+"</li>";
//			}
			
			
			
			}
		if(noA.contains("highContrastDisplay")) {
			noAplicat+="<li>En caso de requerirlo, se deberia incluir opciones para el cambio de color de todo el contenido de la pagina web (Ej: botones para cambiar los contrastes del contenido), esto favorecera a personas con discapacidad visual.</li>";
		}
		noAplicat+="<li>Para implementar actividades dentro del objeto de aprendizaje, se recomienda brindar la posiblidad de que el usaurio pueda controlar los tiempos de dichas actividades en caso de requerir tiempos.</li>";
		noAplicat+="<li>Se recomienda que el objeto de aprendizaje tenga una navegacion estructurada e intuitiva para que facilite, entre otras, a personas con discapacidad cognitiva.</li>";
		noAplicat+="<li>Se recomienda no agregar contenido con destellos o animaciones en constante movimiento (conocidas como flashing), pues podría afectar a las personas con discapacidad cognitiva y psicosocial (epilepsia fotosensitiva).</li>";
		noAplicat+="<li>En caso de añadir simulaciones, se recomienda considerar que las simulaciones puedan ser controladas por el usuario.</li>";
		System.out.println(caracteristicasAccesibilidadN);
		return new String [] {ModosAcceso,ModosAccesoN,caracteristicasAccesibilidad,caracteristicasAccesibilidadN,controlAccesibilidad,controlAccesibilidadN,riesgoAccesibilidad,riesgoAccesibilidadN, noAplicat};
	}
	/**
	 * Metodo que le brinda un delay a la grafica para que se carguen todos los datos y se grafique sin error.
	 */
	public void espera() {
			try {
				 System.out.println("Esperando...");
		          TimeUnit.SECONDS.sleep(1);//tiempo que va a estar el progress bar
		          PrimeFaces.current().executeScript("graficar('"+keys+"','"+vals+"');");
		      } catch (InterruptedException e) {
		          e.printStackTrace();
		      }
	}
	/**
	 * Metodo que pone todos los valores de las diferentes variables y objetos a valores por defecto.
	 */
	public void resetearTodo() {
		visual=true;
		auditiva=false;
		cognitiva=false;
		fisicaMotriz=false;
		psicosocial=false;
		directorioSeleccionado="Seleccionar Directorio y Analizar";
		titulo="";
		numeroHTML="";
		numeroCSS="";
		numeroJS="";
		numeroImagenes="";
		numeroVideos="";
		fechaHora="";
		
		descripcionGeneral="";
		descripcionGeneralN="";
		descripcionGeneralF="";
		consideracionesGenerales="";
		recomendacion=new HashMap<String, String>();
		
		resultadoTotal=new HashMap<String, HashMap<String,Object[]>>();
		resultadosGenerales=new HashMap<String, String>();
		
		///RESTAR STATIC VARIABLE
		VariablesEstaticas.numImagenes=0;
		VariablesEstaticas.numVideos=0;
		VariablesEstaticas.pdfErrors="";
		VariablesEstaticas.numPDFs=0;
		VariablesEstaticas.numPalabras=0;
		VariablesEstaticas.numVideosSubs=0;
		VariablesEstaticas.numContAuditivo=0;
		VariablesEstaticas.numConTactil=0;
		VariablesEstaticas.numImgLongDesc=0;
		VariablesEstaticas.numSonidoFondo=0;
		VariablesEstaticas.numAnimaciones=0;
		VariablesEstaticas.numDependenciaColor=0; 
		VariablesEstaticas.numImagenesTxtAlt=0;
		VariablesEstaticas.numtextOnVisual=0;
		
		//cumpleTodo
		VariablesEstaticas.numCumpleTodoHTML=0;
		VariablesEstaticas.numCumpleTodoHTMLTotal=0;
		
		VariablesEstaticas.numTablaContenidos=0;
		VariablesEstaticas.numFormulasMatematicas=0;
		VariablesEstaticas.numChem=0;
		VariablesEstaticas.numlatex=0;
		VariablesEstaticas.numSimulaciones=0;
		VariablesEstaticas.acTiempos=0;
		System.out.println("Entra a resetear");
	}
	/**
	 * Metodo que s einvoca cuando se selecciona el boton nuevo analisis, este invoa al metodo para resetear
	 * los valores a por defecto.
	 * @return Retorna el nombre de la pagina a al que se va a redirigir.
	 */
	public String nuevoAnalisis() {
		resetearTodo();
		return "seleccion.jsf ? faces-redirect=true";
	}
	
	////PROGRESS BAR
	private AtomicInteger progressInteger = new AtomicInteger();
	  private ExecutorService executorService;
	  /**
	   * Metodo que aumenta los valores del progresbar acorde al progreso de la carga y del analisis.
	   */
	  public void startTask() {
	      executorService = Executors.newSingleThreadExecutor();
	      //executorService.execute(this.startLongTask);
	      executorService.execute(new Runnable() {
			
			@Override
			public void run() {
				// TODO Auto-generated method stub
				startLongTask();
			}
		});
//	      for(int i=0;i<10000000;i++) {
//	      System.out.println("Imprimeeeeeeee");
//	      }
	  }
	  /**
	   * Metodo que se ejecutara para la actualizacion de los valores del progress bar.
	   */
	  private void startLongTask() {
	      progressInteger.set(0);
//	      for (int i = 0; i < 100; i++) {
//	          //progressInteger.getAndIncrement();
//	          progressInteger.getAndSet(incrementa);
//	          //simulating long running task
//	          try {
//	        	  System.out.println("Imprimeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee");
//	              Thread.sleep(ThreadLocalRandom.current().nextInt(1, 100));
//	              
//	          } catch (InterruptedException e) {
//	              e.printStackTrace();
//	          }
//	      }
	      boolean bandera=false;
	      while(!bandera) {
	    	  progressInteger.getAndSet(VariablesEstaticas.incrementaFA);
	    	  //System.out.println(VariablesEstaticas.incrementaF);
//	    	  if(VariablesEstaticas.incrementaF>=30){
//	    		  System.out.println("impremeeeeeeeeeeeeeeee "+VariablesEstaticas.incrementaF);
//	    	  }
//	    	  if(VariablesEstaticas.incrementaF>=60){
//	    		  System.out.println("impremeeeeeeeeeeeeeeee "+VariablesEstaticas.incrementaF);
//	    	  }
//	    	  if(VariablesEstaticas.incrementaF>=80){
//	    		  System.out.println("impremeeeeeeeeeeeeeeee "+VariablesEstaticas.incrementaF);
//	    	  }
	    	  if(VariablesEstaticas.incrementaFA>=100){
	    		  progressInteger.getAndSet(100);
	    		  bandera=true;
	    	  }
	      }
	      executorService.shutdownNow();
	      progressInteger.set(0);
	      VariablesEstaticas.incrementaFA=0;
	      VariablesEstaticas.incrementaF=0;//este es del formulario //se deberia mandar un true o false a al clase upload para q no cambie los valores q son del progress bar del formulario
	      executorService = null;
	  }

	  public int getProgress() {
	      return progressInteger.get();
	  }

	  public String getResult() {
	      return progressInteger.get() == 100 ? "task done" : "";
	  }
}
