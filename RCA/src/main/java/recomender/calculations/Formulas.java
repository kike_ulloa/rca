package recomender.calculations;

public class Formulas {
	int pixelesPorCaracter=30;
	
	/**
	 * Metodo que calcula si el numero de caracteres pueden caber en la imagen y ser leidos sin problema
	 * @param ancho Ancho de la imagen
	 * @param alto Alto de la imagen
	 * @param texto El texto que esta en la imagen
	 * @return
	 */
	public boolean formulaCalculoCaracteresPorPixeles(int ancho, int alto, String texto) {
		long calculado=texto.length()*(pixelesPorCaracter*pixelesPorCaracter);//numero de caracteres*numero de pixeles que ocupa un caracter(30Alto*30Largo)
		long totalPixelesImagen=ancho*alto;
		if(calculado<totalPixelesImagen)//si pueden ser leidos los caracteres en la imagen al hacer zoom
			return true;
		return false;
	}
	/**
	 * Metodo que calcula el porcentaje de accesibilidad para una discapacidad dada por documento
	 * @param valor Es el valor que contiene la discapacidad en ese documento. Valor entero
	 * @param numMetadatos Es el numero de metadatos que contiene el archivo
	 * @return retorna el porcentaje de accesibilidad para la discapacidad dada del documento.
	 */
	public double calculaPorcentajeAccesibilidad(String key,String valor, double numMetadatos) {
		double resultado=0.0;
//		if(key.contains("Auditory"))
//			resultado=(Double.parseDouble(valor)*14.0)/9.0;
//		if(key.contains("Cognitive"))
//			resultado=(Double.parseDouble(valor)*14.0)/9.0;
//		if(key.contains("Psychosocial"))
//			resultado=(Double.parseDouble(valor)*14.0)/6.0;
//		if(key.contains("Physical"))
//			resultado=(Double.parseDouble(valor)*14.0)/6.0;
//		if(key.contains("Visual"))
//			resultado=Double.parseDouble(valor);
		resultado=Double.parseDouble(valor)/numMetadatos;
		//resultado=resultado/numMetadatos;
		if(resultado<0)
			return 0;
		else
			return resultado*100;
	}
	/**
	 * Metodo que calcula el procentaje gneral de accesibilidad en el OA para la discapacidad dada
	 * @param porcentajes Son los porcentajes de una sola discapacidad de cada documento
	 * @return retorna el procentaje general del OA
	 */
	public double porcentajeGeneral(double []porcentajes) {
		double resultado=0d;
		for(double porcentaje:porcentajes) {
			resultado+=(porcentaje/100);
		}
		resultado=resultado/porcentajes.length;
		return resultado*100;
	}
}
