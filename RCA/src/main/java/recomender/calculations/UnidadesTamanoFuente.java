package recomender.calculations;

import recomender.validations.RetornaSoloNumeroDeString;

public class UnidadesTamanoFuente {

	/**
	 * Metodo que convierte de puntos (pt) a pixeles y retorna verdadero o falso, dependiendo si es mayor o no a 20px
	 * @param style Cadea que se va a nalizar
	 * @return Si es mayor a 20px retorna verdadero, caso contraio, retorna falso
	 */
	public boolean pt(String style) {
		double px=1.3281472327365;//esto e sigual a 1pt
		RetornaSoloNumeroDeString ret=new RetornaSoloNumeroDeString();
		double valor=ret.retornaNumero(style);
		double valorEnPX=valor*px;
		if(valorEnPX>=20) {//si el valor convertido a pixeles es igual o mayor a 20
			return true;
		}else {
			return false;
		}
	}
	/**
	 * Metodo que convierte de picos (pc) a pixeles y retorna verdadero o falso, dependiendo si es mayor o no a 20px
	 * @param style Cadea que se va a nalizar
	 * @return Si es mayor a 20px retorna verdadero, caso contraio, retorna falso
	 */
	public boolean pc(String style) {
		double px=63.751067171354;//esto e sigual a 1pc
		RetornaSoloNumeroDeString ret=new RetornaSoloNumeroDeString();
		double valor=ret.retornaNumero(style);
		double valorEnPX=valor*px;
		if(valorEnPX>=20) {//si el valor convertido a pixeles es igual o mayor a 20
			return true;
		}else {
			return false;
		}
	}
	/**
	 * Metodo que convierte de milimetros (mm) a pixeles y retorna verdadero o falso, dependiendo si es mayor o no a 20px
	 * @param style Cadea que se va a nalizar
	 * @return Si es mayor a 20px retorna verdadero, caso contraio, retorna falso
	 */
	public boolean mm(String style) {
		double px=3.779527559;//esto e sigual a 1mm
		RetornaSoloNumeroDeString ret=new RetornaSoloNumeroDeString();
		double valor=ret.retornaNumero(style);
		double valorEnPX=valor*px;
		if(valorEnPX>=20) {//si el valor convertido a pixeles es igual o mayor a 20
			return true;
		}else {
			return false;
		}
	}
	/**
	 * Metodo que convierte de centimetros (cm) a pixeles y retorna verdadero o falso, dependiendo si es mayor o no a 20px
	 * @param style Cadea que se va a nalizar
	 * @return Si es mayor a 20px retorna verdadero, caso contraio, retorna falso
	 */
	public boolean cm(String style) {
		double px=37.79527559;//esto e sigual a 1cm
		RetornaSoloNumeroDeString ret=new RetornaSoloNumeroDeString();
		double valor=ret.retornaNumero(style);
		double valorEnPX=valor*px;
		if(valorEnPX>=20) {//si el valor convertido a pixeles es igual o mayor a 20
			return true;
		}else {
			return false;
		}
	}
	/**
	 * Metodo que convierte de pulgadas (in) a pixeles y retorna verdadero o falso, dependiendo si es mayor o no a 20px
	 * @param style Cadea que se va a nalizar
	 * @return Si es mayor a 20px retorna verdadero, caso contraio, retorna falso
	 */
	public boolean in(String style) {
		double px=95.999999998601;//esto e sigual a 1in
		RetornaSoloNumeroDeString ret=new RetornaSoloNumeroDeString();
		double valor=ret.retornaNumero(style);
		double valorEnPX=valor*px;
		if(valorEnPX>=20) {//si el valor convertido a pixeles es igual o mayor a 20
			return true;
		}else {
			return false;
		}
	}
	/**
	 * Metodo que convierte de em a pixeles y retorna verdadero o falso, dependiendo si es mayor o no a 20px
	 * @param style Cadea que se va a nalizar
	 * @return Si es mayor a 20px retorna verdadero, caso contraio, retorna falso
	 */
	public boolean em(String style) {
		double px=20;//pixeles minimos
		double pxD=16;//pixeles default del navegador
		RetornaSoloNumeroDeString ret=new RetornaSoloNumeroDeString();
		double valor=ret.retornaNumero(style);
		double valorEnPX=px/pxD;
		if(valor>valorEnPX) {//si el valor es mayor al valor minimo
			return true;
		}else {
			return false;
		}
	}
	/**
	 * Metodo que comprueba los pixeles y retorna verdadero o falso, dependiendo si es mayor o no a 20px
	 * @param style Cadea que se va a nalizar
	 * @return Si es mayor a 20px retorna verdadero, caso contraio, retorna falso
	 */
	public boolean px(String style) {
		RetornaSoloNumeroDeString ret=new RetornaSoloNumeroDeString();
		double valor=ret.retornaNumero(style);
		if(valor>=20)
			return true;
		return false;
	}
	/**
	 * Metodo main para anlaizar los diferentes tamanos funete y sus unidades.
	 * @param style Es el codigo aa nalizar
	 * @param html Es el archivo a  anlizar.
	 * @return Retorna los resultados
	 */
	public boolean analiza(String style, String html) {
		if(style.contains("pt")) {//si es igual a puntos 
			if(pt(style))//si es mayor a 20px
				return true;//aumento el numero de los que si son mayor a 20px
		}else if(style.contains("pc")) {//si son igual a picos
			if(pc(style))//si es mayor a 20px
				return true;//aumento el numero de los que son mayor a 20px
		}else if(style.contains("mm")) {//si es igual a milimetros
			if(mm(style))////si es mayor a 20px
				return true;//aumento el numero de los que son mayor a 20px
		}else if(style.contains("cm")) {//si es igual a centimetros
			if(cm(style))//si es mayor a 20px
				return true;//aumento el numero de los que si son mayor a 20px
		}else if(style.contains("in")) {//si es igual a pulgadas
			if(in(style))//si es mayor a 20px
				return true;//aumento el numero de los que si son mayor a 20px
		}else if(style.contains("em")) {//si es igual a em
			if(em(style))//si es mayor a 20px
				return true;//aumento el numero de los que si son mayor a 20px
		}else if(style.contains("px")) {//si es igual a pixels
			if(px(style))//si es mayor a 20px
				return true;//aumento el numero de los que si son mayor a 20px
		}else if(html.equals("html")) {//si es el tag html el que se analiza en el font
			RetornaSoloNumeroDeString ret=new RetornaSoloNumeroDeString();
			double numero=ret.retornaNumero(style);
			if(numero>=3) {//solo puede ser el numero de 1 a 7, si es mayor al 3 que es por defecto del navegador entonces cumple
				return true;
			}
		}else {//si no tiene ninguna unidad (px, pt,pc,em,cm,mm, etc significa que el navegador lo tomara como pixel
			if(px(style))
				return true;
		}
		return false;
	}
}
