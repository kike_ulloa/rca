package recomender.data;

import java.io.File;
import java.io.FileOutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.faces.context.FacesContext;

public class Compress {
	/**
	 * Metodo que comprime el directorio de objeto de aprendizaje en zip.
	 * @param context Es el contexto de la aplicacion en es emomento.
	 * @param pathOA Es la ruta del objeto de aprendizaje a comprimir.
	 * @return Retorna el archivo zip.
	 */
	public String comprimir(FacesContext context, String pathOA) {
		try {
			String pathApplication=context.getExternalContext().getApplicationContextPath();
			String pathZip=pathOA.substring(0,pathOA.lastIndexOf('/'))+ pathOA.substring(pathOA.lastIndexOf("/"))+".zip";//creo el path donde se va a guardar el archivo comprimido
			System.out.println("Paths del zip "+pathZip);
			ZipOutputStream zos=new ZipOutputStream(new FileOutputStream(new File(pathZip)));//Escritor del zip
			escribirEnZip(pathOA, Paths.get(pathOA), zos);
			zos.flush();
			zos.close();
			return pathZip;
		}catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	/**
	 * Metodo recursivo que recorre archivos y directorios y comprime
	 * @param path El path del direcotrio a comprimir
	 * @param pat el path del archivo o direcotrio a comprimir
	 * @param zos el zipwriter
	 */
	public void escribirEnZip(String path, Path pat,ZipOutputStream zos) {
		try {
			File f=new File(path);
			File []files=f.listFiles();
			for(File fi:files) {
				if(fi.isDirectory()) {
					escribirEnZip(fi.getAbsolutePath(),pat,zos);
				}else {
					//System.out.println(fi.getAbsolutePath());
					zos.putNextEntry(new ZipEntry(pat.relativize(Paths.get(fi.getAbsolutePath())).toString()));//crea una nueva entrada para escribir un nuevo archivo.
					Files.copy(Paths.get(fi.getAbsolutePath()), zos);//copia los archivos en el comprimido
					zos.closeEntry();//cierra la nueva entrada
				}
			}
		}catch(Exception e) {
			e.printStackTrace();	
		}
	}
}
