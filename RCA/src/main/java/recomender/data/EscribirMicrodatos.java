package recomender.data;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;

public class EscribirMicrodatos {
	/**
	 * Meotodo que retorna el listado de los metadatos en formato para escribir en un html.
	 * @return Retorna el lisytado de los metadatos en formato para escribir en un archivo.
	 */
	public java.util.ArrayList<String> retornaListaMetas(){
		java.util.ArrayList<String> microdatos=new java.util.ArrayList<String>();
	//	String divMeta="<div itemscope itemtype=\"http://schema.org/CreativeWork\">";
		
		//microdatos.add(divMeta);
		//accessmode
		microdatos.add("<meta itemprop=\"accessMode\" content=\"auditory\" />");
		microdatos.add("<meta itemprop=\"accessMode\" content=\"tactile\" />");
		microdatos.add("<meta itemprop=\"accessMode\" content=\"textual\" />");
		microdatos.add("<meta itemprop=\"accessMode\" content=\"visual\" />");
		microdatos.add("<meta itemprop=\"accessMode\" content=\"colorDependent\"/>");
		microdatos.add("<meta itemprop=\"accessMode\" content=\"textOnVisual\"/>");
		//accessibilityFeature
		microdatos.add("<meta itemprop=\"accessibilityFeature\" content=\"highContrastDisplay\"/>");
		microdatos.add("<meta itemprop=\"accessibilityFeature\" content=\"largePrint\"/>");
		microdatos.add("<meta itemprop=\"accessibilityFeature\" content=\"latex\"/>");
		microdatos.add("<meta itemprop=\"accessibilityFeature\" content=\"tableOfContents\"/>");
		microdatos.add("<meta itemprop=\"accessibilityFeature\" content=\"alternativeText\"/>");
		microdatos.add("<meta itemprop=\"accessibilityFeature\" content=\"longDescription\"/>");
		microdatos.add("<meta itemprop=\"accessibilityFeature\" content=\"captions\"/>");
		microdatos.add("<meta itemprop=\"accessibilityFeature\" content=\"audioDescription\"/>");
		microdatos.add("<meta itemprop=\"accessibilityFeature\" content=\"signLanguage\"/>");
		microdatos.add("<meta itemprop=\"accessibilityFeature\" content=\"ChemML\"/>");
		microdatos.add("<meta itemprop=\"accessibilityFeature\" content=\"MathML\"/>");
		microdatos.add("<meta itemprop=\"accessibilityFeature\" content=\"timingControl\"/>");
		microdatos.add("<meta itemprop=\"accessibilityFeature\" content=\"structuralNavigation\"/>");
		microdatos.add("<meta itemprop=\"accessibilityFeature\" content=\"taggedPDF\"/>");
		//accessibilityControl
		microdatos.add("<meta itemprop=\"accessibilityControl\" content=\"fullKeyboardControl\"/>");
		microdatos.add("<meta itemprop=\"accessibilityControl\" content=\"fullMouseControl\"/>");
		
		//accessibilityHazzard
		microdatos.add("<meta itemprop=\"accessibilityHazard\" content=\"flashing\"/>");//seleccionado
		microdatos.add("<meta itemprop=\"accessibilityHazard\" content=\"motionSimulation\"/>");//seleccionado
		microdatos.add("<meta itemprop=\"accessibilityHazard\" content=\"sound\"/>");//seleccionado
		microdatos.add("<meta itemprop=\"accessibilityHazard\" content=\"noFlashingHazard\"/>");//no seleccionado
		microdatos.add("<meta itemprop=\"accessibilityHazard\" content=\"noMotionSimulationHazard\"/>");//no seleccionado
		microdatos.add("<meta itemprop=\"accessibilityHazard\" content=\"noSoundHazard\"/>");//no seleccionado
		//microdatos.add("<meta itemprop=\"accessibilityHazard\" content=\"unknown\"/>");//desconocido
		//microdatos.add("</div>");
		return microdatos;
	}
	/**
	 * Metodo que escribe los microdatos en los distintos archivos
	 */
	public boolean escribriMicrodatos(String ruta, java.util.ArrayList<String> microdatos, java.util.List<Boolean>checks, boolean reemplazar, boolean []noAplica) {
//		rutaArchivoEditando=ruta;
//		llenaMicrodatos();
//		int [] index=new int[] {0,3,4,7,8,11,16,19,22};//hasta el 25
//		resetearChecks(index);
		for(boolean check:checks) {
			System.out.println(check);
		}
		String contMetas="";
		String divMeta="<div itemscope itemtype=\"http://schema.org/CreativeWork\">";
		try {
			if(checks.contains(true)) {
				contMetas+=divMeta+"\n";
			}
			for (int i=0; i<checks.size();i++) {//recorro la lista de checks (true, false)
				if(checks.get(i)) {//en caso de qu sean los 4 primero valores glob=vales seleccionados (txtual, visual, auditivo, etc)
//					if(i<4) {//si son los primeros 4 checks verdaderos
//						archivo.escribirArchivo(microdatos.get(i)+"alt=\""+menus.get(i)+"\"/>", rutaArchivoEditando);//aniado el texto alternativo a estos
//					}else {
					//System.out.println(i+" "+checks.get(i)+" "+microdatos.get(i));
//						if(i==0||i==checks.size()-1) {
//							contMetas+=microdatos.get(i)+"\n";
//							//System.out.println(i);
//							//escribirArchivo(microdatos.get(i), rutaArchivoEditando);//escribo unicamente los microdatos
//						}else {
							contMetas+="\t"+microdatos.get(i)+"\n";
							//escribirArchivo("\t"+microdatos.get(i), rutaArchivoEditando);//escribo unicamente los microdatos
						///}
					//}
				}else {//para escribir los valores de flaso en caso de que se de ese caso
					if(i==checks.size()-3) {
						//escribirArchivo("\t"+microdatos.get(microdatos.size()-4)+"\n", rutaArchivoEditando);
						if(!noAplica[0])
							contMetas+="\t"+microdatos.get(microdatos.size()-3)+"\n";
					}else if(i==checks.size()-2) {
						//escribirArchivo("\t"+microdatos.get(microdatos.size()-3)+"\n", rutaArchivoEditando);
						if(!noAplica[1])
							contMetas+="\t"+microdatos.get(microdatos.size()-2)+"\n";
					}else if(i==checks.size()-1) {
						//escribirArchivo("\t"+microdatos.get(microdatos.size()-2)+"\n", rutaArchivoEditando);
						if(!noAplica[2])
							contMetas+="\t"+microdatos.get(microdatos.size()-1)+"\n";
						
					}
				}
				
			}
			contMetas+="</div>\n";
			String contenido=leeModifica(ruta, contMetas, reemplazar);
			boolean escribio=escribeArchivo(ruta, contenido);
			return escribio;
		}catch(Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	/**
	 * Metodo que lee y modifica el archivo para escribir los metas.
	 * @param archivo Es el archivo a modificar.
	 * @param divMeta Es el elemento div q se va a incrsutar para escribir los metas.
	 * @param reemplazar reemplaza  los metas existentes
	 * @return Retorna el texot completo reemplazado
	 */
	public String leeModifica(String archivo, String divMeta, boolean reemplazar) {
		try {
			String todo="";
			BufferedReader buffer=new BufferedReader(new FileReader(archivo));
			String linea="";
			String metas=divMeta+"\n";
			boolean esBody=false;
			boolean empizaBody=false;
			while((linea=buffer.readLine())!=null) {
				if(linea.toLowerCase().contains("<body")) {
					esBody=true;
					empizaBody=true;
				}
				if(!esBody) {
					if(linea.toLowerCase().contains("accessmode")||linea.toLowerCase().contains("accessibilityfeature")
							||linea.toLowerCase().contains("accessibilitycontrol")||linea.toLowerCase().contains("accessibilityhazard")) {
						String ltemp=linea.replaceAll("\t", "").replaceAll(" ", "");
						String mt=metas.replaceAll("\t", "").replaceAll(" ", "");
						if(!mt.toLowerCase().contains(ltemp.toLowerCase())) {//si no contiene el meta la linea
							if(!reemplazar) {//si no queiere
								metas=metas.substring(0,metas.indexOf("</div>"));
								metas+="\t"+linea.replaceAll("\t", " ")+"\n </div>\n";
							}
						}
					}else {
						todo+=linea + "\n";
					}
					
				}else {
					todo+=linea + "\n";
					if(empizaBody) {
						todo+=metas;
						empizaBody=false;
					}
				}
			}
			System.out.println(todo);
			return todo;
		}catch(Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	/**
	 * Metoso que escribe los meta sen el archivo.
	 * @param archivo Es el archivo en el que se van a escribir los metas.
	 * @param contenido Es el contenido q se va a escribir en el archivo.
	 * @return Retorna si se escribio o no.
	 */
	public boolean escribeArchivo(String archivo, String contenido) {
		try {
			BufferedWriter bw=new BufferedWriter(new FileWriter(archivo));
			bw.write(contenido);
			bw.close();
			return true;
		}catch(Exception e) {
			e.printStackTrace();
			return false;
		}
	}
}
