package recomender.data;

import java.io.File;
import java.io.FileInputStream;

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;

public class UploadDeletePDF {
	
	/**
	 * NO SE USA: Metodo que sube archivos pdf a un servidor externo para su posterior analisis
	 * @param archivo Es el archivo a subir.
	 * @param nombreOA Es el nombre del objeto de aprendizaje que contiene ese archivo.
	 * @return Retorna el directorio en el que se creo y el enlace online.
	 */
	public String[] sube(java.io.File archivo, String nombreOA) {
		//String archivo="/home/kike/Documents/how to manage wifi capabilities android studio.pdf";
		
		FTPClient con=null;
		try {
			con= new FTPClient();
			//con.setConnectTimeout(5000);
			//con.connect("198.23.48.87");
			con.connect("accesibilidadecuador.com");
			System.out.println("sale");
			if (con.login("paoingavelez", "auquilla2018","paoingavelez.us.tempcloudsite.com")) {
				System.out.println("Logueado");
				String dir="accesibilidadecuador.com/pdf/"+nombreOA;//creo la ruta en donde se vaa crear el directorio
				String enlace=dir+"/"+archivo.getName();//creo la ruta en donde se va a crear el pdf.
				con.enterLocalPassiveMode(); // important!
	            con.setFileType(FTP.BINARY_FILE_TYPE);
	            FileInputStream in = new FileInputStream(archivo);
	           // con.de
	            con.makeDirectory(dir);//creo el directorio
	            boolean result = con.storeFile(enlace, in);
	            in.close();
                if (result) System.out.println("upload result succeeded");
                con.logout();
                con.disconnect();
                return new String[] {dir,enlace};
			}
			return null;
		}catch(Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	/**
	 * Borra el archivo pdf subido a un servidr externo
	 * @param dir Es el directorio en el que se encuentra el archivo en el servidor externo
	 * @param enlace Es el enlace al archivo.
	 * @return Retorna si se borro o no.
	 */
	public boolean elimina(String dir, String enlace) {
		//String archivo="/home/kike/Documents/how to manage wifi capabilities android studio.pdf";
		
		FTPClient con=null;
		try {
			con= new FTPClient();
			//con.setConnectTimeout(5000);
			//con.connect("198.23.48.87");
			con.connect("accesibilidadecuador.com");
			if (con.login("paoingavelez", "auquilla2018","paoingavelez.us.tempcloudsite.com")) {
				System.out.println("Logueado");
				con.enterLocalPassiveMode(); // important!
	            con.setFileType(FTP.BINARY_FILE_TYPE);
	            con.dele(enlace);
	            boolean result=con.removeDirectory(dir);
	            
                //if (result) System.out.println("upload result succeeded");
                con.logout();
                con.disconnect();
                return result;
			}
			return false;
		}catch(Exception e) {
			e.printStackTrace();
			return false;
		}
	}
}
