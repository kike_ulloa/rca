package recomender.datamanipulation;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class ComparaDatos {
	/**
	 * Metodo que obtiene los metadatos del documento.
	 * @param ruta Es al ruta del docuemto
	 * @param meta Son los emtas a tomar en cuenta.
	 * @return Retorna un listado de los metas que contiene el doc.
	 */
	public java.util.ArrayList<String> obtieneMetadatosDocumento(String ruta , HashMap<String, String> meta) {
		try {
			java.util.ArrayList<String> metadatos = new java.util.ArrayList<String>();

			java.io.File html = new java.io.File(ruta);
			Document doc = Jsoup.parse(html, "UTF-8");
			Elements metas = doc.select("meta");

			for (Element element : metas) {
				if (element.hasAttr("itemprop")) {
					String valor = element.attr("itemprop");
					if (valor.trim().toLowerCase().equals("accessmode")
							|| valor.trim().toLowerCase().equals("accessibilityhazard")
							|| valor.trim().toLowerCase().equals("accessibilitycontrol")
							|| valor.trim().toLowerCase().equals("accessibilityfeature")) {
						if (element.hasAttr("content")) {
							if (element.attr("content") != null && !element.attr("content").equals("")) {
								//si esq esta dentro de los metadatos de las discapacidades seleccionadas
								String iri="http://www.accesiblemetada.com/DRDSchema#"+element.attr("content");
								System.out.println("Tiene: "+meta.containsKey(iri)+" "+iri);
								if(meta.containsKey(iri))
									metadatos.add(element.attr("content"));
							}
						} else {
							System.out.println("No contiene el valor " + valor);
						}
					}
				}
			}
			return metadatos;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	/**
	 * metodo para comparar si contiene o no contiene los metadatos.
	 * @param ruta Es la ruta del archovo html.
	 * @param resultadoTotal Es el resultado total de los metas del analisis de contenido
	 * @param analizarSoloExistentes Indica Analizar solo metadatos existentes
	 * @param metas Metadatos a tomar en cuenta.
	 * @return Retorna un listado de metas.
	 */
	public java.util.ArrayList<String> comparaMetadatos(String ruta, HashMap<String,Object[]> resultadoTotal, boolean analizarSoloExistentes, HashMap<String,String>metas) {
		java.util.ArrayList<String> metasDoc=obtieneMetadatosDocumento(ruta, metas);
		java.util.ArrayList<String> metasAn=retornaListadoMetasporIndex(resultadoTotal, metas);
		java.util.ArrayList<String> metasDocClone=new java.util.ArrayList<String>(metasDoc);
		if(analizarSoloExistentes) {
			//comparo que metadatos tiene y cuales no
			for(String metasD: metasDoc) {
				if(!metasAn.contains(metasD)) {
					metasDocClone.remove(metasD);//elimino el metadato si no existe en los encontrados
				}
			}
			for(String metasA: metasAn) {
				if(!metasDoc.contains(metasA)) {
					metasDocClone.add(metasA);//agrego el metadato si esque no existe en el doc pero si en los encontrados
				}
			}
		}
		return metasDocClone;
	}
	/*
	public java.util.ArrayList<String> retornaListadoMetasporIndex(HashMap<String,Object[]> resultadoTotal, HashMap<String, String>metas){
		java.util.ArrayList<String> metadatos=new java.util.ArrayList<String>();
		if((boolean)resultadoTotal.get("imagenes")[1]) {
			metadatos.add("alternativeText");//texto Alternativo
		}
		if((boolean)resultadoTotal.get("imagenes")[3]) {
			metadatos.add("textOnVisual");//textonVisual
		}
		if((boolean)resultadoTotal.get("tablaContenidos")[1]) {
			metadatos.add("tableOfContents");//tabla de COntenidos
		}
		if((boolean)resultadoTotal.get("tamanoFuente")[1]) {
			metadatos.add("largePrint");//tamanoFuente
		}
		if((boolean)resultadoTotal.get("mathML")[1]) {
			metadatos.add("MathML");//mathML
		}
		if((boolean)resultadoTotal.get("chemML")[1]) {
			metadatos.add("ChemML");//ChemML
		}
		if((boolean)resultadoTotal.get("latex")[1]) {
			metadatos.add("latex");//Latex
		}
		if((boolean)resultadoTotal.get("simulations")[1]) {
			metadatos.add("motionSimulation");//simulaciones
		}else {
			metadatos.add("noMotionSimulationHazard");//No simulaciones
		}
		if((boolean)resultadoTotal.get("tiempos")[1]) {
			metadatos.add("timingControl");//tiempos
		}
		if((boolean)resultadoTotal.get("subs")[1]) {
			metadatos.add("captions");//Captions
		}
		if((boolean)resultadoTotal.get("pdf")[1]){
			metadatos.add("taggedPDF");
		}
		if((boolean)resultadoTotal.get("textual")[1]) {
			metadatos.add("textual");//Texual
		}
		if((boolean)resultadoTotal.get("visual")[1]) {
			metadatos.add("visual");//Visual
		}
		if((boolean)resultadoTotal.get("auditiva")[1]) {
			metadatos.add("auditory");//Auditiva
		}
		if((boolean)resultadoTotal.get("tactil")[1]) {
			metadatos.add("tactile");//Tactil
		}
		if((boolean)resultadoTotal.get("mouse")[1]) {
			metadatos.add("fullMouseControl");//Mouse
		}
		if((boolean)resultadoTotal.get("teclado")[1]) {
			metadatos.add("fullKeyboardControl");//Teclado
		}
		if((boolean)resultadoTotal.get("audioDescripcion")[1]) {
			metadatos.add("audioDescription");//audioDescription
		}
		if((boolean)resultadoTotal.get("senas")[1]) {
			metadatos.add("signLanguage");//Lenguaje de Senas
		}
		if((boolean)resultadoTotal.get("longDesc")[1]) {
			metadatos.add("longDescription");//LongDescription
		}
		if((boolean)resultadoTotal.get("sound")[1]) {
			metadatos.add("sound");//Sound
		}else {
			metadatos.add("noSoundHazard");//NO Sound
		}
		if((boolean)resultadoTotal.get("movimiento")[1]) {
			metadatos.add("flashing");//Animaciones
		}else {
			metadatos.add("noFlashingHazard");//No Animaciones
		}
		if((boolean)resultadoTotal.get("colorDependent")[1]) {
			metadatos.add("colorDependent");//ColorDependent
		}
		return metadatos;
	}*/
///////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////
//////METODO PARA INFERIR SOLO METAS NECESARIOS
/**
 * METODO PARA INFERIR SOLO METAS NECESARIOS
 * @param resultadoTotal metas dle resultado total del analisis.
 * @param metas metadatos a tomar en cuenta.
 * @return Retorna el listado de metadatos.
 */
public java.util.ArrayList<String> retornaListadoMetasporIndex(HashMap<String,Object[]> resultadoTotal, HashMap<String, String>metas){
java.util.ArrayList<String> metadatos=new java.util.ArrayList<String>();
if(metas.containsKey("http://www.accesiblemetada.com/DRDSchema#alternativeText")) {
if((boolean)resultadoTotal.get("imagenes")[1]) {
metadatos.add("alternativeText");//texto Alternativo
}
}
if(metas.containsKey("http://www.accesiblemetada.com/DRDSchema#textOnVisual")) {
if((boolean)resultadoTotal.get("imagenes")[3]) {
metadatos.add("textOnVisual");//textonVisual
}
}
if(metas.containsKey("http://www.accesiblemetada.com/DRDSchema#tableOfContents")) {
if((boolean)resultadoTotal.get("tablaContenidos")[1]) {
metadatos.add("tableOfContents");//tabla de COntenidos
}
}
if(metas.containsKey("http://www.accesiblemetada.com/DRDSchema#largePrint")) {
if((boolean)resultadoTotal.get("tamanoFuente")[1]) {
metadatos.add("largePrint");//tamanoFuente
}
}
if(metas.containsKey("http://www.accesiblemetada.com/DRDSchema#MathML")) {
if((boolean)resultadoTotal.get("mathML")[1]) {
metadatos.add("MathML");//mathML
}
}
if(metas.containsKey("http://www.accesiblemetada.com/DRDSchema#ChemML")) {
if((boolean)resultadoTotal.get("chemML")[1]) {
metadatos.add("ChemML");//ChemML
}
}
if(metas.containsKey("http://www.accesiblemetada.com/DRDSchema#latex")) {
if((boolean)resultadoTotal.get("latex")[1]) {
metadatos.add("latex");//Latex
}
}
if(metas.containsKey("http://www.accesiblemetada.com/DRDSchema#motionSimulation")||metas.containsKey("http://www.accesiblemetada.com/DRDSchema#noMotionSimulationHazard")) {
if((boolean)resultadoTotal.get("simulations")[1]) {
metadatos.add("motionSimulation");//simulaciones
}else {
metadatos.add("noMotionSimulationHazard");//No simulaciones
}
}
if(metas.containsKey("http://www.accesiblemetada.com/DRDSchema#timingControl")) {
if((boolean)resultadoTotal.get("tiempos")[1]) {
metadatos.add("timingControl");//tiempos
}
}
if(metas.containsKey("http://www.accesiblemetada.com/DRDSchema#captions")) {
if((boolean)resultadoTotal.get("subs")[1]) {
metadatos.add("captions");//Captions
}
}
if(metas.containsKey("http://www.accesiblemetada.com/DRDSchema#taggedPDF")) {
if((boolean)resultadoTotal.get("pdf")[1]){
metadatos.add("taggedPDF");
}
}
if(metas.containsKey("http://www.accesiblemetada.com/DRDSchema#textual")) {
if((boolean)resultadoTotal.get("textual")[1]) {
metadatos.add("textual");//Texual
}
}
if(metas.containsKey("http://www.accesiblemetada.com/DRDSchema#visual")) {
if((boolean)resultadoTotal.get("visual")[1]) {
metadatos.add("visual");//Visual
}
}
if(metas.containsKey("http://www.accesiblemetada.com/DRDSchema#auditory")) {
if((boolean)resultadoTotal.get("auditiva")[1]) {
metadatos.add("auditory");//Auditiva
}
}
if(metas.containsKey("http://www.accesiblemetada.com/DRDSchema#tactile")) {
if((boolean)resultadoTotal.get("tactil")[1]) {
metadatos.add("tactile");//Tactil
}
}
if(metas.containsKey("http://www.accesiblemetada.com/DRDSchema#fullMouseControl")) {
if((boolean)resultadoTotal.get("mouse")[1]) {
metadatos.add("fullMouseControl");//Mouse
}
}
if(metas.containsKey("http://www.accesiblemetada.com/DRDSchema#fullKeyboardControl")) {
if((boolean)resultadoTotal.get("teclado")[1]) {
metadatos.add("fullKeyboardControl");//Teclado
}
}
if(metas.containsKey("http://www.accesiblemetada.com/DRDSchema#audioDescription")) {
if((boolean)resultadoTotal.get("audioDescripcion")[1]) {
metadatos.add("audioDescription");//audioDescription
}
}
if(metas.containsKey("http://www.accesiblemetada.com/DRDSchema#signLanguague")) {
if((boolean)resultadoTotal.get("senas")[1]) {
metadatos.add("signLanguage");//Lenguaje de Senas
}
}
if(metas.containsKey("http://www.accesiblemetada.com/DRDSchema#longDescription")) {
if((boolean)resultadoTotal.get("longDesc")[1]) {
metadatos.add("longDescription");//LongDescription
}
}
if(metas.containsKey("http://www.accesiblemetada.com/DRDSchema#sound")||metas.containsKey("http://www.accesiblemetada.com/DRDSchema#noSoundHazard")) {
if((boolean)resultadoTotal.get("sound")[1]) {
metadatos.add("sound");//Sound
}else {
metadatos.add("noSoundHazard");//NO Sound
}
}
if(metas.containsKey("http://www.accesiblemetada.com/DRDSchema#flashing")||metas.containsKey("http://www.accesiblemetada.com/DRDSchema#noFlashingHazard") ) {
if((boolean)resultadoTotal.get("movimiento")[1]) {
metadatos.add("flashing");//Animaciones
}else {
metadatos.add("noFlashingHazard");//No Animaciones
}
}if(metas.containsKey("http://www.accesiblemetada.com/DRDSchema#colorDependent")) {
if((boolean)resultadoTotal.get("colorDependent")[1]) {
metadatos.add("colorDependent");//ColorDependent
}
}
return metadatos;
}
///////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////
/**
 * metodo que elimina valores duplicados.
 * @param metasEnDocs SOn los metas en los documentos
 * @return retorna un listado con metas
 */
	public HashMap<String, Double> eliminaDuplicados(HashMap<String, java.util.ArrayList<String>> metasEnDocs) {
		HashMap<String, Double> metaPorcentaje=new HashMap<String, Double>();
		HashMap<String, Double> metaPorcentajeClon=new HashMap<String, Double>(metaPorcentaje);
		HashMap<String, Integer> numDocs=new HashMap<String, Integer>();
		
		//aux
		/*HashMap<String, String> yaAnalizados=new HashMap<String, String>();
		//debo ver los duplicados
		for(Map.Entry<String, java.util.ArrayList<String>> entry: metasEnDocs.entrySet()) {//recorro lso metadatos por documento
			for(String val:entry.getValue()) {
				yaAnalizados.put(entry.getKey(), val);
				if(!numDocs.containsKey(val)) {//si el hasmap no contiene la llave, le agrego con el valor 1
					numDocs.put(val, 1);
				}
				for(Map.Entry<String, java.util.ArrayList<String>> entry2: metasEnDocs.entrySet()) {//recorro de nuevo los metas por doc
					if(entry.getKey()!=entry2.getKey()) {//si esq los documentos sond iferentes
						for(String val2:entry.getValue()) {
							if(val.equals(val2)) {//si el metadato del documento 1 es igual al meta del documento dos significa q tiene los mismos metas
								if(!yaAnalizados.containsKey(entry2.getKey())||!yaAnalizados.containsValue(val)) {
									System.out.println("Este es el q no contiene keys: "+entry.getKey()+" "+val);
								if(numDocs.containsKey(val)) {//si esque contiene el meta se va a sumar
									numDocs.put(val, numDocs.get(val)+1);
								}
//								else {
//									numDocs.put(val, 1);
//								}
								}
							}
						}
					}
				}
			}
		}*/
		java.util.ArrayList<String> listadocompletoMetas=new java.util.ArrayList<String>();
		for(Map.Entry<String, java.util.ArrayList<String>> entry: metasEnDocs.entrySet()) {//recorro lso metadatos por documento
			listadocompletoMetas.addAll(entry.getValue());//agrego los elementos del listado recuperado al final del listado nuevo
		}
		//cuento las veces que se repiten
		for(String meta:listadocompletoMetas) {
			int frecuencia=Collections.frequency(listadocompletoMetas, meta);
			if(!numDocs.containsKey(meta)) {//en caos de q aun no este el metadato
				numDocs.put(meta, frecuencia);
			}
		}
		HashMap<String, Double> metasPorcentajes= new HashMap<String, Double>();//hasmap para los metas con porcentaje
		for(Map.Entry<String, Integer> entry:numDocs.entrySet()) {//recorro los metas con sus respectivos numeros de repeticiones
			double numMetasEnDoc=entry.getValue();
			double numDocumentos=metasEnDocs.size();
			double procentaje= (numMetasEnDoc/numDocumentos)*100;//numm de documentos con el mismo metadato dividido para numero de documentos por 100
			//System.out.println(procentaje);
			metasPorcentajes.put(entry.getKey(), procentaje);//seteo el metadato con us porcentaje correspondient de ocurrencias
		}
		return metasPorcentajes;
	}
}
