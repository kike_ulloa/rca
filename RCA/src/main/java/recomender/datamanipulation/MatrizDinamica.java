package recomender.datamanipulation;

public class MatrizDinamica {

	/**
	 * Metodo que crea un matriz dinamica
	 * @param matriz Matriz a ser manipulada.
	 * @param cols Numero de columnas de la matriz
	 * @param datos Los datos que se van a agregar en la matriz nueva.
	 * @return Retorna la matriz actualizada/
	 */
	public String [][]matriz(String [][]matriz, int cols, String[]datos){
		String [][]matrizAuxiliar=new String [matriz.length+1][cols];
		for(int i=0;i<matriz.length;i++) {
			for(int j=0;j<cols;j++) {
				matrizAuxiliar[i][j]=matriz[i][j];
			}
		}
		for(int col=0;col<cols;col++) {
			matrizAuxiliar[matrizAuxiliar.length-1][col]=datos[col];
		}
		return matrizAuxiliar;
	}
	/**
	 * Metodo que suma y resta matrices.
	 * @param matriz1 Matriz numero unoi
	 * @param matriz2 Matriz numero dos
	 * @param cols Numerod e columnas de la smatrices
	 * @param esSuma Indica si es suma o resta
	 * @return retorna el resultado de la suma o resta en una nueva matriz.
	 */
	public String[][] sumaRestaResultados(String [][]matriz1, String [][]matriz2, int cols, boolean esSuma){
		String [][]resultado;
		if(matriz1.length>matriz2.length) {
			resultado=new String [matriz1.length][cols];
			boolean siSOnIguales=false;//bandera en la que me dira si ningun ele,ento es igual al de la otra matriz..significa q no se va ni a sumar nia  restar, queda como esta
			for(int i=0;i<matriz1.length;i++) {
				for(int j=0;j<matriz2.length;j++) {
					if(matriz1[i][1].equals(matriz2[j][1])&&matriz1[i][2].equals(matriz2[j][2])) {
						resultado[i][0]=matriz1[i][0];
						resultado[i][1]=matriz1[i][1];
						resultado[i][2]=matriz1[i][2];
						int res=0;
						if(esSuma)
							res=Integer.parseInt(matriz1[i][3])+Integer.parseInt(matriz2[j][3]);
						else
							res=Integer.parseInt(matriz1[i][3])-Integer.parseInt(matriz2[j][3]);
						resultado[i][3]=res+"";
						siSOnIguales=true;
					}
				}
				///si no ha sido iguales seteo la posicion de la matriz con los valore spr defecto de la matrriz mas grande
				if(!siSOnIguales) {
					resultado[i][0]=matriz1[i][0];
					resultado[i][1]=matriz1[i][1];
					resultado[i][2]=matriz1[i][2];
					resultado[i][3]=matriz1[i][3];
				}
				siSOnIguales=false;
				
			}
		}else {
			resultado=new String [matriz2.length][cols];
			boolean siSOnIguales=false;//bandera en la que me dira si ningun ele,ento es igual al de la otra matriz..significa q no se va ni a sumar nia  restar, queda como esta
			for(int i=0;i<matriz2.length;i++) {
				for(int j=0;j<matriz1.length;j++) {
					if(matriz2[i][1].equals(matriz1[j][1])&&matriz2[i][2].equals(matriz1[j][2])) {
						resultado[i][0]=matriz2[i][0];
						resultado[i][1]=matriz2[i][1];
						resultado[i][2]=matriz2[i][2];
						int res=0;
						if(esSuma)
							res=Integer.parseInt(matriz2[i][3])+Integer.parseInt(matriz1[j][3]);
						else
							res=Integer.parseInt(matriz2[i][3])-Integer.parseInt(matriz1[j][3]);
						resultado[i][3]=res+"";
						siSOnIguales=true;
					}
				}
				///si no ha sido iguales seteo la posicion de la matriz con los valore spr defecto de la matrriz mas grande
				if(!siSOnIguales) {
					resultado[i][0]=matriz2[i][0];
					resultado[i][1]=matriz2[i][1];
					resultado[i][2]=matriz2[i][2];
					resultado[i][3]=matriz2[i][3];
				}
				siSOnIguales=false;
			}
		}
		return resultado;
	}
	/**
	 * Metodo que separa pro documentos a la matriz
	 * @param matrizGlobal Es la matriz que se quiere separar por docuementos.
	 * @param cols Numero de columnas
	 * @return Retorna un listado con los datos separados por docs.
	 */
	public java.util.ArrayList<String[][]> separaPorDocumentos(String [][]matrizGlobal, int cols) {
		java.util.ArrayList<String[][]> matricesPorDocumento=new java.util.ArrayList<String[][]>();
		String [][] matrizDoc=new String[0][cols];
		for(int i=1;i<matrizGlobal.length;i++) {
			
			if(matrizGlobal[i][1].equals(matrizGlobal[i-1][1])) {
				
				if((i-1)==0) {
					String []data=new String[cols];
	                data[0]=matrizGlobal[i-1][0];
	                data[1]=matrizGlobal[i-1][1];
	                data[2]=matrizGlobal[i-1][2];
	                data[3]=matrizGlobal[i-1][3];
					matrizDoc=matriz(matrizDoc,cols,data);
					data[0]=matrizGlobal[i][0];
	                data[1]=matrizGlobal[i][1];
	                data[2]=matrizGlobal[i][2];
	                data[3]=matrizGlobal[i][3];
	                matrizDoc=matriz(matrizDoc,cols,data);
				}else {
					String []data=new String[cols];
					data[0]=matrizGlobal[i][0];
	                data[1]=matrizGlobal[i][1];
	                data[2]=matrizGlobal[i][2];
	                data[3]=matrizGlobal[i][3];
	                matrizDoc=matriz(matrizDoc,cols,data);
				}
			}else {
				//agrego la matriz a mi listado
				matricesPorDocumento.add(ordenaRetorna(matrizDoc,cols));
				///borro la matriz para el siguiente documento
				matrizDoc=new String [0][cols];
				String []data=new String[cols];
				data[0]=matrizGlobal[i][0];
                data[1]=matrizGlobal[i][1];
                data[2]=matrizGlobal[i][2];
                data[3]=matrizGlobal[i][3];
                matrizDoc=matriz(matrizDoc,cols,data);
			}
			if(i==matrizGlobal.length-1) {//en caso de que sea el final de la lista debo agregar mi ultima matriz al listado
				//agrego la matriz a mi listado
				matricesPorDocumento.add(ordenaRetorna(matrizDoc,cols));
			}
		}
		return matricesPorDocumento;
	}
	//TENGO QUE HACER UN METODO QUE ORDENE LOS DATOS DE LAS MATRICES DE MAYOR A MENOR PARA SACAR LA DISCAPACIDAD MAYOR POR DOCUMENTO
	/**
	 * Metodo que ordcena una matrizx.
	 * @param matriz Matriz a ordenar
	 * @param cols Numero de columnas
	 * @return Matriz ordenada
	 */
	public String[][] ordenaRetorna(String [][]matriz, int cols) {
		String []mayor=new String[cols];
		//metodo burbuja
		for(int i=0;i<matriz.length;i++) {
			for(int j=i+1;j<matriz.length;j++) {
				int valor1=Integer.parseInt(matriz[i][cols-1]);
				int valor2=Integer.parseInt(matriz[j][cols-1]);
				if(valor1<valor2) {
					String pos0,pos1,pos2,pos3;
					pos0=matriz[i][0];
					pos1=matriz[i][1];
					pos2=matriz[i][2];
					pos3=matriz[i][3];
					matriz[i][0]=matriz[j][0];
					matriz[i][1]=matriz[j][1];
					matriz[i][2]=matriz[j][2];
					matriz[i][3]=matriz[j][3];
					matriz[j][0]=pos0;
					matriz[j][1]=pos1;
					matriz[j][2]=pos2;
					matriz[j][3]=pos3;
				}
			}
		}
		//return retornaMayores(matriz, cols);
		return matriz;
	}
	/**
	 * Metodfo que solo devuleve los mayores de la matriz
	 * @param matriz Matriz a verificar
	 * @param cols Numero de columans
	 * @return listado de valores mayores/
	 */
	public String [][] retornaMayores(String [][]matriz, int cols){
		String [][]mayores=new String[1][cols];
		mayores[0][0]=matriz[0][0];
		mayores[0][1]=matriz[0][1];
		mayores[0][2]=matriz[0][2];
		mayores[0][3]=matriz[0][3];
		for(int i=1;i<matriz.length;i++) {
			int val1=Integer.parseInt(matriz[0][3]);
			int val2=Integer.parseInt(matriz[i][3]);
			if(val1==val2) {
				String []datos=new String[cols];
				datos[0]=matriz[i][0];
				datos[1]=matriz[i][1];
				datos[2]=matriz[i][2];
				datos[3]=matriz[i][3];
				mayores=matriz(mayores, cols, datos);
			}
		}
		return mayores;
	}
}
