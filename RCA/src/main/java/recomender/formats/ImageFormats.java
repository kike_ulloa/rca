package recomender.formats;

public class ImageFormats {

	java.util.ArrayList<String>formatosImagenes=new java.util.ArrayList<String>();
	
	/**
	 * Metodo que devuelve los formatos d eimagenes
	 * @return retorna un listado con los formatos de imagenes.
	 */
	public java.util.ArrayList<String> retornaFormatos(){
		formatosImagenes.clear();
		formatosImagenes=new java.util.ArrayList<String>();
		formatosImagenes.add(".BMP"); 
		formatosImagenes.add(".DCX");
		formatosImagenes.add(".GIF");
		formatosImagenes.add(".ICNS"); 
		formatosImagenes.add(".ICO"); 
		formatosImagenes.add(".JBIG2"); 
		formatosImagenes.add(".JPEG"); 
		formatosImagenes.add(".JPG"); 
		formatosImagenes.add(".PAM"); 
		formatosImagenes.add(".PBM"); 
		formatosImagenes.add(".PCX");
		formatosImagenes.add(".PGM"); 
		formatosImagenes.add(".PNG"); 
		formatosImagenes.add(".PNM");
		formatosImagenes.add(".PPM"); 
		formatosImagenes.add(".PSD"); 
		formatosImagenes.add(".RGBE");
		formatosImagenes.add(".TGA");
		formatosImagenes.add(".TIFF");
		formatosImagenes.add(".UNKNOWN"); 
		formatosImagenes.add(".WBMP");
		formatosImagenes.add(".XBM");
		formatosImagenes.add(".XPM");
		return formatosImagenes;
	}
}
