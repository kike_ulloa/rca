package recomender.ontology;

import javax.annotation.PostConstruct;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import com.franz.agraph.repository.AGRepository;
import com.franz.agraph.repository.AGServer;
@ApplicationScoped
public class Connection {

	private static final String SERVER_URL = "http://localhost:10035";
    private static final String CATALOG_ID = "system";
    private static final String REPOSITORY_ID = "Recomendador";
    private static final String USERNAME = "test";
    private static final String PASSWORD = "xyzzy";
    private static final java.io.File DATA_DIR = new java.io.File(".");
    
    private AGRepository repository;
    @PostConstruct
    public void init() {
    	
    }
    
    /**
     * Crea la conexion con la bae de conocimiento alegrGraph.
     */
	public Connection() {
		try {
    		System.out.println("A");
    		System.out.println("\nStarting example1().");
            AGServer server = new AGServer(SERVER_URL, USERNAME, PASSWORD);
            System.out.println("Available catalogs: " + server.listCatalogs());
            System.out.println("A0");
            repository= server.getRootCatalog().openRepository(REPOSITORY_ID);
            System.out.println("A1");
            repository.getConnection();
            System.out.println("A2");
    	}catch(Exception e) {
    		e.printStackTrace();
    	}
	}


	public AGRepository getRepository() {
		return repository;
	}
	public void setRepository(AGRepository repository) {
		this.repository = repository;
	}
    
    
}
