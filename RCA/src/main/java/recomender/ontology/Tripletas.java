package recomender.ontology;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.inject.Inject;

import org.eclipse.rdf4j.model.Value;
import org.eclipse.rdf4j.query.BindingSet;
import org.eclipse.rdf4j.query.QueryLanguage;
import org.eclipse.rdf4j.query.TupleQueryResult;

import com.franz.agraph.repository.AGTupleQuery;

import recomender.datamanipulation.MatrizDinamica;
@RequestScoped
public class Tripletas {
	
	@Inject
	private Connection con;
	
	public java.util.ArrayList<String> crearTripletas() {
		System.out.println("C");
		//con.getRepository();
		java.util.ArrayList<String> irisMetas=new java.util.ArrayList<String>(); 
		try {
    		String query = "SELECT ?s ?p ?o WHERE {?s ?p ?o .} LIMIT 5";
    		String querySoloMetas="PREFIX wcag:<http://www.AccessibleOntology.com/WCAG2.owl#>\n" + 
    				"PREFIX sch:<http://www.accesiblemetada.com/DRDSchema#>\n" + 
    				"PREFIX sko:<http://www.w3.org/2004/02/skos/core#>\n" + 
    				"PREFIX rdf:<http://www.w3.org/1999/02/22-rdf-syntax-ns#>\n" + 
    				"SELECT DISTINCT ?o\n" + 
    				"WHERE {\n" + 
    				"		?a rdf:type wcag:SuccessCriterion .\n"+ 
    				"		?o rdf:type sch:DRD000002." + 
    				"  		OPTIONAL{?o sch:DRD000011R ?a}.\n" + 
    				"  		OPTIONAL{?o sch:DRD000009R ?x}.\n" + 
    				"  		OPTIONAL{?o sch:DRD000007R ?y}.\n" + 
    				"}";
    		System.out.println("C1");
    		con.getRepository();
    		AGTupleQuery tupleQuery = con.getRepository().getConnection().prepareTupleQuery(QueryLanguage.SPARQL, querySoloMetas);
    		System.out.println("C2");
            TupleQueryResult result = tupleQuery.evaluate();
            System.out.println("C3");
            try {
	            while (result.hasNext()) {
	                BindingSet bindingSet = result.next();
	                Value s = bindingSet.getValue("o");
//	                Value p = bindingSet.getValue("p");
//	                Value o = bindingSet.getValue("o");
	                System.out.format("%s \n", s );
	                irisMetas.add(s.stringValue());
	            }
    		}finally {
            	result.close();
            }
            long count = tupleQuery.count();
            System.out.println("Count: "+count);
            return irisMetas;
    	}catch(Exception e) {
    		e.printStackTrace();
    		return null;
    	}
	}
	/**
	 * Metodo que crea la stripletas para favorece A
	 * @param iriMetadato Es el IRI del metadato
	 * @return retorna una lista de los IRIs de las discapacidades a las que favorece
	 */
	public java.util.ArrayList<String> crearTripletaFavoreceA(String iriMetadato) {
		java.util.ArrayList<String> irisDiscapacidades=new java.util.ArrayList<String>(); 
		try {
			
    		String query = "PREFIX wcag:<http://www.AccessibleOntology.com/WCAG2.owl#>\n" + 
    				"PREFIX sch:<http://www.accesiblemetada.com/DRDSchema#>\n" + 
    				"PREFIX sko:<http://www.w3.org/2004/02/skos/core#>\n" + 
    				"PREFIX rdf:<http://www.w3.org/1999/02/22-rdf-syntax-ns#>\n" + 
    				"SELECT DISTINCT ?o\n" + 
    				"WHERE {\n" + 
    				"  		<"+iriMetadato+"> sch:DRD000009R ?o .\n" + 
    				"}";
    		con.getRepository();
    		AGTupleQuery tupleQuery = con.getRepository().getConnection().prepareTupleQuery(QueryLanguage.SPARQL, query);
            TupleQueryResult result = tupleQuery.evaluate();
            try {
	            while (result.hasNext()) {
	                BindingSet bindingSet = result.next();
	                Value s = bindingSet.getValue("o");
	                irisDiscapacidades.add(s.stringValue());
	                //System.out.println(s.stringValue());
	            }
    		}finally {
            	result.close();
            }
            long count = tupleQuery.count();
            //System.out.println("CountDisFav: "+count);
            return irisDiscapacidades;
    	}catch(Exception e) {
    		e.printStackTrace();
    		return null;
    	}
	}
	/**
	 * Metodo que crea las tripletas para el desfavorece o afectaA
	 * @param iriMetadato Es el IRI del metadato
	 * @return Retorna un listado de IRIs de las discapacidades a las que afecta el metadato
	 */
	public java.util.ArrayList<String> crearTripletaAfectaA(String iriMetadato) {
		java.util.ArrayList<String> irisDiscapacidades=new java.util.ArrayList<String>(); 
		try {
    		String query = "PREFIX wcag:<http://www.AccessibleOntology.com/WCAG2.owl#>\n" + 
    				"PREFIX sch:<http://www.accesiblemetada.com/DRDSchema#>\n" + 
    				"PREFIX sko:<http://www.w3.org/2004/02/skos/core#>\n" + 
    				"PREFIX rdf:<http://www.w3.org/1999/02/22-rdf-syntax-ns#>\n" + 
    				"SELECT DISTINCT ?o\n" + 
    				"WHERE {\n" + 
    				"  		<"+iriMetadato+"> sch:DRD000007R ?o .\n" + 
    				"}";
    		con.getRepository();
    		AGTupleQuery tupleQuery = con.getRepository().getConnection().prepareTupleQuery(QueryLanguage.SPARQL, query);
            TupleQueryResult result = tupleQuery.evaluate();
            try {
	            while (result.hasNext()) {
	                BindingSet bindingSet = result.next();
	                Value s = bindingSet.getValue("o");
	                irisDiscapacidades.add(s.stringValue());
	                //System.out.println(s.stringValue());
	            }
    		}finally {
            	result.close();
            }
            long count = tupleQuery.count();
            //System.out.println("CountDisFav: "+count);
            return irisDiscapacidades;
    	}catch(Exception e) {
    		e.printStackTrace();
    		return null;
    	}
	}
	
	/**
	 * Metodo que crea las tripletas para extraer el criterio al que el metadato cumple.
	 * @param iriMetadato Es el IRI del metadato
	 * @return Retorna un listado de IRIs de los criterios que cumple el metadato
	 */
	public java.util.ArrayList<String> crearTripletaCriteriosCumple(String iriMetadato) {
		java.util.ArrayList<String> irisCriterios=new java.util.ArrayList<String>(); 
		try {
    		String query = "PREFIX wcag:<http://www.AccessibleOntology.com/WCAG2.owl#>\n" + 
    				"PREFIX sch:<http://www.accesiblemetada.com/DRDSchema#>\n" + 
    				"PREFIX sko:<http://www.w3.org/2004/02/skos/core#>\n" + 
    				"PREFIX rdf:<http://www.w3.org/1999/02/22-rdf-syntax-ns#>\n" + 
    				"SELECT DISTINCT ?o\n" + 
    				"WHERE {\n" + 
    				"  		<"+iriMetadato+"> sch:DRD000011R ?o .\n" + 
    				"}";
    		con.getRepository();
    		AGTupleQuery tupleQuery = con.getRepository().getConnection().prepareTupleQuery(QueryLanguage.SPARQL, query);
            TupleQueryResult result = tupleQuery.evaluate();
            try {
	            while (result.hasNext()) {
	                BindingSet bindingSet = result.next();
	                Value s = bindingSet.getValue("o");
	                irisCriterios.add(s.stringValue());
	                //System.out.println(s.stringValue());
	            }
    		}finally {
            	result.close();
            }
            long count = tupleQuery.count();
            //System.out.println("CountDisFav: "+count);
            return irisCriterios;
    	}catch(Exception e) {
    		e.printStackTrace();
    		return null;
    	}
	}
	
	/**
	 * Metodo que crea las tripletas para extraer la descripcion en espanol del criterio.
	 * @param iriMetadato Es el IRI del metadato
	 * @return Retorna una cadena con la descripcion del criterio
	 */
	public String crearTripletaCriteriosDescripcion(String iriCriterio) {
		//java.util.ArrayList<String> irisCriterios=new java.util.ArrayList<String>();
		String descripcion="";
		try {
    		String query = "PREFIX wcag:<http://www.AccessibleOntology.com/WCAG2.owl#>\n" + 
    				"PREFIX wcag2:<http://www.AccessibleOntology.com/GenericOntology.owl#>\n" + 
    				"PREFIX sch:<http://www.accesiblemetada.com/DRDSchema#> \n" + 
    				"PREFIX sko:<http://www.w3.org/2004/02/skos/core#>\n" + 
    				"PREFIX rdf:<http://www.w3.org/1999/02/22-rdf-syntax-ns#>\n" + 
    				"SELECT DISTINCT ?o\n" + 
    				" WHERE \n" + 
    				"{\n" + 
    				" "+iriCriterio+" wcag2:hasDescription ?o .\n" + 
    				"	FILTER(lang(?o)='es').\n" + 
    				"}";
    		con.getRepository();
    		AGTupleQuery tupleQuery = con.getRepository().getConnection().prepareTupleQuery(QueryLanguage.SPARQL, query);
            TupleQueryResult result = tupleQuery.evaluate();
            try {
	            while (result.hasNext()) {
	                BindingSet bindingSet = result.next();
	                Value s = bindingSet.getValue("o");
	                descripcion = s.stringValue();
	                ///irisCriterios.add(s.stringValue());
	                //System.out.println(s.stringValue());
	            }
    		}finally {
            	result.close();
            }
            long count = tupleQuery.count();
            //System.out.println("CountDisFav: "+count);
            return descripcion;
    	}catch(Exception e) {
    		e.printStackTrace();
    		return null;
    	}
	}
	
	/**
	 * Metodo que crea las tripletas para extraer la descripcion en espanol del criterio.
	 * @param iriMetadato Es el IRI del metadato
	 * @return Retorna una cadena con la descripcion del criterio
	 */
	public java.util.List<java.util.List<String>> crearTripletaQuery1(String nodoIRI, String rdfType) {
		//java.util.ArrayList<String> irisCriterios=new java.util.ArrayList<String>();
		java.util.List<String> documentos=new java.util.ArrayList<String>();
		java.util.List<String> discapacidades=new java.util.ArrayList<String>();
		java.util.List<String> POCOntiene=new java.util.ArrayList<String>();
		String descripcion="";
		try {
    		String query = "PREFIX fav:<http://www.accesiblemetada.com/DRDSchema#>\n" + 
    				"PREFIX des: <http://www.accesiblemetada.com/DRDSchema#>\n" + 
    				"SELECT  (?o AS ?DOCUMENTO) (?d as ?DISCAPACIDAD) (?s AS ?Nb_Contiene) (?p AS ?PO_CONTIENE)  { "+nodoIRI+" <http://www.accesiblemetada.com/DRDSchema#DRD000021R> ?o .#saca los nodos en blanco que representan al documento\n" + 
    				"                \n" + 
    				"                ?o <http://www.accesiblemetada.com/DRDSchema#DRD000016R> ?s. #saca los nodos en blanco del metadata y nometadata\n" + 
    				"             	?s <http://www.w3.org/1999/02/22-rdf-syntax-ns#type>   "+rdfType+" . #que solo sea de tipo no metadata\n" + 
    				"                \n" + 
    				"              	?s ?p ?d .\n" + 
    				"             \n" + 
    				"                   \n" + 
    				"                FILTER(?p=fav:DRD000009R || ?p=des:DRD000007R)\n" + 
    				"                   \n" + 
    				"               \n" + 
    				"                }"
    				+ "order by ?o";
    		con.getRepository();
    		AGTupleQuery tupleQuery = con.getRepository().getConnection().prepareTupleQuery(QueryLanguage.SPARQL, query);
            TupleQueryResult result = tupleQuery.evaluate();
            try {
	            while (result.hasNext()) {
	                BindingSet bindingSet = result.next();
	                Value o = bindingSet.getValue("DOCUMENTO");
	                Value d = bindingSet.getValue("DISCAPACIDAD");
	                Value s = bindingSet.getValue("Nb_Contiene");
	                Value p = bindingSet.getValue("PO_CONTIENE");
	                documentos.add(o.stringValue());
	                discapacidades.add(d.stringValue());
	                POCOntiene.add(p.stringValue());
	                descripcion = s.stringValue();
	                ///irisCriterios.add(s.stringValue());
	                //System.out.println(s.stringValue());
	            }
    		}finally {
            	result.close();
            }
            long count = tupleQuery.count();
            //System.out.println("CountDisFav: "+count);
            java.util.List<java.util.List<String>> retorna=new java.util.ArrayList<>();
            retorna.add(documentos);
            retorna.add(discapacidades);
            retorna.add(POCOntiene);
            return retorna;
    	}catch(Exception e) {
    		e.printStackTrace();
    		return null;
    	}
	}
	/**
	 * Metodo que crea las tripletas para los metadatos que contiene con discapacidades a favor
	 * @param nodoIRI Es el IRI del objeto de aprendizaje
	 * @param rdfType es el tipo de metadato, si es de tipo contiene o no contiene
	 * @return Retorna un 
	 */
	public  String [][] crearTripletaContieneAFavor(String nodoIRI, String rdfType, String favoreceDesfavorece, String label, String []noAplica) {
		int cols=4;
		String [][]datos=new String[0][cols];
		String LO="";
		String metas="";
		recomender.datamanipulation.MatrizDinamica md=new MatrizDinamica();
		for(String m: noAplica) {
			metas+=m+",";
		}
		metas=metas.substring(0,metas.length()-1);
		try {
			String query="# View triples\n" + 
					"PREFIX sch:<http://www.accesiblemetada.com/DRDSchema#>\n" + 
					"PREFIX rdfs:<http://www.w3.org/2000/01/rdf-schema#>\n" + 
					"PREFIX rdf:<http://www.w3.org/1999/02/22-rdf-syntax-ns#>\n" + 
					"PREFIX skos:<http://www.w3.org/2004/02/skos/core#>\n" + 
					"SELECT  ?o ?l ?de (count(?de)as ?numde) {  #(count(?fa) as ?numfa)\n" + 
					"				"+nodoIRI+" rdfs:label '"+label+"'."+
					"   				"+nodoIRI+" sch:DRD000021R ?o .#saca los nodos en blanco que representan al documento               \n" + 
					"                ?o sch:DRD000016R ?s. #saca los nodos en blanco del metadata y nometadata\n" + 
					"  				?o rdfs:label ?l.\n"
					+ 				"?s sch:DRD000017R ?m." + 
					"             	?s rdf:type  "+rdfType+" . #que solo sea de tipo  metadata                \n" + 
					"  				#?s sch:DRD000007R ?de.  #desfavorece\n" + 
					"  				?s  "+favoreceDesfavorece+" ?de. #favorece\n" + 
					"				filter(?m not in ("+metas+"))."	+		
					"                }\n" + 
					"group by  ?o ?l ?de ?numde #?d  #?numfa \n" + 
					"order by ?l";
			con.getRepository();
    		AGTupleQuery tupleQuery = con.getRepository().getConnection().prepareTupleQuery(QueryLanguage.SPARQL, query);
            TupleQueryResult result = tupleQuery.evaluate();
            try {
            	
	            while (result.hasNext()) {
	                BindingSet bindingSet = result.next();
	                Value oA = bindingSet.getValue("o");
	                Value document = bindingSet.getValue("l");
	                Value discapacidad = bindingSet.getValue("de");
	                Value num = bindingSet.getValue("numde");
	                if(oA==null)
	                	LO=null;
	                else
	                	LO=oA.stringValue();//guardo el oa en LO
	                String []data=new String[cols];
	                data[0]=LO;
	                if(document==null)
	                	data[1]=null;
	                else
	                	data[1]=document.stringValue();
	                if(discapacidad==null)
	                	data[2]=null;
	                else
	                	data[2]=discapacidad.stringValue();
	                if(num==null)
	                	data[3]=null;
	                else
	                	data[3]=num.stringValue();
	                datos=md.matriz(datos, cols, data);//envio a agregar en la matriz
	            }
    		}finally {
            	result.close();
            }
            
			con.getRepository().getConnection().close();
			return datos;
		}catch(Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	/**
	 * Metodo que crea las tripletas para los metadatos que contiene con discapacidades a favor
	 * @param nodoIRI Es el IRI del objeto de aprendizaje
	 * @param rdfType es el tipo de metadato, si es de tipo contiene o no contiene
	 * @return Retorna un 
	 */
	public  String [][] crearTripletaResultadoFinal(String nodoIRI, String favoreceDesfavorece, String label, String []noAplica, String []discapacidades) {
		int cols=4;
		String [][]datos=new String[0][cols];
		String LO="";
		String metas="";
		recomender.datamanipulation.MatrizDinamica md=new MatrizDinamica();
		for(String m: noAplica) {
			metas+=m+",";
		}
		metas=metas.substring(0,metas.length()-1);
		String disc="";
		for(String d:discapacidades) {
			disc+="sch:"+d+",";
		}
		disc=disc.substring(0,disc.length()-1);
		try {
			String query="# View triples\n" + 
					"PREFIX sch:<http://www.accesiblemetada.com/DRDSchema#>\n" + 
					"PREFIX rdfs:<http://www.w3.org/2000/01/rdf-schema#>\n" + 
					"PREFIX rdf:<http://www.w3.org/1999/02/22-rdf-syntax-ns#>\n" + 
					"PREFIX skos:<http://www.w3.org/2004/02/skos/core#>\n" + 
					"SELECT  ?documento  ?label  ?discapacidad  (count(?s)as ?numFavorece) {\n" + 
					"  "+nodoIRI+" rdfs:label '"+label+"'.\n" + 
					" #obtiene los nodos en blanco que representan al documento           \n" + 
					"  "+nodoIRI+" sch:DRD000021R ?documento.\n" + 
					" #obtiene los nodos en blanco del ContieneMetadata y NoContieneMetadata \n" + 
					"  ?documento sch:DRD000016R ?s. \n" + 
					" #obtine los nodos en blanco de los metadatos\n" + 
					"  ?s sch:DRD000017R ?m. \n" + 
					" #obtiene el label de los documentos html\n" + 
					"  ?documento rdfs:label ?label.\n" + 
					"  #que solo sea de tipo  ContieneMetadata                \n" + 
					"  ?s rdf:type  ?contno .\n" + 
					"  #favoreceA\n" + 
					"  ?s  ?favaf ?discapacidad. \n" + 
					"  #Filtro por metadatos\n" + 
					"  \n" + 
					"  filter(?discapacidad in ("+disc+")).\n" + 
					"   filter(?contno in(sch:DRD000015, sch:DRD000014  )).\n" + 
					"  filter(?favaf in("+favoreceDesfavorece+")).\n" + 
					" 	filter(?m NOT in("+metas+"))\n" + 
					"}\n" + 
					"group by  ?documento  ?label ?discapacidad  ?numFavorece \n" + 
					"order by ?label\n" + 
					"";
			con.getRepository();
    		AGTupleQuery tupleQuery = con.getRepository().getConnection().prepareTupleQuery(QueryLanguage.SPARQL, query);
            TupleQueryResult result = tupleQuery.evaluate();
            try {
            	
	            while (result.hasNext()) {
	                BindingSet bindingSet = result.next();
	                Value documento = bindingSet.getValue("documento");
	                Value labelDocumento = bindingSet.getValue("label");
	                Value discapacidad = bindingSet.getValue("discapacidad");
	                Value numTotal = bindingSet.getValue("numFavorece");
	                if(documento==null)
	                	LO=null;
	                else
	                	LO=documento.stringValue();//guardo el oa en LO
	                String []data=new String[cols];
	                data[0]=LO;
	                if(labelDocumento==null)
	                	data[1]=null;
	                else
	                	data[1]=labelDocumento.stringValue();
	                if(discapacidad==null)
	                	data[2]=null;
	                else
	                	data[2]=discapacidad.stringValue();
	                if(numTotal==null)
	                	data[3]=null;
	                else
	                	data[3]=numTotal.stringValue();
	                datos=md.matriz(datos, cols, data);//envio a agregar en la matriz
	            }
    		}finally {
            	result.close();
            }
            
			con.getRepository().getConnection().close();
			return datos;
		}catch(Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	/**
	 * Metodo que retorna los criterios de exito en espanol
	 * @param favoreceDesfavorece es la relaciona  usarse en el query
	 * @param discapacidades SOn las discapacidades seleccionadas
	 * @return Retirna una matriz con los qresultados del query
	 */
	public String [][] retornaResultadoFinal(String discapacidad, String favoreceDesfavorece, String lang, String[] noAplica){
		int cols=5;
		String [][]datos=new String[0][cols];
		recomender.datamanipulation.MatrizDinamica md=new MatrizDinamica();
		String metas="";
		for(String m: noAplica) {
			metas+=m+",";
		}
		metas=metas.substring(0,metas.length()-1);
		try {
			String query="# View triples\n" + 
					"PREFIX sch:<http://www.accesiblemetada.com/DRDSchema#>\n" + 
					"PREFIX rdfs:<http://www.w3.org/2000/01/rdf-schema#>\n" + 
					"PREFIX rdf:<http://www.w3.org/1999/02/22-rdf-syntax-ns#>\n" + 
					"PREFIX skos:<http://www.w3.org/2004/02/skos/core#>\n" + 
					"PREFIX wca:<http://www.AccessibleOntology.com/WCAG2.owl#>\n" + 
					"SELECT ?Metadato ?WCAG ?Descripcion ?Discapacidad_Favorecida ?Discapacidad_Afectada{ ?Metadato rdf:type sch:DRD000002.\n" + 
					"           \n" + 
					"           optional{?Metadato sch:DRD000011R ?WCAG. ?WCAG <http://www.AccessibleOntology.com/GenericOntology.owl#hasDescription> ?Descripcion}.\n" + 
					"           optional{?Metadato sch:DRD000009R ?Discapacidad_Favorecida}.\n" + 
					"           optional{?Metadato sch:DRD000007R ?Discapacidad_Afectada}.\n" + 
					"           \n" + 
					"           FILTER(lang(?Descripcion)=\""+lang+"\").\n" + 
					"           FILTER("+favoreceDesfavorece+"=sch:"+discapacidad+").\n" + 
					"           FILTER(?Metadato not in ("+metas+")).\n"
					+ "# FILTER(?Metadato = sch:fullKeyboardControl).\n" + 
					"           #FILTER(?Metadato = sch:audioDescription).\n" + 
					"         }\n" + 
					"order by ?Metadato";
			con.getRepository();
    		AGTupleQuery tupleQuery = con.getRepository().getConnection().prepareTupleQuery(QueryLanguage.SPARQL, query);
            TupleQueryResult result = tupleQuery.evaluate();
            try {
            	
	            while (result.hasNext()) {
	                BindingSet bindingSet = result.next();
	                Value metadato = bindingSet.getValue("Metadato");
	                Value wcag = bindingSet.getValue("WCAG");
	                Value descripcion = bindingSet.getValue("Descripcion");
	                Value dFavor = bindingSet.getValue("Discapacidad_Favorecida");
	                Value dAfecta = bindingSet.getValue("Discapacidad_Afectada");
	                
	                String []data=new String[cols];
	                if(metadato==null) {
	                	data[0]=null;
	                }else {
	                	data[0]=metadato.stringValue();
	                	//System.out.println(data[0]+"  "+metadato.stringValue());
	                }
	                if(wcag==null) {
	                	data[1]=null;
	                }else {
	                	data[1]=wcag.stringValue();
	                	//System.out.println(data[1]+"  "+wcag.stringValue());
	                }
	                if(descripcion==null) {
	                	data[2]=null;
	                }else {
	                	data[2]=descripcion.stringValue();
	                	//System.out.println(data[2]+"  "+descripcion.stringValue());
	                }
	                if(dFavor==null) {
	                	data[3]=null;
	                }else {
	                	data[3]=dFavor.stringValue();
	                	//System.out.println(data[3]+"   "+dFavor.stringValue());
	                }
	                if(dAfecta==null) {
	                	data[4]=null;
	                }else {
	                	data[4]=dAfecta.stringValue();
	                	//System.out.println(data[4]+"   "+dAfecta);
	                }
	                
	                datos=md.matriz(datos, cols, data);//envio a agregar en la matriz
	            }
    		}finally {
            	result.close();
            }
			con.getRepository().getConnection().close();
			return datos;
		}catch(Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	/**
	 * Metodo que retorna los meta spor discapacidad
	 * @param favoreceDesfavorece es la relaciona  usarse en el query
	 * @param discapacidades SOn las discapacidades seleccionadas
	 * @return Retirna una matriz con los qresultados del query
	 */
	public String [][] retornaMetadatosPorDiscapacidad(String favoreceDesfavorece, String []discapacidades){
		int cols=1;
		String [][]datos=new String[0][cols];
		recomender.datamanipulation.MatrizDinamica md=new MatrizDinamica();
		String disc="";
		for(String d: discapacidades) {
			disc+=d+",";
		}
		disc=disc.substring(0,disc.length()-1);
		try {
			String query="# View triples\n" + 
					"PREFIX sch:<http://www.accesiblemetada.com/DRDSchema#>\n" + 
					"PREFIX rdfs:<http://www.w3.org/2000/01/rdf-schema#>\n" + 
					"PREFIX rdf:<http://www.w3.org/1999/02/22-rdf-syntax-ns#>\n" + 
					"PREFIX skos:<http://www.w3.org/2004/02/skos/core#>\n" + 
					"SELECT distinct ?meta  { \n" + 
					"  		?meta rdf:type sch:DRD000002.\n" + 
					"        ?meta ?favaf ?disc." + 
					"  filter(?disc in ("+disc+")).\n" + 
					"	filter (?favaf in(sch:DRD000007R,sch:DRD000009R)).}";
			con.getRepository();
    		AGTupleQuery tupleQuery = con.getRepository().getConnection().prepareTupleQuery(QueryLanguage.SPARQL, query);
            TupleQueryResult result = tupleQuery.evaluate();
            try {
            	
	            while (result.hasNext()) {
	                BindingSet bindingSet = result.next();
	                Value metadato = bindingSet.getValue("meta");
	                Value discapacidad = bindingSet.getValue("disc");
	                
	                String []data=new String[cols];
	                if(metadato==null) {
	                	data[0]=null;
	                }else {
	                	data[0]=metadato.stringValue();
	                	//System.out.println(data[0]+"  "+metadato.stringValue());
	                }
//	                if(discapacidad==null) {
//	                	data[1]=null;
//	                }else {
//	                	data[1]=discapacidad.stringValue();
//	                	//System.out.println(data[1]+"  "+wcag.stringValue());
//	                }
	                      
	                datos=md.matriz(datos, cols, data);//envio a agregar en la matriz
	            }
    		}finally {
            	result.close();
            }
			con.getRepository().getConnection().close();
			return datos;
		}catch(Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	public Connection getCon() {
		return con;
	}

	public void setCon(Connection con) {
		this.con = con;
	}
	
	
}
