package recomender.ontology;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.HashMap;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;
import javax.print.attribute.HashAttributeSet;

import org.apache.jena.assembler.assemblers.DocumentManagerAssembler;
import org.eclipse.rdf4j.model.BNode;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.franz.agraph.repository.AGRepositoryConnection;
import com.franz.agraph.repository.AGValueFactory;

import recomender.datamanipulation.ComparaDatos;
import recomender.datamanipulation.MatrizDinamica;
import recomender.staticcontent.VariablesEstaticas;

@ViewScoped
public class inferirOntologia {

	@Inject
	private Tripletas tripletas;

	private String iriOA="";
	private String label="";
	public void creacionTripletas() {
		//System.out.println("B");
		tripletas.crearTripletas();

	}
	
	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}
	/**
	 * Metodo que recibe el resutado de los metadatos por discapacidad y retorna los mismos.
	 * @param favoreceDesfavorece Es la relacion para indicar al query que debe extraer tanto del favorece como del desfavorece.
	 * @param discapacidades Listado de las discapacidades seleccionadas.
	 * @return Retorna una matriz con los metadatos por discapacidad seleccionada.
	 */
	public String [][] retornaMetasPorDiscapacidades(String favoreceDesfavorece, String []discapacidades){
		String [][]resultado=tripletas.retornaMetadatosPorDiscapacidad(favoreceDesfavorece, discapacidades);
		return resultado;
	}
	/**
	 * Metodo que retorna los criterios de exito por metadato y discapacidad.
	 * @param discapacidad Listado de discapacdades seleccionadas.
	 * @param favoreceDesfavorece 
	 * @param lang Indica el lenguaje de los criterios de exito a extraer.
	 * @param noAplica Listado de metadatos que no se aplican.
	 * @return Retorna los criterios de exitor por meta y discapacidad.
	 */
	public String [][] returnResultadoFinal(String discapacidad, String favoreceDesfavorece, String lang, String []noAplica){
		String [][]resultado=tripletas.retornaResultadoFinal(discapacidad, favoreceDesfavorece, lang, noAplica);
		return resultado;
	}
	/**
	 * Metodo que llama a los metdos para extraer los conteos de las ocurrncias de metadatos por documento y discapacidad para
	 * determinar los porcentajes de accesibilida dpor cada discapacidad.
	 * @param iriOA Es el IRI del objeto de aprendizaje inferenciado.
	 * @param label Es el label del objeto de aprendizaje inferenciado.
	 * @param noAplica Listado de metadatos que no aplican.
	 * @param discapacidades Listado de discapacidades seleccionadas.
	 * @return Retorna un listado con los valor spor discapacidad que favorecen tanto en el contiene como en el no contiene
	 *  y tambien los valores totales d ela suma de los favorece y desfavorece de los contiene y no contiene.
	 */
	public ArrayList query(String iriOA, String label, String []noAplica, String [] discapacidades) {
		//String nodoIRI="_:b3641F3D2x19974";
		String nodoIRI=iriOA;
		//String rdfType="<http://www.accesiblemetada.com/DRDSchema#DRD000014>";
		String favorece="<http://www.accesiblemetada.com/DRDSchema#DRD000009R>";
		int cols=4;
		//String [][]resultadoContieneFavorece=tripletas.crearTripletaContieneAFavor(nodoIRI, rdfType, favorece, label, noAplica);
//		if(resultadoContieneFavorece!=null) {
//			for(int i=0;i<resultadoContieneFavorece.length;i++) {
//				for(int j=0;j<cols;j++) {
//					System.out.print(resultadoContieneFavorece[i][j]);
//				}
//				System.out.println();
//			}
//		}
//		String desfavorece="<http://www.accesiblemetada.com/DRDSchema#DRD000007R>";
//		String [][]resultadoContieneDesfavorece=tripletas.crearTripletaContieneAFavor(nodoIRI, rdfType, desfavorece, label, noAplica);
		
		
	//	rdfType="<http://www.accesiblemetada.com/DRDSchema#DRD000015>";
//		String noContienefavorece="<http://www.accesiblemetada.com/DRDSchema#DRD000009R>";
//		String [][]resultadoNoContieneFavorece=tripletas.crearTripletaContieneAFavor(nodoIRI, rdfType, noContienefavorece, label, noAplica);
//		String noContienedesfavorece="<http://www.accesiblemetada.com/DRDSchema#DRD000007R>";
//		String [][]resultadoNoContieneDesfavorece=tripletas.crearTripletaContieneAFavor(nodoIRI, rdfType, noContienedesfavorece, label, noAplica);
//		//SUMAR LOS RESULTADOCONTIENEFAVORECE CON LOS RESULTADONOCONTIENEDESFAVORECE
		//SUMAR LOS RESULTADOSCONTIENEDESFAVORECE CON LOS RESULTADOSNOCONTIENEDESFAORECE
		//RESTAR LOS DOS RESULTADOS Y VER LA DISCAPACIDAD CON MAYOR NUMERO
		
		//PARA EL OA
		//SUMAR LOS RESULTADOS DE LAS DISCAPACIDADES POR DOCUMENTO Y VER CUAL ES LA MAYOR
		MatrizDinamica df=new MatrizDinamica();
		//String [][]sumaFavorece=df.sumaRestaResultados(resultadoContieneFavorece, resultadoNoContieneFavorece, cols,true);//suma los que favorecen
		//String [][]sumaFavorece=df.sumaRestaResultados(resultadoContieneFavorece, resultadoNoContieneDesfavorece, cols,true);//suma los que favorecen
		System.out.println("LA MATRIZ ENTRA AQUI");
//		if(sumaFavorece!=null) {
//			for(int i=0;i<sumaFavorece.length;i++) {
//				for(int j=0;j<cols;j++) {
//					System.out.print(sumaFavorece[i][j]+" ");
//				}
//				System.out.println();
//			}
//		}
		//System.out.println("DESFAVORECE");
		//String [][]sumaDesfavorece=df.sumaRestaResultados(resultadoContieneDesfavorece, resultadoNoContieneDesfavorece, cols,true);//suma los que no favorecen
		//String [][]sumaDesfavorece=df.sumaRestaResultados(resultadoContieneDesfavorece, resultadoNoContieneFavorece, cols,true);//suma los que no favorecen
//		if(sumaDesfavorece!=null) {
//			for(int i=0;i<sumaDesfavorece.length;i++) {
//				for(int j=0;j<cols;j++) {
//					System.out.print(sumaDesfavorece[i][j]+" ");
//				}
//				System.out.println();
//			}
//		}
//		if(resultadoNoContieneDesfavorece!=null) {
//			for(int i=0;i<resultadoNoContieneDesfavorece.length;i++) {
//				for(int j=0;j<cols;j++) {
//					System.out.print(resultadoNoContieneDesfavorece[i][j]+" ");
//				}
//				System.out.println();
//			}
//		}
		//RESTO LOS RESULTADOS
		//String [][]globalPorDocumento=df.sumaRestaResultados(sumaFavorece, sumaDesfavorece, cols, false);//obtengo la resta de la suma de todos los favorece contra la suma de todos los desfavorece
		//String [][]globalPorDocumento=df.sumaRestaResultados(sumaFavorece, sumaDesfavorece, cols, true);//obtengo la resta de la suma de todos los favorece contra la suma de todos los desfavorece
		//COMENTADOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
		//String [][]globalPorDocumento=df.sumaRestaResultados(resultadoContieneFavorece, resultadoContieneDesfavorece, cols, true);//obtengo la resta de la suma de todos los favorece contra la suma de todos los desfavorece
		//System.out.println("Documentos: "+globalPorDocumento.length);
		
		
		//separo las matrices por documentos 
//		ArrayList<String [][]>listadoPorDocs=df.separaPorDocumentos(globalPorDocumento, cols);
//		HashMap<String, HashMap<String, Integer>> contiene=new HashMap<String, HashMap<String, Integer>>();
//		HashMap<String, Integer> values=new HashMap<String, Integer>();
//		String anterior="";
//		for(int i=1;i<globalPorDocumento.length;i++) {
//			 if(globalPorDocumento[i][1].equals(globalPorDocumento[i-1][1])) {
//				 if((i-1)==0) {
//					 
//					 values.put(globalPorDocumento[i-1][2], Integer.parseInt(globalPorDocumento[i-1][3]));
//					 values.put(globalPorDocumento[i][2], Integer.parseInt(globalPorDocumento[i][3]));
//				 }else {
//					 values.put(globalPorDocumento[i][2], Integer.parseInt(globalPorDocumento[i][3]));
//				 }
//				 anterior=globalPorDocumento[i][1];
//			 }else {
//				 contiene.put(anterior, values);
//				 values=new HashMap<String, Integer>();
//				 values.put(globalPorDocumento[i][2], Integer.parseInt(globalPorDocumento[i][3]));
//			 }
//			 if(i==globalPorDocumento.length-1) {
//				 contiene.put(anterior, values);
//			 }
//		}
//		System.out.println("VALORES GLOBALES");
//	    for(Map.Entry<String, HashMap<String, Integer>> datos:contiene.entrySet()) {
//	    	for(Map.Entry<String, Integer> dat: datos.getValue().entrySet()) {
//	    		System.out.println(datos.getKey()+" "+dat.getKey()+" "+dat.getValue());
//	    	}
//	    }
//		System.out.println("Numero de documentos: "+listadoPorDocs.size()+" Tamano de un doc: "+listadoPorDocs.get(0).length);
		
		///RETORNO EL RESULTADO
		///recorro los listados para sacar el mayor de cada doc
		//COMENTADOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
		//ArrayList<String [][]>listadoPorDocsFavorece=df.separaPorDocumentos(sumaFavorece, cols);
		//ArrayList<String [][]>listadoPorDocsFavorece=df.separaPorDocumentos(resultadoContieneFavorece, cols);
//		for(String [][] data:listadoPorDocsFavorece) {
//			//String [][]matrizMayores=df.ordenaRetorna(data, cols);
//			System.out.println("=================================");
//			for(String []dat: data) {
//				System.out.print(dat[3]+" "+dat[0]+" "+dat[1]+" "+dat[2]+" "+(Double.parseDouble(dat[3])*100)/contiene.get(dat[1]).get(dat[2]));//en vez del 28 poner el num de ocurrencias por discapacidad q saco al sumar los q restabas
//				
//			}
//			System.out.println();
//			//QUE PASA SI DOS DISCAPACIDADES TIENEN EL MISMO NUMERO de favorece al final
//		}
		String favoreceDesfavorece="sch:DRD000009R";
		String [][] sumaFavorece=tripletas.crearTripletaResultadoFinal(nodoIRI, favoreceDesfavorece, label, noAplica, discapacidades);
		favoreceDesfavorece="sch:DRD000009R, sch:DRD000007R";
		String [][] global=tripletas.crearTripletaResultadoFinal(nodoIRI, favoreceDesfavorece, label, noAplica, discapacidades);
		java.util.ArrayList retorna=new java.util.ArrayList<>();
		
		System.out.println("FAVORECEEEEEEE");
		for(String []f:sumaFavorece) {
			System.out.println(f[2]+" "+f[3]);
		}
		System.out.println("GLOABAL");
		for(String []f:global) {
			System.out.println(f[2]+" "+f[3]);
		}
		retorna.add(sumaFavorece);
		retorna.add(global);
		return retorna;
		//AHCER 4 QUERYS PARA SACAR LOS METADATOS POR CADA UNO Y ASI, CUANDO SE SUMEN VER Q METAS AFAVO SE SUMAN
//		List<List<String>> listado=tripletas.crearTripletaQuery1(nodoIRI, rdfType);
//		
//		HashMap<String,HashMap<String, HashMap<String, Integer>>> documentosRetorna=hashContiene(listado.get(0), listado.get(1), listado.get(2));
//		//imprimeHashMaps(documentosRetorna);
//		hashContieneDisc(listado.get(0), listado.get(1), listado.get(2));
//		//no contiene
//		System.out.println(listado.get(1).size());
//		System.out.println("NO CONTIENEEEEEEEEEEE");
//		rdfType="<http://www.accesiblemetada.com/DRDSchema#DRD000015>";
//		listado=tripletas.crearTripletaQuery1(nodoIRI, rdfType);
//		documentosRetorna=hashContiene(listado.get(0), listado.get(1), listado.get(2));
//		//imprimeHashMaps(documentosRetorna);
//		hashContieneDisc(listado.get(0), listado.get(1), listado.get(2));
//		System.out.println(listado.get(1).size());
	}
	/**
	 * metodo que imprime un hashmap
	 * @param documentosRetorna Retorna el hashmap inpreso.
	 */
	public void imprimeHashMaps(HashMap<String,HashMap<String, HashMap<String, Integer>>> documentosRetorna) {
		System.out.println("Size: "+documentosRetorna.get("_:b3641F3D2x19974").size());
		for(Map.Entry<String,HashMap<String, HashMap<String, Integer>>> documents: documentosRetorna.entrySet()) {
			for(Map.Entry<String, HashMap<String, Integer>> disabilities:documents.getValue().entrySet()) {
				for(Map.Entry<String, Integer> favorContra:disabilities.getValue().entrySet()) {
					System.out.println(documents.getKey()+" "+disabilities.getKey()+" "+favorContra.getKey()+" "+favorContra.getValue());
				}
			}
		}
	}

	/**
	 * Metodo que crea las listas de: Metadatos que contiene el documento,Metadatos
	 * que no Contiene el documento, Discapacidades a las que favorece los metas que
	 * contiene el documento, Discapacidades a las que afecta los metas que contiene
	 * el documento, Discapacidades a las que favorece los metas que no contiene el
	 * documento, Discapacidades a las que afecta los metas que no contiene el
	 * documento, La ruta del Html que se esta analizando y, La ruta del objeto de
	 * aprendizaje que se esta analizando.
	 * 
	 * @param rutasHTML
	 *            Son un listado de rutas de todos los html del OA.
	 * @return 
	 */
	public HashMap<String, java.util.ArrayList<String>> queryDos(java.util.List<String> rutasHTML,String obAprend, HashMap<String, HashMap<String,Object[]>> resultadoTotal, boolean analizarSoloExistentes, HashMap<String, String>metass) {
		java.util.ArrayList<String> todosMetas = tripletas.crearTripletas();
		ArrayList documents=new ArrayList<>();
		ComparaDatos cd=new ComparaDatos();
		int cont=1;
		HashMap<String, java.util.ArrayList<String>> metasEnDocs=new HashMap<String, java.util.ArrayList<String>>();//para los metadatos que existen en cada documento 
		for (String ruta : rutasHTML) {
			//java.util.ArrayList<String> metas = obtieneMetadatosDocumento(ruta);
			java.util.ArrayList<String> metas = cd.comparaMetadatos(ruta, resultadoTotal.get(ruta), analizarSoloExistentes, metass);
			java.util.ArrayList<String> contiene = iriContiene(metas, todosMetas);//
			java.util.ArrayList<String> todos = new ArrayList<>(todosMetas);
			java.util.ArrayList<String> noContiene = iriNoContiene(contiene, todos);
			//System.out.println("Meta sen: " + ruta);
			documents.add(recibeListas(contiene, noContiene, ruta));
			metasEnDocs.put(ruta, contiene);//agrego los iris de cada meta al hashmap con su respectivo documento como llave
			VariablesEstaticas.incrementaFA=((cont*17)/rutasHTML.size())+80;
			cont++;
		//	recibeListas(contiene, noContiene, ruta);
//			java.util.ArrayList<String> iriDiscapacidadesContiene;
//			java.util.ArrayList<String> iriDiscapacidadesAfectaContiene;
//			for (String string : contiene) {// recoro la lista de losd metas que contiene el documento
//				// Favorece a
//				// System.out.println(string);
//				// java.util.ArrayList<String>
//				// iriDiscapacidadesContieneTemp=tripletas.crearTripletaFavoreceA(string);
//				iriDiscapacidadesContiene = tripletas.crearTripletaFavoreceA(string);// creo al lista con los iris de
//																						// las discapaicdades a las que
//																						// favorece el metadato
//				// iriDiscapacidadesContiene.add(iriDiscapacidadesContieneTemp);
//				// for (String discapacidad : iriDiscapacidades) {
//				// System.out.println("=============> "+discapacidad);
//				// }
//			}
//			System.out.println("Afecta A");
//			for (String string : contiene) {
//				// Favorece a
//				// System.out.println(string);
//				iriDiscapacidadesAfectaContiene = tripletas.crearTripletaAfectaA(string);
//				// for (String discapacidad : iriDiscapacidadesAfectaA) {
//				// System.out.println("---------------> "+discapacidad);
//				// }
//			}
//
//			/// NO CONTIENE
//			java.util.ArrayList<String> iriDiscapacidadesNoContiene;
//			java.util.ArrayList<String> iriDiscapacidadesAfectaNoContiene;
//			for (String string : noContiene) {
//				// Favorece a
//				System.out.println(string);
//				iriDiscapacidadesNoContiene = tripletas.crearTripletaFavoreceA(string);
//				// for (String discapacidad : iriDiscapacidades) {
//				// System.out.println("=============> "+discapacidad);
//				// }rdfs:label
//			}
//			System.out.println("Afecta A");
//			for (String string : noContiene) {
//				// Favorece a
//				System.out.println(string);
//				iriDiscapacidadesAfectaNoContiene = tripletas.crearTripletaAfectaA(string);
//				// for (String discapacidad : iriDiscapacidadesAfectaA) {
//				// System.out.println("---------------> "+discapacidad);
//				// }
//			}
//
//			System.out.println("Todas: " + todosMetas.size() + " No contiene: " + noContiene.size() + " Contiene: "
	//				+ contiene.size());
		}
		//objeto de aprendizaje
		AGRepositoryConnection connection = tripletas.getCon().getRepository().getConnection();
		try {
			//Diccionario de IRIS
			String contieneRecurso="http://www.accesiblemetada.com/DRDSchema#DRD000021R";
			String contexto="http://www.recomendador/context#app";
			String opType = "http://www.w3.org/1999/02/22-rdf-syntax-ns#type";
			String obaClass = "http://www.accesiblemetada.com/DRDSchema#DRD000020";
			String opLabel = "http://www.w3.org/2000/01/rdf-schema#label";
			DateFormat f=new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
			Calendar c=Calendar.getInstance();
			
			obAprend+="_"+f.format(new Date());
			AGValueFactory vf = connection.getValueFactory();
			BNode oba = vf.createBNode();
			insertListBNode(Arrays.asList(oba), vf, connection, documents,
					contieneRecurso,contexto);
			connection.add(oba, vf.createIRI(opLabel), vf.createLiteral(obAprend),vf.createIRI(contexto));
			connection.add(oba, vf.createIRI(opType), vf.createIRI(obaClass),vf.createIRI(contexto));
			iriOA="_:"+oba.getID();
			//System.out.println("IDDDDDDD: "+iriOA+"  "+oba.stringValue());
			label=obAprend;
		} finally {
			connection.close();
		}
		return metasEnDocs;//retorno el listado de iris de los metadatos existentes en cada doc
	}
	public String returnIRIOA() {
		return iriOA;
	}
	/**
	 * Metodo que obtiene los metadatos que contiene cad adocumento.
	 * @param ruta Es la ruta del documento html.
	 * @return Retorna un listado con los emtadatos que contiene el doc.
	 */
	public java.util.ArrayList<String> obtieneMetadatosDocumento(String ruta) {
		try {
			java.util.ArrayList<String> metadatos = new java.util.ArrayList<String>();

			java.io.File html = new java.io.File(ruta);
			Document doc = Jsoup.parse(html, "UTF-8");
			Elements metas = doc.select("meta");

			for (Element element : metas) {
				if (element.hasAttr("itemprop")) {
					String valor = element.attr("itemprop");
					if (valor.trim().toLowerCase().equals("accessmode")
							|| valor.trim().toLowerCase().equals("accessibilityhazard")
							|| valor.trim().toLowerCase().equals("accessibilitycontrol")
							|| valor.trim().toLowerCase().equals("accessibilityfeature")) {
						if (element.hasAttr("content")) {
							if (element.attr("content") != null && !element.attr("content").equals("")) {
								metadatos.add(element.attr("content"));
							}
						} else {
							System.out.println("No contiene el valor " + valor);
						}
					}
				}
			}
			return metadatos;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	/**
	 * Metodo que compara si tiene lso metadatos el documento.
	 * @param contieneDocumento Listado de metas que contiene el documento.
	 * @param todosMetadatos Listado de todos los metas.
	 * @return Retorna el conjunto de iris de los metadatos que contiene el documento.
	 */
	public java.util.ArrayList<String> iriContiene(java.util.ArrayList<String> contieneDocumento,
			java.util.ArrayList<String> todosMetadatos) {
		java.util.ArrayList<String> iris = new java.util.ArrayList<String>();
		for (String meta : todosMetadatos) {
			for (int i = 0; i < contieneDocumento.size(); i++) {
				if (meta.toLowerCase().substring(meta.indexOf("#") + 1)
						.equals(contieneDocumento.get(i).toLowerCase())) {
					iris.add(meta);
					i = contieneDocumento.size();
				}
			}
		}
		return iris;
	}
	/**
	 * Metodo que extrae los iris de los metas que no contiene el docu.
	 * @param contieneDocumento Listado de metas que contiene el documento.
	 * @param todosMetadatos Listado de todos los metas.
	 * @return Retorna el conjunto de iris de los metadatos que no contiene el documento.
	 */
	public java.util.ArrayList<String> iriNoContiene(java.util.ArrayList<String> contieneDocumento,
			java.util.ArrayList<String> todosMetadatos) {
		//System.out.println("Tammmmmmmmmmmmmmm " + todosMetadatos.size());
		for (String meta : contieneDocumento) {
			todosMetadatos.remove(meta);
		}
		/*
		 * for (String meta : todosMetadatos) { for(int
		 * i=0;i<contieneDocumento.size();i++) {
		 * if(meta.toLowerCase().substring(meta.indexOf("#")+1).equals(contieneDocumento
		 * .get(i).toLowerCase())) { todosMetadatos.remove(meta);
		 * i=contieneDocumento.size(); } } }
		 */
		return todosMetadatos;
	}
	/**
	 * Metodo que envia a agregar en la ontologia los difernetes nodos en blanco y sus respectiva srelaciones
	 * asi como tambien sus labels, tipos, etc.
	 * @param contiene Listado de metas que contiene el docu.
	 * @param noContiene Listado de metas que no contiene el docu/
	 * @param rutaHTML Ruta del archivo html.
	 * @return Retorna el documenton n=inferenciado
	 */
	public BNode recibeListas(java.util.ArrayList<String> contiene, java.util.ArrayList<String> noContiene, String rutaHTML) {
		//Crear conexion
		AGRepositoryConnection connection = tripletas.getCon().getRepository().getConnection();
		//Crear fabricade valores
		AGValueFactory vf = connection.getValueFactory();
		try {
			//Instancias tudiccionario de IRIS
			String documentClass = "http://www.accesiblemetada.com/DRDSchema#DRD000013";
			String contieneMetadatoClass = "http://www.accesiblemetada.com/DRDSchema#DRD000014";
			String noContieneMetadatoClass = "http://www.accesiblemetada.com/DRDSchema#DRD000015";
			String opTieneValor = "http://www.accesiblemetada.com/DRDSchema#DRD000016R";
			String opType = "http://www.w3.org/1999/02/22-rdf-syntax-ns#type";
			String opFavorece = "http://www.accesiblemetada.com/DRDSchema#DRD000009R";
			String opDesfavorece = "http://www.accesiblemetada.com/DRDSchema#DRD000007R";
			String opTieneMetadato = "http://www.accesiblemetada.com/DRDSchema#DRD000017R";
			String opLabel = "http://www.w3.org/2000/01/rdf-schema#label";
			String contexto="http://www.recomendador/context#app";
			
			DateFormat f=new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
			Calendar c=Calendar.getInstance();
			rutaHTML+="_"+f.format(new Date());
			//Crear nodo en blanco dl documento
			BNode document = vf.createBNode();
			//Agregara tripleta la TDB
			//Creo las tripletas del documento
			connection.add(document, vf.createIRI(opLabel), vf.createLiteral(rutaHTML),vf.createIRI(contexto));
			connection.add(document, vf.createIRI(opType), vf.createIRI(documentClass),vf.createIRI(contexto));
			//Crear la listade nodos en blanco de los elementos que contiene
			ArrayList<BNode> listNodeContiene = getBlanckNodes(vf, connection, contiene);
			int con = 0;
			//System.out.println("BLN List size "+listNodeContiene.size());
			//System.out.println("List size "+contiene.size());
			for (String iriDoc : contiene) {
				//Conecto el documento con el nodo en blanco del valor que tiene
				connection.add(document, vf.createIRI(opTieneValor), listNodeContiene.get(con),vf.createIRI(contexto));
				connection.add(listNodeContiene.get(con), vf.createIRI(opType), vf.createIRI(contieneMetadatoClass),vf.createIRI(contexto));
				//Recupero los iris de las discapacidades a las que favorece
				ArrayList favorece = tripletas.crearTripletaFavoreceA(iriDoc);
				//Recupero los iris de las discapacidades a las que desfavorece
				ArrayList desfavorece = tripletas.crearTripletaAfectaA(iriDoc);
				// Insertar favorece
				insertList(Arrays.asList(listNodeContiene.get(con)), vf, connection, favorece, opFavorece,contexto);
				// Insertar desfavorece
				insertList(Arrays.asList(listNodeContiene.get(con)), vf, connection, desfavorece, opDesfavorece,contexto);
				// Insertar metadato que contiene
				insertList(Arrays.asList(listNodeContiene.get(con)), vf, connection, Arrays.asList(iriDoc),
						opTieneMetadato,contexto);
				con++;
			}
			//Crear la listade nodos en blanco de los elementos que no contiene
			ArrayList<BNode> listNodeNoContiene = getBlanckNodes(vf, connection, noContiene);
			con = 0;
			//System.out.println("BLN List size "+listNodeNoContiene.size());
			//System.out.println("List size "+noContiene.size());
			for (String iriDoc : noContiene) {
				//System.out.println("###################Iri Doc "+iriDoc);
				connection.add(document, vf.createIRI(opTieneValor), listNodeNoContiene.get(con),vf.createIRI(contexto));
				connection.add(listNodeNoContiene.get(con), vf.createIRI(opType), vf.createIRI(noContieneMetadatoClass),vf.createIRI(contexto));
				ArrayList favorece = tripletas.crearTripletaFavoreceA(iriDoc);
				ArrayList desfavorece = tripletas.crearTripletaAfectaA(iriDoc);
				// Insertar favorece
				insertList(Arrays.asList(listNodeNoContiene.get(con)), vf, connection, desfavorece, opFavorece,contexto);
				// Insertar desfavorece
				insertList(Arrays.asList(listNodeNoContiene.get(con)), vf, connection, favorece, opDesfavorece,contexto);
				// Insertar metadato que contiene
				insertList(Arrays.asList(listNodeNoContiene.get(con)), vf, connection, Arrays.asList(iriDoc),
						opTieneMetadato,contexto);
				con++;
			}

			return document;
		} finally {
			connection.close();
		}
	}
	/**
	 * Metodo que devuelve los nodos en blanco necesarios.
	 * @param vf Fabrica de valores.
	 * @param connection La coneccion con la ontologia.
	 * @param list Listado de los labels 
	 * @return Retorna el listado auxiliar de los lebals.
	 */
	public ArrayList<BNode> getBlanckNodes(AGValueFactory vf, AGRepositoryConnection connection,
			ArrayList<String> list) {
		String iriLabel = "http://www.w3.org/2000/01/rdf-schema#label";
		ArrayList listAux = new ArrayList<>();
		for (String label : list) {
			BNode node = vf.createBNode();
			listAux.add(node);
			//connection.add(node, vf.createIRI(iriLabel), vf.createLiteral(label));//revisar esta linea, aqui no se esta insertando el contexto
			connection.add(node, vf.createIRI(iriLabel), vf.createLiteral(label), vf.createIRI("http://www.recomendador/context#app"));
		}
		return listAux;
	}
	/**
	 * Metodo que inserta los nodos en blanco y las relaicones.
	 * @param list Listado de los metas que favorece.
	 * @param vf Fabrica de valores.
	 * @param connection Coneccion con la ontologia.
	 * @param list2 Listado de los metas afecta.
	 * @param op label.
	 * @param contexto Es el contexto en el que se vana  inserta rlos nodos en blanco (app).
	 */
	public void insertList(List<BNode> list, AGValueFactory vf, AGRepositoryConnection connection, List<String> list2,
			String op,String contexto) {
		//System.out.println("Insert ");
		for (BNode bn : list) {
			for (String iri : list2) {
				//System.out.println("Op->"+op);
				//System.out.println("Iri->"+iri);
				connection.add(bn, vf.createIRI(op), vf.createIRI(iri),vf.createIRI(contexto));
			}
		}
	}
	/**
	 * Metodo que inserta los nodos en blanco y las relaicones.
	 * @param list Listado de los metas que favorece.
	 * @param vf Fabrica de valores.
	 * @param connection Coneccion con la ontologia.
	 * @param list2 Listado de los metas afecta.
	 * @param op label.
	 * @param contexto Es el contexto en el que se vana  inserta rlos nodos en blanco (app).
	 */
	public void insertListBNode(List<BNode> list, AGValueFactory vf, AGRepositoryConnection connection, List<BNode> list2,
			String op,String contexto) {
		//System.out.println("Insert ");
		for (BNode bn : list) {
			for (BNode iri : list2) {
				//System.out.println("Op->"+op);
				//System.out.println("Iri->"+iri);
				connection.add(bn, vf.createIRI(op), iri,vf.createIRI(contexto));
			}
		}
	}
	
	/**
	 * Metadatos que contiene el doc. No se usa
	 * @param documentos Listado de documentos html
	 * @param discapacidades Listado de discapacidades seleccionadas.
	 * @param favoreceAfecta Realcion 
	 * @return Retorna el listado de los emtas q contiene.
	 */
	public HashMap<String,HashMap<String, HashMap<String, Integer>>> hashContiene(List<String> documentos,List<String> discapacidades, List<String> favoreceAfecta){
		HashMap<String, HashMap<String, Integer>> retornaValores = new HashMap<String, HashMap<String, Integer>>();
		HashMap<String, Integer> contador =  new HashMap<String, Integer>();
		HashMap<String,HashMap<String, HashMap<String, Integer>>> documentosRetorna=new HashMap<String,HashMap<String, HashMap<String, Integer>>>();
		//contador.put("-", 1);
	//	System.out.println(documentos.size()+"   "+discapacidades.size()+"   "+favoreceAfecta.size()+"    "+contador.size());
		for(int i=0; i<documentos.size();i++) {
			
			if(documentosRetorna.containsKey(documentos.get(i))) {
				//System.out.println("Entra contiene documento");
				retornaValores=documentosRetorna.get(documentos.get(i));
			}else {
				retornaValores = new HashMap<String, HashMap<String, Integer>>();
			}
			int fav=1;
			int des=1;
			contador =  new HashMap<String, Integer>();
			if(retornaValores.containsKey(discapacidades.get(i))) {
				//System.out.println("Contiene discapacidad");
				contador = retornaValores.get(discapacidades.get(i));
				//System.out.println("Size cont "+contador.size());
				if(favoreceAfecta.get(i).equals("http://www.accesiblemetada.com/DRDSchema#DRD000007R")) {
					if(contador.containsKey("-"))
						des=contador.get("-")+1;
					else
						des=1;
					//des++;
					contador.put("-", des);
				}else {
					if(contador.containsKey("+"))
						fav=contador.get("+")+1;
					else
						fav=1;
					contador.put("+", fav);
				}
			}else {
				contador =  new HashMap<String, Integer>();
				
				if(favoreceAfecta.get(i).equals("http://www.accesiblemetada.com/DRDSchema#DRD000007R")) {
					
					contador.put("-", 1);
				}else {
					
					
					contador.put("+", 1);
					
				}
			
			}
			//System.out.println("Contador2: "+contador.size());
				retornaValores.put(discapacidades.get(i), contador);
				documentosRetorna.put(documentos.get(i), retornaValores);
			}
			return documentosRetorna;
		
		
	}
	/**
	 * metodo que verifica si contiene o no dicapacidad.
	 * @param documentos Listado de ruta de los html
	 * @param discapacidades Listado de las discapacidades seleccionadas
	 * @param favoreceAfecta Relacion
	 * @return :istado de discapcidades q contiene.
	 */
	public HashMap<String,HashMap<String, HashMap<String, Integer>>> hashContieneDisc(List<String> documentos,List<String> discapacidades, List<String> favoreceAfecta){
		HashMap<String, HashMap<String, Integer>> retornaValores = new HashMap<String, HashMap<String, Integer>>();
		HashMap<String, Integer> contador =  new HashMap<String, Integer>();
		HashMap<String,HashMap<String, HashMap<String, Integer>>> documentosRetorna=new HashMap<String,HashMap<String, HashMap<String, Integer>>>();
		//contador.put("-", 1);
		//System.out.println(documentos.size()+"   "+discapacidades.size()+"   "+favoreceAfecta.size()+"    "+contador.size());
		for(int i=0;i<discapacidades.size();i++) {
			if(retornaValores.containsKey(discapacidades.get(i))) {
				contador=retornaValores.get(discapacidades.get(i));
				if(favoreceAfecta.get(i).contains("DRD000007R")) {
					if(contador.containsKey("-")) {
						int des=contador.get("-")+1;
						contador.put("-", des);
					}else {
						contador.put("-", 1);
					}
				}else {
					if(contador.containsKey("+")) {
						int des=contador.get("+")+1;
						contador.put("+", des);
					}else {
						contador.put("+", 1);
					}
				}
			}else {
				contador =  new HashMap<String, Integer>();
				if(favoreceAfecta.get(i).contains("DRD000007R")) {
					contador.put("-", 1);
				}else {
					contador.put("+", 1);
				}
			}
			retornaValores.put(discapacidades.get(i), contador);
			documentosRetorna.put(documentos.get(i), retornaValores);
		}
		
		//IMPRIMOOOO
//		for(Map.Entry<String, HashMap<String, Integer>> disabilities:retornaValores.entrySet()) {
//			for(Map.Entry<String, Integer> favorContra:disabilities.getValue().entrySet()) {
//				System.out.println(disabilities.getKey()+" "+favorContra.getKey()+" "+favorContra.getValue());
//			}
//		}
//		for(Map.Entry<String, HashMap<String, Integer>> disabilities:retornaValores.entrySet()) {
//			System.out.println(disabilities.getKey()+" "+disabilities.getValue());
//		}
//		System.out.println(retornaValores.size()+" "+documentosRetorna.size());
//		for(Map.Entry<String,HashMap<String, HashMap<String, Integer>>> documents:documentosRetorna.entrySet()) {
//			System.out.println(documents.getKey()+" "+documents.getValue());
//		}
		
		///imprime suma
		//System.out.println("IMPREME SUM TOTAL");
		sumaTotal(documentosRetorna);
		return documentosRetorna;	
	}
	/**
	 * Devuelve la suma total de los valores
	 * @param documentos Listado de documentos html
	 * @return Retorna la suma total.
	 */
	public HashMap<String , HashMap<String, Integer>> sumaTotal(HashMap<String,HashMap<String, HashMap<String, Integer>>> documentos){
		HashMap<String , HashMap<String, Integer>> retorna=new HashMap<String , HashMap<String, Integer>>();
		HashMap<String, Integer> discapacidadValor=new HashMap<String, Integer>();
		String [] todasDiscapacidades=new String [] {"Visual","Auditory","Cognitive","Psychosocial","Physical"};
		for(Map.Entry<String,HashMap<String, HashMap<String, Integer>>> documents: documentos.entrySet()) {
			for(Map.Entry<String, HashMap<String, Integer>> disabilities:documents.getValue().entrySet()) {
				int cont=0;
				for(Map.Entry<String, Integer> favorContra:disabilities.getValue().entrySet()) {
					//System.out.println(documents.getKey()+" "+disabilities.getKey()+" "+favorContra.getKey()+" "+favorContra.getValue());
					if(favorContra.getKey().equals("+")) {
						cont+=favorContra.getValue();
					}else {
						cont-=favorContra.getValue();
					}
				}
				//System.out.println("LLLAVEEEEEE: ");
				discapacidadValor.put(disabilities.getKey(), cont);
				//System.out.println(disabilities.getKey());
				
			}
			
			eliminaDuplicados(discapacidadValor);
			retorna.put(documents.getKey(), discapacidadValor);
		}
		//System.out.println(discapacidadValor);
		return retorna;
	}
	/**
	 * Metodo que elimina los duplicados
	 * @param discapacidadValor hashmap con los valores necesarios.
	 */
	public void eliminaDuplicados(HashMap<String, Integer> discapacidadValor) {
		HashMap<String, Integer> discapacidadReturn=new HashMap<String, Integer>();
		HashMap<String, Integer> discapacidadV=new HashMap<>(discapacidadValor);
		String [][] mat=new String[discapacidadValor.size()][2];
		int count=0;
		java.util.ArrayList<String> llaves=new java.util.ArrayList<String>();
		//System.out.println("Sizeeeeeeeeeeeeeeeeeeee: "+discapacidadValor.size()+" "+discapacidadV.size());
		for(Map.Entry<String, Integer> vals:discapacidadValor.entrySet()) {
			mat[count][0]=vals.getKey();
			mat[count][1]=vals.getValue()+"";
			llaves.add(vals.getKey());
			discapacidadV.remove(vals.getKey());
			
			count++;
		}
		java.util.ArrayList<Integer> index=new ArrayList<>();
		for(int i=0;i<llaves.size();i++) {
			for(int j=i+1;j<llaves.size();j++) {
				//System.out.println("Comparando: "+llaves.get(i)+" "+llaves.get(j));
				if(llaves.get(i).equals(llaves.get(j))) {
					index.add(i);
					index.add(j);
				}
			}
		}
		for(int i=0;i<index.size();i+=2) {
			int valor=Integer.parseInt((mat[i+1][1]))+Integer.parseInt((mat[i][1]));
			mat[i][1]=valor+"";
			mat[i+1][0]=null;
			mat[i+1][1]=null;
		}
		for(int i=0;i<mat.length;i++) {
			if (discapacidadV.containsKey(mat[i][0])) {
				int val=Integer.parseInt(mat[i][1])+discapacidadV.get(mat[i][0]);
				mat[i][1]=val+"";
			}
		}
		
		//imprimo
		//System.out.println("Matrizzzzzzzzzzz");
//		for(int i=0;i<mat.length;i++) {
//			System.out.println(mat[i][0]+"----->"+mat[i][1]);
//		}
	}
}
