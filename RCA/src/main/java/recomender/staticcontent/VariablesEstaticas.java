package recomender.staticcontent;

import java.util.HashMap;

public class VariablesEstaticas {
	public static HashMap<String, String> jsRecuperado=new HashMap<>();
	
	public static int numImagenes=0;
	public static int numVideos=0;
	public static String pdfErrors="";
	public static int numPDFs=0;
	public static int numPalabras=0;
	public static double numVideosSubs=0;
	public static int numContAuditivo=0;
	public static int numConTactil=0;
	public static int numImgLongDesc=0;
	public static int numSonidoFondo=0;
	public static int numAnimaciones=0;
	public static int numDependenciaColor=0; 
	public static int numImagenesTxtAlt=0;
	public static int numtextOnVisual=0;
	
	//cumpleTodo
	public static int numCumpleTodoHTML=0;
	public static int numCumpleTodoHTMLTotal=0;
	
	public static int numTablaContenidos=0;
	public static int numFormulasMatematicas=0;
	public static int numChem=0;
	public static int numlatex=0;
	public static int numSimulaciones=0;
	public static int acTiempos=0;
	
	public static int incrementaF=0;
	public static int incrementaFA=0;
	
	public static int numIm=0;
	
}
