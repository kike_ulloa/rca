package recomender.tesseract;

import java.awt.image.BufferedImage;

import net.sourceforge.lept4j.util.LoadLibs;
import net.sourceforge.tess4j.ITesseract;
import net.sourceforge.tess4j.Tesseract;

public class TesseractAnalisis {

	/**
	 * Metodo que analiza el texto en las imagenes haciendo usa de OCR
	 * 
	 * @param imagen Es la imagen a analizar
	 * @return Retorna el resultado del texto encontrado.
	 */
	public String retornaTextoEnImagen(BufferedImage imagen) {
		ITesseract instancia=new Tesseract();
		try {
			instancia.setDatapath(LoadLibs.extractNativeResources("recomender/tessdata").getParent());
			//instancia.setDatapath(LoadLibs.extractNativeResources("tessdata").getParent());
			instancia.setLanguage("lat");
			String textoImagen=instancia.doOCR(imagen);
			//System.out.println("Texto: "+textoImagen);
			return textoImagen;
		}catch(Exception e) {
			e.printStackTrace();
			return null;
		}
		//return "";
	}
}
