package recomender.translate;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Properties;

public class Translate {
	//String rutaScript="/home/kike/Documents/TESIS/translate/translator. py";
	//String rutaScript="/home/kike/Documents/translate/translator.py";
	String rutaScript="";
	/**
	 * Metodo que traducira el texto indicado de ingles a espanol.
	 * @param texto Es el texto a sr traducido.
	 * @return Retorna el texto traducido al espanol.
	 */
	public String taducir(String texto) {
		try {
			ClassLoader cl = Thread.currentThread().getContextClassLoader();
			Properties p = new Properties();
			p.load(cl.getResourceAsStream("paths.properties"));
			rutaScript = p.getProperty("scriptTranslate");
			//System.out.println("Este es el path de phantom: "+p.getProperty("phantomjs"));
		}catch(Exception e) {
				e.printStackTrace();
		}
		try {
			String []comando=new String [3];
			comando[0]="python3";
			comando[1]=rutaScript;
			comando[2]=texto;
			Process process= Runtime.getRuntime().exec(comando);
			BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
			String salida="";
			String linea ="";
			while((linea=reader.readLine())!=null) {
				salida+=linea+"\n";
			}
			process.waitFor();
			process.destroy();
			return salida;
		}catch(Exception e) {
			e.printStackTrace();
			return "Error Ejecutando Script";
		}
	}
}
