package recomender.validations;

import java.io.File;
import java.util.ArrayList;

import org.jsoup.nodes.Element;
import recomender.validations.ValidacionesMultimedia;

public class Offline {
/**
 * analiza archivos multimedia offline en archivos html online
 * @param archivo.- html
 * @param valor.- multimedia
 * @param tipoArchivo.- si es un video, imagen,etc.
 * @param contenidoOtroArchivo.- si esta otro tipo de archivo ejemplo audio en vide.
 * @param encontrados.- arreglo donde se agrega lo encontrado.
 * @return
 */
	public ArrayList<String> analizarValorOfflineArchivoOnline(String archivo,String valor,String tipoArchivo,String contenidoOtroArchivo,ArrayList<String> encontrados) {
		String sentencia[]=new String[8];//Creamos un arreglo donde indicaremos la sentencia que se ejecutara en la consola del sistema.
		sentencia[0]="ffmpeg";//Se hara uso sistema youtube-dl que previamente ya fue instalado en el sistema.
		sentencia[1]="-i";//El comando de arriba necesita paramentros para extraer informacion del video, para ello se hara uso del parametro que nos permite listar los subtitulos, de esta forma se comprobara que se trata de un video o no.
		sentencia[2]=valor;//Este metodo recibi como parametro el link que queremos analizar, el cual se usara dentro de la sentencia.
		sentencia[3]="-af";
		sentencia[4]="volumedetect";
		sentencia[5]="-f";
		sentencia[6]="null";
		sentencia[7]="-";
		
		
		ValidacionesMultimedia vm=new ValidacionesMultimedia();
		Online on=new Online();
		boolean formato=false;
		String aux=archivo.substring(archivo.indexOf("//")+2);//Buscamos la posicion donde se encuentra el doble // y cortamos la cadena desde ahi
		String dominio=aux.substring(0,aux.indexOf("/"));//De la cadena cortada volveremos a cortar hasta encontrar el primer / y asi obtendremos el dominio de la pagina. 
		String pathURL=archivo.substring(0,archivo.lastIndexOf("/"));// En caso de que los archivos no esten en la carpeta raiz trabajaremos con el path donde se encuentre el archivo html.
		String rutaCompleta="";
		if(valor.charAt(0)!='/')//Analizamos si el valor obtenido empieza con un /
			valor="/"+valor;// Si no posee se le agrega
		if(on.existeOnline(on.conexionURL("//"+dominio+valor))||on.existeOnline(on.conexionURL(pathURL+valor))) {// Se analiza si el archivo existe en la carpeta raiz del dominio o el path del archivo html
			if(on.existeOnline(on.conexionURL("//"+dominio+valor)))
				rutaCompleta="//"+dominio+valor;
			else
				rutaCompleta=pathURL+valor;
					sentencia[2]=rutaCompleta;
					valor=valor.substring(1);
					if(tipoArchivo=="desconocido")
						formato=true;
					else {
						if(!tipoArchivo.equals("video")) {
								formato=vm.tieneFormato(tipoArchivo, valor);
								if(!formato&&tipoArchivo.equals("imagen")) {
									if(on.conexionURL(valor)!=null) {
									if(on.conexionURL(valor).getContentType()!=null) {
									if(on.conexionURL(valor).getContentType().indexOf("image")>-1)
										formato=true;
									}
								}
								}
						}else { 
							formato=vm.esVideo(valor);
							if(formato&&contenidoOtroArchivo!=null) {
									if(!vm.tieneAudio(valor,sentencia, null))
										formato=false;	
							}
						}
					}
				}
		if(formato)
			encontrados.add(rutaCompleta);
		return encontrados;
	}
	/**
	 * analiza archivos multimedia offline en archivos html online
	 * @param archivo.- html
	 * @param valor.- multimedia
	 * @param tipoArchivo.- si es un video, imagen,etc.
	 * @param contenidoOtroArchivo.- si esta otro tipo de archivo ejemplo audio en vide.
	 * @param encontrados.- arreglo donde se agrega lo encontrado.
	 * @return
	 */
	public ArrayList<String> analizarValorOfflineArchivoOffline(String archivo,String valor,String tipoArchivo,String contenidoOtroArchivo,ArrayList<String> encontrados) {
		String sentencia[]=new String[8];//Creamos un arreglo donde indicaremos la sentencia que se ejecutara en la consola del sistema.
		sentencia[0]="ffmpeg";//Se hara uso sistema youtube-dl que previamente ya fue instalado en el sistema.
		sentencia[1]="-i";//El comando de arriba necesita paramentros para extraer informacion del video, para ello se hara uso del parametro que nos permite listar los subtitulos, de esta forma se comprobara que se trata de un video o no.
		sentencia[2]=valor;//Este metodo recibi como parametro el link que queremos analizar, el cual se usara dentro de la sentencia.
		sentencia[3]="-af";
		sentencia[4]="volumedetect";
		sentencia[5]="-f";
		sentencia[6]="null";
		sentencia[7]="-";
		
		ValidacionesMultimedia vm=new ValidacionesMultimedia();
		boolean formato=false;
		if(!existeOffline(valor)) {
			valor=archivo.substring(0,archivo.lastIndexOf("/")+1)+valor;
			sentencia[2]=valor;
		}
		if(existeOffline(valor)) {//Analizamos si exite el archivo local.
			if(tipoArchivo=="desconocido")
				formato=true;
			else {
				formato=vm.tieneFormato(tipoArchivo, valor);
				if(formato&&(tipoArchivo.equals("audio")||(tipoArchivo.equals("video")&&contenidoOtroArchivo!=null))) {
					String audio=valor;
						if(!vm.tieneAudio(audio,sentencia,null))
							formato=false;		
			}
		}
		}
		if(formato)
			encontrados.add(valor);
		return encontrados;
	}
	/**
	 * Este metodo nos permitira obtener el numero de palabras que se encuentre en un archivo html, en base a las etiquetas que se han propuesto.
	 * @param numeroEncontrados
	 * @param resultadoEtiqueta
	 * @return
	 */
	public boolean existeOffline(String archivo) {
		boolean existe=false;
		try {
				java.io.File arch=new java.io.File(archivo);//Buscamos el archivo local.
				if(arch.exists())  //Analizamos la existencia del archivo
					existe=true;
		}catch(Exception e) {
			e.printStackTrace();
		}
		return existe;
	}

}
