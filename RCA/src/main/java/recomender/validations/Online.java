package recomender.validations;

import java.io.File;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import recomender.validations.ValidacionesMultimedia;

public class Online {
	ValidacionesMultimedia vm=null;
	public Online() {
		vm=new ValidacionesMultimedia();
	}
	/**
	 * permite comprobar la conexion con el sitio web.
	 * @param valor.- pagina o contenido multimedia web.
	 * @return
	 */
	public HttpURLConnection conexionURL(String valor) {
		if(!valor.contains("http"))//Analizamos si la URL contiene http.
			valor="http:"+valor;//Si no contiene se anade.
		if(!valor.contains("//")) 
			valor="http://"+valor.substring(5);
		HttpURLConnection.setFollowRedirects(false);
		HttpURLConnection con;
		try {
			con = (HttpURLConnection) new URL(valor).openConnection();
			con.setRequestMethod("HEAD");//Hace una solicitud con el metodo head
			return con;
		}
		catch(Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	/**
	 * Este metodo nos permite analizar si una URL existe o no.
	 * @param valor
	 * @return
	 */
	public boolean existeOnline(HttpURLConnection con) {
		boolean existe=false;
		try{
		    if(con.getResponseCode() != HttpURLConnection.HTTP_NOT_FOUND) //Comprueba si existe el archivo, con el codigo obtenido de la solicitud.
		    	existe=true;
		} catch (Exception e) {
			e.printStackTrace();
		}		    		    
	  return existe;
	}
	/**
	 * analiza archivos multimedia offline en archivos html online
	 * @param valor.-multimedia o html
	 * @param tipoArchivo.- si es video o imagen ,etc.
	 * @param encontrados.- arreglo de los encontrados
	 * @param contenidoOtroArchivo.- si esta en otro tipo de archivo ejemplo audio en video.
	 * @return
	 */
	public ArrayList<String> analizarValorOnline(String valor,String tipoArchivo,ArrayList<String> encontrados,String contenidoOtroArchivo) {	
		String uno="$(";
		String dos=")";
		String []getUrl= new String[5];
		getUrl[0]="youtube-dl";
		getUrl[1]="-f";
		//getUrl[2]="22";
		getUrl[2]="worst";
		getUrl[3]="--get-url";
		getUrl[4]=valor;//Este metodo recibi como parametro el link que queremos analizar, el cual se usara dentro de la sentencia.
		String sentencia[]=new String[12];//Creamos un arreglo donde indicaremos la sentencia que se ejecutara en la consola del sistema.
		sentencia[0]="ffmpeg";//Se hara uso sistema youtube-dl que previamente ya fue instalado en el sistema.
		sentencia[1]="-i";//El comando de arriba necesita paramentros para extraer informacion del video, para ello se hara uso del parametro que nos permite listar los subtitulos, de esta forma se comprobara que se trata de un video o no.
		sentencia[2]=null;
		sentencia[3]="-ss";
		sentencia[4]="00:00:00.0";
		sentencia[5]="-t";
		sentencia[6]=null;
		sentencia[7]="-af";
		sentencia[8]="volumedetect";
		sentencia[9]="-f";
		sentencia[10]="null";
		sentencia[11]="-";
		//duracion de video
		
		boolean formato=false;
		if(existeOnline(conexionURL(valor))) { //Analizamos si existe el link
			if(tipoArchivo=="desconocido")
				formato=true;
			else {
				if(!tipoArchivo.equals("video")) {
						formato=vm.tieneFormato(tipoArchivo, valor);
						if(!formato&&tipoArchivo.equals("imagen")) {//Esta condicion se usa para links de imagenes que no tienen formato.
							if(conexionURL(valor).getContentType().indexOf("image")>-1)
								formato=true;
						}
				}else { 
						formato=vm.esVideo(valor);
						if(formato&&contenidoOtroArchivo!=null) {	
								if(!vm.tieneAudio(valor,sentencia,getUrl))
									formato=false;							
						}
					}
			}
		}
		if(formato)
			encontrados.add(valor);
		return encontrados;
	}


}
