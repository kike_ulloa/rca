package recomender.validations;

public class RetornaSoloNumeroDeString {

	/**
	 * Metood que extrae numeros de texto 
	 * @param texto Es el texto amanipular
	 * @return Retorna solo el numero extraido
	 */
	public double retornaNumero(String texto) {
		try {
		String soloNumero=texto.replaceAll("[^\\.0123456789]", "");
		double retorna=0;
		if(soloNumero.trim().isEmpty()) {
			if(texto.contains("=")) {
				texto=texto.substring(texto.indexOf("=")+1);
				retorna=numeroPorString(texto.trim());
			}else if(texto.contains(":")) {
				texto=texto.substring(texto.indexOf(":")+1);
				retorna=numeroPorString(texto.trim());
			}
		}else 
			retorna=Double.parseDouble(soloNumero);
		return retorna;
		}catch(Exception e) {
			e.printStackTrace();
			return -1;
		}
	}
	/**
	 * Metodo que devuelve el tamano del string dado
	 * @param tamano es el tamano (large, small, smaller, etc)
	 * @return retorna el tamano en pixeles.
	 */
	public double numeroPorString(String tamano) {
		//System.out.println("Este es el string: "+tamano);
		if(tamano.toLowerCase().equals("large"))
			return 18;
		if(tamano.toLowerCase().equals("larger"))
			return 19;
		if(tamano.toLowerCase().equals("medium"))
			return 16;
		if(tamano.toLowerCase().equals("small"))
			return 13;
		if(tamano.toLowerCase().equals("smaller"))
			return 12;
		if(tamano.toLowerCase().equals("x-large"))
			return 24;
		if(tamano.toLowerCase().equals("xx-large"))
			return 32;
		if(tamano.toLowerCase().equals("x-small"))
			return 10;
		if(tamano.toLowerCase().equals("xx-small"))
			return 9;
		return 0;
	}
}
