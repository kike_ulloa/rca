package recomender.validations;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

public class RevisaTitlesHTML {

	/**
	 * Metodo que extrae los titulos de los html.
	 * @param ruta Es la ruta del html a analizar.
	 * @return Retorna el titulo encontrado.
	 */
	public String retornaTitulo(String ruta) {
		java.io.File archivo=new java.io.File(ruta);
		if(archivo.exists()) {
			try {
			Document doc=Jsoup.parse(archivo, "UTF-8");
			Elements titles=doc.select("title");
			if(titles!=null&&!titles.isEmpty()) {
				return titles.text();
			}
			return null;
			}catch(Exception e) {
				e.printStackTrace();
				return null;
			}
		}
		return null;
	}
}
