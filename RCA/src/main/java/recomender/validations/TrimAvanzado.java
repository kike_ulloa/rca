package recomender.validations;

public class TrimAvanzado {

	/**
	 * Metodo que quita todos los espacios de la cadena, incluyendo los intermedios, a excepcion de los que se encuentren
	 * dentro de comillas simples o dobles.
	 * @param cadena La cadena a quitar espacios.
	 * @return La cadena a retornar sin espacios.
	 */
	public String trimCompleto(String cadena) {
		boolean comillaDoble=false;
		boolean comillaSimple=false;
		boolean comillaSimpleOtra=false;
		boolean comillaDobleOtra=false;
		String cadretorna="";
		for(int i=0;i<cadena.length();i++) {//recorro todos los caracteres de la cadena uno a uno
			if(cadena.charAt(i)=='\"') {//si el caracter es igual a comilla doble
				comillaDoble=!comillaDoble;//si es falso se hara true, si es true se hara falso (falso inicia, true cuando abre comilla, false cuando cierra y asi...)
			}
			if(cadena.charAt(i)=='\'') {//si el caracter encontraod es comilla simple
				comillaSimple=!comillaSimple;//si es falso se hara true y alreves (false cuando empiza, true cuando abre comilla, false cuando cierra y asi...)
			}
			if(cadena.charAt(i)=='‘') {
				comillaSimpleOtra=true;
			}
			if(cadena.charAt(i)=='’') {
				comillaSimpleOtra=false;
			}
			if(cadena.charAt(i)=='“') {
				comillaDobleOtra=true;
			}
			if(cadena.charAt(i)=='”') {
				comillaDobleOtra=false;
			}
			if(i>0&&cadena.charAt(i-1)=='('||i<cadena.length()-1&&cadena.charAt(i+1)==')') {//en caso de que las comillas sea la apertura de toda una etiqueta ejm document.write("<img id=''src='' alt=''")
				comillaDoble=false;
				comillaSimple=false;
				comillaDobleOtra=false;
				comillaSimpleOtra=false;
			}
			if(cadena.charAt(i)==' ') {//si encuentra un expacio
				if(comillaDoble==true) {//si el espacio esta dentro de las comillas dobles
					cadretorna+=cadena.charAt(i);//no cambio el expacio lo dejo tal como esta
				}
				if(comillaSimple==true) {//si el espacio esta dentor de comilla simple
					cadretorna+=cadena.charAt(i);//no cambio el espacio lo dejo como esta
				}
				if(comillaDobleOtra==true) {
					cadretorna+=cadena.charAt(i);
				}
				if(comillaSimpleOtra==true) {
					cadretorna+=cadena.charAt(i);
				}
			}else {//si no encuentra espacio
				cadretorna+=cadena.charAt(i);//va a kedarse como esta
			}
		}
		return cadretorna;
	}
}
