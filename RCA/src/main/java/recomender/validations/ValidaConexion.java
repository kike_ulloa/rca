package recomender.validations;

import java.net.HttpURLConnection;
import java.net.URL;

public class ValidaConexion {

	/**
	 * Valida la conexion con alguna pagina, video o imagen en la web.
	 * @param urlPagina Es el url de la pagina, video o imagen.
	 * @return Retorna si hay o no hay conexion.
	 */
	public boolean hayConexion(String urlPagina) {
		try {
			URL url=new URL(urlPagina);
			HttpURLConnection.setFollowRedirects(true);//cuiando se redirecciona aotra pg
			HttpURLConnection con=(HttpURLConnection)url.openConnection();
			con.setInstanceFollowRedirects(true);
			//System.out.println("Codigo de conexion: "+System.getProperty("http.agent"));
			con.setRequestProperty("User-Agent", "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.167 Safari/537.36");
			con.setRequestMethod("HEAD");
			
			System.out.println("Codigo de conexion222222: "+con.getResponseCode()+" "+HttpURLConnection.HTTP_OK);
			if(con.getResponseCode()==HttpURLConnection.HTTP_OK||con.getResponseCode()==HttpURLConnection.HTTP_MOVED_TEMP||con.getResponseCode()==HttpURLConnection.HTTP_MOVED_PERM||con.getResponseCode()==HttpURLConnection.HTTP_SEE_OTHER)//si no hubo conexion significa que el link esta offline
				return true;
			else
				return false;
		}catch(Exception e) {
			e.printStackTrace();
			return false;
		}
	}
}
