package recomender.validations;

public class ValidaHTML {

	/**
	 * Metodo que devuelve la ruta absoluta del archivo html que se esta analizando
	 * @param html Es el Archovohtml a analizar.
	 * @return Retorna la ruta absoluta dle hrtml
	 */
	public String retornaAbsolutPath(java.io.File html) {
		String completo=html.getAbsolutePath().substring(0, html.getAbsolutePath().indexOf(html.getName()));//corta la ruta para eliminar el nombre del archivo html
		return completo;
	}
}
