package recomender.validations;

public class ValidaTextoImagenTextoAlternativo {

	/**
	 * Metodo que analiza si el texto alternativo esta en blanco, si existe texto en la imagen y comprueba la resolucion
	 * de la imagen. Asi como tambien si el texto alternativo como minimo es igual en caracteres al texto de la imagen.
	 * @param alto Es la altura de la imagen.
	 * @param ancho Es el ancho o largo de la imagen.
	 * @param texto Es el texto encontrado en la imagen.
	 * @param textoAlternativo Es el texto alternativo que tenga la imagen.
	 */
	public boolean[] compruebaTextoImagenTextoAlternativo(int alto, int ancho, String texto, String textoAlternativo) {
		boolean []retorna=new boolean[3];//vector donde indica si [0] imagenbajaresolucion, [1] imagencontextoalternativo, [2] imagenpocotextoalternativo
		retorna[0]=false; retorna[1]=false; retorna[2]=false;
		if(textoAlternativo.equals("")) {
			if(texto!=null) {
			if(!texto.equals("")) {
				//llamo a la calse de las formulas
				recomender.calculations.Formulas form=new recomender.calculations.Formulas();
				//si es falso, no puede caber el texto en la imagen
				if(!form.formulaCalculoCaracteresPorPixeles(ancho, alto, texto)) {
					System.out.println("El texto de la imagen es dificil de leer a esalas mayores");
					//numeroImagenesBajaResolucion++;
					retorna[0]=true;
				}
			}
			System.out.println("La imagen no contiene texto alternativo");
			}
		}else {
			//numeroImagenesTextoAlernativo++;
			retorna[1]=true;
			if(texto!=null) {
			if(!texto.equals("")) {//si el texto en la imagen es diferente de vacio
				//llamo a la calse de las formulas
				recomender.calculations.Formulas form=new recomender.calculations.Formulas();
				if(!form.formulaCalculoCaracteresPorPixeles(ancho, alto, texto)) {//si retorna falso la formula significa que los caracteres no seran visibles al hacer zoom
					//numImagenesConTextoBajaResolucion++;//aumento el numero de imagenes con demasiado texto
					System.out.println("El texto de la imagen es dificil de leer a esalas mayores");
					//numeroImagenesBajaResolucion++;
					retorna[0]=true;
				}
				if(texto.length()>textoAlternativo.length()) { //si el texto de la imagen es mayor al texto alternativo significa que el texto alternativo no esta explicando mucho lo que describe la imagen
					System.out.println("El texto en la imagen es mayor al texto alternativo, como minimo deberian concordar en caracteres");
					//numeroImagenesPocoTextoAlternativo++;
					retorna[2]=true;
				}
			}
		}
		}
		return retorna;
	}
}
