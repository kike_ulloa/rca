package recomender.validations;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class ValidaVideo {
	/**
	 * Valida que el video sea compatible con youtube-dl
	 * @param url Es la url del video
	 * @return Retorna si es compatible o no.
	 */
	public boolean esVideo(String url) {
		String []command=new String [3];
		command[0]="youtube-dl";
		command[1]="--list-subs";
		command[2]=url;
		try {
		Process process = Runtime.getRuntime().exec(command);
		BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
		String salida="";
		String linea ="";
		while((linea=reader.readLine())!=null) {
			salida+=linea+"\n";
		}
		if(salida.toLowerCase().contains("is not a valid url"))
			return false;
		else
			return true;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
	}
}
