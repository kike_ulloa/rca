package recomender.validations;

import java.io.File;
import java.util.ArrayList;

import javax.faces.context.FacesContext;

import org.jsoup.nodes.Element;

import recomender.validations.Offline;
import recomender.validations.Online;

public class ValidacionesBasicas {
	/**
	 * Este metodo nos ayudara a confirmar si una etiqueta contiene cierto atributo.
	 * @param etiqueta
	 * @param atributo
	 * @return
	 */
	public boolean tieneAtributo(Element etiqueta,String atributo) {
		if(atributo!=null)
			return etiqueta.hasAttr(atributo);//Retornamos si la etiqueta tiene el atributo buscado.
		else
			return true;
	}
	/**
	 * Con este metodo comprobaremos si un atributo tiene el valor que estamos buscando.
	 * @param etiqueta
	 * @param atributo
	 * @param valor
	 * @return
	 */
	public boolean tieneValor(Element etiqueta,String atributo,String valor) {
		if(atributo!=null&&valor!=null)
			return etiqueta.attr(atributo).equals(valor);//Retornamos si la etiqueta con el atributo indicado tiene el valor que buscamos.
		else
			return true;
	}
	
	public ArrayList<String> analisisPalabras(ArrayList<String> encontrados, Element resultadoEtiqueta ) {
		if(!resultadoEtiqueta.text().equals("")) {//Indicamos que ignore los espacios vacios.
			String palabras[]=resultadoEtiqueta.text().split(" ");//Mandamos a separar las lineas por espacios para poder asi contar las palabras.
			for (String palabra : palabras) {
				encontrados.add(palabra);//Incrementamos el contador de palabras con cada una de las encontradas.
			}
		}									
		return encontrados;
	}
	/*Para trabajo Futuro
	public String comparativaModosAcceso(int modo1,int modo2,int relacionModos,int valorRango) {
		String porcentaje=null;
		if(modo1>0) {
			if(modo2>0) {
				if(modo1>(relacionModos)-valorRango&&modo1<(relacionModos)+valorRango)
					porcentaje= "Medio (entre 30%-50%)";
				else {
					if(modo1<relacionModos)
						porcentaje="Bajo (menor 30%)";
					else
						porcentaje="Alto (mayor 50%)";
				}
			}else
				porcentaje="Alto (mayor 50%)";
		}else
			porcentaje="Bajo (menor 30%)";
		return porcentaje;
	}
	
	public String porcentajes (String relacion1,String relacion2,String relacion3) {
		String porcentaje="";
		String relaciones[]=new String [3];
		relaciones[0]=relacion1;
		relaciones[1]=relacion2;
		relaciones[2]=relacion3;
		System.out.println(relaciones[0]+" "+ relaciones[1]+" "+relaciones[2]);
		//Criterio utilizado para obtener el porcentaje en base a las relaciones con los otros modos de acceso, 3 relaciones diferentes es Medio, si dos son iguales es el valor de cualquiera de las dos.
		if(!relaciones[0].equals(relaciones[1])&&!relaciones[1].equals(relaciones[2]))
			porcentaje="Medio (entre 30%-50%)";
		else {
			for (int i = 1; i < relaciones.length; i++) {
				if(relaciones[i-1].equals(relaciones[i])) {
					porcentaje=relaciones[i];
					break;
				}
			}
		}
		return porcentaje;
	}
	public String relacionInversa(String relacion) {
		if(relacion.equals("Medio (entre 30%-50%)"))
			return relacion;
		else {
			if(relacion.equals("Bajo"))
				return "Alto (mayor 50%)";
			else
				return "Bajo (menor 30%)";
		}
	}
	*/
	/**
	 * 
	 * @param condicion
	 * @param cadenaAtribVal
	 * @param resultadoEtiqueta
	 * @return
	 */
	public boolean cumpleCondicionHTML(String condicion,String cadenaAtribVal,Element resultadoEtiqueta ) {
		ArrayList<Boolean>tieneAtVal=new ArrayList<Boolean>();
		ArrayList<String[]> atributosValores=new ArrayList<String[]>();
		for (String atVal : cadenaAtribVal.split(";")) {
			atributosValores.add(atVal.split("="));
			tieneAtVal.add(false);
		}
		boolean condicionCumplida=false;
		if(atributosValores.size()>1) {
			tieneAtVal.set(0, true);
			for (int i = 1; i < atributosValores.size(); i++) {
				if(tieneAtributo(resultadoEtiqueta, atributosValores.get(i)[0])) {
					if(atributosValores.get(i).length<2) {
						String aux[]=new String[2];
						aux[0]=atributosValores.get(i)[0];
						aux[1]=null;
						atributosValores.set(i, aux);
					}
					if(tieneValor(resultadoEtiqueta, atributosValores.get(i)[0], atributosValores.get(i)[1]))
						tieneAtVal.set(i, true);
				}			
			}
			for (int i=0; i<tieneAtVal.size();i++) {
				if(condicion!=null) {
					if(condicion.equals("and")) {
						if(!tieneAtVal.get(i)) 
							condicionCumplida=false;
						else condicionCumplida=true;
						}else {
						if(tieneAtVal.get(i))
							condicionCumplida=true;
					}	
				}else condicionCumplida=true;
			}
			}else
				condicionCumplida=true;
		return condicionCumplida;
	}
	public boolean cumpleCondicionJavaScript(String codigoJS,String condicion,String cadenaAtribVal,String variable ) {
		ArrayList<Boolean>tieneAtVal=new ArrayList<Boolean>();
		ArrayList<String[]> atributosValores=new ArrayList<String[]>();
		for (String atVal : cadenaAtribVal.split(";")) {
			atributosValores.add(atVal.split("="));
			tieneAtVal.add(false);
		}
		boolean condicionCumplida=false;
		if(atributosValores.size()>1) {
			for (int i = 1; i < atributosValores.size(); i++) {
				int posicionInicial=codigoJS.indexOf(variable+"."+atributosValores.get(i)[0]);
				if(posicionInicial<0)
					posicionInicial=codigoJS.indexOf("\""+atributosValores.get(i)[0]+"\"");
				if(posicionInicial>-1) {
					if(atributosValores.get(i).length<2) {
						String aux[]=new String[2];
						aux[0]=atributosValores.get(i)[0];
						aux[1]=null;
						atributosValores.set(i, aux);
					}
					if(atributosValores.get(i)[1]!=null) {
						String valor=codigoJS.substring(posicionInicial).replace("\"", "");
						if(valor.indexOf("=")>-1&&valor.indexOf(";")>-1) {
							valor=valor.substring(valor.indexOf("=")+1,valor.indexOf(";"));
						}
						if(valor.contains(atributosValores.get(i)[1]))
							tieneAtVal.set(i, true);
						}else
							tieneAtVal.set(i, true);
				}
			}
			for (int i=1; i<tieneAtVal.size();i++) {
				if(condicion!=null) {
					if(condicion.equals("and")) {
						if(!tieneAtVal.get(i)) 
							condicionCumplida=false;
						else condicionCumplida=true;
						}else {
						if(tieneAtVal.get(i))
							condicionCumplida=true;
					}	
				}else condicionCumplida=true;
			}
			}else
				condicionCumplida=true;
		return condicionCumplida;
	}
	/**
	 * indica si existe un archivo o no
	 * @param archivo
	 * @param valor
	 * @param tipoArchivo
	 * @param encontrados
	 * @param contenidoOtroArchivo
	 * @return
	 */
	public ArrayList<String> existeArchivo(String archivo,String valor,String tipoArchivo,ArrayList<String>encontrados,String contenidoOtroArchivo){
		Online on=new Online();
		Offline of=new Offline();
		if(valor.indexOf("//")>-1||valor.indexOf("www.")>-1)  //Analizamos si el valor pertenece a una URL.
			encontrados=(on.analizarValorOnline(valor, tipoArchivo, encontrados, contenidoOtroArchivo));
	else {
			if(archivo.indexOf("//")>-1||valor.indexOf("www.")>-1) //Analizamos si el archivo html es online u offline.
				encontrados=of.analizarValorOfflineArchivoOnline(archivo, valor, tipoArchivo, contenidoOtroArchivo, encontrados);
			else
				encontrados=of.analizarValorOfflineArchivoOffline(archivo, valor, tipoArchivo, contenidoOtroArchivo, encontrados);	
	}
		return encontrados;
	}
	/*
	public String crearCarpetaDescargas() {
		FacesContext context=FacesContext.getCurrentInstance();//la instancia actual, es decir la ruta actualdonde se encuentra el proyecto en el servidor
		File obj=new File(context.getExternalContext().getRealPath("/descargas"));//se va a cargar en la carpeta uploads con el path completo desde /home
		
		if(!obj.exists()) {//si no existe el objeto procedera a crear
			try {
				obj.mkdir();
			}catch(Exception e) {
				e.printStackTrace();
			}
		}
		obj.mkdir();
		return obj.getAbsolutePath()+"/";
	}*/
	/**
	 * crea subcarpetas
	 * @param carpeta
	 * @return
	 */
	public String crearSubCarpetasDescargas(String carpeta) {
		File obj=new File(carpeta);
		obj=new File(obj.getAbsolutePath()+"/descarga"+(obj.list().length+1));
		obj.mkdir();
		return obj.getAbsolutePath()+"/";
	}
	/**
	 * arreglo de colores
	 * @return
	 */
	public ArrayList<String> obtenerColores() {
		ArrayList<String>colores=new ArrayList<String>();
		colores.add("color");
		colores.add("negr");
		colores.add("azul");
		colores.add("naranja");
		colores.add("tomate");
		colores.add("morad");
		colores.add("blanc");
		colores.add("verde");
		colores.add("amarill");
		return colores;
	}
	/**
	 * arreglo de titulos de actividades.
	 * @return
	 */
	public ArrayList<String> obtenerTitulos(){
		ArrayList<String>titulos=new ArrayList<String>();
		titulos.add("actividad");
		titulos.add("pregunta");
		titulos.add("repas");
		titulos.add("resolv");
		titulos.add("ejercicio");
		titulos.add("evaluacion");
		titulos.add("realizar");
		return titulos;
	}
}
