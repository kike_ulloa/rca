package recomender.validations;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

import recomender.validations.Offline;
public class ValidacionesMultimedia {

/**
 * este metodo permite identificar si un contenido auditivo tiene sonido o no
 * @param archivo.- html
 * @param sentencia.- comandos de consola
 * @param url.- comandos para videos online
 * @return
 */

	public boolean tieneAudio(String archivo,String[]sentencia, String []url) {
		Process p;//A traves de esta variable se hara la ejecucion de la sentencia en la consola del sistema.
				boolean tieneAudio=false;//Usaremos una variable boolean para indicar si se detecto que el link es o no un video.
		try {
			String linea="";
			String tiempoo="";
			BufferedReader lectura;
			if(url!=null) {
				String comando[]=new String [3];
				comando[0]="youtube-dl";
				comando[1]="--get-duration";
				comando[2]=url[4];
				p=Runtime.getRuntime().exec(comando);
				lectura=new BufferedReader(new InputStreamReader(p.getInputStream()));//Obtenemos el resultado de la ejecucion de la sentencia,que se almacenara como texto.
				String tiempo=lectura.readLine();
				tiempoo=tiempo;
				String tf="";
				if(tiempo!=null) {
				if(tiempo.contains(":")) {
					
					//tiempo=tiempo.substring(0, tiempo.indexOf(":"));
					tf="00:01:00.00";
					
				}else {
					tf="00:00:"+tiempo+".00";
				}
				p=Runtime.getRuntime().exec(url);
				lectura=new BufferedReader(new InputStreamReader(p.getInputStream()));//Obtenemos el resultado de la ejecucion de la sentencia,que se almacenara como texto.
				p.getOutputStream();
				String lee="";
				while((lee=lectura.readLine())!=null) {
					linea+=lee;
				}
				//linea=lectura.readLine();//Leemos la primera linea del texto.
				System.out.println("Esta es la linea "+linea);
				sentencia[2]=linea;
				sentencia[6]=tf;
			
//			for(String s: sentencia) {
//				System.out.println(s);
//			}
				}else {
					System.out.println("video caido");
				}
			}
			if(tiempoo!=null&&!tiempoo.equals("")) {
			p = Runtime.getRuntime().exec(sentencia);//Aqui mandamos a ejecutar nuestra sentencia en la consola de nuestro sistema operativo.
			
			lectura=new BufferedReader(new InputStreamReader(p.getErrorStream()));//Obtenemos el resultado de la ejecucion de la sentencia,que se almacenara como texto.
			
			while(linea!=null) {//Recorreremos todas las lineas obtenidas.
				int posicionVolumen=linea.indexOf("mean_volume:");
				if(posicionVolumen>-1) {
					Double volumenMedio=Double.parseDouble(linea.substring(posicionVolumen+12).split(" ")[1]);
					if(volumenMedio>-40) //-40 es el valor de dBFS(decibeles full Scale) en el cual se empieza a considerar silencio de un audio. 
						tieneAudio=true;//Indicamos que el link analizado no es un video.			
				}
				linea=lectura.readLine();//Se sigue leyendo cada linea.
			}
			}
			
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		return tieneAudio;//Retornamos la variable boolean.
	}
//	public String convertirAudio(String archivo,String rutaOrigen,String rutaDestino) {	
//		Offline of=new Offline();
//		Process p=null;//A traves de esta variable se hara la ejecucion de la sentencia en la consola del sistema.
//		String sentencia[]=new String[4];//Creamos un arreglo donde indicaremos la sentencia que se ejecutara en la consola del sistema.
//		sentencia[0]="ffmpeg";//Se hara uso sistema youtube-dl que previamente ya fue instalado en el sistema.
//		sentencia[1]="-i";//El comando de arriba necesita paramentros para extraer informacion del video, para ello se hara uso del parametro que nos permite listar los subtitulos, de esta forma se comprobara que se trata de un video o no.
//		sentencia[2]=rutaOrigen+archivo;//Este metodo recibi como parametro el link que queremos analizar, el cual se usara dentro de la sentencia.
//		sentencia[3]=rutaDestino+archivo.substring(archivo.lastIndexOf("/")+1, archivo.lastIndexOf("."))+".mp3";
//			try {
//				p = Runtime.getRuntime().exec(sentencia);//Aqui mandamos a ejecutar nuestra sentencia en la consola de nuestro sistema operativo.
//				BufferedReader lectura=new BufferedReader(new InputStreamReader(p.getErrorStream()));//Obtenemos el resultado de la ejecucion de la sentencia,que se almacenara como texto.
//				String linea=lectura.readLine();//Leemos la primera linea del texto.
//				while(linea!=null) {//Recorreremos todas las lineas obtenidas.
//					linea=lectura.readLine();//Se sigue leyendo cada linea.
//				}
//				if(!of.existeOffline(sentencia[3]))
//					sentencia[3]=null;
//			}catch (IOException e) {
//			e.printStackTrace();
//			}
//			return sentencia[3];
//	}
//	public boolean eliminarAudio(String ruta) {
//		boolean eliminado=false;
//		if(ruta!=null) {
//		File f=new File(ruta);
//		for (String a : f.list()) {
//			File nf=new File(ruta+a);
//			eliminado=nf.delete();
//		}
//		f.delete();
//		}
//		return eliminado;
//	}
	/***
	 * Este metodo se utiliza para analizar si un link embebido es un video.
	 * @param archivo
	 * @return
	 */
	public boolean esVideo(String archivo) {
		Process p;//A traves de esta variable se hara la ejecucion de la sentencia en la consola del sistema.
		String sentencia[]=new String[3];//Creamos un arreglo donde indicaremos la sentencia que se ejecutara en la consola del sistema.
		sentencia[0]="youtube-dl";//Se hara uso sistema youtube-dl que previamente ya fue instalado en el sistema.
		sentencia[1]="--list-subs";//El comando de arriba necesita paramentros para extraer informacion del video, para ello se hara uso del parametro que nos permite listar los subtitulos, de esta forma se comprobara que se trata de un video o no.
		sentencia[2]=archivo;//Este metodo recibi como parametro el link que queremos analizar, el cual se usara dentro de la sentencia.
		boolean esvid=true;//Usaremos una variable boolean para indicar si se detecto que el link es o no un video.
		try {
			p = Runtime.getRuntime().exec(sentencia);//Aqui mandamos a ejecutar nuestra sentencia en la consola de nuestro sistema operativo.
			BufferedReader lectura=new BufferedReader(new InputStreamReader(p.getInputStream()));//Obtenemos el resultado de la ejecucion de la sentencia,que se almacenara como texto.
			String linea=lectura.readLine();//Leemos la primera linea del texto.
			while(linea!=null) {//Recorreremos todas las lineas obtenidas.
				if(linea.indexOf("[generic]")>-1)//Se condiciona si cada linea analizada tiene el texto [generic] el cual indica que en link a analizar no es un video.
					esvid=false;//Indicamos que el link analizado no es un video.
				linea=lectura.readLine();//Se sigue leyendo cada linea.
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return esvid;//Retornamos la variable boolean.
	}
	
	/**
	 * Este metodo nos permite identificar si un archivo se considera audio,video,imagen, a traves de las extensiones de los formatos propuestas.
	 * @param tipoArchivo
	 * @param archivo
	 * @return
	 */
	public boolean tieneFormato(String tipoArchivo,String archivo){
			boolean tieFormato=false;//Se crea una variable boolean para indicar si el archivo posee el formato del tipo de contenido multimedia que se indica en la variable tipoArchivo.
			ArrayList<String> resultadoFormatos=obtenerFormatos(tipoArchivo);
			for (String formato : resultadoFormatos) {//Llamamos al metodo obtener formatos donde mandamos el tiepo de Archivo y recorremos el arreglo que se obtine de dicho metodo.
				if(archivo.toLowerCase().indexOf(formato.toLowerCase())>-1)//Analizamos si el archivo que estamos analizando posee alguna de las extensiones de formatos recibido.
					tieFormato=true;//Si tiene el formato se indica con el estado true.
			}
			return tieFormato;//Retornamos la variable boolean.
		}
	/***
	 * Este metodo se utilizara para generar arreglos de los formatos de video,audio,imagen.
	 * @param tipoArchivo
	 * @return
	 */
	public ArrayList<String>obtenerFormatos(String tipoArchivo){
		ArrayList<String>formatosAudio=new ArrayList<String>();
		ArrayList<String>formatosImagenes=new ArrayList<String>();
		ArrayList<String>formatosVideos=new ArrayList<String>();
		formatosVideos.add(".MPEG");
		formatosVideos.add(".WMV");
		formatosVideos.add(".OGV");
		formatosVideos.add(".MPEG");
		formatosVideos.add(".WEBM");
		formatosVideos.add(".MP4");
		formatosVideos.add(".FLV");
		formatosVideos.add(".AVI");
		formatosVideos.add(".MOV");
		formatosVideos.add(".MKV");
		formatosVideos.add(".3GP");
		formatosVideos.add(".M4V");
		formatosAudio.add(".MP3");
		formatosAudio.add(".WAV");
		formatosAudio.add(".OGG");
		formatosAudio.add(".AAC");
		formatosAudio.add(".WMA");
		formatosImagenes.add(".BMP");
		formatosImagenes.add(".DCX");
		formatosImagenes.add(".GIF"); 
		formatosImagenes.add(".ICNS"); 
		formatosImagenes.add(".ICO"); 
		formatosImagenes.add(".JBIG2"); 
		formatosImagenes.add(".JPEG");
		formatosImagenes.add(".JPG"); 
		formatosImagenes.add(".PAM"); 
		formatosImagenes.add(".PBM"); 
		formatosImagenes.add(".PCX"); 
		formatosImagenes.add(".PGM"); 
		formatosImagenes.add(".PNG");
		formatosImagenes.add(".PNM"); 
		formatosImagenes.add(".PPM"); 
		formatosImagenes.add(".PSD"); 
		formatosImagenes.add(".RGBE"); 
		formatosImagenes.add(".TGA"); 
		formatosImagenes.add(".TIFF"); 
		formatosImagenes.add(".WBMP"); 
		formatosImagenes.add(".XBM"); 
		formatosImagenes.add(".XPM"); 
		if(tipoArchivo.equals("audio")) 
			return formatosAudio;
		else 
			if(tipoArchivo.equals("imagen"))
				return formatosImagenes;
			else
				return formatosVideos;
	}
}
